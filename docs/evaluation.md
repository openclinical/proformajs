# Expression evaluation

Expressions in proformajs are javascript expressions that can range over the state of an enactment, based on where in the component tree the expression is evaluated, using some inbuilt predicates.  The early versions of proformajs used ``eval`` to establish the proof of concept but this functionality was later moved to a safer separate library called jse-eval.

## Reference

[Mozilla MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_operators) has a good reference for javascript expressions.  Expressions combine operators and operands to return a value and may have side effects (in the case of assignment).  The MDN page lists the available expression operators under 11 headings:

- Basic operators
  - ``this``
  - grouping operator (brackets), e.g. ``(a + b) * c``
  - property accessor (dot), e.g. ``foo.bar``
  - optional chaining (question mark), e.g. ``foo?.bar``
  - ``new``
  - ``super``
- Comparison operators
  - Equal (``==``)
  - Not equal (``!=``)
  - Strict equal (``===``)
  - Strict not equal (``!==``)
  - Greater than (``>``)
  - Greater than or equal (``>=``)
  - Less than (``<``)
  - Less than or equal (``<=``)
- Arithmetic operators
  - Remainder (%)
  - Increment (++), e.g. ``x++``
  - Decrement (--), e.g. ``x--``
  - Unary negation (-), e.g. -3.
  - Unary plus (+). e.g. ``+"3"`` returns ``3``.
  - Exponentiation operator (**), e.g.	``2 ** 3`` returns 8.
- Bitwise Operators
  - Bitwise AND, e.g. ``a & b``
  - Bitwise OR, e.g. ``a | b``
  - Bitwise XOR, e.g. ``a ^ b``
  - Bitwise NOT, e.g. ``~ a``
  - Sign-propagating right shift, e.g. ``a >> b``
  - Zero-fill right shift, e.g. ``a >>> b``
- Logical operators
  - Logical AND (&&), e.g. ``expr1 && expr2``.
  - Logical OR (||), e.g. ``expr1 || expr2``
  - Nullish coalescing operator (??), e.g. ``expr1 ?? expr2``
  - Logical NOT (!), e.g. ``!expr``
- String operators
  - concaternation, e.g. ``str1 + str2``
- Conditional (ternary) operator
  - ``condition ? expr1 : expr2``
- Assignment operators
  - Assignment, e.g. ``x = f()``
  - Addition assignment, e.g. ``x += f()``,
  - Subtraction assignment, e.g. ``x -= f()``
  - Multiplication assignment, e.g. ``x *= f()``
  - Division assignment, e.g. ``x /= f()``
  - Remainder assignment, e.g. ``x %= f()``
  - Exponentiation assignment, e.g. ``x **= f()``
  - Left shift assignment, e.g. ``x <<= f()``
  - Right shift assignment, e.g. ``x >>= f()``
  - Unsigned right shift assignment, e.g. ``x >>>= f()``
  - Bitwise AND assignment, e.g. ``x &= f()``
  - Bitwise XOR assignment, e.g. ``x ^= f()``
  - Bitwise OR assignment, e.g. ``x |= f()``
  - Logical AND assignment, e.g. ``x &&= f()``
  - Logical OR assignment, e.g. ``x ||= f()``
  - Nullish coalescing assignment, e.g. ``x ??= f()``
- Comma operator
  - ``,`` evaluates both of its operands
- Unary operators
  - ``delete``
  - ``typeof``
  - ``void``
- Relational operators
  - ``in``
  - ``instanceof``

The [operator precedence table](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_precedence#table) is another useful list of operators.

## jse-eval

[jse-eval](https://github.com/6utt3rfly/jse-eval) uses another library, [jsep](https://github.com/EricSmekens/jsep) to parse javascript expressions into an abstract syntax tree (known as estree) as a plain old javascript object.  jse-eval provides the means to evaluate the estree object based on a context (set of variable values and additional functions) that the consumer supplies.

See also:

* https://github.com/mattsouth/jse-eval-vue - provides vue components for estree and jse-eval and contains good [notes](https://github.com/mattsouth/jse-eval-vue/blob/main/NOTES.md) on the structure of estree

### jsep

jsep is a library used by jse-eval and does a good job in its documentation of describing itself:

> [jsep](https://ericsmekens.github.io/jsep/) is a simple expression parser written in JavaScript. It can parse JavaScript expressions but not operations. The difference between expressions and operations is akin to the difference between a cell in an Excel spreadsheet vs. a proper JavaScript program.

As well as arithmetic, array and object operators, the library provides configurable support for parsing more complex javascript features via plugins:

|                                   |                                                                                           |
|-----------------------------------|-------------------------------------------------------------------------------------------|
| [ternary](packages/ternary)       | Built-in by default, adds support for ternary `a ? b : c` expressions                     |
| [arrow](packages/arrow)           | Adds arrow-function support: `v => !!v`                                                   |
| [assignment](packages/assignment) | Adds assignment and update expression support: `a = 2`, `a++`                             |
| [comment](packages/comment)       | Adds support for ignoring comments: `a /* ignore this */ > 1 // ignore this too`          |
| [new](packages/new)               | Adds 'new' keyword support: `new Date()`                                                  |
| [numbers](packages/numbers)       | Adds hex, octal, and binary number support, ignore _ char                                 |
| [object](packages/object)         | Adds object expression support: `{ a: 1, b: { c }}`                                       |
| [regex](packages/regex)           | Adds support for regular expression literals: `/[a-z]{2}/ig`                              |
| [spread](packages/spread)         | Adds support for the spread operator, `fn(...[1, ...a])`. Works with `object` plugin, too |
| [template](packages/template)     | Adds template literal support: `` `hi ${name}` ``                                         |
|

jse-eval optionally supports the arrow, assignment, async-await (not listed in the above table which is taken from its documentation), new, object, spread and template jsep plugins (but not the comment, numbers or regex plugins) but these are not necessary for the proformajs evaluator so are not used.

TODO: create an implementation matrix based on the reference list above

TODO: detail the context supplied in proformajs when evaluating an expression
