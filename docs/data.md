In order to define the taskflow for a particular subject pathway, we must reason about the condition of the subject using data from beyond the enactment scope.  At design time a protocol provides the means to describe the data that it needs using the *DataDefinition* class and it's sub-classes.  At runtime an *Enquiry* task provides the mechanism to explicitly request data however an enactment will accept new data at any time.  

An enactment will maintain a history of those data it has received and the expression language can define predicates to allow reasoning over data trends.

The abstract *DataDefinition* component is defined in the ``src/evaluator.coffee`` source file. Sub classes of the DataDefinition class govern what type of data is accepted though the abstract definition has several common attributes.  DataDefinitions may accept a single value or multiple values as indicated by the boolean *multiValued* attribute.  DataDefinitions are dynamic if they define a *valueCondition* expression.  A default may be defined which can be picked up by an Enquiry when it explicitly has useDefaults set to true or Enactment has the useDefaults set to true as an option.  A static default can be defined in the *defaultValue* attribute and a dynamic default can be defined with the *defaultCondition* attribute.  Warnings may also be attached to data definitions.

## Data types

The follow DataDefinition sub-classes are defined:

* Text
* Date
* Boolean
* Integer
* Float

## Warnings / validation

A warning component has a caption/description and a *warningCondition* that, if ``true`` will, show up in the enactment status as a warning.  Use a warning to refine the allowed values of a particular type. E.g. for an ``age`` DataDefinition you may wish to use a warning or warnings that are raised if the Integer value is negative or greater than 120.

## Derived data

If a DataDefinition has a *valueCondition* then it's value will be derived from other data at runtime, including state of the enactment but it cannot be updated manually by a user.  Consider using a *defaultCondition* if you wish a derived value to be overwritable.

## Reasoning about data

At runtime you can use the predicate ``is_known(dataType)`` to detect whether a value exists or not and the predicate ``is_valid(datatype)`` to test whether the current value has any warnings.
