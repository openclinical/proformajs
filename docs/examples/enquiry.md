# Enquiry

An `Enquiry` is a `Task` that has attached `Source`s.  At runtime, once an enquiry task is *in-progress* it updates the *requested* state of each of its sources based on their *requestCondition* expressions.  The task can only be completed when all requests have been satisfied.  If it's autonomous then it will autocomplete as soon as all requests have been satisfied, otherwise the enactment status will show that the enquiry is completeable and it will need to be completed explicitly.

To create a simple enquiry for a subject's age in the repl you can take the following steps (though you can skip the first four lines if you have already setup proformajs):

    you@yourmachine:~$ git clone https://github.com/mattsouth/proformajs.git
    you@yourmachine:~$ cd proformajs
    you@yourmachine:~$ npm install
    you@yourmachine:~$ npm run build
    you@yourmachine:~$ npm run repl

    > @openclinical/proformajs@0.4.3 repl /home/matt/Software/proformajs
    > node src/repl.js

    proformajs Copyright (C) 2017 Openclinical CIC
    This program comes with ABSOLUTELY NO WARRANTY;
    This is free software, and you are welcome to
    redistribute it under certain conditions;

    This is a Javascript REPL with pre-loaded modules:
    * Protocol
    * Enactment
    * Moment
    PROforma> enquiry = new Protocol.Enquiry name: 'demo'
    Enquiry {
      useDefaults: undefined,
      preCondition: undefined,
      waitCondition: undefined,
      name: 'demo',
      caption: undefined,
      description: undefined,
      _parent: undefined,
      meta: undefined,
      optional: false,
      autonomous: false,
      dataDefinitions: [],
      sources: [] }

Notice two additional attributes that a Task does not have: useDefaults and sources. The Source objects are stored in an array we can add to with the addSource function.  We'll discuss the useDefaults attribute in a later section.  enquiry is a valid task, although we'll see a warning that there are no sources defined (after all it's not very useful without any sources defined).

    PROforma> enquiry.isValid()
    true
    PROforma> enquiry.validate()
    [ { type: 'Warning',
        path: 'demo',
        attribute: 'sources',
        msg: 'No sources defined' } ]

Before adding a source we must add a data definition.

    PROforma> enquiry.addDataDefinition new Protocol.Integer name:'age'
    Enquiry {
      useDefaults: undefined,
      preCondition: undefined,
      waitCondition: undefined,
      name: 'demo',
      caption: undefined,
      description: undefined,
      _parent: undefined,
      meta: undefined,
      optional: false,
      autonomous: false,
      dataDefinitions:
       [ Integer {
           range: undefined,
           valueCondition: undefined,
           name: 'age',
           caption: undefined,
           description: undefined,
           _parent: [Circular],
           meta: undefined,
           defaultValue: undefined,
           multiValued: false,
           warnings: [] } ],
      sources: [] }

Note that a data definition has a defaultValue attribute which in this example is undefined.  If this contains a value and the enquiry's useDefaults attribute is true then at runtime the enquiry will fill the source with the default value if has no value when the enquiry becomes in_progress.  This feature is typically used in demos and can be set as a global variable in an enactment so that undefined values of useDefaults can be over-ridden. Now we can add a source that refers to the our new data definition.  

    PROforma> enquiry.addSource new Protocol.Source type: 'age', requestCondition: "!is_known('age')"
    Enquiry {
      useDefaults: undefined,
      preCondition: undefined,
      waitCondition: undefined,
      name: 'demo',
      caption: undefined,
      description: undefined,
      _parent: undefined,
      meta: undefined,
      optional: false,
      autonomous: false,
      dataDefinitions:
       [ Integer {
           range: undefined,
           valueCondition: undefined,
           name: 'age',
           caption: undefined,
           description: undefined,
           _parent: [Circular],
           meta: undefined,
           multiValued: false,
           warnings: [] } ],
      sources:
       [ Source {
           type: 'age',
           requestCondition: '!is_known(\'age\')',
           _parent: [Circular] } ] }
    PROforma> enquiry.validate()
    []

A Source is a component that has a type and requestCondition.  The type is the name of a data definition that the task has access to (i.e. it might not be defined on the task, but on one of it's ancestors). The requestCondition is an expression that evaluates to a boolean value.  The expression provided in this example, `!is_known('age')` is a typical request condition expression that says return true if age is unknown (the ! character is shorthand for `not` so `!is_known` is equivalent to unknown).

Now we can enact our demo enquiry.

    PROforma> enactment = new Enactment protocol: enquiry, start: true
    Enactment {
      protocol:
       Enquiry {
         useDefaults: undefined,
         preCondition: undefined,
         waitCondition: undefined,
         name: 'demo',
         caption: undefined,
         description: undefined,
         _parent: undefined,
         meta: undefined,
         optional: false,
         autonomous: false,
         dataDefinitions: [ [Object] ],
         sources: [ [Object] ] },
      options: undefined,
      _state:
       { demo:
          EnquiryState {
            state: 'in_progress',
            completeable: false,
            cancellable: true,
            path: 'demo',
            enactment: [Circular] },
         'demo:age': SourceState { requested: true, path: 'demo:age', enactment: [Circular] } },
      _data: {},
      _audit:
       [ { action: 'start',
           timestamp: Tue Sep 06 2016 12:03:06 GMT+0100 (BST),
           deltas: [Object] } ],
      tickInterval: undefined,
      stackLimit: 1000 }
    PROforma> enactment.getStatus()
    { started: true,
      finished: false,
      completeable: [],
      cancellable: [ 'demo' ],
      warnings: [],
      requested: { demo: [ 'age' ] },
      confirmable: {} }

`getStatus()` shows that our `demo` task is not yet *completeable* and that age is *requested* so we'll provide a value for it.  But before we do that, let's just have a look at what the enactment's getComponent method yields.

    PROforma> enactment.getComponent 'demo'
    {
      name: 'demo',
      optional: false,
      autonomous: false,
      sources: [ {
        name: 'age',
        multiValued: false,
        warnings: [],      
        requestCondition: "!is_known('age')",
        requested: true,
        path: 'demo:age'
      } ],
      state: 'in_progress',
      completeable: false,
      cancellable: true,
      path: 'demo'    
    }

Note that the the enactment's set method allows multiple values at once, hence the data is passed as an object.

    PROforma> enactment.set('demo', {age: 25}).getStatus()
    { started: true,
      finished: false,
      completeable: [ 'demo' ],
      cancellable: [ 'demo' ],
      warnings: [],
      requested: { demo: [] },
      confirmable: {} }

Now that we've provided a value, *requested* is no longer true and the task is completable.  If we do this, then we'll have finished the enactment.

    PROforma> enactment.complete('demo').getStatus()
    { started: true, finished: true }

## Exceptions and errors

If we had tried to add a Source before the data definition that would have been allowed, but picked up with a validation error.  If we'd have tried to add something that wasnt a Source object as a source then an exception would have been thrown.

## Enforcing the freshness of data

The requestCondition expression can be written to accept recently acquired data but reject out-of-date values using the inbuilt `last_updated(dataDef)` function.  For instance the requestCondition,  `!is_known('cough') || last_updated('cough').add(now(), 'days')<1` will request data if it's unknown or acquired over a day ago.

## Derived data

You cannot source dynamic data.  If you try to do this, a validation error will
be raised.  The reason that this is disallowed is that dynamic data items create
state object's whose paths will be the same as their source, if it was allowed,
leading to difficulties in updating state.  Non dynamic data definitions do not
have an associated state object, though they may have warnings which do create
state objects.
