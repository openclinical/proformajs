In proformajs protocols are design-time models that are used to create runtime enactments.  In user terms, a protocol is a set of tasks with scheduling constraints that may run concurrently or be triggered manually.  In technical terms, a protocol consists of a tree of *Component* objects whose root object is a *Task* object.  This page describes protocols and signposts some of the technical details of their implementation.  

## Tasks

![Four task types](tasks.png)

Tasks are the building blocks of protocols.  proformajs defines four concrete task classes.  The abstract *Task* provides the base class for the *Action", *Plan*, *Decision* and *Enquiry* sub-classes.  Data definitions are attached to a task, via the *dataDefinitions* array and may be attached to a task at any level in the task tree (if Plan tasks are used, see below), though in practice they tend only to be attached to the root task of a protocol.

### Runtime state

At runtime, tasks move between a set of runtime states.  Tasks start in a *dormant* state and, in the absence of scheduling constraints, immediately transition to *in_progress*.  The optional *waitCondition* and *preCondition* task attributes provide the scheduling constraints in the form of expressions.  If present, the *waitCondition* will be evaluated at runtime and must be true before a task can transition from *dormant* to *in_progress* and when that transition occurs, the *preCondition*, if present, must also be true.  If the *preCondition* is not true then the the task will automatically transition to *discarded*.

The *autonomous* flag is used by sub-classes to automatically complete tasks at runtime.  The *optional* flag indicates that a task is not required for it's parent plan (if it has one - see Plan below) to be completable.  Runtime state is more fully explored in the Enactment page.

The *abortCondition* and *terminateCondition* are evaluated when a task is *in_progress*.   If either evaluates to *true* while the task is *in_progress* then the task will automatically transition to discarded (if the abortCondition is true) or completed (if the terminateCondition is true).

### Paths

Every component in a protocol is addressable via a path.  Paths are used in conditions to refer to another component.  Sibling and child components can be addressed by name only.  TODO: more here.

### Cycles

Tasks can be cyclic.  At runtime, a fresh copy of a cyclic task will be created when the previous copy has finished, assuming the *cycleUntil* condition, if it exists, still holds.  As there can be multiple copies of the same task recorded in the runtime state of an enactment, then runtime paths differ from design time paths as they may include one or more indexes to indicate the cycle in question.

## Plans

The Plan class defines a *tasks* array of sub-tasks. Initially, at runtime a plan and it's sub-tasks will be *dormant*.  Once the plan is *in_progress* it reviews the state of it's sub-task's scheduling constraints.  The completeability of a plan depends on the non-optional sub-tasks.  When all non-optional sub-tasks are finished (either completed or discarded) then a plan is completeable.

## Enquiries

The Enquiry class defines a *sources* array.  At runtime an enquiry is completeable when it's list of requested sources is empty.  If it's autonomous flag is set then it will complete automatically when it's last requested source is provided.  If a data definition default value is provided and the *useDefaultValues* flag is set on the enactment then as the enquiry moves from *dormant* to *in_progress* the *defaultValue* will be provided to each source whose dataDefinition has one and doesnt already have a value set.

## Decisions

The Decision class contains a *candidates* array of *Candidate* objects. The set of *recommended* and *confirmed* candidates is available at runtime. An *in_progress* decision is *completeable* when at least one of it's candidates is confirmed.  

If a decision's *autonomous* flag is set then it will complete as soon as a candidate is confirmed.

## Design

The code for the Protocol Components is defined in the ``src/tasks.coffee`` and ``src/core.coffee`` files.  The diagram below illustrates the class definitions used in the proformajs Protocol namespace. At the top of the class hierarchy is the abstract *Component* class.

![Protocol class diagram](protocol.png)

### Component validation

The Component class defines a *validate* and *isValid* function for checking a component object's integrity. *isValid* is false if *validate* returns any Errors (though not if it returns only Warnings).  All *Protocol* classes inherit from *Component* and implement their own validation constraints.  An enactment can only be created from a valid protocol.
