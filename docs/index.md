PRO*forma* is a clinical decision support system (CDSS) language (see [Sutton and Fox 2003](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC212780/)). proformajs is a lightweight Javascript PRO*forma* engine.

## Getting started

proformajs is a library intended to be embedded in other applications.  The easiest way to get started is by trying the [online demonstrator](https://demo.openclinical.net). However, if you are a developer then the [REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) can be used to get your hands on the API. With [Nodejs](https://nodejs.org) (v6+) and [Git](https://git-scm.com/) installed you can first clone the repository, install the dependencies and start it up:

    you@yourmachine:~$ git clone https://gitlab.com/openclinical/proformajs.git
    you@yourmachine:~$ cd proformajs
    you@yourmachine:~$ npm install
    you@yourmachine:~$ npm run build
    you@yourmachine:~$ npm run repl

    > @openclinical/proformajs@0.4.3 repl /home/you/Software/proformajs
    > node src/repl.js

    proformajs Copyright (C) 2017 Openclinical CIC
    This program comes with ABSOLUTELY NO WARRANTY;
    This is free software, and you are welcome to
    redistribute it under certain conditions;

    This is a Javascript REPL with pre-loaded modules:
    * Protocol
    * Enactment
    * Rewinder
    * moment

    PROforma>

## Protocols and Enactments

Protocols
are the design-time representation of guidelines that consist of a tree of tasks
and data definitions.  Runtime enactments of a protocol are created with the
Enactment module which uses the [Moment](http://www.momentjs.com) library
to handle date and time calculations.  The REPL has pre-loaded `Protocol`, `Enactment` and `Moment` modules.  

To create a very simple protocol you can create a single demo task.  Type

    protocol = new Protocol.Task({name: 'demo'})

at the cursor.  You should see:

    PROforma> protocol = new Protocol.Task({name:'demo'})
    Task {
      preCondition: undefined,
      waitCondition: undefined,
      abortCondition: undefined,
      eventTrigger: undefined,
      cycleUntil: undefined,
      name: 'demo',
      caption: undefined,
      description: undefined,
      _parent: undefined,
      meta: undefined,
      optional: false,
      autonomous: false,
      cyclic: false,
      dataDefinitions: []
    }

and use that protocol to create a runtime enactment:

    PROforma> enactment = new Enactment({start:true, protocol: protocol})
    Enactment {
      protocol: Task {
        preCondition: undefined,
        waitCondition: undefined,
        abortCondition: undefined,
        eventTrigger: undefined,
        cycleUntil: undefined,
        name: 'demo',
        caption: undefined,
        description: undefined,
        _parent: undefined,
        meta: undefined,
        optional: false,
        autonomous: false,
        cyclic: false,
        dataDefinitions: []
      },
      options: undefined,
      listeners: {},
      _state: {
        demo: TaskState {
          state: 'in_progress',
          completeable: true,
          cancellable: true,
          index: undefined,
          cycleable: true,
          path: 'demo',
          _enactment: [Circular],
          design: 'demo'
        }
      },
      _data: {},
      _audit: [
        {
          action: 'start',
          timestamp: 2019-08-19T15:51:43.872Z,
          deltas: [Array]
        }
      ],
      _triggers: {},
      tickInterval: undefined,
      stackLimit: 1000
    }

The `getStatus` method shows options available. Ours is a simple protocol and
there's only two things that can be done, completing or cancelling the 'demo' task.  

    PROforma> enactment.getStatus()
    { started: true,
      finished: false,
      completeable: [ 'demo' ],
      cancellable: [ 'demo' ],
      warnings: [],
      requested: {},
      confirmable: {},
      triggers: []
    }

Enactment has a chainable interface so completing the task returns the updated
enactment which is now finished, as is seen when we chain the `getStatus` method.

    PROforma> enactment.complete('demo').getStatus()
    { started: true, finished: true }

## Serialisation

Both protocols and enactments can be serialised to JSON with JSON.stringify and deserialised with a static *inflate* method, e.g.

    PROforma> json = JSON.stringify(protocol)
    '{"class":"Task","name":"demo"}'
    PROforma> Protocol.inflate(json)
    Task {
      preCondition: undefined,
      waitCondition: undefined,
      abortCondition: undefined,
      eventTrigger: undefined,
      cycleUntil: undefined,
      name: 'demo',
      caption: undefined,
      description: undefined,
      _parent: undefined,
      meta: undefined,
      optional: false,
      autonomous: false,
      cyclic: false,
      dataDefinitions: [] }

## Rewinder

The Rewinder object (introduced in v0.19) is a tool for winding back an enactment to an earlier version of itself which is useful for reviewing an enactment.  To use a Rewinder you first pass it an enactment from which you can ask for an earlier version of that enactment which is constructed based on its _audit array.

    PROforma> rewinder = new Rewinder(enactment)
    Rewinder {
      enactment: Enactment {
        protocol: Task {
          _parent: undefined,
          meta: undefined,
          caption: undefined,
          description: undefined,
          name: 'demo',
          preCondition: undefined,
          waitCondition: undefined,
          abortCondition: undefined,
          terminateCondition: undefined,
          eventTrigger: undefined,
          cycleUntil: undefined,
          optional: false,
          autonomous: false,
          cyclic: false,
          dataDefinitions: []
        },
        options: undefined,
        listeners: {},
        _state: { demo: [TaskState] },
        _data: {},
        _audit: [ [Object], [Object] ],
        _triggers: {},
        tickInterval: undefined,
        stackLimit: 1000
      },
      origin: Enactment {
        protocol: Task {
          _parent: undefined,
          meta: undefined,
          caption: undefined,
          description: undefined,
          name: 'demo',
          preCondition: undefined,
          waitCondition: undefined,
          abortCondition: undefined,
          terminateCondition: undefined,
          eventTrigger: undefined,
          cycleUntil: undefined,
          optional: false,
          autonomous: false,
          cyclic: false,
          dataDefinitions: []
        },
        options: undefined,
        listeners: {},
        _state: { demo: [TaskState] },
        _data: {},
        _audit: [],
        _triggers: {},
        tickInterval: undefined,
        stackLimit: 1000
      },
      versions: [],
      maxIdx: -1
    }
    PROforma> rewinder.version(0).getStatus()
    {
      started: true,
      finished: false,
      completeable: [ 'demo' ],
      cancellable: [ 'demo' ],
      warnings: [],
      requested: {},
      confirmable: {},
      triggers: []
    }
