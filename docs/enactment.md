In proformajs the runtime instantiation of a protocol is called an enactment and is implemented as an *Enactment* object whose class is defined in the ``src/enactment.coffee`` file.  This document describes some of the technical details of an *Enactment* object.

## State engine

At runtime every task instance has a *state* attribute that's initialised as *dormant* and then progresses to *in_progress* or *discarded* dependent on its *waitCondition* and *preCondition* evaluation and the state of it's parent plan, if it has one.  An enactment object will update it's internal state whenever it's interacted with or when it's internal ticker ticks (if it has one).  A dormant task whose *waitCondition* is not defined or evaluates to true will transition to in_progress if it's *preCondition* is true (or not defined), else it will transition to *discarded*.  Once *in_progress* a task may be manually completed if it's *completetable* attribute is true.  It may also complete or discard automatically, depending on it's class and it's *autonomous*, *optional* and *abortCondition* attribute values.  If *optional* is true and a task is within a plan whose other non-optional tasks have finished (i.e. they are either completed or discarded) then the parent plan task will complete and it will discard or complete when respectively *dormant* or *in_progress*.  If the task has an *abortCondition* and that expression evaluates to true then the task will automatically discard, adding a note attribute ("aborted") in the audit trail.

![Task instance state engine](states.png)

## Internal ticker

If a tickInterval is set on an enactment (which can be done on construction) then a timeout will be added every time the enactment state engine is cycled to check again for updates.  With this mechanism, delays and time constraints can be created and imposed on tasks.

## Methods

The API of an enactment consists of the following methods

### getStatus()

Call this function to get a summary of the current enactment status.  The returned object may have the following attributes:

* started - true/false
* completeable - a list of completeable task paths
* cancellable - a list of cancellable task paths
* warnings - a list of active warning paths
* requested - an object whose keys are the paths of requesting tasks and values are a list of requested dataDef names
* confirmable - an object whose keys are the paths of in_process decisions with arrays of objects representing their candidate state
* finished - true/false

### start()

If you did not start your enactment when you constructed it, then you'll need to start it by calling this method.

### complete(path)

The enactment status will list the paths of completeable tasks.  Use this method to manually complete a task.

### set(path, data)

The enactment status will list requested data and the path which they are requested from.  Use this method to set that data.  You can provide data for multiple dataDefinitions at one time.  If any of that data is invalid then the good data will be accepted and only the invalid data rejected and a error will be raised.

### unset(path, dataDefinition)

Use this method to delete specific data, though the history of that dataDefinition during the enactments lifetime will still be retained.

### confirm(path)

The enactment status will list the path and state of decision candidates for in_progress decisions.  Use this method to confirm decision candidates.

### unconfirm(path)

The enactment status will list the path and state of decision candidates for in_progress decisions.  Use this method to change your mind about the confirmed status of a candidate.

### cancel(path)

The enactment status will list the paths of tasks that are cancellable.  The cancel method is used to cancel tasks manually.

Cancelling an *in_progress* task will move it to the *discarded* state. If a task is part of a parent plan and it is not *optional* then this action will also cancel it's parent task.  Cancelling tasks manually will often result in the whole enactment being discarded, as optional tasks are not widely used.

If an *in_progress* plan is cancelled then all of it's *dormant* or *in_progress* sub-tasks will be discarded.

## Construction

The Protocol namespace defines a series of State classes, e.g. *ComponentState*, *TaskState* etc that match the hierarchy of the standard component classes.  Accompanying state objects are created by the protocol components (with the *_buildState* function) when an enactment is constructed.  These state objects are stored in the *_state* attribute of an enactment and used to store and manage the state of the enacted protocol components over it's lifetime.

![State class diagram](componentstate.png)
