## General notes

Runtime data should have some providence, when (timestamp), and TODO: who (username),  where (route).

TODO?: As an enhancement it would be good to allow alternative evaluation engines (e.g. sparql)?  
If we do this then a protocol needs an attribute that indicates it's target engine and the data sources need to be appropriate to the evaluation engine.

TODO?: We should be able to support dynamic decisions with a 'candidate generator' and 'argument templates'
* e.g. interventions for a particular diagnosis
* with arguments about efficacy, cost, availability, side-effects and preferences
Inter candidate arguments should be possible arguing that one candidate is better than another when judged against a particular criteria.

TODO?: Should we support markdown for the description instead of supporting <html> enclosing tags.

TODO?: An I18n story must be considered.  Can all user-facing texts be indexed via a
path? (Yes I think).  What is the best mechanism to overlay a translation at runtime?

### arrays versus maps

Q: Is there are reason that arrays, not obects/maps, are used for tasks, candidates, sources?
A: Yes - the ordering is useful for managing versions of serialised protocols and in the UI, particularly for candidates.

### Custom validations

Extra custom validations must be built into the model, e.g. 'age>-1'. Without these, two checks are made to presented data - is it acceptable to type and if a range is provided, is it a range member.  If either condition is violated then a runtime error is thrown with a message that is suitable to be presented back to the user.  Warning components are one mechanism for doing this discussed (briefly) in the original paper.  Two competing custom validation models based on warnings were considered:

#### Model 1 - Error model

Extends what is there already.  warnCondition = "value>5".  On presenting data the warnings are checked. A true warning condition stops data going in and a caption is presented back to user.  
+ conceptually simple
- but requires a different flavour of expression in warnCondition which adds an extra burden to evaluator implementors and extra cognitive load to protocol coders. Note this is required because at the time of presenting the data it hasnt been added to the enactment so expressions must be written with respect to 'this' or 'value' and not the name of the data item.
- and can only be used for dataDefinitions
- and the presented value cannot easily be related to other existing values in the model

#### Model 2 - State model

Warnings are added to enactment state and a separate warnings list is maintained that indicates where warnings are raised.  DataDefinition. warnCondition = 'age<-1'.  Enquiry.requestCondition = !is_known(age) || !is_valid(age)
i reveals difficult problem with paths:
 - dataDefs need paths if they have @valueCondition or @warnings (but these werent previously covered)
 - If a dataDef is defined on an enquiry then the path is ambiguous (does it point to dataDef or source?)
+ extendable model - this design could potentially capture plan abort reasons
- requires additional expression predicates for inspecting state of enactment / components, e.g. is_valid(dataDef) which returns true if dataDef has a value and has no associated warnings

Model 2 won the battle of validation ideas.  Note that the range check is kept as a runtime error but an alternative model would be to attach an automatic warning to any dataDefinition with a range that was made active if data of the correct type was added but which didnt match a stated range value.  The good thing about the range is that it can guide the UI.  The potentially bad thing is that you can't make the dataDefinition optionally range compliant.
