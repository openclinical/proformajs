proformajs uses restricted Javascript expressions to query the state of a runtime enactment and evaluate schedule, value and argument conditions.  The source code for this functionality can be found in ``src/evaluator.coffee``.  This page details the functions that are exposed.

## Validation

Expressions are restricted to logic and arithmetic functions and the predicates listed below.  The aim is to restrict expressions so that they cannot have side effects on the engine state.  All expressions are validated at design time and invalid expressions will block the creation of an runtime enactment.

## Examples

If you define an integer data definition for age then you can use simple comparisons for argument conditions, task pre-conditions or data definition warning conditions, i.e. ``age>=18`` or ``!age<=0``.  When you name a data definition as an argument in a predicate, however, you will need to add single quotes, i.e. ``is_known('age')``.

## Paths

Many of the arguments for predicates in the following sections will need component paths to be passed as arguments.  In all cases the absolute design path will work, but oftentimes a name is sufficient, if that name identifies a child or sibling component.

## Task state

The task state engine evaluates the preCondition and waitCondition expressions that guard how a task moves through the four task states: *dormant*, *in_progress*, *completed* and *discarded*.  The *taskpath* parameter in these predicates can be the full path of a task or the name of sibling/child task.  At runtime the predicates will throw an error if the taskpath does not identify a task (i.e. if an external expression is evaluated via the ``Enactment.evaluate`` method).

### is_dormant

``is_dormant(taskpath): Boolean``

Returns true if the indicated task is dormant, else false.

### is_in_progress

``is_in_progress(taskpath): Boolean``

Returns true if the indicated task is in_progress, else false.

### is_completed

``is_completed(taskpath): Boolean``

Returns true if the indicated task is completed, else false.

### is_discarded

``is_discarded(taskpath): Boolean``

Returns true if the indicated task is discarded, else false.

### is_finished

``is_finished(taskpath): Boolean``

Returns true if the indicated task is completed *or* discarded, else false.

### num_finished

``num_finished([taskpath]): Integer``

Evaluates the status of tasks identified in the array of taskspaths presented and returns the number of them that are finished (discarded *or* completed).

### num_completed

``num_completed([taskpath]): Integer``

Evaluates the status of tasks identified in the array of taskspaths presented and returns the number of them that are completed.

### index

``index(taskpath): Integer``

Returns the current index of the cyclic task identified via the taskpath.  Throws an error if the taskpath fails to identify a task.

### last_finished

``last_finished(taskpath): runtimepath``

Returns the runtimepath of the most recently finished instance (i.e. the previous cycle) of a cyclic task.

## Decision state

Decision tasks have a handful of predicates for use in a Candidate's *recommendCondition* attribute and Argument's *activeCondition* attribute.

### net_support

``net_support(candidatepath): Integer``

Returns the aggregated support of all active arguments for the indicated candidate.  The *candidatepath* parameter may be a full path or the name of a candidate associated with the current decision context or one of the current decision's siblings.  Throws an error at runtime if *candidatepath* fails to identify a candidate.

### result_set

``result_set(decisionpath): [candidatename]``

Returns an array of confirmed candidate names for a particular decision. Returns null if the *decisionpath* parameter fails to identify a decision.

### result_of

``result_of(decisionpath): candidatename``

Returns the confirmed candidate name of a particular decision or undefined.  Returns null if the *decisionpath* parameter fails to identify a decision.

## Data state

Its useful to query the state of externally provided data.

### is_known

``is_known(datadef): Boolean``

Returns true if a value for datadef is available, otherwise false.  Throws an error at runtime if datadef is not defined in the guideline.

### is_valid

``is_valid(datadef): Boolean``

Returns true if a value is provided for a datadef and there are no associated warnings, else false.  Throws an error at runtime if datadef is not defined in the guideline.

### caption

``caption(datadef): String``

Returns the caption of ranged and annotated data definition values.  If the data definition isnt ranged and annotated, it returns the datadef value.  If the data definition is multiValued it returns an array of captions.

### captions

``captions(datadef): String``

Returns a readable list of multivalued range value captions, e.g. "one, two and three" when the datadef value is ``[1,2,3]`` and the data definition is multiValued and has a range of ``[{value: 1, caption: 'one'},{value: 2, caption: 'two'},{value: 3, caption: 'three'}]``.

### includes

``includes(arrayVal, member): Boolean``

Returns true if ``member`` is in ``arrayVal``.  Note that this is much safer than
``arrayVal.includes(member)`` which will be accepted and will work unless ``arrayVal``
is ``undefined`` when it will create a runtime exception. This predicate can be used
to check the result_set of decisions.

## Temporal

Its often important to compare the timing of some events to now or other events. Several predicates allow you to do this.

These predicates return Moment objects which hold dates and provide three useful addition predicates: ``diff``, ``add`` and ``subtract``.  So for instance, a Source *requestCondition* might contain the following expression ``!is_known('cough') || last_updated('cough').diff(now(), 'days')>0`` which would make the source requested if cough isnt known or is more than a day out of date.

### now

``now(): Moment``

Returns the current time.

### last_updated

``last_updated(datadef): Moment``

Returns the date that the last value for datadef was provided else undefined if no value is available.  Throws an error if datadef is not defined in the guideline.

### in_progress_time

``in_progress_time(taskpath): Moment``

Returns the time that the indicated task went to *in_progress*.  Throws an error at runtime if the *taskpath* parameter fails to identify a decision.

### completed_time

``completed_time(taskpath): Moment``

Returns the time that the indicated task was completed.  Throws an error at runtime if the *taskpath* parameter fails to identify a decision.

### discarded_time

``discarded_time(taskpath): Moment``

Returns the time that the indicated task was discarded.  Throws an error at runtime if the *taskpath* parameter fails to identify a decision.

### finished_time

``finished_time(taskpath): Moment``

Returns the time that the indicated task was finished (completed *or* discarded).  Throws an error at runtime if the *taskpath* parameter fails to identify a decision.

## Mathematical

Some external data will be numeric.  Predicates for standard mathematical operators are provided.

### round

``round(float): Integer``

Returns the passed value rounded to the nearest integer.

### abs

``abs(float): Float``

Returns the absolute value of a number, i.e. the size of the value.

### random

``random(): Float``

Returns a number between 0 and 1.

### sin

``sin(float): Float``

Returns the sine of a number.

### cos

``cos(float): Float``

Returns the cosine of a number.

### tan

``tan(float): Float``

Returns the tangent of a number.

### asin

``asin(float): Float``

Returns the arcsine of a number.

### acos

``acos(float): Float``

Returns the arcosine of a number.

### atan

``atan(float): Float``

Returns the arctan of a number.

### count

``count(arr): Integer``

Counts the array members.  Returns the number of members in the array.

### sum

``sum(arr): Float``

Sums the members of an array.

### min

``min(arr): Float``

Returns the minimum value of an array.

### max

``max(arr): Float``

Returns the maximum value of an array.

### nth

``nth(arr, idx): Object``

Returns the idx value of an array.

### exp

``exp(float): Float``

Returns the exponent of a value.

### log

``log(float): Float``

Returns the log of a value.

## Miscellaneous  

### includes

``includes(arrayVal, member): Boolean``

Returns true if ``member`` is in ``arrayVal``.  Note that this is much safer than
``arrayVal.includes(member)`` which will be accepted and will work unless ``arrayVal``
is ``undefined`` when it will create a runtime exception.

### join

``join(arr1, arr2, arr3): arr``

Returns an array that is the result of joining together the first two arrays, ``arr1`` and ``arr2`` and then removing the members of the final array ``arr3``.  ``arr3`` is optional.
