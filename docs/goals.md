# Goals

The proformajs project has several high level design goals:

* Create a web native proforma implementation so that proforma guidelines can be edited and executed in a web browser
* Simplify the creation and sharing of proforma guidelines as much as possible
* ... but maintain backwards compatibility and provide a migration path for existing Tallis guidelines in Openclinical
* Make proformajs guidelines easily composable to encourage reuse
* At runtime improve auditing and audit externally provided data so that data trends can be observed
* At runtime provide an easy to use summary of the status of the enactment
* At runtime provide a mechanism to automatically cycle the engine after a specified delay
