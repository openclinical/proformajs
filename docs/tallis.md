# A comparison with Tallis

PRO*forma* is a clinical decision support system (CDSS) language
that's described in the [2003 Sutton and Fox](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC212780/) paper.  The reference implementation is the Tallis framework which is implemented in Java.  This project is a new implementation of the core element of a PRO*forma* framework, corresponding to the engine from Tallis.  Although the implementation is guided by the original paper, there are several notable differences to the model of a guideline used by proformajs and the Tallis engine.  These differences are mostly simplifications but sometimes include enhancements to features found to be useful in the intervening period.

The headline differences include
* The implementing language
* JSON is used for serialisation
* No separation of definition and instance for tasks and data definitions
* Composable guidelines
* No antecedent tasks attribute for tasks
* Postconditions replaced by dynamic data definitions
* All runtime components are addressable by path
* At runtime a history of data is stored and can be referred to in protocol expressions
* A more accessible audit
* parameters ignored

and are treated in more detail in the following sections.

## Javascript

A Javascript engine is attractive in the current development ecosystem as rich web applications can be built on top of Nodejs with Javascript on the server and the client.

## No PROforma syntax

Tallis implemented its own syntax and expression language for guidelines,
however proformajs uses JSON, a widely used existing format, as the syntax and a subset of Javascript as the expression language.  In proformajs a protocol is a tree of Javascript objects that have associated validation rules that can be checked with a validate method.  To extend the model you should add a new class to the hierarchy and supply your own validation rules.

## No more instances and definitions

The Tallis PROforma implementation separates task instances and definitions and also data definitions and instances in the pursuit of efficiency of representation and reusability but the principles that guided the separation between definition and instance attributes are unclear and this lack of clarity leads to increased cognitive load when using the model.  proformajs takes the attitude that it's better to accept the potential for some redundancy as a tradeoff for clarity.

## All components addressable by path

In a proformajs protocol, all components are addressable by path. At runtime the paths are used to index the state attributes that are updated by the state engine.  This makes debugging more straightforward than trying to track down the UIDs of components and their attributes in a properties table.

## Composable guidelines

One of the design goals of proformajs is for protocols to be reusable through embedding existing protocols in new ones, also known as composability.  

### No static root plan

Tallis guidelines always have a root task that is a cut-down version of a Plan task.  This is one barrier to composability, albeit minor, that is removed in proformajs which allows any task to be the root of a protocol, which means that single tasks can be protocols.

### Scoped data definitions

Another barrier to composability of tasks is that the same concept might be differently defined in two protocols.  By attaching data definitions to any task in the task hierarachy and limiting the visibility of those data defintions to descendent tasks, but not ancestors, then we can isolate those differences.  Thus if we want to sequence two existing protocols that define a *temperature* dataDefinition, though one in Celcius and one in Farenheit then this should be straightforward.  More work needs to be done however in mapping between the two representations and ensuring that the composite protocol needs only requesting temperature once.

## No antecedents task attribute

Tallis tasks have an array of *antecedent* tasks in addition to the *waitCondition* and *preCondition* for implementing scheduling constraints.  This is an efficient representation but imposes some limitations on scheduling and is one of the harder elements to understand when first getting to grips with Tallis. proformajs eschews the antecedents attribute and instead relies more heavily on the *waitCondition* and *preCondition* expressions.  Its hoped that the scheduling constraints will be more explicit and flexibly represented this way, though it's accepted that there may be some loss in efficiency of representation.

## No postconditions

Postconditions in Tallis are mostly a kluge to create dynamic data which can be done in proformajs with dynamic data definitions, so postconditions have not been implemented. An additional benefit of this design decision is that it simplifies the expression evaluation considerably not to have to handle assignment.

## Runtime data

At runtime data definitions can capture and store multiple updates and
predicates will be available to interrogate the data history.
* todo: all_updates(dataDef, (from), (to)) : [Object]
* todo: num_updates(dataDef, (from), (to)) : int
* todo: updated_at(dataDef, idx) : datetime
* last_updated(dataDef) : datetime

When you've previously updated a value and you wish to 'delete' it, you'll need to explicitly unset that value.

Because at runtime, the engine knows how "fresh" data is, an enquiry source can stipulate how stale it will allow data to become before it becomes requested. This means that the requested nature of sources may change while an enquiry is in_progress.

## A more accessible audit

The enactment of a protocol in proformajs carries it's audit with it.  Every state update is recorded against the action that initiated the change and the time that the changes occurred.

## Postscript

Despite this list of implementation detail differences, the spirit of PROforma is sought to be preserved in proformajs and it is believed that the majority of Tallis guidelines can be converted to proformajs protocols and vice versa.
