// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import * as Protocol from '../src/tasks.js';
import fs from 'fs';
import chai from 'chai';
chai.should(); // monkeypatch should function so its available in tests

const tests = {};
for (var filename of fs.readdirSync('./etc')) {
  const nosuffix = filename.split(".")[0];
  const json = JSON.parse(fs.readFileSync('./etc/' + filename, 'utf-8'));
  tests[nosuffix] = Protocol.buildTask(json);
}

describe('When building protocols', function() {

  describe('it should be possible to programmatically build', function() {

    it('a single Task', function(done) {
      const task = new Protocol.Task({name:'hello', caption:'Hello World'});
      task.path().should.equal('hello');
      task.name.should.equal('hello');
      task.caption.should.equal('Hello World');
      task.isValid().should.be.true;
      return done();
    });

    it('an enquiry', function(done) {
      const enquiry = new Protocol.Enquiry({name: 'test'});
      enquiry.addDataDefinition(new Protocol.Date({name: 'dob'}));
      enquiry.addSource(new Protocol.Source({type: 'dob'}));
      enquiry.isValid().should.be.true;
      return done();
    });

    it('a plan with a sub-task', function(done) {
      const plan = new Protocol.Plan({name:'test'});
      const task = new Protocol.Task({name:'hello', caption:'Hello World'});
      plan.addTask(task);
      plan.path().should.equal('test');
      plan.isValid().should.be.true;
      task.path().should.equal('test:hello');
      task.isValid().should.be.true;
      return done();
    });

    it('a task with a pre-condition', function(done) {
      const task = new Protocol.Task({name:'test'});
      task.preCondition = "dob.diff(now(), 'years') > 1";
      task.isValid().should.be.false; // because the protocol doesnt know what a 'dob' is ('diff' and 'now' are system concepts)
      task.addDataDefinition(new Protocol.Date({name: 'dob'}));
      task.isValid().should.be.true;
      return done();
    });

    return it('a task with an event trigger', function() {
      const task = new Protocol.Task({name: 'test'});
      task.eventTrigger = 'runtest';
      task.validate().should.have.length(0);
    });
  });

  describe('Component API', function() {
    it('should provide access to sub-components', function(done) {
      const plan = new Protocol.Plan({name:'test'});
      const task = new Protocol.Task({name:'hello', caption:'Hello World'});
      plan.addTask(task);
      plan.subPaths().should.have.length(2);
      plan.subPaths().should.include('test');
      plan.subPaths().should.include('test:hello');
      plan.getComponent('test:hello').name.should.equal('hello');
      task.subPaths().should.have.length(1);
      task.subPaths().should.include('hello');
      try {
        plan.getComponent('tset');
        false.should.be.true;
      } catch (e) {
        e.message.should.equal("Unable to getComponent 'tset' from 'test'");
      }
      return done();
    });

    it('shouldnt double count enquiry sources', function() {
      // ... when the data definition is also defined in the enquiry
      const enq = new Protocol.Enquiry({name: 'test'});
      enq.addDataDefinition(new Protocol.Integer({name: 'age'}));
      enq.addSource(new Protocol.Source({type: 'age'}));
      enq.subPaths().should.have.length(2);
      enq.subPaths().should.include('test');
      enq.subPaths().should.include('test:age');
    });

    return it('should throw error for non-existent sub-components', function(done) {
      const task = new Protocol.Task({name:'hello', caption:'Hello World'});
      try {
        task.getComponent('hello:test');
        false.should.be.true;
      } catch (e) {
        e.message.should.equal("Unable to getComponent 'hello:test' from 'hello'");
      }
      return done();
    });
  });

  describe('design-time errors', function() {

    it('should prevent the creation of a named component without a name', function(done) {
      try {
        new Protocol.Task;
        false.should.be.true;
      } catch (e) {
        e.message.should.equal('new Named(obj): Unable to create protocol component without a name attribute');
      }
      return done();
    });

    it('should prevent the creation of a Source without a definition', function(done) {
      try {
        new Protocol.Source;
        false.should.be.true;
      } catch (e) {
        e.message.should.equal('new Protocol.Source(obj): Unable to create enquiry source without a type attribute');
      }
      return done();
    });

    return it.skip('should prevent the adding a non task as a sub-task', function() {
      const plan = new Protocol.Plan({name: 'test'});
      try {
        plan.addTask(new Protocol.Integer({name: 'int'}));
        return false.should.be.true;
      } catch (e) {
        e.message.should.equal("Plan.addTask(task): task must be of type Task (or one of it's sub-types)");
      }
    });
  });

  return describe('validation checks', function() {
    describe('for all components', () => it('should warn for unexpected attributes', function(done) {
      const task = new Protocol.Task({name: "test"});
      task.captoin = "Test!";
      task.validate().should.have.length(1);
      task.validate()[0].type.should.equal('Warning');
      task.validate()[0].msg.should.equal('unexpected attribute - captoin');
      return done();
    }));

    describe('for named components', function() {
      it('should prevent non alpha-numeric names', function(done) {
        const task = new Protocol.Task({name: 'Hello World'});
        task.isValid().should.be.false;
        return done();
      });

      it('should prevent names starting with an underscore', function(done) {
        const task = new Protocol.Task({name: '_test'});
        task.isValid().should.be.false;
        return done();
      });

      return it('should prevent names clashing with one of the reserved words', function(done) {
        // note that an implementation detail (Core cannot require Evaluator)
        // means that this check is enforced in three places
        const task = new Protocol.Task({name: 'now'});
        task.isValid().should.be.false;
        const candidate = new Protocol.Candidate({name: 'now'});
        candidate.isValid().should.be.false;
        const dataDefinition = new Protocol.DataDefinition({name: 'now'});
        dataDefinition.isValid().should.be.false;
        return done();
      });
    });

    describe('for tasks', function() {
      it('should prevent reuse of dataDefintions', function(done) {
        const task = new Protocol.Task({name: 'test'});
        const def = new Protocol.Integer({name: 'age'});
        task.addDataDefinition(def);
        task.isValid().should.be.true;
        const task1 = new Protocol.Task({name: 'test1'});
        task1.addDataDefinition(def);
        task1.isValid().should.be.true;
        task.isValid().should.be.false;
        return done();
      });

      it('should prevent duplicate dataDefinition names', function(done) {
        const task = new Protocol.Task({name: 'test'});
        task.addDataDefinition(new Protocol.Integer({name: 'age'}));
        task.addDataDefinition(new Protocol.Integer({name: 'age'}));
        task.isValid().should.be.false;
        return done();
      });

      it('should prevent non-accepted data source range elements', function(done) {
        const dataDef = new Protocol.Integer({name: 'doses'});
        dataDef.range = [1,2];
        dataDef.isValid().should.be.true;
        dataDef.range=[1,'2'];
        dataDef.isValid().should.be.false;
        return done();
      });

      return it('should warn tasks that have eventTrigger and waitCondition attributes', function() {
        const task = new Protocol.Task({name: 'test', eventTrigger: 'trigger', waitCondition: 'true'});
        task.validate().should.have.length(1);
      });
    });

    describe('for plans', function() {

      it('should prevent duplicate sub-task names', function(done) {
        const plan = new Protocol.Plan({name:'test'});
        plan.addTask(new Protocol.Task({name:'hello'}));
        plan.addTask(new Protocol.Task({name:'hello'}));
        plan.isValid().should.be.false;
        return done();
      });

      it('should prevent name clashes between sub-tasks and data definitions', function(done) {
        const plan = new Protocol.Plan({name: 'plan'});
        plan.addDataDefinition(new Protocol.Integer({name: 'task'}));
        plan.isValid().should.be.true;
        plan.addTask(new Protocol.Task({name: 'task'}));
        plan.isValid().should.be.false;
        return done();
      });

      it('should prevent reuse of sub-tasks', function(done) {
        const plan = new Protocol.Plan({name:'test'});
        const task = new Protocol.Task({name:'hello'});
        plan.addTask(task);
        plan.isValid().should.be.true;
        // task reused by another plan
        const plan1 = new Protocol.Plan({name:'plan1'});
        plan1.addTask(task);
        plan1.isValid().should.be.true;
        plan.isValid().should.be.false;
        return done();
      });

      it('should warn if a plan has a zero or one tasks',function(done) {
        const plan = new Protocol.Plan({name:'test'});
        plan.isValid().should.be.true;
        plan.validate().should.have.length(1);
        plan.validate()[0].type.should.equal('Warning');
        return done();
      });

      return it('should prevent a plan that cycles infinitely on enactment', function(done) {
        const plan = new Protocol.Plan({name:'test', autonomous: true, cyclic:true});
        plan.isValid().should.be.false;
        return done();
      });
    });

    describe('for decisions', function() {
      it('should prevent reuse of candidates and aguments', function(done) {
        const decision = new Protocol.Decision({name: 'test'});
        const candidate = new Protocol.Candidate({name: 'one'});
        decision.addCandidate(candidate);
        decision.isValid().should.be.true;
        const decision1 = new Protocol.Decision({name: 'test1'});
        decision1.addCandidate(candidate);
        decision1.isValid().should.be.true;
        decision.isValid().should.be.false;
        const argument = new Protocol.Argument({activeCondition: 'true', support: '+', caption: 'test candidate'});
        candidate.addArgument(argument);
        decision1.isValid().should.be.true;
        const candidate2 = new Protocol.Candidate({name: 'two'});
        candidate2.addArgument(argument);
        decision1.isValid().should.be.false;
        return done();
      });

      it('should prevent duplicate candidate names', function(done) {
        const decision = new Protocol.Decision({name: 'test'});
        const candidate = new Protocol.Candidate({name: 'one'});
        decision.addCandidate(candidate);
        decision.addCandidate(new Protocol.Candidate({name: 'one'}));
        decision.isValid().should.be.false;
        return done();
      });

      return it('should prevent name clashes between candidates and data definitions', function(done) {
        const decision = new Protocol.Decision({name: 'test'});
        decision.addDataDefinition(new Protocol.Integer({name: 'one'}));
        decision.addCandidate(new Protocol.Candidate({name: 'one'}));
        decision.isValid().should.be.false;
        return done();
      });
    });

    describe('for candidates', () => it('should enforce being attached to a decision', function(done) {
      const cand = new Protocol.Candidate({name: 'cand'});
      cand.isValid().should.be.false;
      const dec = new Protocol.Decision({name: 'decision'});
      dec.addCandidate(cand);
      cand.isValid().should.be.true;
      return done();
    }));

    describe('for arguments', function() {
      it('should enforce being attached to a candidate', function(done) {
        const arg = new Protocol.Argument({caption: 'arg', support: '+', activeCondition:'true'});
        arg.isValid().should.be.false; // missing idx and not attached to candidate
        const cand = new Protocol.Candidate({name: 'cand'});
        cand.addArgument(arg);
        arg.isValid().should.be.true;
        return done();
      });

      return it('should enforce numeric or symbolic support', function(done) {
        const arg = new Protocol.Argument({caption: 'arg', support: '1', activeCondition:'true'});
        const cand = new Protocol.Candidate({name: 'cand'});
        cand.addArgument(arg);
        arg.isValid().should.be.false;
        arg.support=1;
        arg.isValid().should.be.true;
        return done();
      });
    });

    describe('for enquiries', function() {
      it('should prevent source of unkonwn data definition', function(done) {
        const enquiry = new Protocol.Enquiry({name: 'test'});
        enquiry.isValid().should.be.true;
        enquiry.addSource(new Protocol.Source({type: 'age'}));
        enquiry.isValid().should.be.false;
        return done();
      });

      it('should prevent reuse of sources', function(done) {
        const enquiry = new Protocol.Enquiry({name: 'test'});
        enquiry.addDataDefinition(new Protocol.Integer({name: 'age'}));
        const source = new Protocol.Source({type: 'age'});
        enquiry.addSource(source);
        enquiry.isValid().should.be.true;
        const enquiry1 = new Protocol.Enquiry({name: 'test1'});
        enquiry1.addDataDefinition(new Protocol.Integer({name: 'age'}));
        enquiry1.addSource(source);
        enquiry1.isValid().should.be.true;
        enquiry.isValid().should.be.false;
        return done();
      });

      it('should prevent duplicate source definitions', function(done) {
        const enquiry = new Protocol.Enquiry({name: 'test'});
        enquiry.addDataDefinition(new Protocol.Integer({name: 'age'}));
        enquiry.addSource(new Protocol.Source({type: 'age'}));
        enquiry.addSource(new Protocol.Source({type: 'age'}));
        enquiry.isValid().should.be.false;
        return done();
      });

      return it('should prevent dynamic data definitions being sourced', function(done) {
        const enquiry = tests.age_with_warnings;
        enquiry.isValid().should.be.true;
        enquiry.addSource(new Protocol.Source({type: 'age'}));
        enquiry.isValid().should.be.false;
        return done();
      });
    });

    describe('for sources', function() {
      // Note that this condition implicitly enforces being attached to a task
      it('should error if a source references a non-existent dataDefinition', function(done) {
        const enquiry = new Protocol.Enquiry({name: 'test'});
        enquiry.isValid().should.be.true;
        const source = new Protocol.Source({type: 'age'});
        source.isValid().should.be.false;
        enquiry.addSource(source);
        enquiry.isValid().should.be.false;
        source.isValid().should.be.false;
        enquiry.addDataDefinition(new Protocol.Integer({name: 'age'}));
        enquiry.isValid().should.be.true;
        source.isValid().should.be.true;
        return done();
      });

      return it('should warn for unexpected attributes', function() {
        const source = new Protocol.Source({type: 'test'});
        source.validate().should.have.length(1); // expecting unknown data definition error
        source.handle = 'brown';
        source.validate().should.have.length(2);
      });
    }); // expecting unknown attribute warning

    describe('for data definitions', function() {
      // Note that data definitions may be persisted unattached to a task, hence no test for being attached to a task
      it('should prevent an invalid defaultValue for an Integer dd', function(done) {
        const dataDef = new Protocol.Integer({name: 'age'});
        dataDef.isValid().should.be.true;
        dataDef.defaultValue = "one";
        dataDef.isValid().should.be.false;
        return done();
      });

      it('should prevent an invalid defaultValue for a Date dd', function() {
        const dd = new Protocol.Date({name: 'age', defaultValue: "No"});
        dd.isValid().should.be.false;
      });

      it('should prevent an invalid defaultCondition expression', function() {
        const dd = new Protocol.Date({name: 'age', defaultCondition: 'now('});
        dd.isValid().should.be.false;
        dd.defaultCondition='now()';
        dd.isValid().should.be.true;
      });

      it('should allow numeric ranges to have captions', function(done) {
        const enquiry = tests.range_with_captions;
        enquiry.isValid().should.be.true;
        return done();
      });

      it('should allow text ranges to have captions', function(done) {
        const decision = tests.text_range_caption;
        decision.isValid().should.be.true;
        return done();
      });

      it('should prevent an invalid default range value', function(done) {
        const dataDef = new Protocol.Text({name: 'test'});
        dataDef.range=["one","two"];
        dataDef.isValid().should.be.true;
        dataDef.defaultValue = "\"one\"";
        dataDef.isValid().should.be.false;
        dataDef.defaultValue = "one";
        dataDef.isValid().should.be.true;
        dataDef.multiValued=true;
        dataDef.isValid().should.be.false;
        dataDef.defaultValue = ["one"];
        dataDef.isValid().should.be.true;
        return done();
      });

      return it('should cope with numeric text values', function(done) {
        tests.numeric_text.isValid().should.be.true;
        return done();
      });
    });

    describe('for warnings', () => // Note that validating the warn condition implicitly checks that it's attached to a dataDefinition so this is not tested
    it('should check warnCondition', function(done) {
      const task = new Protocol.Task({name:'test'});
      const age = new Protocol.Integer({name: 'age', caption: 'Subject\'s age'});
      age.addWarning(new Protocol.Warning({
        warnCondition:'ag>-1',
        caption:'Subect\'s age must be -1 or above'
      })
      );
      task.addDataDefinition(age);
      task.isValid().should.be.false; // due to invalid expression
      task.dataDefinitions[0].warnings[0].warnCondition='age>-1';
      task.isValid().should.be.true;
      return done();
    }));

    return describe('for the empty_conditions protocol', () => it('should warn for empty conditions', function() {
      const plan = tests.empty_conditions;
      plan.validate().should.have.length(5);
    }));
  });
});

describe('Protocol serialisation - ', function() {

  describe('it should be possible to generate JSON', () => it('for a single Task', function(done) {
    const task = new Protocol.Task({name: 'task'});
    const serialisation = JSON.stringify(task);
    // TODO: improve following checks, i.e. JSON.stringify(task).jsonEquals(reference).should.be.true
    serialisation.should.contain('"class":"Task"');
    serialisation.should.contain('"name":"task"');
    return done();
  }));

  describe('it should be possible to parse JSON', function() {
    it('for a single Task', function(done) {
      const task = Protocol.inflate('{"class":"Task","name":"task"}');
      task.name.should.equal("task");
      task.isValid().should.be.true;
      return done();
    });

    it('for a plan with task sequence', function(done) {
      const plan = tests.plan_with_task_sequence;
      plan.tasks.should.have.length(2);
      return done();
    });

    it('for a decision with symbolic arguments', function(done) {
      const decision =tests.decision_with_symbolic_arguments;
      decision.dataDefinitions.should.have.length(1);
      decision.candidates[0].name.should.equal("one");
      decision.candidates[0].path().should.equal("test:one");
      decision.candidates[0].arguments.should.have.length(2);
      decision.candidates[1].name.should.equal("two");
      return done();
    });

    it('for an enquiry with a single source', function(done) {
      const enquiry = tests.enquiry_with_single_source;
      enquiry.sources.should.have.length(1);
      return done();
    });

    it('for a date default value', function(done) {
      const task = Protocol.inflate('{"class":"Task","name":"test","dataDefinitions":[{"class":"Date","name":"dob","defaultValue":"1980-10-24"}]}');
      task.isValid().should.be.true;
      return done();
    });

    it('for the cold or flu example', function(done) {
      const plan = tests.cold_or_flu;
      plan.tasks.should.have.length(7);
      const diagnosis = (plan.tasks.filter((task) => task.name === 'diagnosis'))[0];
      diagnosis.candidates.should.have.length(3);
      return done();
    });

    it('for a task with an invalid dd');
  });

  return describe('round trips should be idempotent', function() {
    // TODO: use json-diff or diff for these?
    it('for a basic task', function(done) {
      const task = new Protocol.Task({name: 'test'});
      const json = JSON.stringify(task);
      const task1 = Protocol.inflate(json);
      task.name.should.equal(task1.name);
      return done();
    });

    return it('for a confirming argument', function(done) {
      const arg = new Protocol.Argument({caption: 'test', support:'++'});
      const json = JSON.stringify(arg);
      const arg1 = new Protocol.Argument(JSON.parse(json));
      arg.support.should.equal(arg1.support);
      return done();
    });
  });
});
