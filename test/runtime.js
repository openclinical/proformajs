// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import * as Protocol from '../src/tasks.js';
import { Enactment } from '../src/enactment.js';
import Moment from 'moment';
import fs from 'fs';
import chai from 'chai';
const should = chai.should();

const tests = {};
for (var filename of fs.readdirSync('./etc')) {
  const nosuffix = filename.split(".")[0];
  const json = JSON.parse(fs.readFileSync('./etc/' + filename, 'utf-8'));
  tests[nosuffix] = Protocol.buildTask(json);
}

describe('An enactment', function() {
  describe('with any protocol', function() {
    it('should error on attempting to start an already started enactment', function(done) {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test'})});
      try {
        enactment.start();
        false.should.be.true;
      } catch (e) {
        e.message.should.equal('Cannot start an enactment that has already started');
      }
      return done();
    });

    it('should error on attempting to complete a non-completeable task', function(done) {
      const enactment = new Enactment({protocol: new Protocol.Task({name: 'test'})});
      try {
        enactment.complete('test');
        false.should.be.true;
      } catch (e) {
        e.message.should.equal('Cannot complete a task which is not completeable');
      }
      return done();
    });

    return it('shouldnt provide access to internals via evaluate', function() {
      const enactment = new Enactment({protocol: new Protocol.Task({name: 'test'})});
      try {
        enactment.evaluate("_enactment")
        false.should.be.true
      } catch (e) {
        e.message.should.equal("_evaluate failed for \"_enactment\" from test (illegal Identifier)");
      }
      try {
        enactment.evaluate("test")
        false.should.be.true
      } catch (e) {
        e.message.should.equal("_evaluate failed for \"test\" from test (illegal Identifier)");
      }
      try {
        enactment.evaluate("_getComponent('test')")
        false.should.be.true
      } catch (e) {
        e.message.should.equal("_evaluate failed for \"_getComponent('test')\" from test (illegal Identifier)");
      }
      enactment.evaluate("now()")._isAMomentObject.should.be.true;
    });
  });

  describe('with an invalid protocol', () => it('should error on creating the enactment', function(done) {
    try {
      new Enactment({start: true, protocol: new Protocol.Task({name: 'task one'})});
      false.should.be.true;
    } catch (e) {
      e.message.should.equal('Invalid Protocol - cannot create enactment');
    }
    return done();
  }));

  describe('of a task', function() {
    it('should have null lastCycled() value before starting', function(done) {
      const enactment = new Enactment({protocol: new Protocol.Task({name: 'test'})});
      should.not.exist(enactment.lastCycled());
      return done();
    });

    it('should update lastCycled() once the task is completed', function(done) {
      const enactment = new Enactment({start:true, protocol: new Protocol.Task({name: 'test'})});
      const date = enactment.lastCycled();
      should.exist(date);
      enactment.complete('test');
      enactment.lastCycled().should.not.equal(date);
      return done();
    });

    it('should be completeable', function(done) {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test'})});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('test');
      enactment.complete('test');
      enactment.getStatus().finished.should.be.true;
      enactment.getComponent('test').state.should.equal('completed');
      enactment.getComponent('test').completeable.should.be.false;
      enactment.getComponent('test').cancellable.should.be.false;
      return done();
    });

    it('should be cancellable', function(done) {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test'})});
      enactment.getStatus().cancellable.should.have.length(1);
      enactment.getStatus().cancellable.should.include('test');
      enactment.cancel('test');
      enactment.getStatus().finished.should.be.true;
      enactment.getComponent('test').state.should.equal('discarded');
      enactment.getComponent('test').completeable.should.be.false;
      enactment.getComponent('test').cancellable.should.be.false;
      return done();
    });

    it('should return getTasks() correctly', function(done) {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test'})});
      enactment.getTasks().should.have.length(1);
      enactment.getTasks()[0].name.should.equal('test');
      return done();
    });

    return it('should demonstrate started and finished listeners', function(done) {
      this.timeout(1000); // millis
      const enactment = new Enactment({protocol: new Protocol.Task({name: 'test'})});
      enactment.on('finished', function(evt) {
        evt.event.should.equal("finished");
        return done();
      });
      enactment.on('started', evt => evt.event.should.equal("started"));
      enactment.start();
      enactment.complete('test');
    });
  });

  describe('of a task called evaluate', () => it('shouldnt kill the evaluator', function(done) {
    const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'evaluate'})});
    enactment.evaluate('in_progress_time("evaluate")').unix().should.be.at.most(new Moment().unix());
    return done();
  }));

  describe('of an autonomous task', () => it('should complete immediately', function() {
    const task = new Protocol.Task({name: 'test', autonomous: true});
    const enactment = new Enactment({start: true, protocol: task});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test').state.should.equal('completed');
  }));

  describe('of a task with a dataDefinition', function() {
    it('should getDataDefinitions() correctly', function(done) {
      const task = new Protocol.Task({name:'test'});
      task.addDataDefinition(new Protocol.Integer({name:'age'}));
      const enactment = new Enactment({start: true, protocol: task});
      enactment.set('test', {age: 70});
      enactment.getDataDefinitions().should.have.length(1);
      enactment.getDataDefinitions()[0].path.should.equal('test:age');
      enactment.getDataDefinitions()[0].value.should.equal(70);
      return done();
    });

    it('should getComponent() for the dataDefinition', function(done) {
      const task = new Protocol.Task({name:'test'});
      task.addDataDefinition(new Protocol.Integer({name:'age'}));
      const enactment = new Enactment({start: true, protocol: task});
      enactment.getComponent('test:age').class.should.equal('Integer');
      return done();
    });

    it('should not error when unset is called first', function(done) {
      const task = new Protocol.Task({name:'test'});
      task.addDataDefinition(new Protocol.Integer({name:'age'}));
      const enactment = new Enactment({start: true, protocol: task});
      enactment.unset('test', 'age');
      return done();
    });

    return it('should not error when now() is used as a valueCondtion', function(done) {
      const task = new Protocol.Task({name:'test'});
      task.addDataDefinition(new Protocol.Date({name:'timestamp', valueCondition:'now()'}));
      const enactment = new Enactment({start: false, protocol: task, stackLimit: 10});
      enactment.start();
      return done();
    });
  });

  describe('of a pathologically recursive protocol', () => it('should throw a stack overflow exception', function() {
    const task = new Protocol.Task({name: 'test', cyclic: true, autonomous: true});
    const enactment = new Enactment({protocol: task, stackLimit: 100});
    try {
      enactment.start();
      return true.should.be.false;
    } catch (e) {
      return e.message.should.equal("stack size exception - delta's array is larger than stackLimit (100)");
    }
  }));

  describe('of a task with a pathological valueCondition', () => it('should be startable', function() {
    const enactment = new Enactment({start: true, protocol: tests.pathological});
    enactment.getStatus().started.should.be.true;
  }));

  describe('of a task with a dynamic caption', () => it('should change it\'s caption based on the data it holds', function(done) {
    const enactment = new Enactment({start:true, protocol: tests.task_with_dynamic_caption});
    enactment.getComponent('test').caption.should.equal("iteration :: , and again :: ");
    enactment.set('test', {iteration: 2});
    enactment.getComponent('test').caption.should.equal("iteration :: 2, and again :: 2");
    return done();
  }));

  describe('of a task with a date-based pre-condition', function() {
    it('should be completable when the pre-condition is met', function(done) {
      const enactment = new Enactment({protocol: tests.task_with_precondition});
      enactment.set('test', {dob: Moment('1970-03-02')});
      enactment.getComponent('test').state.should.equal('in_progress');
      enactment.getStatus().completeable.should.include('test');
      enactment.complete('test');
      enactment.getComponent('test').state.should.equal('completed');
      return done();
    });

    it('should discard when the pre-condition is not met', function(done) {
      const enactment = new Enactment({protocol: tests.task_with_precondition});
      enactment.set('test', {'dob': Moment().subtract(1,'month')});
      enactment.getComponent('test').state.should.equal('discarded');
      return done();
    });

    return it('should error if a moment isnt supplied for the date', function(done) {
      const enactment = new Enactment({protocol: tests.task_with_precondition});
      const today = new Date();
      try {
        enactment.set('test', {dob: new Date()});
        true.should.be.false; // this will generate an 'expected false to be true' Error if it's reached
      } catch (e) {
        e.message.should.equal(`dob - incompatible data provided: ${today}`);
      }
      return done();
    });
  });

  describe('of a task with an empty preCondition', () => it('shouldnt kill an enactment', function() {
    const task = new Protocol.Task({name: 'task', preCondition: ''});
    const enactment = new Enactment({start:true, protocol: task});
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal('task');
  }));

  describe('of a task with an empty waitCondition', () => it('shouldnt kill an enactment', function() {
    const task = new Protocol.Task({name: 'task', waitCondition: ''});
    const enactment = new Enactment({start:true, protocol: task});
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal('task');
  }));

  describe('of a task with a data definition with an empty valueCondition', () => it('shouldnt kill an enactment', function() {
    const task = new Protocol.Task({name: 'task'});
    task.addDataDefinition(new Protocol.Integer({name:'age', valueCondition:''}));
    const enactment = new Enactment({start:true, protocol: task});
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal('task');
  }));

  describe('of a cyclic task with a dynamic data definition', () => {
    return it('shouldnt kill an enactment', function() {
      const enquiry = tests.age;
      enquiry.cyclic=true;
      const enactment = new Enactment({start: true, protocol: enquiry});
      enactment.getStatus().started.should.be.true
    });
  });

  describe('of an empty autonomous task', () => it('should complete immediately', function() {
    const plan = new Protocol.Task({name: 'test', autonomous: true});
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test').state.should.equal('completed');
  }));

  describe('of an action', () => it('should be completable', function() {
    const action = new Protocol.Action({name: 'action'});
    const enactment = new Enactment({start: true, protocol: action});
    enactment.getStatus().started.should.be.true;
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal('action');
    enactment.complete('action');
    enactment.getStatus().finished.should.be.true;
  }));

  describe('of a decision with a candidate with an empty recommendCondition', () => it('shouldnt kill an enactment', function() {
    const dec = new Protocol.Decision({name: 'decision'});
    dec.addCandidate(new Protocol.Candidate({name:'one', recommendConditiion: ''}));
    const enactment = new Enactment({start:true, protocol: dec});
    enactment.getStatus().started.should.be.true;
  }));

  describe('of an empty autonomous decision', () => it('should complete immediately', function() {
    const plan = new Protocol.Decision({name: 'test', autonomous: true});
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test').state.should.equal('completed');
  }));

  describe('of an enquiry with a source with an empty requestCondition', () => it('shouldnt kill an enactment', function() {
    const enq = new Protocol.Enquiry({name: 'enquiry'});
    enq.addDataDefinition(new Protocol.Integer({name: 'age'}));
    enq.addSource(new Protocol.Source({type:'age', requestConditiion: ''}));
    const enactment = new Enactment({start:true, protocol: enq});
    enactment.getStatus().started.should.be.true;
  }));

  describe('of an empty autonomous enquiry', () => it('should complete immediately', function() {
    const plan = new Protocol.Enquiry({name: 'test', autonomous: true});
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test').state.should.equal('completed');
  }));

  describe('of an empty autonomous plan', () => it('should complete immediately', function() {
    const plan = new Protocol.Plan({name: 'test', autonomous: true});
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test').state.should.equal('completed');
  }));

  describe('of a simple plan', () => it('should require manual completion', function() {
    const plan = new Protocol.Plan({name:'plan'});
    plan.addTask(new Protocol.Task({name:'task'}));
    const enactment = new Enactment({start:true, protocol: plan});
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable.should.include('plan:task');
    enactment.complete('plan:task');
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable.should.include('plan');
    enactment.complete('plan');
    enactment.getStatus().finished.should.be.true;
  }));

  describe('of a plan with a task sequence and precondition', () => it('should discard the second task on completion of the first', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.plan_with_task_sequence_and_precondition});
    enactment.complete('plan:one');
    enactment.getComponent('plan').state.should.equal('completed');
    enactment.getComponent('plan:one').state.should.equal('completed');
    enactment.getComponent('plan:two').state.should.equal('discarded');
    return done();
  }));

  describe('of a single task with a time-based waitCondition', () => it('should automatically move to in_progress after a short delay', function(done) {
    const enactment = new Enactment({start: true, tickInterval: 0.1, protocol: tests.plan_tick_test});
    enactment.getComponent('test').state.should.equal('in_progress');
    enactment.getComponent('test:one').state.should.equal('dormant');
    // after 100 milliseconds, the engine should tick and the sub-task's wait condition should let task move to in_progress
    return setTimeout(function() {
      enactment.getComponent('test:one').state.should.equal('in_progress');
      return done();
    }
    , 150);
  })); // milliseconds

  describe('of the plan with a task sequence example', function() {
    it('should demonstrate sequencing', function(done) {
      const enactment = new Enactment({protocol: tests.plan_with_task_sequence});
      enactment.start();
      enactment.getComponent('test').state.should.equal('in_progress');
      enactment.getComponent('test:one').state.should.equal('in_progress');
      enactment.getComponent('test:two').state.should.equal('dormant');
      enactment.complete('test:one');
      enactment.getComponent('test:one').state.should.equal('completed');
      enactment.getComponent('test:two').state.should.equal('in_progress');
      enactment.complete('test:two');
      enactment.getComponent('test:two').state.should.equal('completed');
      enactment.getComponent('test').state.should.equal('completed');
      return done();
    });

    it('should discard the following task and discard the plan when the first is cancelled', function(done) {
      const enactment = new Enactment({start:true, protocol: tests.plan_with_task_sequence});
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include('test');
      enactment.getStatus().cancellable.should.include('test:one');
      enactment.cancel('test:one');
      enactment.getStatus().finished.should.be.true;
      enactment._state['test:one'].state.should.equal('discarded');
      enactment._state['test:two'].state.should.equal('discarded');
      enactment._state['test'].state.should.equal('completed');
      return done();
    });

    it('should complete the plan when the second task is cancelled', function(done) {
      const enactment = new Enactment({start:true, protocol: tests.plan_with_task_sequence});
      enactment.complete('test:one');
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include('test');
      enactment.getStatus().cancellable.should.include('test:two');
      enactment.cancel('test:two');
      enactment.getStatus().finished.should.be.true;
      enactment._state['test:one'].state.should.equal('completed');
      enactment._state['test:two'].state.should.equal('discarded');
      enactment._state['test'].state.should.equal('completed');
      return done();
    });

    return it('should discard the tasks when the plan is cancelled', function(done) {
      const enactment = new Enactment({start:true, protocol: tests.plan_with_task_sequence});
      enactment.cancel('test');
      enactment.getStatus().finished.should.be.true;
      enactment._state['test:one'].state.should.equal('discarded');
      enactment._state['test:two'].state.should.equal('discarded');
      enactment._state['test'].state.should.equal('discarded');
      return done();
    });
  });

  describe('of the sequence of plans example', () => {
    it('should allow taskB to be completed', () => {
      const enactment = new Enactment({start:true, protocol: tests.sequence_of_plans});
      enactment.getStatus().completeable.should.include('plan:planB:actionB');
      enactment._state['plan:planA:actionA'].state.should.equal('discarded');
      enactment._state['plan:planA'].state.should.equal('completed');
      enactment._state['plan'].state.should.equal('in_progress');
    })
  })

  describe('of the plan with two enquiries example', () => it('should not use the default value in the second enquiry', function() {
    const enactment = new Enactment({protocol: tests.plan_with_two_enquiries, start: true, options: {Enquiry: {useDefaults:true}}});
    enactment.set('test:enquiryA', {symptoms:['runny nose']});
    enactment.getData().test.symptoms.should.deep.equal(['runny nose']);
    enactment.complete('test:enquiryA');
    enactment.getData().test.symptoms.should.deep.equal(['runny nose']);
  }));

  describe('of the plans with waits example', () => it('should demonstrate sequencing', function(done) {
    const enactment = new Enactment({protocol: tests.plans_with_waits});
    enactment.start();
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal("test:one:three");
    enactment.complete('test:one:three');
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal("test:two:four");
    enactment.complete('test:two:four');
    enactment.getStatus().finished.should.be.true;
    return done();
  }));

  describe('of the decision with symbolic arguments example', function() {
    it('should have no candidates recommended without data', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.decision_with_symbolic_arguments});
      enactment._state['test'].recommended().should.have.length(0);
      return done();
    });

    it('should not be completable without confirming a candidate', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      enactment.start();
      enactment._state['test'].recommended().should.have.length(0);
      enactment.getStatus().completeable.should.have.length(0);
      enactment.confirm('test:one');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.complete('test');
      return done();
    });

    it('should have candidate one recommended if age>17', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      enactment.set('test', {'age': 20});
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('one');
      enactment.getComponent('test:one').recommended.should.be.true;
      enactment.getComponent('test:two').recommended.should.be.false;
      return done();
    });

    it('should have candidate two recommended if age<18 and be completeable after being confirmed', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      enactment.set('test', {age: 10});
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('two');
      enactment.getComponent('test:one').recommended.should.be.false;
      enactment.getComponent('test:two').recommended.should.be.true;
      enactment.confirm('test:two');
      enactment.complete('test');
      enactment.getComponent('test').state.should.equal('completed');
      return done();
    });

    it('should change recommendation upon significant data update', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      enactment.set('test', {'age': 20});
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('one');
      enactment.getComponent('test:one').recommended.should.be.true;
      enactment.getComponent('test:two').recommended.should.be.false;
      enactment.set('test', {'age': 10});
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('two');
      enactment.getComponent('test:one').recommended.should.be.false;
      enactment.getComponent('test:two').recommended.should.be.true;
      return done();
    });

    it('should not have its recommendation updated when completed', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      enactment.set('test', {age: 20});
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('one');
      enactment.getComponent('test:one').recommended.should.be.true;
      enactment.getComponent('test:two').recommended.should.be.false;
      enactment.confirm('test:one');
      enactment.complete('test');
      enactment.set('test', {'age': 10});
      // recommendation should not have been updated as decision is completed
      enactment._state['test'].recommended().should.have.length(1);
      enactment._state['test'].recommended()[0].should.equal('one');
      enactment.getComponent('test:one').recommended.should.be.true;
      enactment.getComponent('test:two').recommended.should.be.false;
      return done();
    });

    return it('should error if non-integer is supplied', function(done) {
      const enactment = new Enactment({protocol: tests.decision_with_symbolic_arguments});
      try {
        enactment.set('test', {age: "20"});
        true.should.be.false;
      } catch (e) {
        e.message.should.equal('age - incompatible data provided: 20');
      }
      return done();
    });
  });

  describe('of the enquiry with a single source example', function() {
    it('should not be completable until mandatory data is presented', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.enquiry_with_single_source});
      should.not.exist(enactment.getTasks()[0].sources[0].value);
      enactment.getComponent('test').state.should.equal('in_progress');
      enactment.getStatus().completeable.should.have.length(0);
      enactment.set('test', {firstName: 'Matt'});
      enactment.getTasks()[0].sources[0].value.should.equal('Matt');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.complete('test');
      enactment.getComponent('test').state.should.equal('completed');
      return done();
    });

    it('should be completable if data is already presented', function(done) {
      const enactment = new Enactment({protocol: tests.enquiry_with_single_source});
      enactment.set('test', {firstName: 'John'});
      enactment.getComponent('test').state.should.equal('in_progress');
      enactment.getComponent('test').completeable.should.be.true;
      enactment.complete('test');
      enactment.getComponent('test').state.should.equal('completed');
      return done();
    });

    it('should not have requested sources when completed', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.enquiry_with_single_source});
      enactment.getComponent('test').state.should.equal('in_progress');
      enactment.getStatus().requested.test.should.have.length(1);
      enactment.getComponent('test').completeable.should.be.false;
      enactment.set('test', {firstName: 'John'});
      enactment.getStatus().requested.test.should.have.length(0);
      enactment.complete('test');
      enactment.getComponent('test').state.should.equal('completed');
      enactment.getStatus().finished.should.be.true;
      return done();
    });

    return it('should not have requested sources when cancelled', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.enquiry_with_single_source});
      enactment.getStatus().requested.test.should.include('firstName');
      enactment.cancel('test');
      enactment.getStatus().finished.should.be.true;
      enactment.getComponent('test').state.should.equal('discarded');
      enactment.getComponent('test:firstName').requested.should.be.false;
      return done();
    });
  });

  describe('of the autonomous enquiry', () => it('should complete on receiving requested data', function() {
    const enactment = new Enactment({start: true, protocol: tests.autonomous_enquiry});
    enactment.getStatus().requested.autonomous_enquiry.should.include('age');
    enactment.set('autonomous_enquiry', {age: 21});
    enactment.getStatus().finished.should.be.true;
  }));

  describe('of the enquiry for a data definition with a default condition', () => it('should be set automatically', function() {
    const enactment = new Enactment({start: true, protocol: tests.source_default_expression, options: {Enquiry: {useDefaults: true}}});
    // TODO: check for date value in recent past
    should.exist(enactment.get('test', 'assessTime'));
  }));

  describe('of a decision with net_support references to other candidates in arguments', () => it('shouldnt fall over on start', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.decision_components});
    enactment.evaluate('net_support("one")', 'test').should.equal(0);
    try {
      enactment.evaluate('net_support("three")', 'test');
      false.should.be.true;
    } catch (e) {
      e.message.should.equal("_evaluate failed for \"net_support(\"three\")\" from test (unknown candidate: three)");
    }
    return done();
  }));

  describe('of a decision whose result_set is recorded in a data definition', () => it('should be set automatically', function() {
    const enactment = new Enactment({start: true, protocol: tests.result_set_default_condition, options: {Enquiry: {useDefaults: true}}});
    enactment.confirm("plan:decisionA:apple")
    enactment.complete("plan:decisionA")
    should.exist(enactment.get('plan', 'fruit'));
    enactment.get('plan', 'fruit').should.deep.equal(['apple']);
  }));

  describe('of a plan with sibling decisions', () => it('should still identify unknown candidates in net_support at runtime', function(done) {
    let e;
    const plan = new Protocol.Plan({name: 'plan'});
    plan.addTask(new Protocol.Decision({name: 'dec1'}));
    plan.tasks[0].addCandidate(new Protocol.Candidate({name: 'one'}));
    plan.addTask(new Protocol.Decision({name: 'dec2'}));
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.evaluate('net_support("one")', 'plan:dec1').should.equal(0);
    try {
      enactment.evaluate('net_support("two")', 'plan:dec1');
      false.should.be.true;
    } catch (error) {
      e = error;
      e.message.should.equal("_evaluate failed for \"net_support(\"two\")\" from plan:dec1 (unknown candidate: two)");
    }
    try {
      enactment.evaluate('net_support("two")', 'plan:dec2');
      false.should.be.true;
    } catch (error1) {
      e = error1;
      e.message.should.equal("_evaluate failed for \"net_support(\"two\")\" from plan:dec2 (unknown candidate: two)");
    }
    return done();
  }));

  describe('of a decision with confirming argument', () => it('should recommend candidate one', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.decision_with_confirming_argument});
    enactment.getComponent("Decision:one").support.should.be.above(0);
    enactment.getStatus().confirmable['Decision:one'].isRecommended.should.be.true;
    enactment.getStatus().confirmable['Decision:one'].isConfirmed.should.be.false;
    enactment.confirm('Decision:one');
    enactment.getStatus().confirmable['Decision:one'].isRecommended.should.be.true;
    enactment.getStatus().confirmable['Decision:one'].isConfirmed.should.be.true;
    enactment.complete('Decision');
    enactment.getStatus().finished.should.be.true;
    return done();
  }));

  describe('of a plan with autonomous tasks', () => it('should complete', function(done) {
    const plan = new Protocol.Plan({name: 'test', autonomous: 'true'});
    plan.addDataDefinition(new Protocol.Integer({name: 'age', defaultValue: 20}));
    const enquiry = new Protocol.Enquiry({name: 'enquiry', autonomous: true});
    enquiry.addSource(new Protocol.Source({type: 'age'}));
    plan.addTask(enquiry);
    const decision = new Protocol.Decision({name: 'decision', autonomous: true, waitCondition: 'is_completed("enquiry")'});
    const candidate = new Protocol.Candidate({name: 'one', recommendCondition: 'age>=18'});
    decision.addCandidate(candidate);
    plan.addTask(decision);
    const enactment = new Enactment({start: true, options: { Enquiry: {useDefaults:true}, Candidate: {autoConfirmRecommended:true}}, protocol: plan});
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent("test:decision:one").confirmed.should.be.true;
    return done();
  }));

  describe('of a plan with an optional task', () => it('should discard the optional task when parent plan completes and the task is dormant', function(done) {
    const plan = new Protocol.Plan({name: 'test', autonomous: 'true'});
    plan.addTask(new Protocol.Task({name: 'task1'}));
    plan.addTask(new Protocol.Task({name: 'task2', optional:true, waitCondition: "false"}));
    const enactment = new Enactment({start: true, protocol: plan});
    enactment.getStatus().completeable.length.should.equal(1, 'there should initially be one completeable task');
    enactment.complete('test:task1');
    enactment.getStatus().finished.should.be.true;
    enactment.getComponent('test:task2').state.should.equal('discarded');
    return done();
  }));

  describe('of the eligibility example', () => it('should demonstrate dynamic data', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.eligibility});
    enactment.set('test:demographics', {dob: new Moment('1960-04-09')});
    if ((enactment._data['test'].age == null)) {
      false.should.be.true;
    }
    try {
      enactment.set('test:demographics', {age: 20});
      true.should.be.false;
    } catch (e) {
      e.message.should.equal('age - cannot set a derived data definition value');
    }
    // todo: use this to test autonomous attribute?
    enactment.complete('test:demographics');
    enactment.set('test:allergies', {hasPenicillinAllergy: false});
    enactment.complete('test:allergies');
    Object.keys(enactment.getStatus().confirmable).should.have.length(2);
    enactment.getStatus().confirmable['test:eligibility:eligible'].isRecommended.should.be.true;
    enactment.confirm('test:eligibility:eligible');
    enactment.complete('test:eligibility');
    enactment.getStatus().finished.should.be.true;
    return done();
  }));

  describe('of the age with warnings example', () => it('should demonstrate warnings', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.age_with_warnings});
    enactment.getStatus().warnings.should.be.empty;
    enactment.getStatus().completeable.should.be.empty;
    enactment.set('test', {dob: new Moment('1840-01-01')});
    enactment.getStatus().completeable.should.be.empty;
    enactment.getStatus().warnings.should.have.length(1);
    enactment.set('test', {dob: new Moment('1960-01-01')});
    enactment.getStatus().warnings.should.be.empty;
    enactment.getStatus().completeable.should.have.length(1);
    enactment.complete('test');
    enactment.getStatus().finished.should.be.true;
    return done();
  }));

  describe('of a protocol with hierarchical data', function() {
    it('should supply the current state with getData()', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.hierarchical_data});
      enactment.set('root', {avar: 0});
      enactment.getData().root.avar.should.equal(0);
      enactment.set('root:one', {bvar: 1});
      enactment.getData()['root:one'].bvar.should.equal(1);
      enactment.set('root:one:two', {cvar: 2});
      enactment.getData()['root:one:two'].cvar.should.equal(2);
      return done();
    });

    it('should accept the current sate', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.hierarchical_data});
      enactment.setData({'root':{avar:0}, 'root:one': {bvar:1}, 'root:one:two': {cvar:2}});
      enactment.get('root').avar.should.equal(0);
      enactment.get('root:one').bvar.should.equal(1);
      enactment.get('root:one:two').cvar.should.equal(2);
      return done();
    });

    return it('should show different views of the state at different levels in the hierarchy', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.hierarchical_data});
      enactment.setData({'root':{avar:0}, 'root:one': {bvar:1}, 'root:one:two': {cvar:2}});
      Object.keys(enactment.get('root')).should.have.length(1);
      Object.keys(enactment.get('root:one')).should.have.length(2);
      Object.keys(enactment.get('root:one:two')).should.have.length(3);
      return done();
    });
  });

  describe('of the Cold or Flu example', function() {
    it('should diagnose flu', function(done) {
      const enactment = new Enactment({protocol: tests.cold_or_flu});
      enactment.start();
      enactment.getStatus().completeable.should.have.length(1);
      enactment.complete('cold_or_flu:introduction');
      enactment.set('cold_or_flu:information', {
        'age': 10,
        'temperature': 38.1,
        'cough': 'Severe',
        'complaints': ['Headache'],
        'vaccine': false
      }
      );
      enactment.getComponent('cold_or_flu:information').sources[0].value.should.equal(10);
      enactment.complete('cold_or_flu:information');
      enactment._state['cold_or_flu:diagnosis'].recommended().should.have.length(1);
      enactment._state['cold_or_flu:diagnosis'].recommended()[0].should.equal('flu');
      enactment.confirm('cold_or_flu:diagnosis:flu');
      enactment.complete('cold_or_flu:diagnosis');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable[0].should.equal("cold_or_flu:treatment_flu");
      enactment.complete("cold_or_flu:treatment_flu");
      enactment.getComponent("cold_or_flu").state.should.equal('completed');
      return done();
    });

    it('should diagnose cold', function(done) {
      const enactment = new Enactment({protocol: tests.cold_or_flu});
      enactment.start();
      enactment.complete('cold_or_flu:introduction');
      enactment.set('cold_or_flu:information', {
        age: 10,
        temperature: 36,
        cough: 'Mild to moderate',
        complaints: ['Runny stuffy nose'],
        vaccine: false
      }
      );
      enactment.complete('cold_or_flu:information');
      enactment._state['cold_or_flu:diagnosis'].recommended().should.have.length(1);
      enactment._state['cold_or_flu:diagnosis'].recommended()[0].should.equal('cold');
      enactment.confirm('cold_or_flu:diagnosis:cold');
      enactment.complete('cold_or_flu:diagnosis');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable[0].should.equal("cold_or_flu:treatment_cold");
      enactment.complete("cold_or_flu:treatment_cold");
      enactment.getComponent("cold_or_flu").state.should.equal('completed');
      return done();
    });

    return it('should diagnose other and recommend vaccine', function(done) {
      const enactment = new Enactment({protocol: tests.cold_or_flu});
      enactment.start();
      enactment.complete('cold_or_flu:introduction');
      enactment.set('cold_or_flu:information', {
        age: 70,
        temperature: 36,
        cough: 'Mild to moderate',
        complaints: ['Headache'],
        vaccine: false
      }
      );
      enactment.complete('cold_or_flu:information');
      enactment.getStatus().completeable.should.include("cold_or_flu:recommend_vaccine");
      enactment._state['cold_or_flu:diagnosis'].recommended().should.have.length(1);
      enactment._state['cold_or_flu:diagnosis'].recommended()[0].should.equal('other');
      enactment.confirm('cold_or_flu:diagnosis:other');
      enactment.complete('cold_or_flu:diagnosis');
      enactment.getStatus().completeable.should.include("cold_or_flu:treatment_other");
      enactment.getStatus().completeable.should.include("cold_or_flu:recommend_vaccine");
      enactment.complete("cold_or_flu:treatment_other");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.complete('cold_or_flu:recommend_vaccine');
      enactment.getComponent("cold_or_flu").state.should.equal('completed');
      return done();
    });
  });

  describe('of a protocol with defaults in the data definitions', () => it('should load defaults if useDefaults option is set', function(done) {
    const protocol = new Protocol.Enquiry({name: 'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'dob', defaultValue: 40}));
    protocol.addSource(new Protocol.Source({type:'dob'}));
    const enactment = new Enactment({start:true, protocol, options: {Enquiry: {useDefaults: true}}});
    Object.keys(enactment._data).should.have.length(1);
    enactment._data.test.dob.should.have.length(1);
    enactment.get('test','dob').should.equal(40);
    return done();
  }));

  describe('of the Cambra V2 guideline', () => it('should show autoConfirmMostSupported on the decision', function(done) {
    const enactment = new Enactment({
      start: true,
      protocol: tests.CAMBRA_V2,
      options: {
        Enquiry: {useDefaults: true},
        Decision: {autoConfirmMostSupported: true},
        Candidate: {autoConfirmRecommended: true}
      }});
    enactment.complete(enactment.getStatus().completeable[0]);
    enactment.complete(enactment.getStatus().completeable[0]);
    enactment.complete(enactment.getStatus().completeable[0]);
    enactment.complete(enactment.getStatus().completeable[0]);
    enactment.getComponent("plan_task_03321:Patient_assessment:Risk_assessment_decision:Moderate_risk").confirmed.should.be.true;
    enactment.getComponent("plan_task_03321:Patient_assessment:Risk_assessment_decision:High_risk").confirmed.should.be.false;
    //TODO: change data and ensure that previously committed candidates are now not committed
    return done();
  }));

  describe('of a protocol with an abort condition', () => it('should abort when that condition is true and the task is in_progress', function() {
    const enactment =new Enactment({start: true, protocol: tests.abortable});
    enactment.getStatus().finished.should.be.false;
    enactment.set('test', {age: 17});
    enactment.getStatus().finished.should.be.true;
  }));

  describe('of the initial assessment protocol', function() {
    it('should be able to use a default value of 0', function() {
      const enactment =new Enactment({start: true, options: {Enquiry: {useDefaults: true}}, protocol: tests.assess});
      enactment.getStatus().completeable.should.have.length(2);
    });

    return it('should be able to include result_of in a dynamic description', function() {
      const enactment =new Enactment({start: true, options: {Enquiry: {useDefaults: true}}, protocol: tests.assess});
      enactment.complete('Initial_assessment:Relevant_questions');
      enactment.complete('Initial_assessment:Symptoms_enquiry_initial');
      enactment.confirm('Initial_assessment:Severity_decision_initial:Major_depression');
      enactment.complete('Initial_assessment:Severity_decision_initial');
      enactment.getComponent('Initial_assessment:Assessment_results_initial').description.should.include("Major_depression");
    });
  });

  describe('of early_intervention', () => it('should allow net_support to refer to sibling decision candidates', function() {
    let enactment = new Enactment({start: true, options: {Enquiry: {useDefaults: true}}, protocol: tests.early_intervention});
    enactment.getStatus().started.should.be.true
    // TODO: more assertions here?
  }));

  describe('of an event triggerable task', function() {
    it('should expose the event trigger and trigger the task when sent', function() {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test', eventTrigger: 'runtest'})});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtest");
      // TODO: sending Unknown trigger should raise exception
      enactment.sendTrigger('runtest');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test");
      enactment.getStatus().triggers.should.have.length(0);
      enactment.complete("test");
      enactment.getStatus().finished.should.be.true;
    });

    return it('should respect pre-conditions', function() {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test', eventTrigger: 'runtest', preCondition: 'false'})});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtest");
      // As the preCondition evaluates to false it should force the task to discard so that the enactment finishes
      enactment.sendTrigger('runtest');
      enactment.getStatus().finished.should.be.true;
    });
  });

  describe('of a nested triggerable task', () => it('shouldnt expose the nested trigger until its parent is in_progress', function() {
    const enactment = new Enactment({start:true, protocol: tests.nested_trigger});
    enactment.getStatus().triggers.should.have.length(1);
    enactment.getStatus().triggers.should.include('trigA');
    enactment.sendTrigger('trigA');
    enactment.getStatus().triggers.should.have.length(1);
    enactment.getStatus().triggers.should.include('trigB');
  }));

  describe('of an autonomous decision combined with dataDefinition', () => it('should set the value on completion', function() {
    const enactment = new Enactment({start: true, protocol: tests.dec_result_of});
    enactment.getStatus().finished.should.be.true;
    enactment.evaluate("result_of('test')").should.equal("one");
    enactment.evaluate("res").should.equal("one");
  }));

  describe('of a plan with triggerable tasks', function() {
    it('should handle multiple triggers', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous: true});
      plan.addTask(new Protocol.Task({name:'one', eventTrigger: 'runone'}));
      plan.addTask(new Protocol.Task({name:'two', eventTrigger: 'runtwo'}));
      const enactment = new Enactment({start:true, protocol: plan});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(2);
      enactment.getStatus().triggers.should.include("runone");
      enactment.getStatus().triggers.should.include("runtwo");
      enactment.sendTrigger('runone');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:one');
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtwo");
      enactment.complete('plan:one');
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtwo");
      enactment.sendTrigger('runtwo');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:two');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.complete('plan:two');
      enactment.getStatus().finished.should.be.true;
    });

    it('should allow multiple tasks to be fired from a single trigger', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous: true});
      plan.addTask(new Protocol.Task({name:'one', eventTrigger: 'runtask'}));
      plan.addTask(new Protocol.Task({name:'two', eventTrigger: 'runtask'}));
      const enactment = new Enactment({start:true, protocol: plan});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtask");
      enactment.sendTrigger('runtask');
      enactment.getStatus().completeable.should.have.length(2);
      enactment.getStatus().completeable.should.include('plan:one');
      enactment.getStatus().completeable.should.include('plan:two');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.complete('plan:one');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:two');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.complete('plan:two');
      enactment.getStatus().finished.should.be.true;
    });

    it('should allow one of the tasks to be discarded and the other completed', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous: true});
      plan.addTask(new Protocol.Task({name:'one', eventTrigger: 'runtask'}));
      plan.addTask(new Protocol.Task({name:'two', eventTrigger: 'runtask'}));
      const enactment = new Enactment({start:true, protocol: plan});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runtask");
      enactment.sendTrigger('runtask');
      enactment.getStatus().completeable.should.have.length(2);
      enactment.getStatus().completeable.should.include('plan:one');
      enactment.getStatus().completeable.should.include('plan:two');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.cancel('plan:one');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:two');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.complete('plan:two');
      enactment.getStatus().finished.should.be.true;
    });

    it('should allow the plan to be autonomous and cyclic/triggered', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous: true, cyclic: true, eventTrigger: 'runplan'});
      plan.addTask(new Protocol.Task({name:'task'}));
      const enactment = new Enactment({start:true, protocol: plan});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().cancellable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runplan");
      enactment.sendTrigger('runplan');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan[0]:task');
      enactment.getStatus().triggers.should.have.length(0);
      enactment.cancel('plan[0]:task');
      // the parent task has automatically completed, but a new task with an incremented index should be created
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include("runplan");
    });

    it('should not treat dormant triggered tasks as optional', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous:true});
      plan.addTask(new Protocol.Task({name:'task', cyclic:true, eventTrigger:'runtask'}));
      const enactment = new Enactment({start:true, protocol:plan});
      enactment.getStatus().completeable.should.have.length(0);
      enactment.getStatus().triggers.should.have.length(1);
      enactment.getStatus().triggers.should.include('runtask');
      enactment.sendTrigger('runtask');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:task[0]');
      enactment.complete('plan:task[0]').getStatus().finished.should.be.false;
    });

    return it('should respect the optional attribute', function() {
      const plan = new Protocol.Plan({name:'plan', autonomous:true});
      plan.addTask(new Protocol.Task({name:'intro'}));
      plan.addTask(new Protocol.Task({name:'task', cyclic:true, eventTrigger:'runtask', optional: true}));
      let enactment = new Enactment({start:true, protocol:plan});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:intro');
      enactment.complete('plan:intro').getStatus().finished.should.be.true;
      enactment = new Enactment({start:true, protocol:plan});
      enactment.sendTrigger('runtask');
      enactment.getStatus().completeable.should.have.length(2);
      enactment.getStatus().completeable.should.include('plan:intro');
      enactment.getStatus().completeable.should.include('plan:task[0]');
      enactment.complete('plan:intro');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('plan:task[0]');
      enactment.complete('plan:task[0]').getStatus().finished.should.be.true;
    });
  });

  describe('of a cyclic task', function() {
    it('should cycle if completed or discarded', function() {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test', cyclic: true})});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test[0]");
      enactment.complete("test[0]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test[1]");
      enactment.getStatus().cancellable.should.have.length(1);
      enactment.getStatus().cancellable.should.include("test[1]");
      enactment.cancel("test[1]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test[2]");
      enactment.getStatus().cancellable.should.have.length(1);
      enactment.getStatus().cancellable.should.include("test[2]");
    });
      // TODO test that cancelling test[1] throws error

    it('should be limitable to a particular number of cycles', function() {
      const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test', cyclic: true, cycleUntil: "index()>0"})});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test[0]");
      enactment.complete("test[0]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("test[1]");
      enactment.getStatus().cancellable.should.have.length(1);
      enactment.getStatus().cancellable.should.include("test[1]");
      enactment.cancel("test[1]");
      enactment.getStatus().finished.should.be.true;
    });

    it('should support cyclic plans with cyclic tasks', function() {
      const plan = new Protocol.Plan({name: 'plan', cyclic: true});
      plan.addTask(new Protocol.Task({name: 'task', cyclic: true}));
      const enactment = new Enactment({start:true, protocol: plan});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("plan[0]:task[0]");
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include("plan[0]");
      enactment.getStatus().cancellable.should.include("plan[0]:task[0]");
      enactment.runtimeFromDesignPath("plan:task").should.equal("plan[0]:task[0]");
      enactment.complete("plan[0]:task[0]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("plan[0]:task[1]");
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include("plan[0]");
      enactment.getStatus().cancellable.should.include("plan[0]:task[1]");
      enactment.runtimeFromDesignPath("plan:task").should.equal("plan[0]:task[1]");
      enactment.cancel("plan[0]:task[1]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("plan[0]:task[2]");
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include("plan[0]");
      enactment.getStatus().cancellable.should.include("plan[0]:task[2]");
      enactment.runtimeFromDesignPath("plan:task").should.equal("plan[0]:task[2]");
      enactment.cancel("plan[0]");
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include("plan[1]:task[0]");
      enactment.getStatus().cancellable.should.have.length(2);
      enactment.getStatus().cancellable.should.include("plan[1]");
      enactment.getStatus().cancellable.should.include("plan[1]:task[0]");
      enactment.runtimeFromDesignPath("plan:task").should.equal("plan[1]:task[0]");
    });

    it('should be able to space out cycles temporally', function(done) {
      const task = new Protocol.Task({
        name: 'test',
        cyclic: true,
        cycleUntil: "index('test')>0",
        waitCondition: "index('test')==0 || now().diff(finished_time(last_finished('test')), 'milliseconds')>50"
      });
      const enactment = new Enactment({start:true, protocol: task});
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable.should.include('test[0]');
      enactment.complete('test[0]');
      enactment.getStatus().completeable.should.have.length(0);
      return setTimeout(function() {
        enactment._cycle({action: 'manual'});
        enactment.getStatus().completeable.should.have.length(1);
        enactment.getStatus().completeable.should.include('test[1]');
        enactment.complete('test[1]'); // this finishes the enactment and stops the ticking
        enactment.getStatus().finished.should.be.true;
        return done();
      }
      , 100);
    }); // milliseconds

    it('should allow an additional task to be restartable', function() {
      const enactment = new Enactment({start: true, protocol: tests.restartable});
      enactment.set('test:demographics', {gender:'Female'});
      enactment.getStatus().cancellable.should.have.length(3);
      enactment.getStatus().cancellable.should.include("test:hx_female[0]");
      enactment.set('test:demographics', {gender:'Male'});
      enactment.getStatus().cancellable.should.have.length(2, "setting gender to Male should cancel hx_female task");
      enactment.set('test:demographics', {gender:'Female'});
      enactment.getStatus().cancellable.should.have.length(3, "resetting gender to Female should start another hx_female task");
      enactment.getStatus().cancellable.should.include("test:hx_female[1]");
      enactment.complete('test:demographics');
      enactment.set('test:hx_female[1]', {menstruation_age: 12});
      enactment.complete('test:hx_female[1]');
      enactment.getStatus().finished.should.be.true;
    });

    it('shouldnt change semantics of child plan', function(done) {
      // cf 'should diagnose flu' above - this is the same test with a cyclic root
      const protocol = Protocol.inflate(JSON.stringify(tests.cold_or_flu));
      protocol.cyclic=true;
      const enactment = new Enactment({protocol});
      enactment.start();
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().cancellable.should.have.length(2); // added
      enactment.complete('cold_or_flu[0]:introduction');
      enactment.getStatus().cancellable.should.have.length(2); // added
      enactment.set('cold_or_flu[0]:information', {
        'age': 10,
        'temperature': 38.1,
        'cough': 'Severe',
        'complaints': ['Headache'],
        'vaccine': false
      }
      );
      enactment.getComponent('cold_or_flu[0]:information').sources[0].value.should.equal(10);
      enactment.complete('cold_or_flu[0]:information');
      enactment._state['cold_or_flu[0]:diagnosis'].recommended().should.have.length(1);
      enactment._state['cold_or_flu[0]:diagnosis'].recommended()[0].should.equal('flu');
      enactment.confirm('cold_or_flu[0]:diagnosis:flu');
      enactment.complete('cold_or_flu[0]:diagnosis');
      enactment.getStatus().completeable.should.have.length(1);
      enactment.getStatus().completeable[0].should.equal("cold_or_flu[0]:treatment_flu");
      enactment.complete("cold_or_flu[0]:treatment_flu");
      enactment.getComponent("cold_or_flu[0]").state.should.equal('completed');
      return done();
    });

    it('shouldnt break getData()', function() {
      const protocol = new Protocol.Task({name:'test', cyclic:true});
      protocol.addDataDefinition(new Protocol.Integer({name: 'age'}));
      const enactment = new Enactment({protocol, start: true});
      Object.keys(enactment.getData()).should.have.length(0);
      enactment.set('test[0]', {age: 44});
      Object.keys(enactment.getData()).should.have.length(1);
    });
      //enactment.getData().test.age.should equal 44

    it('shouldnt break getDataDefinitions()', function() {
      const protocol = new Protocol.Task({name:'test', cyclic:true});
      protocol.addDataDefinition(new Protocol.Integer({name: 'age'}));
      const enactment = new Enactment({protocol, start: true});
      enactment.getDataDefinitions().should.have.length(1);
      enactment.complete('test[0]');
      enactment.getDataDefinitions().should.have.length(1);
    });

    return it('shouldnt overwrite previous cycles dynamic captions', function() {
      const protocol = new Protocol.Task({name: 'test', cyclic: true, cycleUntil: "index('test')==2", caption: "Test ${index('test')}"});
      const enactment = new Enactment({start: true, protocol});
      enactment.getComponent('test[0]').caption.should.equal("Test 0");
      enactment.complete('test[0]');
      enactment.getComponent('test[1]').caption.should.equal("Test 1");
      enactment.getComponent('test[0]').caption.should.equal("Test 0");
    });
  });

  describe('of the audit example', function() {
    it('should complete on prescribing', function() {
      const enactment = new Enactment({start: true, protocol: tests.audit})
      enactment.getStatus().started.should.be.true;
      enactment.getStatus().cancellable.should.have.length(1)
      enactment.getStatus().cancellable[0].should.equal('audit1')
      enactment.set('audit1', {diagnoses: ['diabetes_T2']})
      enactment.getStatus().cancellable.should.have.length(3)
      enactment.set('audit1:diabetes_management:checkup1', {ACR: 4, eGFR: 60, SBP: 120, DBP: 80})
      enactment.complete('audit1:diabetes_management:checkup1')
      enactment.getStatus().finished.should.be.false;
      enactment.set('audit1', {prescriptions: ['ACE_I']})
      enactment.getStatus().finished.should.be.true
    });

    // the real life protocol says abort after a month, but we reduce this to a second for testing
    return it('should abort automatically after a second', function(done) {
      const enactment = new Enactment({start: true, protocol: tests.audit})
      enactment.getStatus().started.should.be.true;
      enactment.getStatus().cancellable.should.have.length(1)
      enactment.getStatus().cancellable[0].should.equal('audit1')
      enactment.set('audit1', {diagnoses: ['diabetes_T2']})
      enactment.getStatus().cancellable.should.have.length(3)
      enactment.set('audit1:diabetes_management:checkup1', {ACR: 4, eGFR: 60, SBP: 120, DBP: 80})
      enactment.complete('audit1:diabetes_management:checkup1')
      enactment.getStatus().finished.should.be.false;
      return setTimeout(function() {
        enactment._cycle({action: 'manual'})
        enactment.getStatus().finished.should.be.true
        return done()
      }
      , 1500);
    });
  });

  describe('of a task with conflicting abort and terminate conditions', function() {
    return it('should prefer the terminate condition', function() {
      const task = new Protocol.Task({name: 'test', abortCondition: 'true', terminateCondition: 'true'})
      const enactment = new Enactment({start: true, protocol: task})
      enactment.getStatus().finished.should.be.true
      enactment.getComponent('test').state.should.equal('discarded')
    });
  });

  describe('of a task with an array valued dynamic data definition', function() {
    it('shouldnt stackoverflow when started', function() {
      const e1 = new Enactment({start: true, protocol: tests.stackoverflow1, stackLimit: 10})
      e1.getStatus().started.should.be.true;
    })

    return it('shouldnt stackoverflow when started with data', function() {
      const e1 = new Enactment({protocol: tests.stackoverflow1, stackLimit: 10})
      e1.set('test', {age:49})
      e1.getStatus().started.should.be.true;
    })
  });

  describe('of a compound data definition', function() {
    it('example', function() {
      const protocol = tests.compound_dd
      protocol.isValid().should.be.true
      const enactment = new Enactment({start: true, protocol})
      enactment.set('plan:enquiryA', {cps_select: ["Nausea", "Vomiting", "Other current physiological symptoms (specify)"]})
      enactment.complete("plan:enquiryA")
      enactment.set('plan:enquiryB', {cps_other: "Swollen legs"})
      enactment.complete("plan:enquiryB")
      enactment.getData().plan.cps.should.deep.equal(["Nausea", "Vomiting", "Swollen legs"])
    });
  })

  describe('of a dynamic integer data definition', function() {
    it('should reset to undefined when its dependent variable is unset', function() {
      const protocol = tests.dynamic_integer_undefined
      const enactment = new Enactment({start: true, protocol})
      enactment.set('task', {int1: 2})
      enactment.getData().task.int2.should.equal(4)
      enactment.unset('task', 'int1')
      should.equal(enactment.getData().task.int2, undefined)
    });
  })

  return describe('of a decision with dynamic captions', function() {
    return it('should resolve dynamic argument captions', function() {
      const enactment = new Enactment({start: true, protocol: tests.dynamic_decision_captions});
      enactment.set('decision',{age: 40});
      enactment.getComponent('decision').description.should.equal("Patient is 40 years old");
      enactment.getComponent('decision').candidates[0].description.should.equal("Patient is 40 years old");
      enactment.getComponent('decision').candidates[0].arguments[0].description.should.equal("Patient is 40 years old");
    });
  });
});

describe('Enactment serialisation', function() {
  it('should work for a single task protocol', function(done) {
    const enactment = new Enactment({protocol: new Protocol.Task({name: 'task'})});
    enactment.start();
    const json = JSON.stringify(enactment);
    // TODO improve this check
    json.should.contain("\"_data\":{}");
    const enactment1 = new Enactment({enactment: JSON.parse(json)});
    enactment1.complete('task');
    const json1 = JSON.stringify(enactment);
    json1.should.contain("\"_data\":{}");
    return done();
  });

  // dates in guideline data are a pain because JSON needs assistance in inflating them
  // a lot of work goes into handling them, that is checked in this test
  it('should work for a single task with a date data definition', function(done) {
    const protocol = new Protocol.Task({name: 'test'});
    protocol.addDataDefinition(new Protocol.Date({name: 'dob'}));
    const enactment = new Enactment({protocol});
    enactment.set('test', {dob: new Moment("24/10/1990","DD/MM/YYYY")});
    enactment.get('test','dob')._isAMomentObject.should.be.true;
    enactment._audit[0].action.data.dob._isAMomentObject.should.be.true;
    const json = JSON.stringify(enactment);
    const enactment2 = Enactment.inflate(json);
    enactment2.get('test','dob')._isAMomentObject.should.be.true;
    enactment2.get('test','dob').format("DD/MM/YYYY").should.equal("24/10/1990");
    enactment2._audit[0].action.data.dob._isAMomentObject.should.be.true;
    return done();
  });

  it('should work for an enquiry with single source protocol', function(done) {
    // earlier test should work with serialisation round trip
    const enactment1 = new Enactment({protocol: tests.enquiry_with_single_source});
    enactment1.start();
    const json = JSON.stringify(enactment1);
    const enactment = new Enactment({enactment: JSON.parse(json)});
    const test = enactment._state['test'];
    test.state.should.equal('in_progress');
    enactment.getStatus().completeable.should.have.length(0);
    test.set({'firstName': 'Matt'});
    enactment.getStatus().completeable.should.have.length(1);
    test.complete();
    test.state.should.equal('completed');
    return done();
  });

  it('should work for getComponent results, particularly arguments', function(done) {
    const dec = Protocol.inflate('{"class":"Decision","name":"test","candidates":[{"name":"cand0","arguments":[{"caption":"winning arg","activeCondition":"true","support":"++"},{"caption":"weak opposing","activeCondition":"true","support":"-"}]}]}');
    const enactment = new Enactment({start:true, protocol:dec});
    enactment.getComponent('test:cand0').support.should.equal(Infinity);
    enactment.getComponent('test:cand0:0').support.should.equal(Infinity);
    enactment.getComponent('test:cand0:1').support.should.equal(-1);
    const unpickled = JSON.parse(JSON.stringify(enactment.getComponent('test:cand0')));
    // check that the support values that were Infinity are not now null
    unpickled.support.should.equal('++');
    unpickled.arguments[0].support.should.equal('++');
    unpickled.arguments[1].support.should.equal(-1);
    return done();
  });

  it('should work for cold_or_flu', function(done) {
    const enactment1 = new Enactment({protocol: tests.cold_or_flu});
    enactment1.start();
    enactment1.complete('cold_or_flu:introduction');
    enactment1.set('cold_or_flu:information', {
      age: 10,
      temperature: 36,
      cough: 'Mild to moderate',
      complaints: ['Runny stuffy nose'],
      vaccine: false
    }
    );
    enactment1.complete('cold_or_flu:information');
    const json = JSON.stringify(enactment1);
    const enactment = new Enactment({enactment: JSON.parse(json)});
    enactment._state['cold_or_flu:diagnosis'].recommended().should.have.length(1);
    enactment._state['cold_or_flu:diagnosis'].recommended()[0].should.equal('cold');
    enactment.confirm('cold_or_flu:diagnosis:cold');
    enactment.complete('cold_or_flu:diagnosis');
    enactment.getStatus().completeable.should.have.length(1);
    enactment.getStatus().completeable[0].should.equal("cold_or_flu:treatment_cold");
    enactment.complete("cold_or_flu:treatment_cold");
    enactment.getComponent("cold_or_flu").state.should.equal('completed');
    return done();
  });

  // FIXME: this test hangs mocha without the --exit flag.
  // setTimeout creates the problem. see https://github.com/mochajs/mocha/issues/3044
  it('should work for a ticking enactment', function(done) {
    const enactment = new Enactment({start: true, tickInterval: 0.1, protocol: tests.plan_tick_test});
    enactment.getComponent('test').state.should.equal('in_progress');
    enactment.getComponent('test:one').state.should.equal('dormant');
    const json1 = JSON.stringify(enactment);
    const enactment1 = Enactment.inflate(json1);
    enactment1.getComponent('test').state.should.equal('in_progress');
    enactment1.getComponent('test:one').state.should.equal('dormant');
    // todo: destroy original enactment
    // after 100 milliseconds, the engine should tick and the sub-task's wait condition should let task move to in_progress
    return setTimeout(function() {
      enactment.getComponent('test:one').state.should.equal('in_progress');
      enactment.complete('test:one'); // this finishes the enactment and stops the ticking
      enactment1.getComponent('test:one').state.should.equal('in_progress');
      enactment1.complete('test:one');
      return done();
    }
    , 150);
  }); // milliseconds

  it('should work with unset date values', function() {
    const protocol = new Protocol.Enquiry({name: 'test'});
    protocol.addDataDefinition(new Protocol.Date({name: 'dob'}));
    protocol.addSource(new Protocol.Source({type: 'dob', requestCondition: '!is_known("dob")'}));
    const enactment = new Enactment({start: true, protocol});
    enactment.getStatus().completeable.should.have.length(0);
    enactment.set('test', {dob: Moment('1990-04-22')});
    enactment.getStatus().completeable.should.have.length(1);
    const json1 = JSON.stringify(enactment);
    const enactment1 = Enactment.inflate(json1);
    enactment1.getStatus().completeable.should.have.length(1);
    enactment1.unset('test', 'dob');
    enactment1.getStatus().completeable.should.have.length(0);
    const json2 = JSON.stringify(enactment1);
    const enactment2 = Enactment.inflate(json2);
    enactment2.getStatus().completeable.should.have.length(0);
    enactment2.set('test', {dob: Moment('1990-04-22')});
    enactment2.getStatus().completeable.should.have.length(1);
  });

  it('should work with unset boolean values', function() {
    const protocol = new Protocol.Enquiry({name: 'test'});
    protocol.addDataDefinition(new Protocol.Boolean({name: 'bool'}));
    protocol.addSource(new Protocol.Source({type: 'bool', requestCondition: '!is_known("bool")'}));
    const enactment = new Enactment({start: true, protocol});
    enactment.getStatus().completeable.should.have.length(0);
    enactment.set('test', {bool: true});
    enactment.getStatus().completeable.should.have.length(1);
    const json1 = JSON.stringify(enactment);
    const enactment1 = Enactment.inflate(json1);
    enactment1.getStatus().completeable.should.have.length(1);
    enactment1.unset('test', 'bool');
    enactment1.getStatus().completeable.should.have.length(0);
    const json2 = JSON.stringify(enactment1);
    const enactment2 = Enactment.inflate(json2);
    enactment2.getStatus().completeable.should.have.length(0);
    enactment2.set('test', {bool: false});
    enactment2.getStatus().completeable.should.have.length(1);
  });

  it('should work with hierarchical data', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.hierarchical_data});
    enactment.set('root', {avar: 0});
    enactment.set('root:one', {bvar:1});
    enactment.set('root:one:two', {cvar:2});
    const json = JSON.stringify(enactment);
    const enactment2 = Enactment.inflate(json);
    enactment2.getData()['root:one:two'].cvar.should.equal(2);
    return done();
  });

  it('should work with dynamic data defs', function() {
    const enactment = new Enactment({start: true, protocol: tests.loan_decision});
    const s1 = enactment.getStatus();
    const e2 = Enactment.inflate(JSON.stringify(enactment));
    const s2 = e2.getStatus();
    s1.should.deep.equal(s2);
  });

  it('should work for multiValued numeric data', function() {
    const plan = new Protocol.Plan({name: 'plan'});
    plan.addDataDefinition(new Protocol.Integer({name:'int01', multiValued:true}));
    const en1 = new Enactment({start:true, protocol:plan});
    en1.set('plan', {int01: [5]});
    en1._data.plan.int01[0].value.should.have.length(1);
    en1._data.plan.int01[0].value[0].should.equal(5);
    const json = JSON.stringify(en1);
    const en2 = Enactment.inflate(json);
    en2._data.plan.int01[0].value.should.have.length(1);
    en2._data.plan.int01[0].value[0].should.equal(5);
  });

  it('should work dynamic text values', function() {
    const t1 = new Protocol.Task({name: 'test'})
    t1.addDataDefinition(new Protocol.Integer({name: 'score'}))
    t1.addDataDefinition(new Protocol.Text({name: 'risk', valueCondition: "score>10 ? 'high' : 'low'", range:['low', 'high']}))
    const e1 = new Enactment({start: true, protocol: t1});
    e1.set("test", {score: 11});
    const j1 = JSON.stringify(e1);
    new Enactment({enactment: JSON.parse(j1)});
  });

  return it('should allow and record editing data', function() {
    // standard run
    let en = new Enactment({start: true, protocol: tests.age_trigger});
    en.getStatus().finished.should.be.false
    en.getStatus().cancellable.should.deep.equal(['test'])
    en.set('test', {age: 11})
    en.getStatus().completeable.should.deep.equal(['test:prompt'])
    en.complete('test:prompt')
    en.getStatus().finished.should.be.true
    // change of mind that terminates the prompt
    en = new Enactment({start: true, protocol: tests.age_trigger});
    en.getStatus().finished.should.be.false
    en.getStatus().cancellable.should.deep.equal(['test'])
    en.set('test', {age: 15})
    en.getStatus().completeable.should.deep.equal(['test:prompt'])
    en.set('test', {age: 17})
    en.getStatus().finished.should.be.true
    // edit that terminates the prompt and is tracked in the audit
    en = new Enactment({start: true, protocol: tests.age_trigger});
    en.getStatus().finished.should.be.false
    en.getStatus().cancellable.should.deep.equal(['test'])
    en.set('test', {age: 15})
    en.getStatus().completeable.should.deep.equal(['test:prompt'])
    en.edit('test:age', 0, 17)
    en.getStatus().finished.should.be.true
  })
});

describe('Predicates:', function() {
  it('is_dormant', function() {
    const enactment = new Enactment({start: true, protocol: tests.plan_with_task_sequence});
    // whole taskpath
    enactment.evaluate("is_dormant('test:two')").should.be.true;
    // siblings need not use the whole path
    enactment.evaluate("is_dormant('two')", 'test:one').should.be.true;
    // children can be addressed by name only
    enactment.evaluate("is_dormant('two')", 'test').should.be.true;
  });

  it('num_finished', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.plan_with_task_sequence_and_precondition});
    enactment.complete('plan:one');
    enactment.evaluate('num_finished(["plan:one","plan:two"])').should.equal(2);
    return done();
  });

  it('num_completed', function(done) {
    const enactment = new Enactment({start: true, protocol: tests.plan_with_task_sequence_and_precondition});
    enactment.complete('plan:one');
    enactment.evaluate('num_completed(["plan:one","plan:two"])').should.equal(1);
    return done();
  });

  it('mathematical', function() {
    const enactment = new Enactment({start: true, protocol: new Protocol.Task({name: 'test'})});
    enactment.evaluate("abs(-2)").should.equal(2);
    enactment.evaluate("random()").should.be.greaterThan(0);
    enactment.evaluate("sin(0)").should.equal(0);
    enactment.evaluate("cos(0)").should.equal(1);
    enactment.evaluate("tan(0)").should.equal(0);
    enactment.evaluate("asin(1)").should.equal(Math.PI/2);
    enactment.evaluate("acos(1)").should.equal(0);
    enactment.evaluate("atan(0)").should.equal(0);
    enactment.evaluate("exp(1)").should.equal(Math.E);
    enactment.evaluate("log(1)").should.equal(0);
  });

  it('arrays', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'arr', multiValued:true}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {arr: [0,1,2,3]});
    enactment.evaluate("sum(arr)").should.equal(6);
    enactment.evaluate("max(arr)").should.equal(3);
    enactment.evaluate("min(arr)").should.equal(0);
    enactment.evaluate("count(arr)").should.equal(4);
    enactment.evaluate("nth(arr, 2)").should.equal(2);
    enactment.evaluate("join(arr, [4,5])").should.deep.equal([0,1,2,3,4,5])
    enactment.evaluate("join(arr, [4,5], [3])").should.deep.equal([0,1,2,4,5])
    enactment.evaluate("join(undefined, undefined, undefined)").should.deep.equal([])
  });

  it('raw data', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'age'}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {age: 40});
    enactment.evaluate("age+1", "test").should.equal(41);
    enactment.evaluate("age", "test").should.equal(40);
  });

  it('age calc', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Date({name: 'dob'}));
    protocol.addDataDefinition(new Protocol.Integer({name: 'age', valueCondition: "now().diff(dob, 'years')"}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {dob: new Moment("2000-01-01")});
    enactment.evaluate("age", "test").should.be.greaterThan(17);
  });

  it('caption function (annotated range value)', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'category', range: [{value: 1, caption: "one"}, {value: 2, caption: "two"}]}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {category: 1});
    enactment.evaluate("caption('category')", "test").should.equal('one');
  });

  it('caption function (non annotated value)', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'category', range: [1, 2]}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {category: 1});
    enactment.evaluate("caption('category')", "test").should.equal(1);
  });

  it('caption function (annotated range array value)', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'category', range: [{value: 1, caption: "one"}, {value: 2, caption: "two"}, {value: 3, caption: "three"}], multiValued: true}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {category: [1,2,3]});
    enactment.evaluate("caption('category')", "test").should.deep.equal(['one', 'two', 'three']);
    enactment.evaluate("captions('category')", "test").should.equal('one, two and three');
  });

  it('caption function (non annotated array value)', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'category', range: [1, 2, 3], multiValued: true}));
    const enactment = new Enactment({start: true, protocol});
    enactment.set('test', {category: [1, 2, 3]});
    enactment.evaluate("caption('category')", "test").should.deep.equal([1,2,3]);
});

  it('caption function (undefined value)', function() {
    const protocol = new Protocol.Task({name:'test'});
    protocol.addDataDefinition(new Protocol.Integer({name: 'category', range: [1, 2]}));
    const enactment = new Enactment({start: true, protocol});
    should.equal(enactment.evaluate("caption('category')", "test"), undefined);
    enactment.set('test', {category: 1});
    enactment.evaluate("caption('category')", "test").should.equal(1);
    enactment.unset('test', 'category');
    should.equal(enactment.evaluate("caption('category')", "test"), undefined);
  });

  return it('result_of', function() {
    const protocol = new Protocol.Decision({name: 'test'});
    protocol.addCandidate(new Protocol.Candidate({name: 'one', recommendCondition: "true"}));
    protocol.addCandidate(new Protocol.Candidate({name: 'two', recommendCondition: "true"}));
    const enactment = new Enactment({start: true, protocol});
    should.equal(enactment.evaluate("result_of('test')"), undefined);
    enactment.confirm('test:one');
    enactment.evaluate("result_of('test')").should.equal('one');
    enactment.confirm('test:two');
    enactment.evaluate("result_of('test')").should.deep.equal(['one', 'two']);
    enactment.unconfirm('test:one');
    enactment.evaluate("result_of('test')").should.equal('two');
    enactment.unconfirm('test:two');
    should.equal(enactment.evaluate("result_of('test')"), undefined);
  });
});
