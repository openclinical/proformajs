// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import * as Protocol from '../src/tasks.js'
import { Enactment } from '../src/enactment.js'
import chai from 'chai'
chai.should() // monkeypatch should function so its available in tests
import { Rewinder } from '../src/rewinder.js'
import fs from 'fs'

describe('Rewinder tests', function () {
  it('1 - sequence of tasks', function () {
    let plan = new Protocol.Plan({ name: 'plan', autonomous: true })
    plan.addTask(new Protocol.Task({ name: 'task1' }))
    plan.addTask(
      new Protocol.Task({
        name: 'task2',
        waitCondition: "is_finished('task1')",
        preCondition: "is_completed('task1')"
      })
    )
    let enactment = new Enactment({ protocol: plan })
    let rewinder = new Rewinder(enactment)
    enactment.start()
    let status0 = enactment.getStatus()
    enactment.complete('plan:task1')
    let status1 = enactment.getStatus()
    enactment.complete('plan:task2')
    let status2 = enactment.getStatus()
    status2.finished.should.be.true
    rewinder.version(0).getStatus().should.deep.equal(status0)
    rewinder.version(1).getStatus().should.deep.equal(status1)
    rewinder.version(2).getStatus().should.deep.equal(status2)
    try {
      rewinder.version(-1)
      false.should.be.true
    } catch (e) {
      // passing a negative index should generate an error
      e.message.should.equal('Index out of range, -1')
    }
    try {
      rewinder.version(4)
      false.should.be.true
    } catch (e) {
      // passing an future index should generate an error
      e.message.should.equal('Index out of range, 4')
    }
  })

  it('2.1 - cold or flu example (started manually)', function () {
    let plan = Protocol.inflate(fs.readFileSync('../proformajs/etc/cold_or_flu.json', 'UTF-8'))
    let enactment = new Enactment({ start: false, protocol: plan })
    let rewinder = new Rewinder(enactment)
    let status = [enactment.start().getStatus()]
    status.push(enactment.complete('cold_or_flu:introduction').getStatus())
    status.push(
      enactment
        .set('cold_or_flu:information', {
          age: 50,
          cough: 'No cough',
          complaints: ['Extreme exhaustion', 'Sore throat', 'Headache'],
          temperature: 39.2,
          vaccine: false
        })
        .getStatus()
    )
    status.push(enactment.unset('cold_or_flu:information', 'age').getStatus())
    status.push(enactment.set('cold_or_flu:information', { age: 70 }).getStatus())
    status.push(enactment.complete('cold_or_flu:information').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.unconfirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:flu').getStatus())
    status.push(enactment.complete('cold_or_flu:diagnosis').getStatus())
    status.push(enactment.complete('cold_or_flu:treatment_flu').getStatus())
    status.push(enactment.complete('cold_or_flu:recommend_vaccine').getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 0; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })

  it('2.2 - cold or flu example (started automatically)', function () {
    let plan = Protocol.inflate(fs.readFileSync('../proformajs/etc/cold_or_flu.json', 'UTF-8'))
    let enactment = new Enactment({ start: true, protocol: plan })
    let rewinder = new Rewinder(enactment)
    let status = [enactment.getStatus()]
    status.push(enactment.complete('cold_or_flu:introduction').getStatus())
    status.push(
      enactment
        .set('cold_or_flu:information', {
          age: 50,
          cough: 'No cough',
          complaints: ['Extreme exhaustion', 'Sore throat', 'Headache'],
          temperature: 39.2,
          vaccine: false
        })
        .getStatus()
    )
    status.push(enactment.unset('cold_or_flu:information', 'age').getStatus())
    status.push(enactment.set('cold_or_flu:information', { age: 70 }).getStatus())
    status.push(enactment.complete('cold_or_flu:information').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.unconfirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:flu').getStatus())
    status.push(enactment.complete('cold_or_flu:diagnosis').getStatus())
    status.push(enactment.complete('cold_or_flu:treatment_flu').getStatus())
    status.push(enactment.complete('cold_or_flu:recommend_vaccine').getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 1; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })

  it('2.3 - cold or flu example (started with data)', function () {
    let plan = Protocol.inflate(fs.readFileSync('../proformajs/etc/cold_or_flu.json', 'UTF-8'))
    let enactment = new Enactment({ start: false, protocol: plan })
    let rewinder = new Rewinder(enactment)
    let status = [
      enactment
        .set('cold_or_flu:information', {
          age: 50,
          cough: 'No cough',
          complaints: ['Extreme exhaustion', 'Sore throat', 'Headache'],
          temperature: 39.2,
          vaccine: false
        })
        .getStatus()
    ]
    status.push(enactment.complete('cold_or_flu:introduction').getStatus())
    status.push(enactment.unset('cold_or_flu:information', 'age').getStatus())
    status.push(enactment.set('cold_or_flu:information', { age: 70 }).getStatus())
    status.push(enactment.complete('cold_or_flu:information').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.unconfirm('cold_or_flu:diagnosis:cold').getStatus())
    status.push(enactment.confirm('cold_or_flu:diagnosis:flu').getStatus())
    status.push(enactment.complete('cold_or_flu:diagnosis').getStatus())
    status.push(enactment.complete('cold_or_flu:treatment_flu').getStatus())
    status.push(enactment.complete('cold_or_flu:recommend_vaccine').getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 1; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })

  it('3 - single task cancelled', function () {
    let enactment = new Enactment({
      protocol: new Protocol.Task({ name: 'test' })
    })
    let rewinder = new Rewinder(enactment)
    let status = [enactment.start().getStatus()]
    status.push(enactment.cancel('test').getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 0; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })

  it('4 - single task triggered', function () {
    let enactment = new Enactment({
      protocol: new Protocol.Task({ name: 'test', eventTrigger: 'doit' })
    })
    let rewinder = new Rewinder(enactment)
    let status = [enactment.start().getStatus()]
    status.push(enactment.sendTrigger('doit').getStatus())
    status.push(enactment.complete('test').getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 0; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })

  it('5 - data recovery on repeated rewindings', function() {
    let plan = Protocol.inflate(fs.readFileSync('../proformajs/etc/loan_decision.json', 'UTF-8'))
    let data = {credit_history: 'good', loan_amount: 1000, loan_term: 12, applicant_income: 1000}
    let en = new Enactment({protocol: plan})
    let rw = new Rewinder(en)
    en.start()
    en.set('demo:enquire', {credit_history: 'good', loan_amount: 1000, loan_term: 12, applicant_income: 1000})
    en.complete('demo:enquire')
    en.confirm('demo:decide:deny')
    en.complete('demo:decide')
    let en1 = rw.version(1)
    en1.getData().demo.should.deep.equal({...data, total_income: 1000})
    // now repeat starting from en1
    let rw1 = new Rewinder(en1)
    en1.complete('demo:enquire')
    en1.confirm('demo:decide:deny')
    en1.complete('demo:decide')
    let en2 = rw1.version(3)
    en2.getData().demo.should.deep.equal({...data, total_income: 1000})
  })

  it('6 - age trigger with edited value', function () {
    let enactment = new Enactment({
      protocol: Protocol.inflate(fs.readFileSync('./etc/age_trigger.json', 'UTF-8'))
    })
    let rewinder = new Rewinder(enactment)
    let status = [enactment.start().getStatus()]
    status.push(enactment.set('test', {age: 15}).getStatus())
    status.push(enactment.edit('test:age', 0, 17).getStatus())
    enactment.getStatus().finished.should.be.true
    for (let idx = 0; idx < status.length; idx++) {
      rewinder.version(idx).getStatus().should.deep.equal(status[idx])
    }
  })
})
