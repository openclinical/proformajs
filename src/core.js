// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This module is required by the tasks and evaluator modules
// A design-time protocol consists of a tree of Component sub-class objects

// This abstract class is at the top of the component inheritance hierarchy
export class Component {
  constructor(options) {
    ({_parent: this._parent, meta: this.meta} = (options || {}));
  }

  // List expected attributes for this class.
  // Used by validate function to raise warnings for unexpected attributes..
  static _attributes() { return ['meta','_parent']; }

  // Keys that should not be flattened, see @flatten()
  static _ignoreKeys() { return ['_parent']; }

  // returns array of warning/error validations for this component and it's children
  // see also helper functions, _createWarningValidation and _createErrorValidation
  validate(identifiers) {
    if (identifiers == null) { identifiers = []; }
    const result = [];
    for (var key of Object.keys(this)) {
      if (!this.constructor._attributes().includes(key)) {
        result.push(this._createWarningValidation(key, `unexpected attribute - ${key}`));
      }
    }
    return result;
  }

  // returns false if there are any error validations
  isValid() { return this.validate().filter(issue => issue.type==='Error').length===0; }

  // overridden to add a class attribute, ignore empty values and drop the circular _parent attribute when serialised
  toJSON() {
    const result = {}
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_parent'].includes(key) && !(value == null)) { result[key] = value; }
    }
    return result;
  }

  // Returns a pojo (plain old javascript object) version of component without
  // child components (hence it's flatness).  Used in runtime getComponent calls.
  flatten() {
    const result =
      {class: this.constructor.name};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!this.constructor._ignoreKeys().includes(key) && value != null) { result[key] = value; }
    }
    return result;
  }

  // for walking down the tree.  to be overridden in sub-classes.
  _child() { return null; }

  // returns root component of tree
  _getRoot() {
    if (this._parent != null) {
      return this._parent._getRoot();
    } else {
      return this;
    }
  }

  // 'private' methods for creating validation messages
  _createValidation(type, attribute, message) {
    return {
      type,
      path: (this.path != null) ? this.path() : undefined,
      attribute,
      msg: message
    };
  }

  _createErrorValidation(attribute, message) {
    return this._createValidation('Error', attribute, message);
  }

  _createWarningValidation(attribute, message) {
    return this._createValidation('Warning', attribute, message);
  }
}

// A state class holds runtime state and state management functions
// The component state class hierarchy matches the component hierarchy
export class ComponentState {
  constructor(options) {
    ({path: this.path, _enactment: this._enactment} = (options || {}));
    // runtime component paths may include a task cycle index
    // which is removed and stored as the design path
    const comps = this.path.split(Named.PATH_SEPARATOR);
    const mapped = comps.map(function(comp) {
      const idx = comp.indexOf("[");
      if (idx>-1) { return comp.slice(0,idx); } else { return comp; }
    });
    this.design = mapped.join(Named.PATH_SEPARATOR);
  }

  // overridden to add a class attribute and drop the circular _enactment attribute when serialised
  toJSON() {
    const result = {};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_enactment'].includes(key) && !(value == null)) { result[key] = value; }
    }
    return result;
  }

  // provide data object that combines design and state
  getComponent() {
    const result = this._enactment.protocol.getComponent(this.design).flatten();
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_enactment'].includes(key) && !(value == null)) { result[key] = value; }
    }
    result.toJSON = this.toJSON;
    return result;
  }

  _parentStatePath() {
    const comps = this.path.split(Named.PATH_SEPARATOR);
    if (comps.length>1) {
      return comps.slice(0,comps.length-1).join(Named.PATH_SEPARATOR);
    } else {
      return null;
    }
  }
}

// Abstract class. An annotated component holds caption and description
export class Annotated extends Component {
  constructor(options) {
    super(options);
    ({caption: this.caption, description: this.description} = (options || {}));
  }

  static _attributes() { return super._attributes(...arguments).concat(['caption','description']); }

  // Does the named attribute (caption/description) include references to data values?
  _isDynamic(attribute) { if (this[attribute] != null) { return this[attribute].indexOf("${")>-1; } else { return false; } }
}

// maintain state of dynamic caption and description attributes
export class AnnotatedState extends ComponentState {

  constructor(options) {
    super(options);
    ({caption: this.caption, description: this.description} = (options || {}));
  }

  // replaces instances of ${expression} with the corresponding evaluated expression
  _getDynamic(attribute) {
    let result = this._enactment.protocol.getComponent(this.design)[attribute];
    if (result != null) {
      let app;
      const matches = [];
      // regex breakdown:
      // \$\{ matches the opening bracket
      // [^$]* matches any character that isnt a $, multiple times
      // the brackets around [^$]* capture whatever's in it
      // \} closes the expression
      const regex = /\$\{([^$]*)\}/g;
      while ((app = regex.exec(result)) != null) {
        matches.push(app);
      }
      if (matches.length>0) {
        for (let match of matches) {
          let exp = match[1]; // contents of capture group
          let resolved = this._enactment.evaluate(exp, this.design);
          result = result.replace(match[0], (resolved != null) ? resolved : "");
        }
      }
    }
    return result;
  }

  // By default dynamic attributes cannot be updated
  _isUpdateable() { return false; }

  // The state engine.  Returns an array of state updates
  _updateState() {
    const result=[];
    const updateDynamic = attribute => {
      if (this._enactment.protocol.getComponent(this.design)._isDynamic(attribute) && this._isUpdateable()) {
        const newvalue = this._getDynamic(attribute);
        if (newvalue !== this[attribute]) {
          return result.push({
            path: this.path,
            attribute,
            value: newvalue
          });
        }
      }
    };
      // # Note that the next block simplifies the API (just look at state for
      // # the value of possibly dynamic attributes) but creates unnecessary
      // # duplication in the enactment so isnt used.  Its kept here for context.
      // else if @_enactment.protocol.getComponent(@path)[attribute]? and (not @[attribute]?)
      //   result.push
      //     path: @path
      //     attribute: attribute
      //     value: @_enactment.protocol.getComponent(@path)[attribute]
    updateDynamic('caption');
    updateDynamic('description');
    return result;
  }
}

// A named component's name should be unique amongst its siblings
// and can be used to construct a path to the component within the protocol
// This abstract class is the superclass for a lot of classes in the design
export class Named extends Annotated {
  static PATH_SEPARATOR = ":";

  constructor(options) {
    super(options);
    ({name: this.name} = (options || {}));
    if ((this.name == null)) {
      throw new Error("new Named(obj): Unable to create protocol component without a name attribute");
    }
  }

  static _attributes() { return super._attributes(...arguments).concat(['name']); }

  // a component's path uniquely locates it in the protocol
  path() { if (this._parent != null) { return `${this._parent.path()}${Named.PATH_SEPARATOR}${this.name}`; } else { return this.name; } }

  // return the subPaths of all descendent components.
  // A subPath is a path that starts at this component, irrespective of whether
  // this component is the root of the component tree
  subPaths() { return [this.name]; }

  // Get descendent component as identified by it's subPath
  getComponent(subPath) {
    let names = subPath.split(Named.PATH_SEPARATOR);
    const root = names[0];
    if (root === this.name) {
      names = names.slice(1);
      let component = this;
      while (names.length>0) {
        var child = component._child(names[0]);
        if (child != null) {
          component = component._child(names[0]);
          names  = names.slice(1);
        } else {
          throw new Error(`Unable to getComponent '${subPath}' from '${this.path()}'`);
        }
      }
      return component;
    } else {
      throw new Error(`Unable to getComponent '${subPath}' from '${this.path()}'`);
    }
  }

  validate() {
    const result = super.validate();
    if (!(/^[\w\d]+$/.test(this.name))) {
      result.push(this._createErrorValidation('name', "names can only include alpha-numeric characters and underscores"));
    }
    if (this.name.indexOf('_') === 0) {
      result.push(this._createErrorValidation('name', 'name cannot start with an underscore'));
    }
    return result;
  }
}

// placeholder that maintains the principle of least surprise
export class NamedState extends AnnotatedState {}
