// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This evaluator allows javascript expressions to interrogate the state of
// an enactment at runtime, via a set of predicates defined in the
// EvaluatorContext class

// The interface of this evaluator consists of two methods,
// * validateExpression(expression, path, expression_name, local_identifiers)
// * buildEvaluator(enactment)
// a static array
// * reservedWords
// and a set of data definition classes that inherit from Core.Named and
// implement an accept(value) method

import { parse, evaluate } from 'jse-eval';
import moment from 'moment';
import * as Core from './core.js';

export const PATH_SEPARATOR = Core.Named.PATH_SEPARATOR;

// At design time expressions can be validated to a certain extent by checking
// that the expression is valid javascript and that every non-reserved token is
// recognised within the protocol.
export const validateExpression = function(expression, path, expression_name, local_identifiers) {
  var findIdentifiers = function(obj, result) {
    if (result == null) { result = []; }
    if ((obj.type != null) && (obj.type === 'Identifier')) {
      result.push(obj.name);
    } else if ((obj.type != null) && (obj.type === 'Literal') && (typeof obj.value === 'string')) {
      result.push(obj.value);
    } else {
      for (var key of Object.keys(obj)) {
        var value = obj[key];
        if (typeof obj === 'object') {
          result = findIdentifiers(value, result);
        }
      }
    }
    return result;
  };
  const context = local_identifiers.concat(reservedWords);
  try {
    const ast = parse(expression);
    const expression_identifiers = findIdentifiers(ast);
    const result = [];
    for (var identifier of expression_identifiers) {
      if (!context.includes(identifier)) {
        result.push({
          type: 'Error',
          attribute: expression_name,
          path,
          msg: `Unknown component/predicate: ${identifier}`
        });
      }
    }
    return result;
  } catch (error) {
    return [{type:'Error',path, msg:`parse Error: ${error}`, attribute: expression_name}];
  }
};

class EvaluatorContext {
  constructor(enactment, timestamp) {
    this._enactment = enactment;
    this._timestamp = timestamp;
    // add current data
    for (const path of Object.keys(enactment._data)) {
      for (const key of Object.keys(enactment._data[path])) {
        var arr = enactment._data[path][key];
        if (Array.isArray(arr) && (arr[arr.length-1].value != null)) {
          this[key] = arr[arr.length-1].value;
        }
      }
    }
    // add tasks
    for (const key of Object.keys(enactment._state)) {
      this[key] = enactment._state[key];
    }
  }

  _evaluate(exp, calling_path) {
    this._path = calling_path;
    const defs = (calling_path != null) ? this._enactment.protocol.getComponent(calling_path).allDataDefinitions().map(def => def.name) : this._enactment.protocol.allDataDefinitions().map(def => def.name);
    if ((exp != null ? exp.length : undefined)>0) {
      try {
        const ast = parse(exp);
        if (Object.keys(ast).length==2 && ast.type=="Identifier" && ast.name && !defs.includes(ast.name)) {
          throw new Error("illegal Identifier")
        }
        if (ast.type=="CallExpression" && ast.callee && ast.callee.type == "Identifier" && ast.callee.name.startsWith("_")) {
          throw new Error("illegal Identifier")
        }
        return evaluate(ast, this);
      } catch (e) {
        e.message = `_evaluate failed for "${exp}" from ${calling_path ? calling_path : this._enactment.protocol.name} (${e.message})`;
        throw e;
      }
    }
  }

  // returns time (as moment) that task identified by path reached a particular state using the audit
  _getTime(runtimepath, state) {
    for (var event of this._enactment._audit) {
      for (var cycle of event.deltas) {
        for (var update of cycle) {
          if ((update.path === runtimepath) && (update.value === state)) {
            return moment(event.timestamp);
          }
        }
      }
    }
  }

  // look for sibling as well as full path
  _getComponent(runtimepath) {
    if (this[runtimepath] != null) {
      return this[runtimepath];
    } else {
      // assume that runtimepath is in fact the name of a sibling component
      const paths = this._path.split(PATH_SEPARATOR);
      paths.splice(paths.length-1,1,runtimepath);
      const siblingpath = this._enactment.runtimeFromDesignPath(paths.join(PATH_SEPARATOR));
      if (this[siblingpath] != null) {
        return this[siblingpath];
      } else {
        // finally check for child path
        if (paths.length>0) {
          const childpath = this._enactment.runtimeFromDesignPath(this._path+PATH_SEPARATOR+runtimepath);
          if (this[childpath] != null) {
            return this[childpath];
          }
        }
      }
    }
  }

  _testTask(taskpath, test) {
    const task = this._getComponent(this._enactment.runtimeFromDesignPath(taskpath));
    if (task != null) {
      return test(task);
    } else {
      throw new Error(`unknown task: ${taskpath}`);
    }
  }

  now() { return this._timestamp || moment(); }
  is_dormant(taskpath) { return this._testTask(taskpath, task => task.state === 'dormant'); }
  is_in_progress(taskpath) { return this._testTask(taskpath, task => task.state === 'in_progress'); }
  is_completed(taskpath) { return this._testTask(taskpath, task => task.state === 'completed'); }
  is_discarded(taskpath) { return this._testTask(taskpath, task => task.state === 'discarded'); }
  is_finished(taskpath) { return this._testTask(taskpath, task => ['discarded','completed'].includes(task.state)); }
  in_progress_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(runtimepath, 'in_progress');
    } else {
      throw new Error(`unknown task: ${taskpath}`);
    }
  }
  completed_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(this._enactment.runtimeFromDesignPath(taskpath), 'completed');
    } else {
      throw new Error(`unknown task: ${taskpath}`);
    }
  }
  discarded_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(this._enactment.runtimeFromDesignPath(taskpath), 'discarded');
    } else {
      throw new Error(`unknown task: ${taskpath}`);
    }
  }
  finished_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      if (task.state === 'completed') { return this._getTime(runtimepath, 'completed'); } else { return this._getTime(runtimepath, 'discarded'); }
    } else {
      throw new Error(`unknown task: ${taskpath}`);
    }
  }
  is_valid(dataDef) {
    const comp = (this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter((def) => def.name === dataDef))[0];
    if (comp != null) {
      for (var warning of comp.warnings) {
        if (this[warning.path()].active) { return false; }
      }
      return true;
    } else {
      throw new Error(`unknown data definition: ${dataDef}`);
    }
  }
  is_known(dataDef) {
    const comp = (this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter((def) => def.name === dataDef))[0];
    if (comp != null) {
      return (this[dataDef] != null);
    } else {
      throw new Error(`unknown data definition: ${dataDef}`);
    }
  }
  caption(dataDef) {
    const getRangeItem = (value, range) => (range.filter((item) => item.value === value))[0];
    const comp = (this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter((def) => def.name === dataDef))[0];
    if (comp != null) {
      if ((comp.range != null) && (this[dataDef] != null)) {
        if (comp.multiValued) {
          const rangeItems = this[dataDef].map((value) => getRangeItem(value, comp.range));
          if ((rangeItems.length>0) && (rangeItems[0] != null)) {
            return rangeItems.map((item) => item.caption);
          } else {
            return this[dataDef];
          }
        } else {
          const rangeItem = getRangeItem(this[dataDef], comp.range);
          return (rangeItem != null ? rangeItem.caption : undefined) || this[dataDef];
        }
      } else {
        return this[dataDef];
      }
    } else {
      throw new Error(`unknown data definition: ${dataDef}`);
    }
  }
  captions(dataDef) {
    const arr = this.caption(dataDef);
    return arr.slice(0, +-2 + 1 || undefined).join(", ") + " and " + arr.slice(-1);
  }
  net_support(candidatepath) {
    let candidate = this._getComponent(this._enactment.runtimeFromDesignPath(candidatepath));
    if (candidate == null) {
      // following checks only work if path is a single name, i.e. the candidate
      if (candidatepath.split(PATH_SEPARATOR).length === 1) {
        const caller = this[this._enactment.runtimeFromDesignPath(this._path)];
        if (caller._getCandidateFromName != null) {
          candidate = caller._getCandidateFromName(candidatepath);
        }
      }
    }
    if (candidate != null) {
      return candidate.support || 0;
    } else {
      throw new Error(`unknown candidate: ${candidatepath}`);
    }
  }
  result_set(decisionpath) {
    const comp = this._getComponent(this._enactment.runtimeFromDesignPath(decisionpath));
    return comp ? (comp.confirmed ? comp.confirmed() : []) : null;
  }
  result_of(decisionpath) {
    const comp = this._getComponent(this._enactment.runtimeFromDesignPath(decisionpath));
    const confirmed = comp ? (comp.confirmed ? comp.confirmed() : null) : null;
    if ((confirmed != null ? confirmed.length : undefined) === 0) {
      return undefined;
    } else if ((confirmed != null ? confirmed.length : undefined) === 1) {
      return confirmed[0];
    } else {
      return confirmed;
    }
  }
  includes(arr, value) { return (arr != null) && arr.includes(value); }
  last_updated(dataDef) {
    const comp = (this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter((def) => def.name === dataDef))[0];
    if (comp != null) {
      for (var path of Object.keys(this._enactment._data)) {
        var vals = this._enactment._data[path];
        for (var key of Object.keys(vals)) {
          var arr = vals[key];
          if (key === dataDef) {
            return moment(arr[arr.length-1].when);
          }
        }
      }
      return undefined;
    } else {
      throw new Error(`unknown data definition: ${dataDef}`);
    }
  }
  num_finished(paths) {
    return paths.filter((path) => this.is_finished(path)).length;
  }
  num_completed(paths) {
    return paths.filter((path) => this.is_completed(path)).length;
  }
  date_part_hour(datetime) { return (this[datetime] != null ? this[datetime].hour() : undefined); }
  round(float) { return Math.round(float); }
  abs(float) { return Math.abs(float); }
  random() { return Math.random(); }
  sin(float) { return Math.sin(float); }
  cos(float) { return Math.cos(float); }
  tan(float) { return Math.tan(float); }
  asin(float) { return Math.asin(float); }
  acos(float) { return Math.acos(float); }
  atan(float) { return Math.atan(float); }
  count(arr) { return (arr != null ? arr.length : undefined); }
  sum(arr) { return arr.reduce((cum, val) => cum + val); }
  min(arr) { return arr.reduce(function(cum, val) { if ((cum != null) && (val<cum)) { return val; } else { return cum; } }); }
  max(arr) { return arr.reduce(function(cum, val) { if ((cum != null) && (val>cum)) { return val; } else { return cum; } }); }
  nth(arr, idx) { return arr[idx]; }
  exp(float) { return Math.exp(float); }
  log(float) { return Math.log(float); }
  // TODO: union / intersect / diff
  // returns index of current cycle of indicated task designPath or -1 if not cyclic
  index(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath || this._path);
    if (runtimePath != null) {
      if (this._enactment.getComponent(runtimePath).state != null) {
        return this._enactment.getComponent(runtimePath).index;
      } else {
        return this._enactment.getComponent(runtimePath).index-1;
      }
    } else { return -1; }
  }
  // returns path of previous runtime cycle of indicated designPath
  last_finished(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath);
    if (runtimePath.lastIndexOf(']') === (runtimePath.length-1)) {
      // get current index number from path
      const currentIndex = parseInt(runtimePath.substr(runtimePath.lastIndexOf('[')+1, runtimePath.lastIndexOf(']')-runtimePath.lastIndexOf('[')-1));
      return runtimePath.substr(0, runtimePath.lastIndexOf('[')+1) + (currentIndex-1) + ']';
    } else {
      return designPath;
    }
  }
  // returns the runtime path of the calling component (if the designPath is ommitted) or the passed designpath
  path(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath || this._path);
    const comp = this._getComponent(runtimePath);
    if (comp != null) {
      return runtimePath;
    } else {
      throw new Error(`unknown component: ${designPath}`);
    }
  }
  join(arr1, arr2, arr3) {
    // concaternate arr1 and arr2 and remove contents of arr3 from final array
    const result = (arr1 || []).concat(arr2 || []);
    if (arr3 != null) {
      return result.filter((item) => !arr3.includes(item))
    } else {
      return result
    }
  }
}

// A fairly raw evaluation strategy that seems good enough to at least get up and running
class JavascriptEvaluator {
  constructor(enactment, timestamp) {
    this.context = new EvaluatorContext(enactment, timestamp);
  }

  // evaluate an expression from the context of an (optional) designPath
  evaluate(exp, calling_path) {
    return this.context._evaluate(exp, calling_path);
  }
}

export const buildEvaluator = (enactment, timestamp) => new JavascriptEvaluator(enactment, timestamp);

export const reservedWords = Object.getOwnPropertyNames(EvaluatorContext.prototype).filter((key) => key.indexOf("_")!==0).concat(['diff','add','subtract','undefined','years','months','weeks','days','hours','seconds','milliseconds']);

// Warnings provide a mechanism to range check data
export class Warning extends Core.Annotated {
  constructor(options) {
    super(options);
    ({idx: this.idx, warnCondition: this.warnCondition} = (options || {}));
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['idx','warnCondition']); }

  path() { return `${this._parent.path()}${PATH_SEPARATOR}${this.idx}`; }

  subPaths() { return [this.idx.toString()]; }

  validate(identifiers) {
    if (identifiers == null) { identifiers = []; }
    let result = super.validate();
    if ((this.warnCondition == null) || (this.warnCondition === '')) {
      result.push({
        type: 'Error',
        attribute: 'warnCondition',
        msg: "missing or empty"
      });
    } else {
      result=result.concat(validateExpression(this.warnCondition, this.path(), 'warnCondition', identifiers));
    }
    return result;
  }

  allDataDefinitions() { return this._parent.allDataDefinitions(); }

  toJSON() {
    const result = super.toJSON();
    delete result.idx;
    return result;
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath+Core.Named.PATH_SEPARATOR+this.idx;
    return enactment._state[statePath] = new WarningState({path: statePath, _enactment: enactment});
  }
}

export class WarningState extends Core.AnnotatedState {
  constructor(options) {
    super(options);
    ({active: this.active} = (options || {}));
  }

  _updateState(evaluator) {
    const result = [];
    const comp = this._enactment.protocol.getComponent(this.design);
    const warnCondition = comp ? comp.warnCondition : null;
    if (warnCondition != null) {
      const active = evaluator.evaluate(warnCondition, this.design);
      if (active !== this.active) {
        result.push({
          path: this.path,
          attribute: 'active',
          value:active
        });
      }
    }
    return result;
  }
}

// A DataDefinition is a placeholder for external state as understood by the protocol.
// Use the accept method to constrain the possible values to be held within sub-classes.
// @defaultCondition - a combination of defaultValue and valueCondition - a default value expression
export class DataDefinition extends Core.Named {
  constructor(options) {
    super(options);
    ({range: this.range, valueCondition: this.valueCondition, defaultCondition: this.defaultCondition} = (options || {}));
    if (options.defaultValue!==undefined) {
      try {
        this.defaultValue = this.constructor.inflate(options.defaultValue);
      } catch (e) {
        // get the raw value and use protocol validation to signal the problem
        this.defaultValue = options.defaultValue;
      }
    } else {
      this.defaultValue = undefined;
    }
    this.multiValued = options.multiValued || false;
    this.warnings=[];
    if ((options.warnings != null) && Array.isArray(options.warnings)) {
      for (var meta of options.warnings) {
        var warning = new Warning(meta);
        this.addWarning(warning);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['range','multiValued', 'valueCondition', 'defaultValue','warnings', 'defaultCondition']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['warnings']); }

  // override this when you need to do class specific deserialisation
  static inflate(val) { return val; }

  // override this when you want to control how a value is displayed
  static render(val) { return val.toString(); }

  _child(idx) { return this.warnings[idx]; }

  subPaths() { return super.subPaths(...arguments).concat((this.warnings.map((warning) => this.name + PATH_SEPARATOR + warning.idx))); }

  addWarning(warning) {
    warning._parent = this;
    warning.idx = this.warnings.length;
    return this.warnings.push(warning);
  }

  // test to see if this data definition will accept this value
  accept(value) {
    if (this.multiValued) {
      if (Array.isArray(value)) {
        return value.every((item) => this._accept(item))
      } else {
        return false;
      }
    } else {
      return this._accept(value);
    }
  }

  // check atomic value
  _accept(value) {
    if (this.range != null) {
      // check that value is in range
      const values = ((this.range[0] != null ? this.range[0].value : undefined) != null) ? (this.range.map((element) => element.value)) : this.range;
      return values.includes(value);
    } else {
      return true; // default response
    }
  }

  validate(identifiers) {
    let element;
    if (identifiers == null) { identifiers = []; }
    let result = super.validate();
    if (this.range != null) {
      for (element of this.range) {
        if (element.value != null) {
          if (!this._accept(element.value)) {
            result.push(this._createErrorValidation('range', `Incompatible range value: ${element.value}`));
          }
        } else {
          if (!this._accept(element)) {
            result.push(this._createErrorValidation('range', `Incompatible range value: ${element}`));
          }
        }
      }
    }
    // though this next check looks better placed in Named, exports.reservedWords is not available to core
    if (reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    if (this.defaultValue != null) {
      let value, values;
      if (this.multiValued) {
        // defaultValue should be array
        if (!Array.isArray(this.defaultValue)) {
          result.push(this._createErrorValidation('defaultValue', "Expected array for multiValued dataDefinition"));
        } else {
          for (value of this.defaultValue) {
            if (!this._accept(value)) {
              result.push(this._createErrorValidation('defaultValue', `Incompatible defaultValue: ${value}`));
            }
            if (this.range != null) {
              values = this.range.length>0 ? ( Object.keys(this.range[0]).includes('value') ? this.range.map((item) => item.value) : this.range ) : null;
              if (!(values.includes(value))) {
                result.push(this._createErrorValidation('defaultValue', `defaultValue not in range: ${value}`));
              }
            }
          }
        }
      } else {
        if (!this.accept(this.defaultValue)) {
          result.push(this._createErrorValidation('defaultValue', `Incompatible defaultValue: ${this.defaultValue}`));
        }
        if (this.range != null) {
          values = this.range.length>0 ? ( Object.keys(this.range[0]).includes('value') ? this.range.map((item) => item.value) : this.range ) : null;
          if (!(values.includes(this.defaultValue))) {
            result.push(this._createErrorValidation('defaultValue', `defaultValue not in range: ${this.defaultValue}`));
          }
        }
      }
    }
    if (this.valueCondition === '') {
      result.push(this._createWarningValidation('valueCondition', "empty expression"));
    }
    if (this.valueCondition != null) { result = result.concat(validateExpression(this.valueCondition,this.path(),'valueCondition',identifiers)); }
    if (this.defaultCondition === '') {
      result.push(this._createWarningValidation('defaultCondition', "empty expression"));
    }
    if (this.defaultCondition != null) { result = result.concat(validateExpression(this.defaultCondition,this.path(),'defaultCondition',identifiers)); }
    if (this.warnings != null) {
      for (let idx = 0; idx < this.warnings.length; idx++) {
        var warning = this.warnings[idx];
        result = result.concat(warning.validate(identifiers));
      }
    }
    return result;
  }

  isDerived() {
    return (this.valueCondition != null);
  }

  allDataDefinitions() { return this._parent.allDataDefinitions(); }

  toJSON() {
    const result = super.toJSON();
    // remove default values set in the constructor
    if (result.multiValued === false) {
      delete result.multiValued;
    }
    if (result.warnings.length === 0) {
      delete result.warnings;
    }
    return result;
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new DataDefinitionState({path: statePath, _enactment: enactment});
    }
    if (this.warnings != null) { return this.warnings.map((warning) => warning._buildState(enactment, statePath)); }
  }
}

// dataDefinitions with a valueCondition attribute need a state object
export class DataDefinitionState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({value: this.value} = (options || {}));
  }

  getComponent(shallow) {
    if (shallow == null) { shallow = false; }
    const design = this._enactment.protocol.getComponent(this.path);
    const result = super.getComponent();
    if (!shallow) {
      result.warnings = [];
      for (let idx = 0; idx < design.warnings.length; idx++) {
        var stateObj = this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + idx];
        if (stateObj != null) {
          result.warnings.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  // returns true if values are different
  _differentValues(left, right) {
    if (Array.isArray(left) && Array.isArray(right)) {
      return left.every((val, idx) => this._differentValues(val, right[idx]));
    } else {
      // note that undefined !== 4 evaluates to true (!)
      // but typeof undefined == 'undefined'
      // so we can test for differences of typeof
      return (typeof left !== typeof right) || left !== right;
    }
  }

  _updateState(evaluator) {
    const result = super._updateState();
    const designPath = this._enactment._state[this.path].design;
    const {
      valueCondition
    } = this._enactment.protocol.getComponent(designPath);
    if (valueCondition != null) {
      const value = evaluator.evaluate(valueCondition, designPath);
      if (this._differentValues(value, this.value)) {
        result.push({
          path: this.path,
          data: true,
          attribute: 'value',
          value
        });
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "DataDefinitionState";
    return result;
  }
}

// A Date accepts moment objects that wrap and extend the javascript date
// literal, and provide useful methods such as format and diff
export class Date extends DataDefinition {
  constructor(options) {
    super(options);
  }

  // TODO: handle array
  static inflate(val) {
    let result = val;
    if (val != null) {
      if ((val._isAMomentObject == null) || !val._isAMomentObject) {
        if (val instanceof Date) {
          result = moment(val);
        } else {
          result = moment(val,'YYYY-MM-DDTHH:mm:ss.SSSZ');
        }
      }
      if (!result.isValid()) {
        throw new Error(`inflating date string ${val} yields invalid momentjs object`);
      }
    }
    return result;
  }

  // TODO use locale or mask attribute to define format
  static render(val) { return val.format("DD/MM/YYYY"); }

  _accept(value) {
    return (value._isAMomentObject != null) && value._isAMomentObject;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Date";
    return result;
  }
}

export class Integer extends DataDefinition {
  constructor(options) {
    super(options);
  }

  static inflate(val) {
    if (typeof val === 'string') {
      return JSON.parse(val);
    } else {
      return val;
    }
  }

  _accept(value) {
    return (typeof value === 'number') && super._accept(value);
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new NumberDataDefinitionState({path: statePath, _enactment: enactment});
    }
    if (this.warnings != null) { return this.warnings.map((warning) => warning._buildState(enactment, statePath)); }
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Integer";
    return result;
  }
}

export class NumberDataDefinitionState extends DataDefinitionState {

  // in javascript NaN!=NaN so a NaN value would recurse infinitely without this extra guard
  _differentValues(left, right) {
    if (Array.isArray(left) && Array.isArray(right)) {
      return left.every((val, idx) => this._differentValues(val, right[idx]));
    } else {
      return (typeof left !== typeof right) || (!(isNaN(left)) && (left !== right));
    }
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "NumberDataDefinitionState";
    return result;
  }
}

export class Float extends DataDefinition {
  constructor(options) {
    super(options);
  }

  static inflate(val) {
    if (typeof val === 'string') {
      return JSON.parse(val);
    } else {
      return val;
    }
  }

  _accept(value) {
    return (typeof value === 'number') && super._accept(value);
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new NumberDataDefinitionState({path: statePath, _enactment: enactment});
    }
    if (this.warnings != null) { return this.warnings.map((warning) => warning._buildState(enactment, statePath)); }
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Float";
    return result;
  }
}

export class Text extends DataDefinition {
  constructor(options) {
    super(options);
  }

  _accept(value) {
    return (typeof value === 'string') && super._accept(value);
  }

  static inflate(value) {
    // strings are not JSON.parsed unless there's a strong indication that the need it
    // see numeric_text example
    if (((value != null ? value.startsWith : undefined) != null) && value.startsWith("[")) {
      return JSON.parse(value);
    } else {
      return value;
    }
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Text";
    return result;
  }
}

export class Boolean extends DataDefinition {
  constructor(options) {
    super(options);
  }

  _accept(value) {
    return (value === true) || (value === false);
  }

  static inflate(value) {
    if (typeof value === 'string') {
      return JSON.parse(value);
    } else {
      return value;
    }
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Boolean";
    return result;
  }
}

export const dataDefinitionClassMap = new Map([
  ['Integer', Integer],
  ['Float', Float],
  ['Text', Text],
  ['Date', Date],
  ['Boolean', Boolean],
])
