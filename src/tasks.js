// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
A Protocol has two life-stages: json <-> component-tree

Creating a protocol object, i.e. a component-tree with a task at it's root
takes the declarative attributes (which are serialised in the toJson call) and
adds some design-time validation and runtime state management behaviours:
* the ability for a component to "look up the tree", using the assigned @_parent attribute and @children() function
* the ability to locate a component in the guideline via the path() function
* validation which ensures that paths arent ambiguous and expressions refer to known things
* the ability to create a stateful instance of the component that can be manipulated in an enactment
*/

import * as Core from './core.js';
import * as Evaluator from './evaluator.js';
import moment from 'moment';
export * from './evaluator.js';

// Deserialise a PROforma JSON string
export const inflate = function(json) {
  const obj = JSON.parse(json);
  return this.buildTask(obj);
};

// build a proformajs task object from a serialised version
// converts a tree of pojos to a tree of components, with _parent references and validate methods
// TODO (later!): handle different versions of proformajs documents
export const buildTask = function(obj) {
  if (obj.class != null) {
    try {
      // ES6 trick for dynamically instantiating task
      return new(taskClassMap.get(obj.class))(obj);
    } catch(e) {
      throw new Error('cannot create Protocol ' + obj.class, e)
    }
  } else {
    throw new Error('cannot parse PROforma task: missing class attribute');
  }
}

// A vanilla Task that may be used as a placeholder or extended to be used concretely
export class Task extends Core.Named {
  constructor(options) {
    super(options);
    ({preCondition: this.preCondition, waitCondition: this.waitCondition, abortCondition: this.abortCondition, terminateCondition: this.terminateCondition, eventTrigger: this.eventTrigger, cycleUntil: this.cycleUntil} = (options || {}));
    this.optional = options.optional != null ? options.optional : false;
    this.autonomous = options.autonomous != null ? options.autonomous : false;
    this.cyclic = options.cyclic != null ? options.cyclic : false;
    this.dataDefinitions = [];
    if ((options.dataDefinitions != null) && Array.isArray(options.dataDefinitions)) {
      for (var meta of options.dataDefinitions) {
        if (meta.class != null) {
          var dataDefinition = new (Evaluator[meta.class])(meta);
          this.addDataDefinition(dataDefinition);
        } else {
          throw new Error('cannot construct dataDefinition: missing class attribute');
        }
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['preCondition','waitCondition','optional','dataDefinitions','autonomous', 'abortCondition', 'terminateCondition', 'eventTrigger', 'cyclic', 'cycleUntil']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['dataDefinitions']); }

  addDataDefinition(dataDefinition) {
    //TODO: throw error if dataDefinition is of wrong type?
    dataDefinition._parent = this;
    this.dataDefinitions.push(dataDefinition);
    return this;
  }

  // returns data definitions defined for this task and all it's ancestor tasks
  allDataDefinitions() {
    if (this._parent != null) { return this._parent.allDataDefinitions().concat(this.dataDefinitions); } else { return this.dataDefinitions; }
  }

  subPaths() { return super.subPaths(...arguments).concat((this.dataDefinitions.map((dataDef) => this.name + Evaluator.PATH_SEPARATOR + dataDef.name))); }

  // returns all sub-tasks
  allTasks() { return [this]; }

  validate() {
    let result = super.validate();
    const names=[];
    const identifiers = this._identifiers();
    for (var definition of this.dataDefinitions) {
      if (names.includes(definition.name)) {
        result.push(this._createErrorValidation('name', "duplicate dataDefinition names"));
      }
      names.push(definition.name);
      if (definition._parent !== this) {
        result.push(this._createErrorValidation('_parent', `mis-attached dataDefinition: ${definition.name}`));
      }
      result = result.concat(definition.validate(identifiers));
    }
    if (this.preCondition === '') {
      result.push(this._createWarningValidation('preCondition', "empty expression"));
    }
    if (this.waitCondition === '') {
      result.push(this._createWarningValidation('waitCondition', "empty expression"));
    }
    if (this.preCondition != null) { result = result.concat(Evaluator.validateExpression(this.preCondition, this.path(), 'preCondition', identifiers)); }
    if (this.waitCondition != null) { result = result.concat(Evaluator.validateExpression(this.waitCondition, this.path(), 'waitCondition', identifiers)); }
    if (this.abortCondition != null) { result = result.concat(Evaluator.validateExpression(this.abortCondition, this.path(), 'abortCondition', identifiers)); }
    if (this.terminateCondition != null) { result = result.concat(Evaluator.validateExpression(this.terminateCondition, this.path(), 'terminateCondition', identifiers)); }
    if (this.cycleUntil != null) { result = result.concat(Evaluator.validateExpression(this.cycleUntil, this.path(), 'cycleUntil', identifiers)); }
    if (Evaluator.reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    // if @eventTrigger? and @preCondition?
    //   result.push @_createWarningValidation 'preCondition', "preConditions are ignored if a task has an eventTrigger"
    if ((this.eventTrigger != null) && (this.waitCondition != null)) {
      result.push(this._createWarningValidation('waitCondition', "waitConditions are ignored if a task has an eventTrigger"));
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Task";
    // remove default values set in the constructor
    if (result.optional === false) {
      delete result.optional;
    }
    if (result.cyclic === false) {
      delete result.cyclic;
    }
    if (result.autonomous === false) {
      delete result.autonomous;
    }
    if (result.dataDefinitions.length === 0) {
      delete result.dataDefinitions;
    }
    return result;
  }

  _child(name) { return (this.dataDefinitions.filter((child) => child.name === name))[0]; }

  _identifiers() {
    const result = [];
    for (var dataDef of this.allDataDefinitions()) {
      result.push(dataDef.name);
      if ((dataDef.range != null) && (dataDef.constructor.name === Evaluator.dataDefinitionClassMap.get("Text").name)) {
        for (var rangeItem of dataDef.range) {
          if (rangeItem.value != null) {
            result.push(rangeItem.value);
          } else {
            result.push(rangeItem);
          }
        }
      }
    }
    // At first only sibling task names and their candidate names were captured here,
    // but some guidelines reference candidates in sibling children tasks so
    // it now captures all task and candidate names.   This may be overkill.
    for (var task of this._getRoot().allTasks()) {
      result.push(task.name);
      if (task.candidates != null) {
        for (var candidate of task.candidates) { result.push(candidate.name); }
      }
    }
    return result.concat(this._getRoot().subPaths());
  }

  _buildState(enactment, index, parentStatePath=null) {
    if (index == null) { index = -1; }
    const statePath = ((parentStatePath != null) ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index>-1 ? "[" + index + "]" : "");
    if (index>-1) {
      enactment._state[statePath] = new TaskState({path: statePath, _enactment: enactment, index});
    } else {
      enactment._state[statePath] = new TaskState({path: statePath, _enactment: enactment});
    }
    for (var dataDef of this.dataDefinitions) { dataDef._buildState(enactment, statePath); }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}

export class TaskState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({state: this.state, completeable: this.completeable, cancellable: this.cancellable, index: this.index, cycleable: this.cycleable} = (options || {}));
  }

  // returns true if task is ready to complete
  // expected to be overidden by sub-classes
  _isCompleteable() {
    return this.state === 'in_progress';
  }

  // returns true if task can be cancellad
  _isCancellable() {
    return this.state === 'in_progress';
  }

  // true if dynamic attributes can be updated
  _isUpdateable() {
    return this.state === 'in_progress';
  }

  // complete task
  complete() {
    if (this._isCompleteable()) {
      const comp = this._enactment.protocol.getComponent(this.design);
      if (comp.cyclic && this.cycleable) {
        comp._buildState(this._enactment, this.index+1, this._parentStatePath());
      }
      return this._enactment._cycle({
        action: {
          complete: this.path
        },
        seed: [ { path: this.path, attribute: 'state', value: 'completed' } ]});
    } else {
      throw new Error('Cannot complete a task which is not completeable');
    }
  }

  // cancel task
  cancel() {
    if (this._isCancellable()) {
      const comp = this._enactment.protocol.getComponent(this.design);
      if (comp.cyclic && this.cycleable) {
        comp._buildState(this._enactment, this.index+1, this._parentStatePath());
      }
      return this._enactment._cycle({
        action: {
          cancel: this.path
        },
        seed: [ { path: this.path, attribute: 'state', value: 'discarded' } ]});
    } else {
      throw new Error('Cannot cancel a task which is not cancellable');
    }
  }

  // retrive current value of requested dataDefinition
  get(key) {
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === key);
    if (target != null) {
      const targettask = target._parent.path();
      const values = this._enactment._data[targettask] != null ? this._enactment._data[targettask][key] : undefined;
      if (values != null) { return values[values.length-1].value; } else { return undefined; }
    } else {
      return undefined; // or raise unknown datadefinition error?
    }
  }

  // provide runtime data, in the form of an object so that
  // multiple data items can be supplied at once.
  // Invalid data is filtered out and signalled by an Error
  // *after* the valid data has been set
  set(data) {
    let dataDefinition;
    let added = false;
    const timestamp = moment();
    const errors = [];
    for (dataDefinition of Object.keys(data || {})) {
      var value = data[dataDefinition];
      var target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === dataDefinition);
      if (target != null) {
        if (target.isDerived()) {
          errors.push({
            dataDefinition,
            message: 'cannot set a derived data definition value'
          });
        } else {
          var valid = true;
          if (!target.accept(value)) {
            valid = false;
            errors.push({
              dataDefinition,
              message: `incompatible data provided: ${value}`
            });
          }
          // todo: who, how
          var taskpath = target._parent.path();
          if (valid) {
            if ((this._enactment._data[taskpath] == null)) {
              this._enactment._data[taskpath]={};
            }
            if ((this._enactment._data[taskpath][dataDefinition] == null)) {
              this._enactment._data[taskpath][dataDefinition]=[];
            }
            this._enactment._data[taskpath][dataDefinition].push({
              value,
              when: timestamp
            });
            added=true;
          }
        }
      } else {
        errors.push({
          dataDefinition,
          message: "unknown dataDefinition"
        });
      }
    }
    if (added) { this._enactment._cycle({action: {set: this.path, data}}); }
    if (errors.length>0) {
      let msg="";
      for (let idx = 0; idx < errors.length; idx++) {
        var error = errors[idx];
        msg = msg + `${error.dataDefinition} - ${error.message}`;
        if (idx < (errors.length-1)) {
          msg = msg + "\n";
        }
      }
      throw new Error(msg);
    }
  }

  unset(type) {
    const timestamp = moment();
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === type);
    if (target != null) {
      const taskpath = target._parent.path();
      if ((this._enactment._data[taskpath] == null)) {
        this._enactment._data[taskpath]={};
      }
      if ((this._enactment._data[taskpath][type] == null)) {
        this._enactment._data[taskpath][type]=[];
      }
      this._enactment._data[taskpath][type].push({
        value: undefined,
        when: timestamp
      });
      return this._enactment._cycle({action: {unset: this.path, type}});
    } else {
      throw new Error(`unable to unset ${type} - not a valid datadefinition in ${this.path}`);
    }
  }

  // provide a mechanism for correcting mistakes / typos from set
  // instead of adding a new timestamp of data, you can go back and tweak
  // the tweak is of course reflected in the audit
  edit(type, index, value) {
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === type);
    if (target != null) {
      const taskpath = target._parent.path();
      if (Array.isArray(this._enactment._data[taskpath][type]) && this._enactment._data[taskpath][type].length>index) {
        this._enactment._data[taskpath][type][index].value = value;
      }
      return this._enactment._cycle({action: {edit: this.path, type, index, value}});
    } else {
      throw new Error(`unable to edit ${type} - not a valid datadefinition in ${this.path}`);
    }
  }

  trigger() {
    // offers a trigger if
    // * its design has a trigger,
    // * its state is currently dormant
    // * and it's parent (if it has one) is in_progress
    let result = null;
    const {
      eventTrigger
    } = this._enactment.protocol.getComponent(this.design);
    if (eventTrigger != null) {
      const parent = this._enactment._state[this._parentStatePath()];
      if ((this.state === 'dormant') && ((parent != null) ? parent.state === 'in_progress' : true)) {
        result = eventTrigger;
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "TaskState";
    return result;
  }

  // return the value and all meta of a visible data definition
  _getData(type) {
    const def = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(dataDef => dataDef.name === type);
    let result = null;
    if (def != null) {
      result = def.flatten();
      const dataDef = this._enactment._data[def._parent.path()];
      if (dataDef) {
        const data = dataDef[type];
        if ((data != null) && (data.length>0)) {
          result.value = data[data.length-1].value;
        } else {
          result.value = undefined;
        }
      }
    }
    return result;
  }

  // The state engine.  Returns an array of state updates
  _updateState(evaluator) {
    let comp, cycleable;
    const result=super._updateState();
    if ((this.state == null)) {
      result.push({
        path: this.path,
        attribute: 'state',
        value: 'dormant'
      });
    } else {
      let _parentstate, cancellable, completeable;
      comp = this._enactment.protocol.getComponent(this.design);
      const _parentpath = this._parentStatePath();
      if (_parentpath != null) {
        _parentstate = this._enactment._state[_parentpath].state;
      }
      if ((this.state === 'dormant') && (comp.eventTrigger == null) && ((_parentpath == null) || (_parentstate === 'in_progress')) && ((comp.waitCondition == null) || (comp.waitCondition === '') || evaluator.evaluate(comp.waitCondition, this.design))) {
        const preCondition = (comp.preCondition == null) || (comp.preCondition === '') || evaluator.evaluate(comp.preCondition, this.design);
        result.push({
          path: this.path,
          attribute: 'state',
          value: preCondition ? 'in_progress' : 'discarded'
        });
      } else if ((this.state === 'dormant') && comp.optional && ['completed','discarded'].includes(_parentstate)) {
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'discarded'
        });
      } else if (this.state === 'in_progress') {
        completeable = this._isCompleteable();
        if (completeable !== this.completeable) {
          result.push({
            path: this.path,
            attribute: 'completeable',
            value: completeable
          });
        }
        cancellable = this._isCancellable();
        if (cancellable !== this.cancellable) {
          result.push({
            path: this.path,
            attribute: 'cancellable',
            value: cancellable
          });
        }
        cycleable = (comp.cycleUntil == null) || !evaluator.evaluate(comp.cycleUntil, this.design);
        if (cycleable !== this.cycleable) {
          result.push({
            path: this.path,
            attribute: 'cycleable',
            value: cycleable
          });
        }
        if (comp.abortCondition != null && evaluator.evaluate(comp.abortCondition, this.design)) {
          if ((this.index != null) && this.cycleable) {
            comp._buildState(this._enactment, this.index+1, _parentpath);
          }
          result.push({
            path: this.path,
            attribute: 'state',
            value: 'discarded',
            comment: 'abortCondition true'
          });
        } else if (comp.terminateCondition != null && evaluator.evaluate(comp.terminateCondition, this.design)) {
          if ((this.index != null) && this.cycleable) {
            comp._buildState(this._enactment, this.index+1, _parentpath);
          }
          result.push({
            path: this.path,
            attribute: 'state',
            value: 'completed',
            comment: 'terminateCondition true'
          });
        }
        if (comp.optional && ['completed','discarded'].includes(_parentstate)) {
          result.push({
            path: this.path,
            attribute: 'state',
            value: _parentstate
          });
        }
      } else {
        // in case there are any loose ends for completed or discarded tasks
        if (this.completeable) {
          result.push({
            path: this.path,
            attribute: 'completeable',
            value: false
          });
        }
        if (this.cancellable) {
          result.push({
            path: this.path,
            attribute: 'cancellable',
            value: false
          });
        }
        if (this.cycleable) {
          result.push({
            path: this.path,
            attribute: 'cycleable',
            value: false
          });
        }
      }
    }
    if ((result != null ? result.length : undefined) === 0) { // this check ensures that enquiry state has settled, without it requested state can be set at the same time as completed state
      if (comp.autonomous && this._isCompleteable()) {
        if ((this.index != null) && this.cycleable) {
          comp._buildState(this._enactment, this.index+1, this._parentStatePath());
        }
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'completed'
        });
      }
    }
    return result;
  }
}

// An Action is a placeholder for a different kind of task
// TODO: add an action method
export class Action extends Task {
  toJSON() {
    const result = super.toJSON();
    result.class = "Action";
    return result;
  }
}

export class ActionState extends TaskState {
  toJSON() {
    const result = super.toJSON();
    result.class = "ActionState";
    return result;
  }
}

// An Enquiry is a task that actively attempts to gather data
// sources = Source[]
// useDefaults = runtime flag
export class Enquiry extends Task {
  constructor(options) {
    super(options);
    ({useDefaults: this.useDefaults} = (options || {}));
    this.sources=[];
    if ((options.sources != null) && Array.isArray(options.sources)) {
      for (var meta of options.sources) {
        var source = new Source(meta);
        this.addSource(source);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['sources','useDefaults']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['sources']); }

  addSource(source) {
    // if (!(source instanceof exports.Source)) {
    //   throw new Error(`${source} must be of type Source`);
    // }
    source._parent = this;
    this.sources.push(source);
    return this;
  }

  subPaths() {
    const result = super.subPaths();
    for (var source of this.sources) {
      var path = source.path();
      if (!result.includes(path)) {
        result.push(path);
      }
    }
    return result;
  }

  // note that checking a source relates to an existing data definition is left to the source component
  validate() {
    let result = super.validate();
    const identifiers = this._identifiers();
    const names=[];
    if (this.sources.length === 0) {
      result.push(this._createWarningValidation('sources', "no sources defined"));
    } else {
      for (var source of this.sources) {
        if (names.includes(source.type)) {
          result.push(this._createErrorValidation('sources', `duplicate source type: ${source.type}`));
        }
        var def = this.allDataDefinitions().find(dataDef => dataDef.name === source.type);
        if (((def != null ? def.valueCondition : undefined) != null) && (def.valueCondition.length>0)) {
          result.push(this._createErrorValidation('sources', `cannot source a dynamic data definition: ${source.type}`));
        }
        names.push(source.type);
        if (source._parent !== this) {
          result.push(this._createErrorValidation('sources', `mis-attached source: ${source.type}`));
        }
        result = result.concat(source.validate(identifiers));
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class="Enquiry";
    // remove default values set in the constructor
    if (result.sources.length === 0) {
      delete result.sources;
    }
    return result;
  }

  // TODO: handle case when datadefinition and source are defined in the same task
  _child(name) { return (this.sources.filter((child) => child.type === name))[0] || super._child(name); }

  _buildState(enactment, index, parentStatePath=null) {
    if (index == null) { index = -1; }
    const statePath = ((parentStatePath != null) ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index>-1 ? "[" + index + "]" : "");
    if (index>-1) {
      enactment._state[statePath] = new EnquiryState({path: statePath, _enactment: enactment, index});
    } else {
      enactment._state[statePath] = new EnquiryState({path: statePath, _enactment: enactment});
    }
    for (var source of this.sources) { source._buildState(enactment, statePath); }
    for (var dataDef of this.dataDefinitions) { dataDef._buildState(enactment, statePath); }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}

// TODO: Find a way to include a dynamic data item as a read-only source
export class EnquiryState extends TaskState {
  constructor(options) {
    if (options == null) { options = {}; }
    super(options);
  }

  // returns array of requested source types
  requested() {
    const result = [];
    for (var source of this._enactment.protocol.getComponent(this.design).sources) {
      if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + source.type].requested) {
        result.push(this._enactment.protocol.getComponent(source.path()).type);
      }
    }
    return result;
  }

  // carefully constructed to combine source and dataDefinition attributes in sources
  getComponent(shallow) {
    if (shallow == null) { shallow = false; }
    const design = this._enactment.protocol.getComponent(this.design);
    const result = super.getComponent();
    if (!shallow) {
      result.sources = [];
      for (var source of design.sources) {
        var component = this._getData(source.type);
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(this.design) + Core.Named.PATH_SEPARATOR + source.type];
        if (stateObj != null) {
          var object = stateObj.getComponent();
          for (const key in object) {
            const value = object[key];
            if (!['class'].includes(key)) {
              component[key]=value;
            }
          }
          result.sources.push(component);
        }
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "EnquiryState";
    return result;
  }

  _isCompleteable() {
    const requested = this._enactment.protocol.getComponent(this.design).sources.filter((source) => this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + source.type].requested);
    return (this.state === 'in_progress') && (requested.length === 0);
  }

  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    const comp = this._enactment.protocol.getComponent(this.design);
    // TODO: Why are default values not added in the burst / update cycle?
    if (comp.useDefaults || (this._enactment.options != null && this._enactment.options.Enquiry  != null && Object.keys(this._enactment.options.Enquiry).includes("useDefaults") && this._enactment.options.Enquiry.useDefaults)) {
      // check for a change to in_progress and set default values
      if ((result != null ? result.length : undefined)>0) {
        let set_defaults=false;
        for (var update of result) {
          if ((update.attribute === 'state') && (update.value === 'in_progress')) {
            set_defaults=true;
          }
        }
        if (set_defaults) {
          // TODO: get timestamp from this set of cycles...
          const timestamp = moment();
          for (var source of comp.sources) {
            for (var dataDef of comp.allDataDefinitions()) {
              if (dataDef.name === source.type) {
                if ((dataDef.defaultValue != null) || (dataDef.defaultCondition != null)) {
                  var taskpath = dataDef._parent.path();
                  // only supply default if there is no existing value
                  const taskpathData = this._enactment.getData()[taskpath];
                  if (!taskpathData || taskpathData[dataDef.name] == null) {
                    if ((this._enactment._data[taskpath] == null)) {
                      this._enactment._data[taskpath]={};
                    }
                    if ((this._enactment._data[taskpath][dataDef.name] == null)) {
                      this._enactment._data[taskpath][dataDef.name]=[];
                    }
                    if (dataDef.defaultValue != null) {
                      this._enactment._data[taskpath][dataDef.name].push({
                        value: dataDef.defaultValue,
                        when: timestamp
                      });
                    } else if (dataDef.defaultCondition != null) {
                      const val = evaluator.evaluate(dataDef.defaultCondition, this.design);
                      if (dataDef.accept(val)) {
                        this._enactment._data[taskpath][dataDef.name].push({
                          value: val,
                          when: timestamp
                        });
                      } else {
                        console.log(timestamp, dataDef.defaultCondition, 'rejected');
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return result;
  }
}

// A Source is referenced by an Enquiry and itself references a DataDefinition.
// It does not extend Named as it has no need for name, caption and description
// attributes which are derived instead from the named dataDefinition but this
// means that the path function must be redefined.
export class Source extends Core.Component {
  constructor(options) {
    super(options);
    ({type: this.type, requestCondition: this.requestCondition} = (options || {}));
    if ((this.type == null)) {
      throw new Error('new Protocol.Source(obj): Unable to create enquiry source without a type attribute');
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['type','requestCondition']); }

  path() { if (this._parent != null) { return `${this._parent.path()}:${this.type}`; } else { return this.type; } }

  allDataDefinitions() { return this._parent.allDataDefinitions(); }

  validate(identifiers) {
    let result = super.validate();
    // identifiers can be (optionally) passed as an optimisation
    if ((identifiers == null)) {
      if ((this._parent != null) && (this._parent.allDataDefinitions().length>0)) {
        identifiers = this._parent.allDataDefinitions().map(dataDefinition => dataDefinition.name);
      } else {
        identifiers = [];
      }
    }
    if (!(identifiers.includes(this.type))) {
      result.push(this._createErrorValidation('type', `unknown data definition - ${this.type}`));
    }
    if ((this.requestCondition != null) && (this.requestCondition.length === 0)) {
      result.push(this._createWarningValidation('requestCondition', 'empty expression'));
    }
    if (this.requestCondition != null) { result = result.concat(Evaluator.validateExpression(this.requestCondition, this.path(), 'requestCondition', identifiers)); }
    return result;
  }

  // a source's subPaths are routed through to it's data definitions...
  subPaths() {
    const dd = this.allDataDefinitions().find(dd => dd.name === this.type);
    return (dd != null ? dd.subPaths() : undefined) || [this.type];
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Evaluator.PATH_SEPARATOR + this.type;
    return enactment._state[statePath] = new SourceState({path: statePath, _enactment: enactment});
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Source";
    return result;
  }
}

export class SourceState extends Core.ComponentState {
  constructor(options) {
    super(options);
    ({requested: this.requested} = (options || {}));
  }

  _updateState(evaluator) {
    const result=[];
    const _parentstate = this._enactment._state[this._parentStatePath()].state;
    if (_parentstate != null) {
      let requested;
      if (_parentstate === 'in_progress') {
        if (this._enactment.protocol.getComponent(this.design).requestCondition != null) {
          requested = evaluator.evaluate(this._enactment.protocol.getComponent(this.design).requestCondition, this.design);
          if (requested !== this.requested) {
            result.push({
              path: this.path,
              attribute: 'requested',
              value: requested
            });
          }
        }
      } else if (this.requested) {
        result.push({
          path: this.path,
          attribute: 'requested',
          value: false
        });
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "SourceState";
    return result;
  }
}

// A Plan is a task that contains sub-tasks
export class Plan extends Task {
  constructor(options) {
    super(options);
    this.tasks = [];
    if ((options.tasks != null) && Array.isArray(options.tasks)) {
      for (var meta of options.tasks) {
        if (meta.class != null) {
          // ES6 trick for dynamically instantiating task
          const task = new(taskClassMap.get(meta.class))(meta);
          this.addTask(task);
        } else {
          throw new Error('cannot parse PROforma task: missing class attribute');
        }
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['tasks']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['tasks']); }

  addTask(task) {
    // if (!(task instanceof exports.Task)) {
    //   throw new Error("Plan.addTask(task): task must be of type Task (or one of it's sub-types)");
    // }
    task._parent = this;
    this.tasks.push(task);
    return this;
  }

  subPaths() {
    let result = [];
    for (var task of this.tasks) {
      result = result.concat(task.subPaths().map((subPath) => this.name + Evaluator.PATH_SEPARATOR + subPath));
    }
    return super.subPaths(...arguments).concat(result);
  }

  // returns all sub-tasks
  allTasks() {
    let result = [this];
    for (var task of this.tasks) {
      result = result.concat(task.allTasks());
    }
    return result;
  }

  validate() {
    let result = super.validate();
    const names = [];
    const datadefs = (this.dataDefinitions.map((dataDef) => dataDef.name));
    if (this.tasks.length === 0) {
      result.push(this._createWarningValidation('tasks', "no sub-tasks defined"));
    }
    for (var task of this.tasks) {
      if (names.includes(task.name)) {
        result.push(this._createErrorValidation('tasks', `duplicate sub-task names: ${task.name}`));
      }
      if (datadefs.includes(task.name)) {
        result.push(this._createErrorValidation('tasks', `task name clashes with dataDefinition: ${task.name}`));
      }
      names.push(task.name);
      if (task._parent !== this) {
        result.push(this._createErrorValidation('tasks', `mis-attached sub-task: ${task.name}`));
      }
      result = result.concat(task.validate());
    }
    if ((this.tasks.filter(task => task.optional===false).length===0) && this.autonomous && this.cyclic) {
      result.push(this._createErrorValidation('cyclic', "an autonomous cyclic plan with no non-optional tasks is pathological"));
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Plan";
    // remove default values set in the constructor
    if (result.tasks.length === 0) {
      delete result.tasks;
    }
    return result;
  }

  _child(name) { return (this.tasks.filter((task) => task.name === name))[0] || super._child(name); }

  _buildState(enactment, index, parentStatePath=null) {
    if (index == null) { index = -1; }
    const statePath = ((parentStatePath != null) ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index>-1 ? "[" + index + "]" : "");
    if (index>-1) {
      enactment._state[statePath] = new PlanState({path: statePath, _enactment: enactment, index});
    } else {
      enactment._state[statePath] = new PlanState({path: statePath, _enactment: enactment});
    }
    for (var task of this.tasks) {
      if (task.cyclic) {
        task._buildState(enactment, 0, statePath);
      } else {
        task._buildState(enactment, -1, statePath);
      }
    }
    for (var dataDef of this.dataDefinitions) { dataDef._buildState(enactment, statePath); }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}

export class PlanState extends TaskState {
  constructor(options) {
    super(options);
  }

  getComponent(shallow) {
    if (shallow == null) { shallow = false; }
    const design = this._enactment.protocol.getComponent(this.design);
    const result = super.getComponent();
    if (!shallow) {
      result.tasks = [];
      for (var task of design.tasks) {
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(task.path())];
        if (stateObj != null) {
          result.tasks.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "PlanState";
    return result;
  }

  // a plan is completeable if all it's non-optional sub-tasks are finished
  _isCompleteable() {
    if (this.state === 'in_progress') {
      for (var task of this._enactment.protocol.getComponent(this.design).tasks) {
        var taskStatePath = this._enactment.runtimeFromDesignPath(task.path());
        var task_state = this._enactment._state[taskStatePath].state;
        if (task_state != null) {
          if (!task.optional && ['dormant','in_progress'].includes(task_state)) {
            return false;
          }
          if (task.optional && (task_state === 'in_progress')) {
            return false; // dont auto-complete optional tasks that are in_progress
          }
        } else {
          return false; // task_state can be undefined when a task is first instantiated
        }
      }
      return true;
    } else {
      return false;
    }
  }

  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    if (this.state === 'discarded') {
      const comp = this._enactment.protocol.getComponent(this.design);
      // if a task has been cancelled / aborted then it needs to handle unfinished sub-tasks
      for (let task of comp.tasks) {
        let taskStatePath = this.path+Evaluator.PATH_SEPARATOR+task.name;
        if (task.cyclic) {
          // find the highest cycle
          // note that we dont use runtimeFromDesignPath because a new plan and sub tasks will already have been created
          var keys = Object.keys(this._enactment._state).filter(key => key.startsWith(taskStatePath) && (key.slice(taskStatePath.length).indexOf(Evaluator.PATH_SEPARATOR)===-1));
          taskStatePath = keys.pop();
        }
        let task_state = this._enactment._state[taskStatePath].state;
        if (['dormant', 'in_progress'].includes(task_state)) {
          result.push({
            path: taskStatePath,
            attribute: 'state',
            value: 'discarded'
          });
        }
      }
    }
    return result;
  }
}

// A Decision is a task that represents decisions and keeps a track of their state
// TODO should candidate recommended state change when not in_progress
export class Decision extends Task {
  constructor(options) {
    super(options);
    ({autoConfirmMostSupported: this.autoConfirmMostSupported} = (options || {}));
    this.candidates=[];
    if ((options.candidates != null) && Array.isArray(options.candidates)) {
      for (var meta of options.candidates) {
        var candidate = new Candidate(meta);
        this.addCandidate(candidate);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['candidates','autoConfirmMostSupported']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['candidates']); }

  addCandidate(candidate) {
    // if (!(candidate instanceof exports.Candidate)) {
    //   throw new Error("Decision.addCandidate(candidate): candidate must be of type Candidate");
    // }
    candidate._parent = this;
    this.candidates.push(candidate);
    return this;
  }

  subPaths() {
    let result = [];
    for (var candidate of this.candidates) {
      result = result.concat(candidate.subPaths().map((subPath) => this.name + Evaluator.PATH_SEPARATOR + subPath));
    }
    return super.subPaths(...arguments).concat(result);
  }

  validate() {
    let result = super.validate();
    const names = [];
    const identifiers = this._identifiers();
    const datadefs = (this.dataDefinitions.map((dataDef) => dataDef.name));
    if (this.candidates.length === 0) {
      result.push(this._createWarningValidation('candidates', "no candidates defined"));
    }
    for (var candidate of this.candidates) {
      if (names.includes(candidate.name)) {
        result.push(this._createErrorValidation('candidates', `non-uniquely named sub-candidates: ${candidate.name}`));
      }
      if (datadefs.includes(candidate.name)) {
        result.push(this._createErrorValidation('candidates', `dataDefinition/candidate name clash: ${candidate.name}`));
      }
      names.push(candidate.name);
      if (candidate._parent !== this) {
        result.push(this._createErrorValidation('candidates', `mis-attached sub-candidate: ${candidate.name}`));
      }
      result = result.concat(candidate.validate(identifiers));
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Decision";
    // remove default values set in the constructor
    if (result.candidates.length === 0) {
      delete result.candidates;
    }
    return result;
  }

  _identifiers() { return super._identifiers(...arguments).concat((this.candidates.map((candidate) => candidate.name))); }

  _child(name) { return (this.candidates.filter((candidate) => candidate.name === name))[0] || super._child(name); }

  _buildState(enactment, index, parentStatePath=null) {
    if (index == null) { index = -1; }
    const statePath = ((parentStatePath != null) ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index>-1 ? "[" + index + "]" : "");
    if (index>-1) {
      enactment._state[statePath] = new DecisionState({path: statePath, _enactment: enactment, index});
    } else {
      enactment._state[statePath] = new DecisionState({path: statePath, _enactment: enactment});
    }
    for (var candidate of this.candidates) { candidate._buildState(enactment, statePath); }
    for (var dataDef of this.dataDefinitions) { dataDef._buildState(enactment, statePath); }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}

export class DecisionState extends TaskState {
  constructor(options) {
    super(options);
  }

  // returns array of confirmed candidate names
  confirmed() {
    const result=[];
    for (var candidate of this._enactment.protocol.getComponent(this.design).candidates) {
      if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].confirmed) {
        result.push(candidate.name);
      }
    }
    return result;
  }

  // returns array of recommended candidate names
  recommended() {
    const result = [];
    for (var candidate of this._enactment.protocol.getComponent(this.design).candidates) {
      if ((this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].recommended != null) && this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].recommended) { result.push(candidate.name); }
    }
    return result;
  }

  getComponent(shallow) {
    if (shallow == null) { shallow = false; }
    const result = super.getComponent();
    if (!shallow) {
      result.candidates = [];
      const design = this._enactment.protocol.getComponent(this.design);
      for (var candidate of design.candidates) {
        var stateObj = this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name];
        if (stateObj != null) {
          result.candidates.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "DecisionState";
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const candidate = this._enactment._state[this.path + Evaluator.PATH_SEPARATOR + name];
    if (candidate != null) {
      return candidate;
    } else {
      // look at candidates of sibling decisions
      const paths = this.design.split(Evaluator.PATH_SEPARATOR);
      const taskname = paths.pop();
      if (paths.length>0) {
        const parentdesign = this._enactment.protocol.getComponent(paths.join(Evaluator.PATH_SEPARATOR));
        if (parentdesign.tasks != null) {
          for (var task of parentdesign.tasks) {
            if ((task.candidates != null) && (task.name !== taskname)) {
              for (var cand of task.candidates) {
                if (cand.name === name) {
                  return this._enactment._state[this._enactment.runtimeFromDesignPath(cand.path())];
                }
              }
            }
          }
        }
      }
    }
  }

  _isCompleteable() {
    if (this.state === 'in_progress') {
      return this.confirmed().length>0 || this._enactment.protocol.getComponent(this.design).candidates.length === 0
    } else {
      return false
    }
  }

  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    const comp = this._enactment.protocol.getComponent(this.design);
    if (this.state === 'in_progress') {
      if (comp.autonomous && this._isCompleteable()) {
        if ((this.index != null) && this.cycleable) {
          comp._buildState(this._enactment, this.index+1, this._parentStatePath());
        }
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'completed'
        });
      }
      if (comp.autoConfirmMostSupported || (this._enactment.options != null && this._enactment.options.Decision != null && Object.keys(this._enactment.options.Decision).includes('autoConfirmMostSupported') && this._enactment.options.Decision.autoConfirmMostSupported)) {
        // commit candidate with highest support
        let most_supported = {};
        for (var candidate of comp.candidates) {
          var candidateruntimepath = this._enactment.runtimeFromDesignPath(this.path) + Core.Named.PATH_SEPARATOR + candidate.name;
          var candidate_state = this._enactment._state[candidateruntimepath];
          if ((most_supported.path == null) || ((candidate_state.support != null) && (candidate_state.support>most_supported.support))) {
            if (candidate_state.support != null) {
              most_supported= {
                path: candidateruntimepath,
                support: candidate_state.support,
                confirmed: candidate_state.confirmed
              };
            }
          }
        }
        if ((most_supported.path != null) && !most_supported.confirmed && (most_supported.support>0)) {
          result.push({
            path: most_supported.path,
            attribute: 'confirmed',
            value: true
          });
        }
      }
    }
    return result;
  }
}

// A Candidate is used by a Decision to represent decision choices
export class Candidate extends Core.Named {
  constructor(options) {
    super(options);
    ({recommendCondition: this.recommendCondition, autoConfirmRecommended: this.autoConfirmRecommended} = (options || {}));
    this.arguments=[];
    if ((options.arguments != null) && Array.isArray(options.arguments)) {
      for (var meta of options.arguments) {
        var argument = new Argument(meta);
        this.addArgument(argument);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['recommendCondition','arguments','autoConfirmRecommended']); }

  // see Core.Component.flatten()
  static _ignoreKeys() { return super._ignoreKeys(...arguments).concat(['arguments']); }

  addArgument(argument) {
    // if (!(argument instanceof exports.Argument)) {
    //   throw new Error("Candidate.addArgument(argument): argument must be of type Argument");
    // }
    argument._parent = this;
    argument.idx=this.arguments.length;
    this.arguments.push(argument);
    return this;
  }

  toJSON() {
    const result = super.toJSON();
    result.class = "Candidate";
    // remove default values set in the constructor
    if (result.arguments.length === 0) {
      delete result.arguments;
    }
    return result;
  }

  subPaths() { return super.subPaths(...arguments).concat(this.arguments.map((argument) => this.name + Evaluator.PATH_SEPARATOR + argument.idx)); }

  allDataDefinitions() { return this._parent.allDataDefinitions(); }

  // Note no warning for empty arguments because logic may be all in recommendCondition by design
  validate(identifiers) {
    if (identifiers == null) { identifiers = []; }
    let result = super.validate();
    if (this._parent == null) {
      result.push(this._createErrorValidation('_parent', "a Candidate must be attached to a Decision"));
    }
    for (var argument of this.arguments) {
      if (argument._parent !== this) {
        result.push(this._createErrorValidation('arguments', `mis-attached sub-argument: ${argument.name}`));
      }
      result = result.concat(argument.validate(identifiers));
    }
    if (Evaluator.reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    if ((this.recommendCondition == null) || (this.recommendCondition === '')) {
      result.push(this._createWarningValidation('recommendCondition', "missing or empty expression"));
    }
    if (this.recommendCondition != null) { result = result.concat(Evaluator.validateExpression(this.recommendCondition, this.path(), 'recommendCondition', identifiers)); }
    return result;
  }

  _child(idx) { return this.arguments[idx]; }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    enactment._state[statePath] = new CandidateState({path: statePath, _enactment: enactment});
    return this.arguments.map((argument) => argument._buildState(enactment, statePath));
  }
}

export class CandidateState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({recommended: this.recommended, support: this.support} = (options || {}));
    this.confirmed = options.confirmed || false;
    if (this.support === "++") { this.support=+Infinity; }
    if (this.support === "--") { this.support=-Infinity; }
  }

  confirm() {
    return this._setConfirm(true);
  }

  unconfirm() {
    return this._setConfirm(false);
  }

  _isUpdateable() {
    return this._enactment._state[this._parentStatePath()].state === 'in_progress';
  }

  getComponent(shallow) {
    if (shallow == null) { shallow = false; }
    const result = super.getComponent();
    if (!shallow) {
      result.arguments = [];
      const design = this._enactment.protocol.getComponent(this.design);
      for (let idx = 0; idx < design.arguments.length; idx++) {
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(this.design) + Core.Named.PATH_SEPARATOR + idx];
        if (stateObj != null) {
          result.arguments.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const paths = this.path.split(Evaluator.PATH_SEPARATOR);
    const test = paths.pop();
    if (test === name) {
      return this;
    } else {
      const state =  this._enactment._state[paths.join(Evaluator.PATH_SEPARATOR)];
      return state ? state._getCandidateFromName(name) : null;
    }
  }

  _setConfirm(value) {
    if (value !== this.confirmed) {
      return this._enactment._cycle({
        action:
          value ? {confirm: this.design} : {unconfirm: this.design},
        seed: [ { path: this.path, attribute: 'confirmed', value } ]});
    }
  }

  _updateState(evaluator) {
    const result = super._updateState();
    if (this._enactment._state[this._parentStatePath()].state === 'in_progress') {
      let recommended;
      let support=0;
      const candidate = this._enactment.protocol.getComponent(this.design);
      for (var argument of candidate.arguments) {
        if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + argument.idx].active) {
          support=support+argument.support;
        }
      }
      if (candidate.recommendCondition != null) {
        recommended = evaluator.evaluate(candidate.recommendCondition, this.design);
      }
      if (!isNaN(support) && (support !== this.support)) {
        result.push({ path: this.path, attribute: 'support', value: support});
      }
      if (recommended !== this.recommended) {
        result.push({ path: this.path, attribute: 'recommended', value: recommended});
      } else if (this.recommended && !this.confirmed) {
        if (this._enactment.protocol.getComponent(this.design).autoConfirmRecommended || (this._enactment.options && this._enactment.options.Candidate && this._enactment.options.Candidate.autoConfirmRecommended)) {
          result.push({
            path: this.path,
            attribute: 'confirmed',
            value: true
          });
        }
      }
    }
    return result;
  }

  // overridden to handle ++ and -- values which get nullified when the result of @getComponent is stringified
  toJSON() {
    const result = super.toJSON();
    result.class = "CandidateState";
    if ((this.support>0) && !isFinite(this.support)) {
      result.support="++";
    } else if ((this.support<0) && !isFinite(this.support)) {
      result.support="--";
    }
    return result;
  }
}

// Arguments are aggregated by candidates
export class Argument extends Core.Annotated {
  constructor(options) {
    super(options);
    ({idx: this.idx, caption: this.caption, description: this.description, support: this.support, activeCondition: this.activeCondition} = (options || {}));
    if (this.support === "+") { this.support=1; }
    if (this.support === "++") { this.support=+Infinity; }
    if (this.support === "-") { this.support=-1; }
    if (this.support === "--") { this.support=-Infinity; }
  }

  allDataDefinitions() { return this._parent.allDataDefinitions(); }

  // see Core.Component.validate()
  static _attributes() { return super._attributes(...arguments).concat(['idx','support','activeCondition']); }

  path() { return `${(this._parent != null ? this._parent.path() : undefined)}${Core.Named.PATH_SEPARATOR}${this.idx}`; }

  subPaths() { return [this.idx.toString()]; }

  validate(identifiers) {
    if (identifiers == null) { identifiers = []; }
    let result = [];
    if (this._parent == null) {
      result.push(this._createErrorValidation('_parent', "an Argument must be attached to a Candidate"));
    }
    if ((this.activeCondition == null) || (this.activeCondition === '')) {
      result.push(this._createErrorValidation('activeCondition', "required attribute"));
    }
    if ((this.caption == null)) {
      result.push(this._createErrorValidation('caption', "required attribute"));
    }
    if ((this.support == null)) {
      result.push(this._createErrorValidation('support', "required attribute"));
    } else if (!((['+','-','++','--'].includes(this.support)) || (typeof this.support === 'number'))) {
      result.push(this._createErrorValidation('support', "must be numeric or in '+', '-', '--', '++'"));
    }
    if ((this.idx == null)) {
      result.push(this._createErrorValidation('idx', "required attribute"));
    }
    if (this.activeCondition != null) { result = result.concat(Evaluator.validateExpression(this.activeCondition, this.path(), 'activeCondition', identifiers)); }
    return result;
  }

  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.idx;
    return enactment._state[statePath] = new ArgumentState({path: statePath, _enactment: enactment});
  }

  // overridden to handle ++ and -- values which get nullified by JSON.stringify
  toJSON() {
    const result = super.toJSON();
    result.class = "Argument"
    if ((this.support>0) && !isFinite(this.support)) {
      result.support="++";
    } else if ((this.support<0) && !isFinite(this.support)) {
      result.support="--";
    }
    delete result.idx;
    return result;
  }
}

export class ArgumentState extends Core.AnnotatedState {
  constructor(options) {
    super(options);
    ({active: this.active} = (options || {}));
  }

  _isUpdateable() {
    return true;
  }

  // TODO handle weight as expression instead of constant
  _updateState(evaluator) {
    const result = super._updateState();
    const {
      activeCondition
    } = this._enactment.protocol.getComponent(this.design);
    const active = evaluator.evaluate(activeCondition, this.design);
    if (active !== this.active) {
      result.push({
        path: this.path,
        attribute: 'active',
        value: active
      });
    }
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const paths = this.path.split(Evaluator.PATH_SEPARATOR);
    paths.pop();
    const state = this._enactment._state[paths.join(Evaluator.PATH_SEPARATOR)];
    return state ? state._getCandidateFromName(name) : null;
  }

  // overridden to handle ++ and -- values which get nullified when the result of @getComponent is stringified
  toJSON() {
    const result = super.toJSON();
    result.class = "ArgumentState";
    if ((this.support>0) && !isFinite(this.support)) {
      result.support="++";
    } else if ((this.support<0) && !isFinite(this.support)) {
      result.support="--";
    }
    return result;
  }
}

// mapping that mitigates minification issues
const taskClassMap = new Map([
  ['Task', Task],
  ['Action', Action],
  ['Enquiry', Enquiry],
  ['Decision', Decision],
  ['Plan', Plan],
])
