// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import * as Evaluator from './evaluator.js';
import * as Protocol  from './tasks.js';
import moment from 'moment';

// An enactment is a serialisable runtime instance of a protocol.
export class Enactment {
  static inflate(json) {
    const pojo = JSON.parse(json);
    for (const act of pojo._audit) {
      act.timestamp = moment(act.timestamp, 'YYYY-MM-DDTHH:mm:ss.SSSZ')
    }
    for (const path of Object.keys(pojo._data)) {
      for (const key of Object.keys(pojo._data[path])) {
        pojo._data[path][key].forEach((val, idx) => {
          pojo._data[path][key][idx].when = moment(val.when, 'YYYY-MM-DDTHH:mm:ss.SSSZ')
        })
      }
    }
    return new Enactment({enactment: pojo});
  }

  // Options:
  // * protocol | enactment - an enactment can be created from a protocol or a serialised enactment
  // * start - a protocol can be automatically started by setting start to true
  // * validate - a protocol will be automatically validated and throw an error for invalid protocols unless this is set to false
  // * tickInterval - a tickInterval sets an automatic timeout (seconds) for cycling the state engine - defaults to undefined
  // * stackLimit - max number of cycle chains in any engine event - defaults to 1000
  // * options - an extensible set of default runtime values, keyed by task type which for proformajs tasks are:
  //   * Enquiry.useDefaults - use default values in enquiries when value unknown - defaults to false
  //   * Candidate.autoConfirmRecommended - automatically commit recommended candidates in non-autonomous decisions - defaults to false
  //   * Decision.autoConfirmMostSupported - automatically commit the candidate with the highest support in a decision - defaults to false
  constructor(options) {
    ({protocol: this.protocol, options: this.options} = (options || {}));
    this.listeners = {};
    if (options.enactment != null) {
      let data, dataMap, inflated, key, type, value;
      for (key of Object.keys(options.enactment)) {
        value = options.enactment[key];
        this[key] = value;
      }
      // 'inflate' protocol, state, data and audit
      this.protocol = Protocol.buildTask(this.protocol);
      this.tickInterval = options.enactment.tickInterval;
      this.stackLimit = options.enactment.stackLimit;
      for (key of Object.keys(this._state)) {
        value = this._state[key];
        inflated = new (Protocol[value.class])(value);
        inflated._enactment = this;
        this._state[key] = inflated;
      }
      for (var path in this._data) {
        data = this._data[path];
        dataMap = {}; // map of data definition types by data definition name
        for (var dataDef of this.protocol.getComponent(path).allDataDefinitions()) {
          dataMap[dataDef.name] = dataDef.toJSON().class;
        }
        for (key in data) {
          var versions = data[key];
          inflated = [];
          for (var version of versions) {
            type = dataMap[key];
            if (type != null) {
              version.value = Evaluator.dataDefinitionClassMap.get(type).inflate(version.value);
              inflated.push(version);
            } else {
              console.log('inflate data: missing key from datamap', key, dataMap);
            }
          }
          this._data[path][key] = inflated;
        }
      }
      for (var event of this._audit) {
        if (event.action.data != null) {
          for (key in event.action.data) {
            value = event.action.data[key];
            type = dataMap[key];
            if (type != null) {
              event.action.data[key] = Evaluator.dataDefinitionClassMap.get(type).inflate(value);
            } else {
              console.log('inflate audit data: missing key from datamap', key, dataMap);
            }
          }
        }
      }
      if (this.tickInterval != null) {
        // add function to automatically cycle enactment that can be passed to setTimeout
        this._tick = () => this._cycle({action: 'tick'});
        if (this._state[this.protocol.name].state === 'in_progress') { this.tick = setTimeout(this._tick, this.tickInterval*1000); }
      }
    } else {
      // TODO allow uri to be passed
      if ((this.protocol == null)) {
        throw new Error('Missing Protocol - cannot create enactment');
      }
      const validate = options.validate || true;
      if (validate && !this.protocol.isValid()) {
        throw new Error('Invalid Protocol - cannot create enactment');
      }
      this._state = {};
      this._data = {};
      this._audit = [];
      this._triggers = {};
      if (this.protocol.cyclic) {
        this.protocol._buildState(this, 0);
      } else {
        this.protocol._buildState(this);
      }
      this.tickInterval = options.tickInterval || undefined; // seconds, see also @lastTicked()
      if (this.tickInterval != null) {
        // add function to automatically cycle enactment that can be passed to setTimeout
        this._tick = () => this._cycle({action: 'tick'});
      }
    }
    if (options.start || false) { this.start(); }
    this.stackLimit = options.stackLimit || 1000;
  }

  // start enactment
  // errors if enactment is already started
  start() {
    if (!this.startedAt()) {
      const timestamp = moment();
      this._cycle({timestamp, action: 'start'});
      if (this.listeners["started"] != null) {
        for (var listener of this.listeners["started"]) {
          var evt = {
            event: 'started',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
      return this;
    } else {
      throw new Error('Cannot start an enactment that has already started');
    }
  }

  // get data object that merges state and design of component by path
  // setting the second 'shallow' parameter to true will ignore sub-components
  getComponent(path, shallow) {
    if (shallow == null) { shallow = false; }
    if (this._state[path] != null) {
      return this._state[path].getComponent(shallow);
    } else {
      // inert data Definitions dont have a state, but still ought to be retrievable
      const result = this.protocol.getComponent(path).flatten();
      result.path = path;
      return result;
    }
  }

  // Cyclic tasks mean runtime paths may be different from designtime paths.
  // Returns the path of the "latest" task associated with the designPath or echos back the designPath
  runtimeFromDesignPath(designPath) {
    // extracts indexes from a path
    const indexes = function(path) {
      const vals = [];
      for (var comp of path.split(Evaluator.PATH_SEPARATOR)) {
        if (comp.indexOf("[")>0) {
          vals.push(parseInt(comp.substr(comp.lastIndexOf('[')+1, comp.lastIndexOf(']')-comp.lastIndexOf('[')-1)));
        }
      }
      return vals;
    };
    // compares two arrays of indexes, returns true if test is newer than existing
    const better = function(test, existing) {
      const test_indexes = indexes(test);
      const existing_indexes = indexes(existing);
      for (let idx = 0; idx < test_indexes.length; idx++) {
        var val = test_indexes[idx];
        if (val>existing_indexes[idx]) { return true; }
      }
      return false;
    };
    let result = null;
    for (var key in this._state) {
      var val = this._state[key];
      if (val.design === designPath) {
        if ((result == null)) {
          result = key;
        } else {
          if (better(key, result)) { result=key; }
        }
      }
    }
    // NB if the designPath was a sibling component name the above algorithm wont have worked
    if (result === null) {
      return designPath;
    } else {
      return result;
    }
  }

  // return all task components
  getTasks() {
    const result = [];
    for (var path of Object.keys(this._state)) {
      var task = this._state[path];
      if (task.state != null) {
        result.push(this._state[path].getComponent());
      }
    }
    return result;
  }

  // return all data definitions referred to in this enactment and their current value
  getDataDefinitions() {
    const result = [];
    const indexes = [];
    for (var path of Object.keys(this._state)) {
      var task = this._state[path];
      if (task.state != null) {
        for (var dataDef of this.protocol.getComponent(task.design).dataDefinitions) {
          var comp = dataDef.flatten();
          comp.path = task.design + Evaluator.PATH_SEPARATOR + comp.name;
          if (!indexes.includes(comp.path)) {
            var values = this._data[task.design] != null ? this._data[task.design][comp.name] : undefined;
            if (values != null) {
              comp.value = values[values.length-1].value;
            }
            result.push(comp);
            indexes.push(comp.path);
          }
        }
      }
    }
    return result;
  }

  // returns object with at least one of the following attributes
  //   started: boolean
  //   finished: boolean
  //   warnings: array ot compoent paths for active warnings
  //   completeable: array of completeable task paths
  //   cancellabel: array of cancellable task paths
  //   requested: object whose keys are task paths with requested data
  //   confirmable: object whose keys are task paths with confirmable candidates
  // TODO: make this extensible so that state is looped through only once
  getStatus() {
    const result =
      {started: (this.startedAt() != null)};
    if (result.started) {
      let keys;
      if (this.protocol.cyclic) {
        keys = Object.keys(this._state).filter(key => key.indexOf(Evaluator.PATH_SEPARATOR) === -1);
        result.finished = ['completed','discarded'].includes(this._state[keys.pop()].state);
      } else {
        result.finished = ['completed','discarded'].includes(this._state[this.protocol.name].state);
      }
      if (!result.finished) {
        result.completeable=[];
        result.cancellable=[];
        result.warnings=[];
        result.requested={};
        result.confirmable={};
        result.triggers=[];
        for (var key of Object.keys(this._state)) {
          var value = this._state[key];
          if (value.state != null) {
            if ((value.cancellable != null) && value.cancellable) {
              result.cancellable.push(key);
            }
            if (value.state === 'in_progress') {
              if ((value.completeable != null) && value.completeable) {
                result.completeable.push(key);
              }
              if (value.requested != null) {
                result.requested[key] = value.requested();
              } else if (value.recommended != null) {
                var recommended = this._state[key].recommended();
                var confirmed = this._state[key].confirmed();
                for (var candidate of this.protocol.getComponent(this._state[key].design).candidates) {
                  result.confirmable[candidate.path()] = {
                    isRecommended: recommended.includes(candidate.name),
                    isConfirmed: confirmed.includes(candidate.name)
                  };
                }
              }
            }
            // check for available trigger
            var eventTrigger = value.trigger();
            if ((eventTrigger != null) && !result.triggers.includes(eventTrigger)) {
              result.triggers.push(eventTrigger);
            }
          }
          // TODO: smells bad? - replace use of instanceof here
          if (value instanceof Protocol.WarningState && (value.active != null) && value.active) {
            result.warnings.push(key);
          }
        }
      }
    }
    return result;
  }

  // complete task by path, errors if task not completeable
  complete(path) {
    if (this._state[path] != null) {
      this._state[path].complete();
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // cancel task by path. erros if task not cancellable.
  cancel(path) {
    if (this._state[path] != null) {
      if (this._state[path] != null) {
        this._state[path].cancel();
      }
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // retrieve current data view at a given task path
  get(path, key=null) {
    const dataDefs = this.protocol.getComponent(this._state[path].design).allDataDefinitions();
    if (key != null) {
      return this._state[path].get(key);
    } else {
      const result={};
      for (var dataDef of dataDefs) {
        result[dataDef.name] = this._state[path].get(dataDef.name);
      }
      return result;
    }
  }

  // provide data (as object whose keys must be valid data definitions) to the engine
  set(path, data) {
    if (this._state[path] != null) {
      this._state[path].set(data);
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // clear data value from engine state
  unset(path, type) {
    if (this._state[path] != null) {
      this._state[path].unset(type);
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  edit(path, index, value) {
    let arr = path.split(Evaluator.PATH_SEPARATOR)
    let type = arr.pop()
    let taskpath = arr.join(Evaluator.PATH_SEPARATOR)
    if (this._state[taskpath] != null) {
      this._state[taskpath].edit(type, index, value);
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // get current view of all *known* data (or external state)
  // Note that *known* data is different to *all* possible data as it will not
  // contain data for dataDefinitions that have yet to be encountered during enactment.
  getData() {
    const result = {};
    for (var path of Object.keys(this._data)) {
      var data = this._data[path];
      result[path]={};
      for (var key of Object.keys(data)) {
        result[path][key] = this._state[this.runtimeFromDesignPath(path)].get(key);
      }
    }
    return result;
  }

  // set current data in bulk, e.g. as returned from getData()
  setData(indexed) {
    const result = [];
    for (var path of Object.keys(indexed)) {
      var data = indexed[path];
      result.push(this.set(path, data));
    }
    return result;
  }

  // confirm decision candidate
  confirm(path) {
    if (this._state[path] != null) {
      this._state[path].confirm();
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // unconfirm decision candidate
  unconfirm(path) {
    if (this._state[path] != null) {
      this._state[path].unconfirm();
      return this;
    } else {
      throw new Error(`unknown component '${path}'`);
    }
  }

  // evaluate an expression - a way of probing state
  // if path is omitted then the the root path will be used
  evaluate(exp, path) {
    const evaluator = Evaluator.buildEvaluator(this);
    return evaluator.evaluate(exp, path);
  }

  // return timestamp (javascript Date) of last cycle or null if not started
  lastCycled() { if (this._audit.length>0) { return this._audit[this._audit.length-1].timestamp; } else { return null; } }

  // return timestamp (javascript Date) when started or null if not started
  startedAt() { if (this._audit.length>0) { return this._audit[0].timestamp; } else { return null; } }

  sendTrigger(trigger) {
    const triggers = Object.keys(this._triggers);
    if (triggers.includes(trigger)) {
      const update = [];
      for (var path of this._triggers[trigger]) {
        if (this._state[path].state === 'dormant') {
          var comp = this.getComponent(path, true);
          var preCondition = (comp.preCondition == null) || (comp.preCondition === '') || this.evaluate(comp.preCondition, path);
          update.push({
            path,
            attribute: 'state',
            value: preCondition ? 'in_progress' : 'discarded'
          });
        }
      }
      if (update.length>0) {
        this._cycle({
          action: {
            trigger
          },
          seed: update
        });
      }
      return this;
    } else {
      throw new Error(`unknown trigger ('${trigger}'). Expect one of ${triggers}`);
    }
  }

  // this override needed for a ticking enactment
  toJSON() {
    const result = {};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['tick'].includes(key) && !(value == null)) { result[key] = value; }
    }
    return result;
  }

  // add event listener
  // possible keys: "started", "finished", "cycled"
  // a listener is a function with one event parameter
  on(key, listener) {
    if ((this.listeners[key] == null)) {
      this.listeners[key]=[];
    }
    return this.listeners[key].push(listener);
  }

  // used to decide whether or not continue ticking and to send finished event
  _isFinished() {
    let protocolRootPath = this.protocol.name;
    if (this.protocol.cyclic) {
      // find the highest cycle
      const keys = Object.keys(this._state).filter(key => key.startsWith(protocolRootPath) && (key.slice(protocolRootPath.length).indexOf(Evaluator.PATH_SEPARATOR)===-1));
      protocolRootPath = keys.pop();
    }
    return ['completed', 'discarded'].includes(this._state[protocolRootPath].state);
  }

  // To cycle the engine, every component is checked for updates that
  // can be applied to the state.  The updates are collected together and then
  // applied en masse. (they are order independent)
  // This is repeated until there are no more updates to be made.
  // Options:
  // * action - should be provided at the start of a chain of cycles for audit purposes
  // * seed - updates to kick off a chain of cycles, e.g. to complete a task
  // * timestamp - a timestamp indicates that this cycle is part of a chain of cycles and should be audited under the same timestamp
  _cycle(options) {
    // clear tick timeout if set (we'll apply it again once this set of cycles is complete)
    let name, value;
    if (options == null) { options = {}; }
    if (this.tick != null) { clearTimeout(this.tick); }
    const timestamp = options.timestamp || moment();
    // an evaluator is built for every engine cycle to ensure that the starting state is properly captured/cached
    const evaluator = Evaluator.buildEvaluator(this, timestamp);
    let result = options.seed || [];
    for (var key of Object.keys(this._state)) {
      value = this._state[key];
      if (value._updateState != null) {
        result = result.concat(value._updateState(evaluator));
      } else {
        throw new Error(`mis-configured runtime component: ${value}`);
      }
    }
    if (result.length>0) {
      if ((this._audit.length>0) && (options.timestamp != null)) {
        this._audit[this._audit.length-1].deltas.push(result);
        if (this._audit[this._audit.length-1].deltas.length>this.stackLimit) {
          throw new Error(`stack size exception - delta's array is larger than stackLimit (${this.stackLimit})`);
        }
      } else {
        this._audit.push({
          action: options.action,
          timestamp,
          deltas: [result]});
      }
      for (var delta of result) {
        this._state[delta.path][delta.attribute]=delta.value;
        if (delta.data != null) {
          var parts = delta.path.split(Evaluator.PATH_SEPARATOR);
          name = parts.pop();
          var parent = parts.join(Evaluator.PATH_SEPARATOR);
          if ((this._data[parent] == null)) {
            this._data[parent]={};
          }
          if ((this._data[parent][name] == null)) {
            this._data[parent][name]=[];
          }
          this._data[parent][name].push({
            value: delta.value,
            when: timestamp
          });
        }
      }
      return this._cycle({timestamp}); // iterate until stabilised
    } else {
      // this set of cycles is finished.  But there are some checks to do before we finish up.
      let evt, listener;
      if ((options.timestamp == null) && (options.action !== 'tick')) {
        // data changes may have no effect on state, but should still be auditted
        this._audit.push({
          action: options.action,
          timestamp,
          deltas: []});
      }
      if ((this.tickInterval != null) && !this._isFinished()) {
        // set tick if there are still things to do
        if (this._state[this.runtimeFromDesignPath(this.protocol.name)].state === 'in_progress') { this.tick = setTimeout(this._tick, this.tickInterval*1000); }
      }
      // raise events if there are listeners
      if (this.listeners["cycled"] != null) {
        for (listener of this.listeners["cycled"]) {
          evt = {
            event: 'cycled',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
      if ((this.listeners["finished"] != null) && this._isFinished()) {
        for (listener of this.listeners["finished"]) {
          evt = {
            event: 'finished',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
    }
  }
}
