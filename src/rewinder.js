// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Allows you to construct earlier versions of an `Enactment` based on the enactment's `_audit`.

import { Enactment } from './enactment.js'
import * as Evaluator from './evaluator.js';

export class Rewinder {
  constructor(enactment) {
    this.enactment = enactment
    this.origin = new Enactment({
      start: false,
      protocol: enactment.protocol,
      options: enactment.options
    })
    this.versions = [] // cache of version state
    this.maxIdx = -1 // max index of the current cache
  }

  // the passed enactment has an `_audit` array of timestamped interactions that have changed its state
  // version(idx) returns a clone of that enactment at each indexed audit event
  version(idx) {
    if (idx < 0 || idx > this.enactment._audit.length - 1) {
      throw new Error('Index out of range, ' + idx)
    } else {
      if (idx > this.maxIdx) {
        let rewind = this.maxIdx == -1 ? this.origin : this.version(this.maxIdx)
        for (let act of this.enactment._audit.slice(this.maxIdx + 1, idx + 1)) {
          if (act.action == 'start') {
            rewind.start()
          } else if (act.action.complete) {
            rewind.complete(act.action.complete)
          } else if (act.action.set) {
            rewind.set(act.action.set, act.action.data)
          } else if (act.action.unset) {
            rewind.unset(act.action.unset, act.action.type)
          } else if (act.action.confirm) {
            rewind.confirm(act.action.confirm)
          } else if (act.action.unconfirm) {
            rewind.unconfirm(act.action.unconfirm)
          } else if (act.action.cancel) {
            rewind.cancel(act.action.cancel)
          } else if (act.action.trigger) {
            rewind.sendTrigger(act.action.trigger)
          } else if (act.action.edit) {
            rewind.edit(act.action.edit + Evaluator.PATH_SEPARATOR + act.action.type, act.action.index, act.action.value)
          } else {
            throw new Error('unknown action: ', act.action)
          }
          let clone = Enactment.inflate(JSON.stringify(rewind))
          this.versions.push({ state: clone._state, triggers: clone._triggers })
        }
        this.maxIdx = idx
      }
      let final = this.origin
      final._state = this.versions[idx].state
      let data = {}
      for (let path of Object.keys(this.enactment._data)) {
        data[path] = {}
        for (let dd of Object.keys(this.enactment._data[path])) {
          let arr = this.enactment._data[path][dd].filter(
            (val) => val.when.valueOf() <= this.enactment._audit[idx].timestamp.valueOf()
          )
          if (arr.length > 0) {
            data[path][dd] = arr
          }
        }
      }
      final._data = data
      final._triggers = this.versions[idx].triggers
      final._audit = this.enactment._audit.slice(0, idx + 1)
      return Enactment.inflate(JSON.stringify(final))
    }
  }
}
