The dist npm script uses babel to create a transpiled version of the library with polyfills for the target versions, see ``babel.config.json``, in the transpiled directory.  Then it runs browserify against ``dist.brownser.js`` to combine the transpiled code with all its dependencies into a single file ``proforma.browser.js``.  Finally it uses terser to minify that file and create ``proforma.browser.min.js``.

Run ``python3 -m http.server`` in this directory to test the browser build of the minified proformajs.

The dist files are included in the release so that they can be automatically downloaded from the [unpkg CDN](https://www.unpkg.com/browse/@openclinical/proformajs/).
