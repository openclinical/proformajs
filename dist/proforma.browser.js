(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function() {
  Protocol = require('../transpiled/tasks');
  Enactment = require('../transpiled/enactment').Enactment;
  Rewinder = require('../transpiled/rewinder').Rewinder;
}());

},{"../transpiled/enactment":189,"../transpiled/rewinder":191,"../transpiled/tasks":192}],2:[function(require,module,exports){
'use strict';
var isCallable = require('../internals/is-callable');
var tryToString = require('../internals/try-to-string');

var $TypeError = TypeError;

// `Assert: IsCallable(argument) is true`
module.exports = function (argument) {
  if (isCallable(argument)) return argument;
  throw new $TypeError(tryToString(argument) + ' is not a function');
};

},{"../internals/is-callable":80,"../internals/try-to-string":145}],3:[function(require,module,exports){
'use strict';
var isConstructor = require('../internals/is-constructor');
var tryToString = require('../internals/try-to-string');

var $TypeError = TypeError;

// `Assert: IsConstructor(argument) is true`
module.exports = function (argument) {
  if (isConstructor(argument)) return argument;
  throw new $TypeError(tryToString(argument) + ' is not a constructor');
};

},{"../internals/is-constructor":81,"../internals/try-to-string":145}],4:[function(require,module,exports){
'use strict';
var isPossiblePrototype = require('../internals/is-possible-prototype');

var $String = String;
var $TypeError = TypeError;

module.exports = function (argument) {
  if (isPossiblePrototype(argument)) return argument;
  throw new $TypeError("Can't set " + $String(argument) + ' as a prototype');
};

},{"../internals/is-possible-prototype":85}],5:[function(require,module,exports){
'use strict';
var wellKnownSymbol = require('../internals/well-known-symbol');
var create = require('../internals/object-create');
var defineProperty = require('../internals/object-define-property').f;

var UNSCOPABLES = wellKnownSymbol('unscopables');
var ArrayPrototype = Array.prototype;

// Array.prototype[@@unscopables]
// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
if (ArrayPrototype[UNSCOPABLES] === undefined) {
  defineProperty(ArrayPrototype, UNSCOPABLES, {
    configurable: true,
    value: create(null)
  });
}

// add a key to Array.prototype[@@unscopables]
module.exports = function (key) {
  ArrayPrototype[UNSCOPABLES][key] = true;
};

},{"../internals/object-create":103,"../internals/object-define-property":105,"../internals/well-known-symbol":150}],6:[function(require,module,exports){
'use strict';
var charAt = require('../internals/string-multibyte').charAt;

// `AdvanceStringIndex` abstract operation
// https://tc39.es/ecma262/#sec-advancestringindex
module.exports = function (S, index, unicode) {
  return index + (unicode ? charAt(S, index).length : 1);
};

},{"../internals/string-multibyte":133}],7:[function(require,module,exports){
'use strict';
var isPrototypeOf = require('../internals/object-is-prototype-of');

var $TypeError = TypeError;

module.exports = function (it, Prototype) {
  if (isPrototypeOf(Prototype, it)) return it;
  throw new $TypeError('Incorrect invocation');
};

},{"../internals/object-is-prototype-of":112}],8:[function(require,module,exports){
'use strict';
var isObject = require('../internals/is-object');

var $String = String;
var $TypeError = TypeError;

// `Assert: Type(argument) is Object`
module.exports = function (argument) {
  if (isObject(argument)) return argument;
  throw new $TypeError($String(argument) + ' is not an object');
};

},{"../internals/is-object":84}],9:[function(require,module,exports){
'use strict';
// FF26- bug: ArrayBuffers are non-extensible, but Object.isExtensible does not report it
var fails = require('../internals/fails');

module.exports = fails(function () {
  if (typeof ArrayBuffer == 'function') {
    var buffer = new ArrayBuffer(8);
    // eslint-disable-next-line es/no-object-isextensible, es/no-object-defineproperty -- safe
    if (Object.isExtensible(buffer)) Object.defineProperty(buffer, 'a', { value: 8 });
  }
});

},{"../internals/fails":49}],10:[function(require,module,exports){
'use strict';
var toIndexedObject = require('../internals/to-indexed-object');
var toAbsoluteIndex = require('../internals/to-absolute-index');
var lengthOfArrayLike = require('../internals/length-of-array-like');

// `Array.prototype.{ indexOf, includes }` methods implementation
var createMethod = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIndexedObject($this);
    var length = lengthOfArrayLike(O);
    if (length === 0) return !IS_INCLUDES && -1;
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare -- NaN check
    if (IS_INCLUDES && el !== el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare -- NaN check
      if (value !== value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) {
      if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

module.exports = {
  // `Array.prototype.includes` method
  // https://tc39.es/ecma262/#sec-array.prototype.includes
  includes: createMethod(true),
  // `Array.prototype.indexOf` method
  // https://tc39.es/ecma262/#sec-array.prototype.indexof
  indexOf: createMethod(false)
};

},{"../internals/length-of-array-like":97,"../internals/to-absolute-index":136,"../internals/to-indexed-object":137}],11:[function(require,module,exports){
'use strict';
var bind = require('../internals/function-bind-context');
var uncurryThis = require('../internals/function-uncurry-this');
var IndexedObject = require('../internals/indexed-object');
var toObject = require('../internals/to-object');
var lengthOfArrayLike = require('../internals/length-of-array-like');
var arraySpeciesCreate = require('../internals/array-species-create');

var push = uncurryThis([].push);

// `Array.prototype.{ forEach, map, filter, some, every, find, findIndex, filterReject }` methods implementation
var createMethod = function (TYPE) {
  var IS_MAP = TYPE === 1;
  var IS_FILTER = TYPE === 2;
  var IS_SOME = TYPE === 3;
  var IS_EVERY = TYPE === 4;
  var IS_FIND_INDEX = TYPE === 6;
  var IS_FILTER_REJECT = TYPE === 7;
  var NO_HOLES = TYPE === 5 || IS_FIND_INDEX;
  return function ($this, callbackfn, that, specificCreate) {
    var O = toObject($this);
    var self = IndexedObject(O);
    var length = lengthOfArrayLike(self);
    var boundFunction = bind(callbackfn, that);
    var index = 0;
    var create = specificCreate || arraySpeciesCreate;
    var target = IS_MAP ? create($this, length) : IS_FILTER || IS_FILTER_REJECT ? create($this, 0) : undefined;
    var value, result;
    for (;length > index; index++) if (NO_HOLES || index in self) {
      value = self[index];
      result = boundFunction(value, index, O);
      if (TYPE) {
        if (IS_MAP) target[index] = result; // map
        else if (result) switch (TYPE) {
          case 3: return true;              // some
          case 5: return value;             // find
          case 6: return index;             // findIndex
          case 2: push(target, value);      // filter
        } else switch (TYPE) {
          case 4: return false;             // every
          case 7: push(target, value);      // filterReject
        }
      }
    }
    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
  };
};

module.exports = {
  // `Array.prototype.forEach` method
  // https://tc39.es/ecma262/#sec-array.prototype.foreach
  forEach: createMethod(0),
  // `Array.prototype.map` method
  // https://tc39.es/ecma262/#sec-array.prototype.map
  map: createMethod(1),
  // `Array.prototype.filter` method
  // https://tc39.es/ecma262/#sec-array.prototype.filter
  filter: createMethod(2),
  // `Array.prototype.some` method
  // https://tc39.es/ecma262/#sec-array.prototype.some
  some: createMethod(3),
  // `Array.prototype.every` method
  // https://tc39.es/ecma262/#sec-array.prototype.every
  every: createMethod(4),
  // `Array.prototype.find` method
  // https://tc39.es/ecma262/#sec-array.prototype.find
  find: createMethod(5),
  // `Array.prototype.findIndex` method
  // https://tc39.es/ecma262/#sec-array.prototype.findIndex
  findIndex: createMethod(6),
  // `Array.prototype.filterReject` method
  // https://github.com/tc39/proposal-array-filtering
  filterReject: createMethod(7)
};

},{"../internals/array-species-create":17,"../internals/function-bind-context":53,"../internals/function-uncurry-this":59,"../internals/indexed-object":72,"../internals/length-of-array-like":97,"../internals/to-object":140}],12:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');

module.exports = function (METHOD_NAME, argument) {
  var method = [][METHOD_NAME];
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call -- required for testing
    method.call(null, argument || function () { return 1; }, 1);
  });
};

},{"../internals/fails":49}],13:[function(require,module,exports){
'use strict';
var aCallable = require('../internals/a-callable');
var toObject = require('../internals/to-object');
var IndexedObject = require('../internals/indexed-object');
var lengthOfArrayLike = require('../internals/length-of-array-like');

var $TypeError = TypeError;

var REDUCE_EMPTY = 'Reduce of empty array with no initial value';

// `Array.prototype.{ reduce, reduceRight }` methods implementation
var createMethod = function (IS_RIGHT) {
  return function (that, callbackfn, argumentsLength, memo) {
    var O = toObject(that);
    var self = IndexedObject(O);
    var length = lengthOfArrayLike(O);
    aCallable(callbackfn);
    if (length === 0 && argumentsLength < 2) throw new $TypeError(REDUCE_EMPTY);
    var index = IS_RIGHT ? length - 1 : 0;
    var i = IS_RIGHT ? -1 : 1;
    if (argumentsLength < 2) while (true) {
      if (index in self) {
        memo = self[index];
        index += i;
        break;
      }
      index += i;
      if (IS_RIGHT ? index < 0 : length <= index) {
        throw new $TypeError(REDUCE_EMPTY);
      }
    }
    for (;IS_RIGHT ? index >= 0 : length > index; index += i) if (index in self) {
      memo = callbackfn(memo, self[index], index, O);
    }
    return memo;
  };
};

module.exports = {
  // `Array.prototype.reduce` method
  // https://tc39.es/ecma262/#sec-array.prototype.reduce
  left: createMethod(false),
  // `Array.prototype.reduceRight` method
  // https://tc39.es/ecma262/#sec-array.prototype.reduceright
  right: createMethod(true)
};

},{"../internals/a-callable":2,"../internals/indexed-object":72,"../internals/length-of-array-like":97,"../internals/to-object":140}],14:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var isArray = require('../internals/is-array');

var $TypeError = TypeError;
// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// Safari < 13 does not throw an error in this case
var SILENT_ON_NON_WRITABLE_LENGTH_SET = DESCRIPTORS && !function () {
  // makes no sense without proper strict mode support
  if (this !== undefined) return true;
  try {
    // eslint-disable-next-line es/no-object-defineproperty -- safe
    Object.defineProperty([], 'length', { writable: false }).length = 1;
  } catch (error) {
    return error instanceof TypeError;
  }
}();

module.exports = SILENT_ON_NON_WRITABLE_LENGTH_SET ? function (O, length) {
  if (isArray(O) && !getOwnPropertyDescriptor(O, 'length').writable) {
    throw new $TypeError('Cannot set read only .length');
  } return O.length = length;
} : function (O, length) {
  return O.length = length;
};

},{"../internals/descriptors":35,"../internals/is-array":79}],15:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');

module.exports = uncurryThis([].slice);

},{"../internals/function-uncurry-this":59}],16:[function(require,module,exports){
'use strict';
var isArray = require('../internals/is-array');
var isConstructor = require('../internals/is-constructor');
var isObject = require('../internals/is-object');
var wellKnownSymbol = require('../internals/well-known-symbol');

var SPECIES = wellKnownSymbol('species');
var $Array = Array;

// a part of `ArraySpeciesCreate` abstract operation
// https://tc39.es/ecma262/#sec-arrayspeciescreate
module.exports = function (originalArray) {
  var C;
  if (isArray(originalArray)) {
    C = originalArray.constructor;
    // cross-realm fallback
    if (isConstructor(C) && (C === $Array || isArray(C.prototype))) C = undefined;
    else if (isObject(C)) {
      C = C[SPECIES];
      if (C === null) C = undefined;
    }
  } return C === undefined ? $Array : C;
};

},{"../internals/is-array":79,"../internals/is-constructor":81,"../internals/is-object":84,"../internals/well-known-symbol":150}],17:[function(require,module,exports){
'use strict';
var arraySpeciesConstructor = require('../internals/array-species-constructor');

// `ArraySpeciesCreate` abstract operation
// https://tc39.es/ecma262/#sec-arrayspeciescreate
module.exports = function (originalArray, length) {
  return new (arraySpeciesConstructor(originalArray))(length === 0 ? 0 : length);
};

},{"../internals/array-species-constructor":16}],18:[function(require,module,exports){
'use strict';
var anObject = require('../internals/an-object');
var iteratorClose = require('../internals/iterator-close');

// call something on iterator step with safe closing on error
module.exports = function (iterator, fn, value, ENTRIES) {
  try {
    return ENTRIES ? fn(anObject(value)[0], value[1]) : fn(value);
  } catch (error) {
    iteratorClose(iterator, 'throw', error);
  }
};

},{"../internals/an-object":8,"../internals/iterator-close":90}],19:[function(require,module,exports){
'use strict';
var wellKnownSymbol = require('../internals/well-known-symbol');

var ITERATOR = wellKnownSymbol('iterator');
var SAFE_CLOSING = false;

try {
  var called = 0;
  var iteratorWithReturn = {
    next: function () {
      return { done: !!called++ };
    },
    'return': function () {
      SAFE_CLOSING = true;
    }
  };
  iteratorWithReturn[ITERATOR] = function () {
    return this;
  };
  // eslint-disable-next-line es/no-array-from, no-throw-literal -- required for testing
  Array.from(iteratorWithReturn, function () { throw 2; });
} catch (error) { /* empty */ }

module.exports = function (exec, SKIP_CLOSING) {
  try {
    if (!SKIP_CLOSING && !SAFE_CLOSING) return false;
  } catch (error) { return false; } // workaround of old WebKit + `eval` bug
  var ITERATION_SUPPORT = false;
  try {
    var object = {};
    object[ITERATOR] = function () {
      return {
        next: function () {
          return { done: ITERATION_SUPPORT = true };
        }
      };
    };
    exec(object);
  } catch (error) { /* empty */ }
  return ITERATION_SUPPORT;
};

},{"../internals/well-known-symbol":150}],20:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');

var toString = uncurryThis({}.toString);
var stringSlice = uncurryThis(''.slice);

module.exports = function (it) {
  return stringSlice(toString(it), 8, -1);
};

},{"../internals/function-uncurry-this":59}],21:[function(require,module,exports){
'use strict';
var TO_STRING_TAG_SUPPORT = require('../internals/to-string-tag-support');
var isCallable = require('../internals/is-callable');
var classofRaw = require('../internals/classof-raw');
var wellKnownSymbol = require('../internals/well-known-symbol');

var TO_STRING_TAG = wellKnownSymbol('toStringTag');
var $Object = Object;

// ES3 wrong here
var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) === 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (error) { /* empty */ }
};

// getting tag from ES6+ `Object.prototype.toString`
module.exports = TO_STRING_TAG_SUPPORT ? classofRaw : function (it) {
  var O, tag, result;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (tag = tryGet(O = $Object(it), TO_STRING_TAG)) == 'string' ? tag
    // builtinTag case
    : CORRECT_ARGUMENTS ? classofRaw(O)
    // ES3 arguments fallback
    : (result = classofRaw(O)) === 'Object' && isCallable(O.callee) ? 'Arguments' : result;
};

},{"../internals/classof-raw":20,"../internals/is-callable":80,"../internals/to-string-tag-support":143,"../internals/well-known-symbol":150}],22:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var defineBuiltIns = require('../internals/define-built-ins');
var getWeakData = require('../internals/internal-metadata').getWeakData;
var anInstance = require('../internals/an-instance');
var anObject = require('../internals/an-object');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var isObject = require('../internals/is-object');
var iterate = require('../internals/iterate');
var ArrayIterationModule = require('../internals/array-iteration');
var hasOwn = require('../internals/has-own-property');
var InternalStateModule = require('../internals/internal-state');

var setInternalState = InternalStateModule.set;
var internalStateGetterFor = InternalStateModule.getterFor;
var find = ArrayIterationModule.find;
var findIndex = ArrayIterationModule.findIndex;
var splice = uncurryThis([].splice);
var id = 0;

// fallback for uncaught frozen keys
var uncaughtFrozenStore = function (state) {
  return state.frozen || (state.frozen = new UncaughtFrozenStore());
};

var UncaughtFrozenStore = function () {
  this.entries = [];
};

var findUncaughtFrozen = function (store, key) {
  return find(store.entries, function (it) {
    return it[0] === key;
  });
};

UncaughtFrozenStore.prototype = {
  get: function (key) {
    var entry = findUncaughtFrozen(this, key);
    if (entry) return entry[1];
  },
  has: function (key) {
    return !!findUncaughtFrozen(this, key);
  },
  set: function (key, value) {
    var entry = findUncaughtFrozen(this, key);
    if (entry) entry[1] = value;
    else this.entries.push([key, value]);
  },
  'delete': function (key) {
    var index = findIndex(this.entries, function (it) {
      return it[0] === key;
    });
    if (~index) splice(this.entries, index, 1);
    return !!~index;
  }
};

module.exports = {
  getConstructor: function (wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
    var Constructor = wrapper(function (that, iterable) {
      anInstance(that, Prototype);
      setInternalState(that, {
        type: CONSTRUCTOR_NAME,
        id: id++,
        frozen: null
      });
      if (!isNullOrUndefined(iterable)) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
    });

    var Prototype = Constructor.prototype;

    var getInternalState = internalStateGetterFor(CONSTRUCTOR_NAME);

    var define = function (that, key, value) {
      var state = getInternalState(that);
      var data = getWeakData(anObject(key), true);
      if (data === true) uncaughtFrozenStore(state).set(key, value);
      else data[state.id] = value;
      return that;
    };

    defineBuiltIns(Prototype, {
      // `{ WeakMap, WeakSet }.prototype.delete(key)` methods
      // https://tc39.es/ecma262/#sec-weakmap.prototype.delete
      // https://tc39.es/ecma262/#sec-weakset.prototype.delete
      'delete': function (key) {
        var state = getInternalState(this);
        if (!isObject(key)) return false;
        var data = getWeakData(key);
        if (data === true) return uncaughtFrozenStore(state)['delete'](key);
        return data && hasOwn(data, state.id) && delete data[state.id];
      },
      // `{ WeakMap, WeakSet }.prototype.has(key)` methods
      // https://tc39.es/ecma262/#sec-weakmap.prototype.has
      // https://tc39.es/ecma262/#sec-weakset.prototype.has
      has: function has(key) {
        var state = getInternalState(this);
        if (!isObject(key)) return false;
        var data = getWeakData(key);
        if (data === true) return uncaughtFrozenStore(state).has(key);
        return data && hasOwn(data, state.id);
      }
    });

    defineBuiltIns(Prototype, IS_MAP ? {
      // `WeakMap.prototype.get(key)` method
      // https://tc39.es/ecma262/#sec-weakmap.prototype.get
      get: function get(key) {
        var state = getInternalState(this);
        if (isObject(key)) {
          var data = getWeakData(key);
          if (data === true) return uncaughtFrozenStore(state).get(key);
          if (data) return data[state.id];
        }
      },
      // `WeakMap.prototype.set(key, value)` method
      // https://tc39.es/ecma262/#sec-weakmap.prototype.set
      set: function set(key, value) {
        return define(this, key, value);
      }
    } : {
      // `WeakSet.prototype.add(value)` method
      // https://tc39.es/ecma262/#sec-weakset.prototype.add
      add: function add(value) {
        return define(this, value, true);
      }
    });

    return Constructor;
  }
};

},{"../internals/an-instance":7,"../internals/an-object":8,"../internals/array-iteration":11,"../internals/define-built-ins":33,"../internals/function-uncurry-this":59,"../internals/has-own-property":68,"../internals/internal-metadata":76,"../internals/internal-state":77,"../internals/is-null-or-undefined":83,"../internals/is-object":84,"../internals/iterate":89}],23:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var globalThis = require('../internals/global-this');
var uncurryThis = require('../internals/function-uncurry-this');
var isForced = require('../internals/is-forced');
var defineBuiltIn = require('../internals/define-built-in');
var InternalMetadataModule = require('../internals/internal-metadata');
var iterate = require('../internals/iterate');
var anInstance = require('../internals/an-instance');
var isCallable = require('../internals/is-callable');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var isObject = require('../internals/is-object');
var fails = require('../internals/fails');
var checkCorrectnessOfIteration = require('../internals/check-correctness-of-iteration');
var setToStringTag = require('../internals/set-to-string-tag');
var inheritIfRequired = require('../internals/inherit-if-required');

module.exports = function (CONSTRUCTOR_NAME, wrapper, common) {
  var IS_MAP = CONSTRUCTOR_NAME.indexOf('Map') !== -1;
  var IS_WEAK = CONSTRUCTOR_NAME.indexOf('Weak') !== -1;
  var ADDER = IS_MAP ? 'set' : 'add';
  var NativeConstructor = globalThis[CONSTRUCTOR_NAME];
  var NativePrototype = NativeConstructor && NativeConstructor.prototype;
  var Constructor = NativeConstructor;
  var exported = {};

  var fixMethod = function (KEY) {
    var uncurriedNativeMethod = uncurryThis(NativePrototype[KEY]);
    defineBuiltIn(NativePrototype, KEY,
      KEY === 'add' ? function add(value) {
        uncurriedNativeMethod(this, value === 0 ? 0 : value);
        return this;
      } : KEY === 'delete' ? function (key) {
        return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, key === 0 ? 0 : key);
      } : KEY === 'get' ? function get(key) {
        return IS_WEAK && !isObject(key) ? undefined : uncurriedNativeMethod(this, key === 0 ? 0 : key);
      } : KEY === 'has' ? function has(key) {
        return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, key === 0 ? 0 : key);
      } : function set(key, value) {
        uncurriedNativeMethod(this, key === 0 ? 0 : key, value);
        return this;
      }
    );
  };

  var REPLACE = isForced(
    CONSTRUCTOR_NAME,
    !isCallable(NativeConstructor) || !(IS_WEAK || NativePrototype.forEach && !fails(function () {
      new NativeConstructor().entries().next();
    }))
  );

  if (REPLACE) {
    // create collection constructor
    Constructor = common.getConstructor(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER);
    InternalMetadataModule.enable();
  } else if (isForced(CONSTRUCTOR_NAME, true)) {
    var instance = new Constructor();
    // early implementations not supports chaining
    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) !== instance;
    // V8 ~ Chromium 40- weak-collections throws on primitives, but should return false
    var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
    // most early implementations doesn't supports iterables, most modern - not close it correctly
    // eslint-disable-next-line no-new -- required for testing
    var ACCEPT_ITERABLES = checkCorrectnessOfIteration(function (iterable) { new NativeConstructor(iterable); });
    // for early implementations -0 and +0 not the same
    var BUGGY_ZERO = !IS_WEAK && fails(function () {
      // V8 ~ Chromium 42- fails only with 5+ elements
      var $instance = new NativeConstructor();
      var index = 5;
      while (index--) $instance[ADDER](index, index);
      return !$instance.has(-0);
    });

    if (!ACCEPT_ITERABLES) {
      Constructor = wrapper(function (dummy, iterable) {
        anInstance(dummy, NativePrototype);
        var that = inheritIfRequired(new NativeConstructor(), dummy, Constructor);
        if (!isNullOrUndefined(iterable)) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
        return that;
      });
      Constructor.prototype = NativePrototype;
      NativePrototype.constructor = Constructor;
    }

    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
      fixMethod('delete');
      fixMethod('has');
      IS_MAP && fixMethod('get');
    }

    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);

    // weak collections should not contains .clear method
    if (IS_WEAK && NativePrototype.clear) delete NativePrototype.clear;
  }

  exported[CONSTRUCTOR_NAME] = Constructor;
  $({ global: true, constructor: true, forced: Constructor !== NativeConstructor }, exported);

  setToStringTag(Constructor, CONSTRUCTOR_NAME);

  if (!IS_WEAK) common.setStrong(Constructor, CONSTRUCTOR_NAME, IS_MAP);

  return Constructor;
};

},{"../internals/an-instance":7,"../internals/check-correctness-of-iteration":19,"../internals/define-built-in":32,"../internals/export":48,"../internals/fails":49,"../internals/function-uncurry-this":59,"../internals/global-this":67,"../internals/inherit-if-required":73,"../internals/internal-metadata":76,"../internals/is-callable":80,"../internals/is-forced":82,"../internals/is-null-or-undefined":83,"../internals/is-object":84,"../internals/iterate":89,"../internals/set-to-string-tag":128}],24:[function(require,module,exports){
'use strict';
var hasOwn = require('../internals/has-own-property');
var ownKeys = require('../internals/own-keys');
var getOwnPropertyDescriptorModule = require('../internals/object-get-own-property-descriptor');
var definePropertyModule = require('../internals/object-define-property');

module.exports = function (target, source, exceptions) {
  var keys = ownKeys(source);
  var defineProperty = definePropertyModule.f;
  var getOwnPropertyDescriptor = getOwnPropertyDescriptorModule.f;
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (!hasOwn(target, key) && !(exceptions && hasOwn(exceptions, key))) {
      defineProperty(target, key, getOwnPropertyDescriptor(source, key));
    }
  }
};

},{"../internals/has-own-property":68,"../internals/object-define-property":105,"../internals/object-get-own-property-descriptor":106,"../internals/own-keys":118}],25:[function(require,module,exports){
'use strict';
var wellKnownSymbol = require('../internals/well-known-symbol');

var MATCH = wellKnownSymbol('match');

module.exports = function (METHOD_NAME) {
  var regexp = /./;
  try {
    '/./'[METHOD_NAME](regexp);
  } catch (error1) {
    try {
      regexp[MATCH] = false;
      return '/./'[METHOD_NAME](regexp);
    } catch (error2) { /* empty */ }
  } return false;
};

},{"../internals/well-known-symbol":150}],26:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');

module.exports = !fails(function () {
  function F() { /* empty */ }
  F.prototype.constructor = null;
  // eslint-disable-next-line es/no-object-getprototypeof -- required for testing
  return Object.getPrototypeOf(new F()) !== F.prototype;
});

},{"../internals/fails":49}],27:[function(require,module,exports){
'use strict';
// `CreateIterResultObject` abstract operation
// https://tc39.es/ecma262/#sec-createiterresultobject
module.exports = function (value, done) {
  return { value: value, done: done };
};

},{}],28:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var definePropertyModule = require('../internals/object-define-property');
var createPropertyDescriptor = require('../internals/create-property-descriptor');

module.exports = DESCRIPTORS ? function (object, key, value) {
  return definePropertyModule.f(object, key, createPropertyDescriptor(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

},{"../internals/create-property-descriptor":29,"../internals/descriptors":35,"../internals/object-define-property":105}],29:[function(require,module,exports){
'use strict';
module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

},{}],30:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var definePropertyModule = require('../internals/object-define-property');
var createPropertyDescriptor = require('../internals/create-property-descriptor');

module.exports = function (object, key, value) {
  if (DESCRIPTORS) definePropertyModule.f(object, key, createPropertyDescriptor(0, value));
  else object[key] = value;
};

},{"../internals/create-property-descriptor":29,"../internals/descriptors":35,"../internals/object-define-property":105}],31:[function(require,module,exports){
'use strict';
var makeBuiltIn = require('../internals/make-built-in');
var defineProperty = require('../internals/object-define-property');

module.exports = function (target, name, descriptor) {
  if (descriptor.get) makeBuiltIn(descriptor.get, name, { getter: true });
  if (descriptor.set) makeBuiltIn(descriptor.set, name, { setter: true });
  return defineProperty.f(target, name, descriptor);
};

},{"../internals/make-built-in":98,"../internals/object-define-property":105}],32:[function(require,module,exports){
'use strict';
var isCallable = require('../internals/is-callable');
var definePropertyModule = require('../internals/object-define-property');
var makeBuiltIn = require('../internals/make-built-in');
var defineGlobalProperty = require('../internals/define-global-property');

module.exports = function (O, key, value, options) {
  if (!options) options = {};
  var simple = options.enumerable;
  var name = options.name !== undefined ? options.name : key;
  if (isCallable(value)) makeBuiltIn(value, name, options);
  if (options.global) {
    if (simple) O[key] = value;
    else defineGlobalProperty(key, value);
  } else {
    try {
      if (!options.unsafe) delete O[key];
      else if (O[key]) simple = true;
    } catch (error) { /* empty */ }
    if (simple) O[key] = value;
    else definePropertyModule.f(O, key, {
      value: value,
      enumerable: false,
      configurable: !options.nonConfigurable,
      writable: !options.nonWritable
    });
  } return O;
};

},{"../internals/define-global-property":34,"../internals/is-callable":80,"../internals/make-built-in":98,"../internals/object-define-property":105}],33:[function(require,module,exports){
'use strict';
var defineBuiltIn = require('../internals/define-built-in');

module.exports = function (target, src, options) {
  for (var key in src) defineBuiltIn(target, key, src[key], options);
  return target;
};

},{"../internals/define-built-in":32}],34:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');

// eslint-disable-next-line es/no-object-defineproperty -- safe
var defineProperty = Object.defineProperty;

module.exports = function (key, value) {
  try {
    defineProperty(globalThis, key, { value: value, configurable: true, writable: true });
  } catch (error) {
    globalThis[key] = value;
  } return value;
};

},{"../internals/global-this":67}],35:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');

// Detect IE8's incomplete defineProperty implementation
module.exports = !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] !== 7;
});

},{"../internals/fails":49}],36:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var isObject = require('../internals/is-object');

var document = globalThis.document;
// typeof document.createElement is 'object' in old IE
var EXISTS = isObject(document) && isObject(document.createElement);

module.exports = function (it) {
  return EXISTS ? document.createElement(it) : {};
};

},{"../internals/global-this":67,"../internals/is-object":84}],37:[function(require,module,exports){
'use strict';
var $TypeError = TypeError;
var MAX_SAFE_INTEGER = 0x1FFFFFFFFFFFFF; // 2 ** 53 - 1 == 9007199254740991

module.exports = function (it) {
  if (it > MAX_SAFE_INTEGER) throw $TypeError('Maximum allowed index exceeded');
  return it;
};

},{}],38:[function(require,module,exports){
'use strict';
// iterable DOM collections
// flag - `iterable` interface - 'entries', 'keys', 'values', 'forEach' methods
module.exports = {
  CSSRuleList: 0,
  CSSStyleDeclaration: 0,
  CSSValueList: 0,
  ClientRectList: 0,
  DOMRectList: 0,
  DOMStringList: 0,
  DOMTokenList: 1,
  DataTransferItemList: 0,
  FileList: 0,
  HTMLAllCollection: 0,
  HTMLCollection: 0,
  HTMLFormElement: 0,
  HTMLSelectElement: 0,
  MediaList: 0,
  MimeTypeArray: 0,
  NamedNodeMap: 0,
  NodeList: 1,
  PaintRequestList: 0,
  Plugin: 0,
  PluginArray: 0,
  SVGLengthList: 0,
  SVGNumberList: 0,
  SVGPathSegList: 0,
  SVGPointList: 0,
  SVGStringList: 0,
  SVGTransformList: 0,
  SourceBufferList: 0,
  StyleSheetList: 0,
  TextTrackCueList: 0,
  TextTrackList: 0,
  TouchList: 0
};

},{}],39:[function(require,module,exports){
'use strict';
// in old WebKit versions, `element.classList` is not an instance of global `DOMTokenList`
var documentCreateElement = require('../internals/document-create-element');

var classList = documentCreateElement('span').classList;
var DOMTokenListPrototype = classList && classList.constructor && classList.constructor.prototype;

module.exports = DOMTokenListPrototype === Object.prototype ? undefined : DOMTokenListPrototype;

},{"../internals/document-create-element":36}],40:[function(require,module,exports){
'use strict';
// IE8- don't enum bug keys
module.exports = [
  'constructor',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf'
];

},{}],41:[function(require,module,exports){
'use strict';
var ENVIRONMENT = require('../internals/environment');

module.exports = ENVIRONMENT === 'NODE';

},{"../internals/environment":44}],42:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');

var navigator = globalThis.navigator;
var userAgent = navigator && navigator.userAgent;

module.exports = userAgent ? String(userAgent) : '';

},{"../internals/global-this":67}],43:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var userAgent = require('../internals/environment-user-agent');

var process = globalThis.process;
var Deno = globalThis.Deno;
var versions = process && process.versions || Deno && Deno.version;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  // in old Chrome, versions of V8 isn't V8 = Chrome / 10
  // but their correct versions are not interesting for us
  version = match[0] > 0 && match[0] < 4 ? 1 : +(match[0] + match[1]);
}

// BrowserFS NodeJS `process` polyfill incorrectly set `.v8` to `0.0`
// so check `userAgent` even if `.v8` exists, but 0
if (!version && userAgent) {
  match = userAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = userAgent.match(/Chrome\/(\d+)/);
    if (match) version = +match[1];
  }
}

module.exports = version;

},{"../internals/environment-user-agent":42,"../internals/global-this":67}],44:[function(require,module,exports){
'use strict';
/* global Bun, Deno -- detection */
var globalThis = require('../internals/global-this');
var userAgent = require('../internals/environment-user-agent');
var classof = require('../internals/classof-raw');

var userAgentStartsWith = function (string) {
  return userAgent.slice(0, string.length) === string;
};

module.exports = (function () {
  if (userAgentStartsWith('Bun/')) return 'BUN';
  if (userAgentStartsWith('Cloudflare-Workers')) return 'CLOUDFLARE';
  if (userAgentStartsWith('Deno/')) return 'DENO';
  if (userAgentStartsWith('Node.js/')) return 'NODE';
  if (globalThis.Bun && typeof Bun.version == 'string') return 'BUN';
  if (globalThis.Deno && typeof Deno.version == 'object') return 'DENO';
  if (classof(globalThis.process) === 'process') return 'NODE';
  if (globalThis.window && globalThis.document) return 'BROWSER';
  return 'REST';
})();

},{"../internals/classof-raw":20,"../internals/environment-user-agent":42,"../internals/global-this":67}],45:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');

var $Error = Error;
var replace = uncurryThis(''.replace);

var TEST = (function (arg) { return String(new $Error(arg).stack); })('zxcasd');
// eslint-disable-next-line redos/no-vulnerable, sonarjs/slow-regex -- safe
var V8_OR_CHAKRA_STACK_ENTRY = /\n\s*at [^:]*:[^\n]*/;
var IS_V8_OR_CHAKRA_STACK = V8_OR_CHAKRA_STACK_ENTRY.test(TEST);

module.exports = function (stack, dropEntries) {
  if (IS_V8_OR_CHAKRA_STACK && typeof stack == 'string' && !$Error.prepareStackTrace) {
    while (dropEntries--) stack = replace(stack, V8_OR_CHAKRA_STACK_ENTRY, '');
  } return stack;
};

},{"../internals/function-uncurry-this":59}],46:[function(require,module,exports){
'use strict';
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var clearErrorStack = require('../internals/error-stack-clear');
var ERROR_STACK_INSTALLABLE = require('../internals/error-stack-installable');

// non-standard V8
var captureStackTrace = Error.captureStackTrace;

module.exports = function (error, C, stack, dropEntries) {
  if (ERROR_STACK_INSTALLABLE) {
    if (captureStackTrace) captureStackTrace(error, C);
    else createNonEnumerableProperty(error, 'stack', clearErrorStack(stack, dropEntries));
  }
};

},{"../internals/create-non-enumerable-property":28,"../internals/error-stack-clear":45,"../internals/error-stack-installable":47}],47:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var createPropertyDescriptor = require('../internals/create-property-descriptor');

module.exports = !fails(function () {
  var error = new Error('a');
  if (!('stack' in error)) return true;
  // eslint-disable-next-line es/no-object-defineproperty -- safe
  Object.defineProperty(error, 'stack', createPropertyDescriptor(1, 7));
  return error.stack !== 7;
});

},{"../internals/create-property-descriptor":29,"../internals/fails":49}],48:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var getOwnPropertyDescriptor = require('../internals/object-get-own-property-descriptor').f;
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var defineBuiltIn = require('../internals/define-built-in');
var defineGlobalProperty = require('../internals/define-global-property');
var copyConstructorProperties = require('../internals/copy-constructor-properties');
var isForced = require('../internals/is-forced');

/*
  options.target         - name of the target object
  options.global         - target is the global object
  options.stat           - export as static methods of target
  options.proto          - export as prototype methods of target
  options.real           - real prototype method for the `pure` version
  options.forced         - export even if the native feature is available
  options.bind           - bind methods to the target, required for the `pure` version
  options.wrap           - wrap constructors to preventing global pollution, required for the `pure` version
  options.unsafe         - use the simple assignment of property instead of delete + defineProperty
  options.sham           - add a flag to not completely full polyfills
  options.enumerable     - export as enumerable property
  options.dontCallGetSet - prevent calling a getter on target
  options.name           - the .name of the function if it does not match the key
*/
module.exports = function (options, source) {
  var TARGET = options.target;
  var GLOBAL = options.global;
  var STATIC = options.stat;
  var FORCED, target, key, targetProperty, sourceProperty, descriptor;
  if (GLOBAL) {
    target = globalThis;
  } else if (STATIC) {
    target = globalThis[TARGET] || defineGlobalProperty(TARGET, {});
  } else {
    target = globalThis[TARGET] && globalThis[TARGET].prototype;
  }
  if (target) for (key in source) {
    sourceProperty = source[key];
    if (options.dontCallGetSet) {
      descriptor = getOwnPropertyDescriptor(target, key);
      targetProperty = descriptor && descriptor.value;
    } else targetProperty = target[key];
    FORCED = isForced(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
    // contained in target
    if (!FORCED && targetProperty !== undefined) {
      if (typeof sourceProperty == typeof targetProperty) continue;
      copyConstructorProperties(sourceProperty, targetProperty);
    }
    // add a flag to not completely full polyfills
    if (options.sham || (targetProperty && targetProperty.sham)) {
      createNonEnumerableProperty(sourceProperty, 'sham', true);
    }
    defineBuiltIn(target, key, sourceProperty, options);
  }
};

},{"../internals/copy-constructor-properties":24,"../internals/create-non-enumerable-property":28,"../internals/define-built-in":32,"../internals/define-global-property":34,"../internals/global-this":67,"../internals/is-forced":82,"../internals/object-get-own-property-descriptor":106}],49:[function(require,module,exports){
'use strict';
module.exports = function (exec) {
  try {
    return !!exec();
  } catch (error) {
    return true;
  }
};

},{}],50:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4` since it's moved to entry points
require('../modules/es.regexp.exec');
var call = require('../internals/function-call');
var defineBuiltIn = require('../internals/define-built-in');
var regexpExec = require('../internals/regexp-exec');
var fails = require('../internals/fails');
var wellKnownSymbol = require('../internals/well-known-symbol');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');

var SPECIES = wellKnownSymbol('species');
var RegExpPrototype = RegExp.prototype;

module.exports = function (KEY, exec, FORCED, SHAM) {
  var SYMBOL = wellKnownSymbol(KEY);

  var DELEGATES_TO_SYMBOL = !fails(function () {
    // String methods call symbol-named RegExp methods
    var O = {};
    O[SYMBOL] = function () { return 7; };
    return ''[KEY](O) !== 7;
  });

  var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails(function () {
    // Symbol-named RegExp methods call .exec
    var execCalled = false;
    var re = /a/;

    if (KEY === 'split') {
      // We can't use real regex here since it causes deoptimization
      // and serious performance degradation in V8
      // https://github.com/zloirock/core-js/issues/306
      re = {};
      // RegExp[@@split] doesn't call the regex's exec method, but first creates
      // a new one. We need to return the patched regex when creating the new one.
      re.constructor = {};
      re.constructor[SPECIES] = function () { return re; };
      re.flags = '';
      re[SYMBOL] = /./[SYMBOL];
    }

    re.exec = function () {
      execCalled = true;
      return null;
    };

    re[SYMBOL]('');
    return !execCalled;
  });

  if (
    !DELEGATES_TO_SYMBOL ||
    !DELEGATES_TO_EXEC ||
    FORCED
  ) {
    var nativeRegExpMethod = /./[SYMBOL];
    var methods = exec(SYMBOL, ''[KEY], function (nativeMethod, regexp, str, arg2, forceStringMethod) {
      var $exec = regexp.exec;
      if ($exec === regexpExec || $exec === RegExpPrototype.exec) {
        if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
          // The native String method already delegates to @@method (this
          // polyfilled function), leasing to infinite recursion.
          // We avoid it by directly calling the native @@method method.
          return { done: true, value: call(nativeRegExpMethod, regexp, str, arg2) };
        }
        return { done: true, value: call(nativeMethod, str, regexp, arg2) };
      }
      return { done: false };
    });

    defineBuiltIn(String.prototype, KEY, methods[0]);
    defineBuiltIn(RegExpPrototype, SYMBOL, methods[1]);
  }

  if (SHAM) createNonEnumerableProperty(RegExpPrototype[SYMBOL], 'sham', true);
};

},{"../internals/create-non-enumerable-property":28,"../internals/define-built-in":32,"../internals/fails":49,"../internals/function-call":55,"../internals/regexp-exec":121,"../internals/well-known-symbol":150,"../modules/es.regexp.exec":167}],51:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');

module.exports = !fails(function () {
  // eslint-disable-next-line es/no-object-isextensible, es/no-object-preventextensions -- required for testing
  return Object.isExtensible(Object.preventExtensions({}));
});

},{"../internals/fails":49}],52:[function(require,module,exports){
'use strict';
var NATIVE_BIND = require('../internals/function-bind-native');

var FunctionPrototype = Function.prototype;
var apply = FunctionPrototype.apply;
var call = FunctionPrototype.call;

// eslint-disable-next-line es/no-reflect -- safe
module.exports = typeof Reflect == 'object' && Reflect.apply || (NATIVE_BIND ? call.bind(apply) : function () {
  return call.apply(apply, arguments);
});

},{"../internals/function-bind-native":54}],53:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this-clause');
var aCallable = require('../internals/a-callable');
var NATIVE_BIND = require('../internals/function-bind-native');

var bind = uncurryThis(uncurryThis.bind);

// optional / simple context binding
module.exports = function (fn, that) {
  aCallable(fn);
  return that === undefined ? fn : NATIVE_BIND ? bind(fn, that) : function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

},{"../internals/a-callable":2,"../internals/function-bind-native":54,"../internals/function-uncurry-this-clause":58}],54:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');

module.exports = !fails(function () {
  // eslint-disable-next-line es/no-function-prototype-bind -- safe
  var test = (function () { /* empty */ }).bind();
  // eslint-disable-next-line no-prototype-builtins -- safe
  return typeof test != 'function' || test.hasOwnProperty('prototype');
});

},{"../internals/fails":49}],55:[function(require,module,exports){
'use strict';
var NATIVE_BIND = require('../internals/function-bind-native');

var call = Function.prototype.call;

module.exports = NATIVE_BIND ? call.bind(call) : function () {
  return call.apply(call, arguments);
};

},{"../internals/function-bind-native":54}],56:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var hasOwn = require('../internals/has-own-property');

var FunctionPrototype = Function.prototype;
// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var getDescriptor = DESCRIPTORS && Object.getOwnPropertyDescriptor;

var EXISTS = hasOwn(FunctionPrototype, 'name');
// additional protection from minified / mangled / dropped function names
var PROPER = EXISTS && (function something() { /* empty */ }).name === 'something';
var CONFIGURABLE = EXISTS && (!DESCRIPTORS || (DESCRIPTORS && getDescriptor(FunctionPrototype, 'name').configurable));

module.exports = {
  EXISTS: EXISTS,
  PROPER: PROPER,
  CONFIGURABLE: CONFIGURABLE
};

},{"../internals/descriptors":35,"../internals/has-own-property":68}],57:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var aCallable = require('../internals/a-callable');

module.exports = function (object, key, method) {
  try {
    // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
    return uncurryThis(aCallable(Object.getOwnPropertyDescriptor(object, key)[method]));
  } catch (error) { /* empty */ }
};

},{"../internals/a-callable":2,"../internals/function-uncurry-this":59}],58:[function(require,module,exports){
'use strict';
var classofRaw = require('../internals/classof-raw');
var uncurryThis = require('../internals/function-uncurry-this');

module.exports = function (fn) {
  // Nashorn bug:
  //   https://github.com/zloirock/core-js/issues/1128
  //   https://github.com/zloirock/core-js/issues/1130
  if (classofRaw(fn) === 'Function') return uncurryThis(fn);
};

},{"../internals/classof-raw":20,"../internals/function-uncurry-this":59}],59:[function(require,module,exports){
'use strict';
var NATIVE_BIND = require('../internals/function-bind-native');

var FunctionPrototype = Function.prototype;
var call = FunctionPrototype.call;
var uncurryThisWithBind = NATIVE_BIND && FunctionPrototype.bind.bind(call, call);

module.exports = NATIVE_BIND ? uncurryThisWithBind : function (fn) {
  return function () {
    return call.apply(fn, arguments);
  };
};

},{"../internals/function-bind-native":54}],60:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var isCallable = require('../internals/is-callable');

var aFunction = function (argument) {
  return isCallable(argument) ? argument : undefined;
};

module.exports = function (namespace, method) {
  return arguments.length < 2 ? aFunction(globalThis[namespace]) : globalThis[namespace] && globalThis[namespace][method];
};

},{"../internals/global-this":67,"../internals/is-callable":80}],61:[function(require,module,exports){
'use strict';
// `GetIteratorDirect(obj)` abstract operation
// https://tc39.es/proposal-iterator-helpers/#sec-getiteratordirect
module.exports = function (obj) {
  return {
    iterator: obj,
    next: obj.next,
    done: false
  };
};

},{}],62:[function(require,module,exports){
'use strict';
var classof = require('../internals/classof');
var getMethod = require('../internals/get-method');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var Iterators = require('../internals/iterators');
var wellKnownSymbol = require('../internals/well-known-symbol');

var ITERATOR = wellKnownSymbol('iterator');

module.exports = function (it) {
  if (!isNullOrUndefined(it)) return getMethod(it, ITERATOR)
    || getMethod(it, '@@iterator')
    || Iterators[classof(it)];
};

},{"../internals/classof":21,"../internals/get-method":65,"../internals/is-null-or-undefined":83,"../internals/iterators":96,"../internals/well-known-symbol":150}],63:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var tryToString = require('../internals/try-to-string');
var getIteratorMethod = require('../internals/get-iterator-method');

var $TypeError = TypeError;

module.exports = function (argument, usingIterator) {
  var iteratorMethod = arguments.length < 2 ? getIteratorMethod(argument) : usingIterator;
  if (aCallable(iteratorMethod)) return anObject(call(iteratorMethod, argument));
  throw new $TypeError(tryToString(argument) + ' is not iterable');
};

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/function-call":55,"../internals/get-iterator-method":62,"../internals/try-to-string":145}],64:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var isArray = require('../internals/is-array');
var isCallable = require('../internals/is-callable');
var classof = require('../internals/classof-raw');
var toString = require('../internals/to-string');

var push = uncurryThis([].push);

module.exports = function (replacer) {
  if (isCallable(replacer)) return replacer;
  if (!isArray(replacer)) return;
  var rawLength = replacer.length;
  var keys = [];
  for (var i = 0; i < rawLength; i++) {
    var element = replacer[i];
    if (typeof element == 'string') push(keys, element);
    else if (typeof element == 'number' || classof(element) === 'Number' || classof(element) === 'String') push(keys, toString(element));
  }
  var keysLength = keys.length;
  var root = true;
  return function (key, value) {
    if (root) {
      root = false;
      return value;
    }
    if (isArray(this)) return value;
    for (var j = 0; j < keysLength; j++) if (keys[j] === key) return value;
  };
};

},{"../internals/classof-raw":20,"../internals/function-uncurry-this":59,"../internals/is-array":79,"../internals/is-callable":80,"../internals/to-string":144}],65:[function(require,module,exports){
'use strict';
var aCallable = require('../internals/a-callable');
var isNullOrUndefined = require('../internals/is-null-or-undefined');

// `GetMethod` abstract operation
// https://tc39.es/ecma262/#sec-getmethod
module.exports = function (V, P) {
  var func = V[P];
  return isNullOrUndefined(func) ? undefined : aCallable(func);
};

},{"../internals/a-callable":2,"../internals/is-null-or-undefined":83}],66:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var toObject = require('../internals/to-object');

var floor = Math.floor;
var charAt = uncurryThis(''.charAt);
var replace = uncurryThis(''.replace);
var stringSlice = uncurryThis(''.slice);
// eslint-disable-next-line redos/no-vulnerable -- safe
var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d{1,2}|<[^>]*>)/g;
var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d{1,2})/g;

// `GetSubstitution` abstract operation
// https://tc39.es/ecma262/#sec-getsubstitution
module.exports = function (matched, str, position, captures, namedCaptures, replacement) {
  var tailPos = position + matched.length;
  var m = captures.length;
  var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
  if (namedCaptures !== undefined) {
    namedCaptures = toObject(namedCaptures);
    symbols = SUBSTITUTION_SYMBOLS;
  }
  return replace(replacement, symbols, function (match, ch) {
    var capture;
    switch (charAt(ch, 0)) {
      case '$': return '$';
      case '&': return matched;
      case '`': return stringSlice(str, 0, position);
      case "'": return stringSlice(str, tailPos);
      case '<':
        capture = namedCaptures[stringSlice(ch, 1, -1)];
        break;
      default: // \d\d?
        var n = +ch;
        if (n === 0) return match;
        if (n > m) {
          var f = floor(n / 10);
          if (f === 0) return match;
          if (f <= m) return captures[f - 1] === undefined ? charAt(ch, 1) : captures[f - 1] + charAt(ch, 1);
          return match;
        }
        capture = captures[n - 1];
    }
    return capture === undefined ? '' : capture;
  });
};

},{"../internals/function-uncurry-this":59,"../internals/to-object":140}],67:[function(require,module,exports){
(function (global){(function (){
'use strict';
var check = function (it) {
  return it && it.Math === Math && it;
};

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
module.exports =
  // eslint-disable-next-line es/no-global-this -- safe
  check(typeof globalThis == 'object' && globalThis) ||
  check(typeof window == 'object' && window) ||
  // eslint-disable-next-line no-restricted-globals -- safe
  check(typeof self == 'object' && self) ||
  check(typeof global == 'object' && global) ||
  check(typeof this == 'object' && this) ||
  // eslint-disable-next-line no-new-func -- fallback
  (function () { return this; })() || Function('return this')();

}).call(this)}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],68:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var toObject = require('../internals/to-object');

var hasOwnProperty = uncurryThis({}.hasOwnProperty);

// `HasOwnProperty` abstract operation
// https://tc39.es/ecma262/#sec-hasownproperty
// eslint-disable-next-line es/no-object-hasown -- safe
module.exports = Object.hasOwn || function hasOwn(it, key) {
  return hasOwnProperty(toObject(it), key);
};

},{"../internals/function-uncurry-this":59,"../internals/to-object":140}],69:[function(require,module,exports){
'use strict';
module.exports = {};

},{}],70:[function(require,module,exports){
'use strict';
var getBuiltIn = require('../internals/get-built-in');

module.exports = getBuiltIn('document', 'documentElement');

},{"../internals/get-built-in":60}],71:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var fails = require('../internals/fails');
var createElement = require('../internals/document-create-element');

// Thanks to IE8 for its funny defineProperty
module.exports = !DESCRIPTORS && !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  return Object.defineProperty(createElement('div'), 'a', {
    get: function () { return 7; }
  }).a !== 7;
});

},{"../internals/descriptors":35,"../internals/document-create-element":36,"../internals/fails":49}],72:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var fails = require('../internals/fails');
var classof = require('../internals/classof-raw');

var $Object = Object;
var split = uncurryThis(''.split);

// fallback for non-array-like ES3 and non-enumerable old V8 strings
module.exports = fails(function () {
  // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
  // eslint-disable-next-line no-prototype-builtins -- safe
  return !$Object('z').propertyIsEnumerable(0);
}) ? function (it) {
  return classof(it) === 'String' ? split(it, '') : $Object(it);
} : $Object;

},{"../internals/classof-raw":20,"../internals/fails":49,"../internals/function-uncurry-this":59}],73:[function(require,module,exports){
'use strict';
var isCallable = require('../internals/is-callable');
var isObject = require('../internals/is-object');
var setPrototypeOf = require('../internals/object-set-prototype-of');

// makes subclassing work correct for wrapped built-ins
module.exports = function ($this, dummy, Wrapper) {
  var NewTarget, NewTargetPrototype;
  if (
    // it can work only with native `setPrototypeOf`
    setPrototypeOf &&
    // we haven't completely correct pre-ES6 way for getting `new.target`, so use this
    isCallable(NewTarget = dummy.constructor) &&
    NewTarget !== Wrapper &&
    isObject(NewTargetPrototype = NewTarget.prototype) &&
    NewTargetPrototype !== Wrapper.prototype
  ) setPrototypeOf($this, NewTargetPrototype);
  return $this;
};

},{"../internals/is-callable":80,"../internals/is-object":84,"../internals/object-set-prototype-of":116}],74:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var isCallable = require('../internals/is-callable');
var store = require('../internals/shared-store');

var functionToString = uncurryThis(Function.toString);

// this helper broken in `core-js@3.4.1-3.4.4`, so we can't use `shared` helper
if (!isCallable(store.inspectSource)) {
  store.inspectSource = function (it) {
    return functionToString(it);
  };
}

module.exports = store.inspectSource;

},{"../internals/function-uncurry-this":59,"../internals/is-callable":80,"../internals/shared-store":130}],75:[function(require,module,exports){
'use strict';
var isObject = require('../internals/is-object');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');

// `InstallErrorCause` abstract operation
// https://tc39.es/proposal-error-cause/#sec-errorobjects-install-error-cause
module.exports = function (O, options) {
  if (isObject(options) && 'cause' in options) {
    createNonEnumerableProperty(O, 'cause', options.cause);
  }
};

},{"../internals/create-non-enumerable-property":28,"../internals/is-object":84}],76:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var uncurryThis = require('../internals/function-uncurry-this');
var hiddenKeys = require('../internals/hidden-keys');
var isObject = require('../internals/is-object');
var hasOwn = require('../internals/has-own-property');
var defineProperty = require('../internals/object-define-property').f;
var getOwnPropertyNamesModule = require('../internals/object-get-own-property-names');
var getOwnPropertyNamesExternalModule = require('../internals/object-get-own-property-names-external');
var isExtensible = require('../internals/object-is-extensible');
var uid = require('../internals/uid');
var FREEZING = require('../internals/freezing');

var REQUIRED = false;
var METADATA = uid('meta');
var id = 0;

var setMetadata = function (it) {
  defineProperty(it, METADATA, { value: {
    objectID: 'O' + id++, // object ID
    weakData: {}          // weak collections IDs
  } });
};

var fastKey = function (it, create) {
  // return a primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!hasOwn(it, METADATA)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMetadata(it);
  // return object ID
  } return it[METADATA].objectID;
};

var getWeakData = function (it, create) {
  if (!hasOwn(it, METADATA)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMetadata(it);
  // return the store of weak collections IDs
  } return it[METADATA].weakData;
};

// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZING && REQUIRED && isExtensible(it) && !hasOwn(it, METADATA)) setMetadata(it);
  return it;
};

var enable = function () {
  meta.enable = function () { /* empty */ };
  REQUIRED = true;
  var getOwnPropertyNames = getOwnPropertyNamesModule.f;
  var splice = uncurryThis([].splice);
  var test = {};
  test[METADATA] = 1;

  // prevent exposing of metadata key
  if (getOwnPropertyNames(test).length) {
    getOwnPropertyNamesModule.f = function (it) {
      var result = getOwnPropertyNames(it);
      for (var i = 0, length = result.length; i < length; i++) {
        if (result[i] === METADATA) {
          splice(result, i, 1);
          break;
        }
      } return result;
    };

    $({ target: 'Object', stat: true, forced: true }, {
      getOwnPropertyNames: getOwnPropertyNamesExternalModule.f
    });
  }
};

var meta = module.exports = {
  enable: enable,
  fastKey: fastKey,
  getWeakData: getWeakData,
  onFreeze: onFreeze
};

hiddenKeys[METADATA] = true;

},{"../internals/export":48,"../internals/freezing":51,"../internals/function-uncurry-this":59,"../internals/has-own-property":68,"../internals/hidden-keys":69,"../internals/is-object":84,"../internals/object-define-property":105,"../internals/object-get-own-property-names":108,"../internals/object-get-own-property-names-external":107,"../internals/object-is-extensible":111,"../internals/uid":146}],77:[function(require,module,exports){
'use strict';
var NATIVE_WEAK_MAP = require('../internals/weak-map-basic-detection');
var globalThis = require('../internals/global-this');
var isObject = require('../internals/is-object');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var hasOwn = require('../internals/has-own-property');
var shared = require('../internals/shared-store');
var sharedKey = require('../internals/shared-key');
var hiddenKeys = require('../internals/hidden-keys');

var OBJECT_ALREADY_INITIALIZED = 'Object already initialized';
var TypeError = globalThis.TypeError;
var WeakMap = globalThis.WeakMap;
var set, get, has;

var enforce = function (it) {
  return has(it) ? get(it) : set(it, {});
};

var getterFor = function (TYPE) {
  return function (it) {
    var state;
    if (!isObject(it) || (state = get(it)).type !== TYPE) {
      throw new TypeError('Incompatible receiver, ' + TYPE + ' required');
    } return state;
  };
};

if (NATIVE_WEAK_MAP || shared.state) {
  var store = shared.state || (shared.state = new WeakMap());
  /* eslint-disable no-self-assign -- prototype methods protection */
  store.get = store.get;
  store.has = store.has;
  store.set = store.set;
  /* eslint-enable no-self-assign -- prototype methods protection */
  set = function (it, metadata) {
    if (store.has(it)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
    metadata.facade = it;
    store.set(it, metadata);
    return metadata;
  };
  get = function (it) {
    return store.get(it) || {};
  };
  has = function (it) {
    return store.has(it);
  };
} else {
  var STATE = sharedKey('state');
  hiddenKeys[STATE] = true;
  set = function (it, metadata) {
    if (hasOwn(it, STATE)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
    metadata.facade = it;
    createNonEnumerableProperty(it, STATE, metadata);
    return metadata;
  };
  get = function (it) {
    return hasOwn(it, STATE) ? it[STATE] : {};
  };
  has = function (it) {
    return hasOwn(it, STATE);
  };
}

module.exports = {
  set: set,
  get: get,
  has: has,
  enforce: enforce,
  getterFor: getterFor
};

},{"../internals/create-non-enumerable-property":28,"../internals/global-this":67,"../internals/has-own-property":68,"../internals/hidden-keys":69,"../internals/is-object":84,"../internals/shared-key":129,"../internals/shared-store":130,"../internals/weak-map-basic-detection":149}],78:[function(require,module,exports){
'use strict';
var wellKnownSymbol = require('../internals/well-known-symbol');
var Iterators = require('../internals/iterators');

var ITERATOR = wellKnownSymbol('iterator');
var ArrayPrototype = Array.prototype;

// check on default Array iterator
module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayPrototype[ITERATOR] === it);
};

},{"../internals/iterators":96,"../internals/well-known-symbol":150}],79:[function(require,module,exports){
'use strict';
var classof = require('../internals/classof-raw');

// `IsArray` abstract operation
// https://tc39.es/ecma262/#sec-isarray
// eslint-disable-next-line es/no-array-isarray -- safe
module.exports = Array.isArray || function isArray(argument) {
  return classof(argument) === 'Array';
};

},{"../internals/classof-raw":20}],80:[function(require,module,exports){
'use strict';
// https://tc39.es/ecma262/#sec-IsHTMLDDA-internal-slot
var documentAll = typeof document == 'object' && document.all;

// `IsCallable` abstract operation
// https://tc39.es/ecma262/#sec-iscallable
// eslint-disable-next-line unicorn/no-typeof-undefined -- required for testing
module.exports = typeof documentAll == 'undefined' && documentAll !== undefined ? function (argument) {
  return typeof argument == 'function' || argument === documentAll;
} : function (argument) {
  return typeof argument == 'function';
};

},{}],81:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var fails = require('../internals/fails');
var isCallable = require('../internals/is-callable');
var classof = require('../internals/classof');
var getBuiltIn = require('../internals/get-built-in');
var inspectSource = require('../internals/inspect-source');

var noop = function () { /* empty */ };
var construct = getBuiltIn('Reflect', 'construct');
var constructorRegExp = /^\s*(?:class|function)\b/;
var exec = uncurryThis(constructorRegExp.exec);
var INCORRECT_TO_STRING = !constructorRegExp.test(noop);

var isConstructorModern = function isConstructor(argument) {
  if (!isCallable(argument)) return false;
  try {
    construct(noop, [], argument);
    return true;
  } catch (error) {
    return false;
  }
};

var isConstructorLegacy = function isConstructor(argument) {
  if (!isCallable(argument)) return false;
  switch (classof(argument)) {
    case 'AsyncFunction':
    case 'GeneratorFunction':
    case 'AsyncGeneratorFunction': return false;
  }
  try {
    // we can't check .prototype since constructors produced by .bind haven't it
    // `Function#toString` throws on some built-it function in some legacy engines
    // (for example, `DOMQuad` and similar in FF41-)
    return INCORRECT_TO_STRING || !!exec(constructorRegExp, inspectSource(argument));
  } catch (error) {
    return true;
  }
};

isConstructorLegacy.sham = true;

// `IsConstructor` abstract operation
// https://tc39.es/ecma262/#sec-isconstructor
module.exports = !construct || fails(function () {
  var called;
  return isConstructorModern(isConstructorModern.call)
    || !isConstructorModern(Object)
    || !isConstructorModern(function () { called = true; })
    || called;
}) ? isConstructorLegacy : isConstructorModern;

},{"../internals/classof":21,"../internals/fails":49,"../internals/function-uncurry-this":59,"../internals/get-built-in":60,"../internals/inspect-source":74,"../internals/is-callable":80}],82:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var isCallable = require('../internals/is-callable');

var replacement = /#|\.prototype\./;

var isForced = function (feature, detection) {
  var value = data[normalize(feature)];
  return value === POLYFILL ? true
    : value === NATIVE ? false
    : isCallable(detection) ? fails(detection)
    : !!detection;
};

var normalize = isForced.normalize = function (string) {
  return String(string).replace(replacement, '.').toLowerCase();
};

var data = isForced.data = {};
var NATIVE = isForced.NATIVE = 'N';
var POLYFILL = isForced.POLYFILL = 'P';

module.exports = isForced;

},{"../internals/fails":49,"../internals/is-callable":80}],83:[function(require,module,exports){
'use strict';
// we can't use just `it == null` since of `document.all` special case
// https://tc39.es/ecma262/#sec-IsHTMLDDA-internal-slot-aec
module.exports = function (it) {
  return it === null || it === undefined;
};

},{}],84:[function(require,module,exports){
'use strict';
var isCallable = require('../internals/is-callable');

module.exports = function (it) {
  return typeof it == 'object' ? it !== null : isCallable(it);
};

},{"../internals/is-callable":80}],85:[function(require,module,exports){
'use strict';
var isObject = require('../internals/is-object');

module.exports = function (argument) {
  return isObject(argument) || argument === null;
};

},{"../internals/is-object":84}],86:[function(require,module,exports){
'use strict';
module.exports = false;

},{}],87:[function(require,module,exports){
'use strict';
var isObject = require('../internals/is-object');
var classof = require('../internals/classof-raw');
var wellKnownSymbol = require('../internals/well-known-symbol');

var MATCH = wellKnownSymbol('match');

// `IsRegExp` abstract operation
// https://tc39.es/ecma262/#sec-isregexp
module.exports = function (it) {
  var isRegExp;
  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : classof(it) === 'RegExp');
};

},{"../internals/classof-raw":20,"../internals/is-object":84,"../internals/well-known-symbol":150}],88:[function(require,module,exports){
'use strict';
var getBuiltIn = require('../internals/get-built-in');
var isCallable = require('../internals/is-callable');
var isPrototypeOf = require('../internals/object-is-prototype-of');
var USE_SYMBOL_AS_UID = require('../internals/use-symbol-as-uid');

var $Object = Object;

module.exports = USE_SYMBOL_AS_UID ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  var $Symbol = getBuiltIn('Symbol');
  return isCallable($Symbol) && isPrototypeOf($Symbol.prototype, $Object(it));
};

},{"../internals/get-built-in":60,"../internals/is-callable":80,"../internals/object-is-prototype-of":112,"../internals/use-symbol-as-uid":147}],89:[function(require,module,exports){
'use strict';
var bind = require('../internals/function-bind-context');
var call = require('../internals/function-call');
var anObject = require('../internals/an-object');
var tryToString = require('../internals/try-to-string');
var isArrayIteratorMethod = require('../internals/is-array-iterator-method');
var lengthOfArrayLike = require('../internals/length-of-array-like');
var isPrototypeOf = require('../internals/object-is-prototype-of');
var getIterator = require('../internals/get-iterator');
var getIteratorMethod = require('../internals/get-iterator-method');
var iteratorClose = require('../internals/iterator-close');

var $TypeError = TypeError;

var Result = function (stopped, result) {
  this.stopped = stopped;
  this.result = result;
};

var ResultPrototype = Result.prototype;

module.exports = function (iterable, unboundFunction, options) {
  var that = options && options.that;
  var AS_ENTRIES = !!(options && options.AS_ENTRIES);
  var IS_RECORD = !!(options && options.IS_RECORD);
  var IS_ITERATOR = !!(options && options.IS_ITERATOR);
  var INTERRUPTED = !!(options && options.INTERRUPTED);
  var fn = bind(unboundFunction, that);
  var iterator, iterFn, index, length, result, next, step;

  var stop = function (condition) {
    if (iterator) iteratorClose(iterator, 'normal', condition);
    return new Result(true, condition);
  };

  var callFn = function (value) {
    if (AS_ENTRIES) {
      anObject(value);
      return INTERRUPTED ? fn(value[0], value[1], stop) : fn(value[0], value[1]);
    } return INTERRUPTED ? fn(value, stop) : fn(value);
  };

  if (IS_RECORD) {
    iterator = iterable.iterator;
  } else if (IS_ITERATOR) {
    iterator = iterable;
  } else {
    iterFn = getIteratorMethod(iterable);
    if (!iterFn) throw new $TypeError(tryToString(iterable) + ' is not iterable');
    // optimisation for array iterators
    if (isArrayIteratorMethod(iterFn)) {
      for (index = 0, length = lengthOfArrayLike(iterable); length > index; index++) {
        result = callFn(iterable[index]);
        if (result && isPrototypeOf(ResultPrototype, result)) return result;
      } return new Result(false);
    }
    iterator = getIterator(iterable, iterFn);
  }

  next = IS_RECORD ? iterable.next : iterator.next;
  while (!(step = call(next, iterator)).done) {
    try {
      result = callFn(step.value);
    } catch (error) {
      iteratorClose(iterator, 'throw', error);
    }
    if (typeof result == 'object' && result && isPrototypeOf(ResultPrototype, result)) return result;
  } return new Result(false);
};

},{"../internals/an-object":8,"../internals/function-bind-context":53,"../internals/function-call":55,"../internals/get-iterator":63,"../internals/get-iterator-method":62,"../internals/is-array-iterator-method":78,"../internals/iterator-close":90,"../internals/length-of-array-like":97,"../internals/object-is-prototype-of":112,"../internals/try-to-string":145}],90:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var anObject = require('../internals/an-object');
var getMethod = require('../internals/get-method');

module.exports = function (iterator, kind, value) {
  var innerResult, innerError;
  anObject(iterator);
  try {
    innerResult = getMethod(iterator, 'return');
    if (!innerResult) {
      if (kind === 'throw') throw value;
      return value;
    }
    innerResult = call(innerResult, iterator);
  } catch (error) {
    innerError = true;
    innerResult = error;
  }
  if (kind === 'throw') throw value;
  if (innerError) throw innerResult;
  anObject(innerResult);
  return value;
};

},{"../internals/an-object":8,"../internals/function-call":55,"../internals/get-method":65}],91:[function(require,module,exports){
'use strict';
var IteratorPrototype = require('../internals/iterators-core').IteratorPrototype;
var create = require('../internals/object-create');
var createPropertyDescriptor = require('../internals/create-property-descriptor');
var setToStringTag = require('../internals/set-to-string-tag');
var Iterators = require('../internals/iterators');

var returnThis = function () { return this; };

module.exports = function (IteratorConstructor, NAME, next, ENUMERABLE_NEXT) {
  var TO_STRING_TAG = NAME + ' Iterator';
  IteratorConstructor.prototype = create(IteratorPrototype, { next: createPropertyDescriptor(+!ENUMERABLE_NEXT, next) });
  setToStringTag(IteratorConstructor, TO_STRING_TAG, false, true);
  Iterators[TO_STRING_TAG] = returnThis;
  return IteratorConstructor;
};

},{"../internals/create-property-descriptor":29,"../internals/iterators":96,"../internals/iterators-core":95,"../internals/object-create":103,"../internals/set-to-string-tag":128}],92:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var create = require('../internals/object-create');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var defineBuiltIns = require('../internals/define-built-ins');
var wellKnownSymbol = require('../internals/well-known-symbol');
var InternalStateModule = require('../internals/internal-state');
var getMethod = require('../internals/get-method');
var IteratorPrototype = require('../internals/iterators-core').IteratorPrototype;
var createIterResultObject = require('../internals/create-iter-result-object');
var iteratorClose = require('../internals/iterator-close');

var TO_STRING_TAG = wellKnownSymbol('toStringTag');
var ITERATOR_HELPER = 'IteratorHelper';
var WRAP_FOR_VALID_ITERATOR = 'WrapForValidIterator';
var setInternalState = InternalStateModule.set;

var createIteratorProxyPrototype = function (IS_ITERATOR) {
  var getInternalState = InternalStateModule.getterFor(IS_ITERATOR ? WRAP_FOR_VALID_ITERATOR : ITERATOR_HELPER);

  return defineBuiltIns(create(IteratorPrototype), {
    next: function next() {
      var state = getInternalState(this);
      // for simplification:
      //   for `%WrapForValidIteratorPrototype%.next` our `nextHandler` returns `IterResultObject`
      //   for `%IteratorHelperPrototype%.next` - just a value
      if (IS_ITERATOR) return state.nextHandler();
      try {
        var result = state.done ? undefined : state.nextHandler();
        return createIterResultObject(result, state.done);
      } catch (error) {
        state.done = true;
        throw error;
      }
    },
    'return': function () {
      var state = getInternalState(this);
      var iterator = state.iterator;
      state.done = true;
      if (IS_ITERATOR) {
        var returnMethod = getMethod(iterator, 'return');
        return returnMethod ? call(returnMethod, iterator) : createIterResultObject(undefined, true);
      }
      if (state.inner) try {
        iteratorClose(state.inner.iterator, 'normal');
      } catch (error) {
        return iteratorClose(iterator, 'throw', error);
      }
      if (iterator) iteratorClose(iterator, 'normal');
      return createIterResultObject(undefined, true);
    }
  });
};

var WrapForValidIteratorPrototype = createIteratorProxyPrototype(true);
var IteratorHelperPrototype = createIteratorProxyPrototype(false);

createNonEnumerableProperty(IteratorHelperPrototype, TO_STRING_TAG, 'Iterator Helper');

module.exports = function (nextHandler, IS_ITERATOR) {
  var IteratorProxy = function Iterator(record, state) {
    if (state) {
      state.iterator = record.iterator;
      state.next = record.next;
    } else state = record;
    state.type = IS_ITERATOR ? WRAP_FOR_VALID_ITERATOR : ITERATOR_HELPER;
    state.nextHandler = nextHandler;
    state.counter = 0;
    state.done = false;
    setInternalState(this, state);
  };

  IteratorProxy.prototype = IS_ITERATOR ? WrapForValidIteratorPrototype : IteratorHelperPrototype;

  return IteratorProxy;
};

},{"../internals/create-iter-result-object":27,"../internals/create-non-enumerable-property":28,"../internals/define-built-ins":33,"../internals/function-call":55,"../internals/get-method":65,"../internals/internal-state":77,"../internals/iterator-close":90,"../internals/iterators-core":95,"../internals/object-create":103,"../internals/well-known-symbol":150}],93:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var call = require('../internals/function-call');
var IS_PURE = require('../internals/is-pure');
var FunctionName = require('../internals/function-name');
var isCallable = require('../internals/is-callable');
var createIteratorConstructor = require('../internals/iterator-create-constructor');
var getPrototypeOf = require('../internals/object-get-prototype-of');
var setPrototypeOf = require('../internals/object-set-prototype-of');
var setToStringTag = require('../internals/set-to-string-tag');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var defineBuiltIn = require('../internals/define-built-in');
var wellKnownSymbol = require('../internals/well-known-symbol');
var Iterators = require('../internals/iterators');
var IteratorsCore = require('../internals/iterators-core');

var PROPER_FUNCTION_NAME = FunctionName.PROPER;
var CONFIGURABLE_FUNCTION_NAME = FunctionName.CONFIGURABLE;
var IteratorPrototype = IteratorsCore.IteratorPrototype;
var BUGGY_SAFARI_ITERATORS = IteratorsCore.BUGGY_SAFARI_ITERATORS;
var ITERATOR = wellKnownSymbol('iterator');
var KEYS = 'keys';
var VALUES = 'values';
var ENTRIES = 'entries';

var returnThis = function () { return this; };

module.exports = function (Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
  createIteratorConstructor(IteratorConstructor, NAME, next);

  var getIterationMethod = function (KIND) {
    if (KIND === DEFAULT && defaultIterator) return defaultIterator;
    if (!BUGGY_SAFARI_ITERATORS && KIND && KIND in IterablePrototype) return IterablePrototype[KIND];

    switch (KIND) {
      case KEYS: return function keys() { return new IteratorConstructor(this, KIND); };
      case VALUES: return function values() { return new IteratorConstructor(this, KIND); };
      case ENTRIES: return function entries() { return new IteratorConstructor(this, KIND); };
    }

    return function () { return new IteratorConstructor(this); };
  };

  var TO_STRING_TAG = NAME + ' Iterator';
  var INCORRECT_VALUES_NAME = false;
  var IterablePrototype = Iterable.prototype;
  var nativeIterator = IterablePrototype[ITERATOR]
    || IterablePrototype['@@iterator']
    || DEFAULT && IterablePrototype[DEFAULT];
  var defaultIterator = !BUGGY_SAFARI_ITERATORS && nativeIterator || getIterationMethod(DEFAULT);
  var anyNativeIterator = NAME === 'Array' ? IterablePrototype.entries || nativeIterator : nativeIterator;
  var CurrentIteratorPrototype, methods, KEY;

  // fix native
  if (anyNativeIterator) {
    CurrentIteratorPrototype = getPrototypeOf(anyNativeIterator.call(new Iterable()));
    if (CurrentIteratorPrototype !== Object.prototype && CurrentIteratorPrototype.next) {
      if (!IS_PURE && getPrototypeOf(CurrentIteratorPrototype) !== IteratorPrototype) {
        if (setPrototypeOf) {
          setPrototypeOf(CurrentIteratorPrototype, IteratorPrototype);
        } else if (!isCallable(CurrentIteratorPrototype[ITERATOR])) {
          defineBuiltIn(CurrentIteratorPrototype, ITERATOR, returnThis);
        }
      }
      // Set @@toStringTag to native iterators
      setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true, true);
      if (IS_PURE) Iterators[TO_STRING_TAG] = returnThis;
    }
  }

  // fix Array.prototype.{ values, @@iterator }.name in V8 / FF
  if (PROPER_FUNCTION_NAME && DEFAULT === VALUES && nativeIterator && nativeIterator.name !== VALUES) {
    if (!IS_PURE && CONFIGURABLE_FUNCTION_NAME) {
      createNonEnumerableProperty(IterablePrototype, 'name', VALUES);
    } else {
      INCORRECT_VALUES_NAME = true;
      defaultIterator = function values() { return call(nativeIterator, this); };
    }
  }

  // export additional methods
  if (DEFAULT) {
    methods = {
      values: getIterationMethod(VALUES),
      keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
      entries: getIterationMethod(ENTRIES)
    };
    if (FORCED) for (KEY in methods) {
      if (BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) {
        defineBuiltIn(IterablePrototype, KEY, methods[KEY]);
      }
    } else $({ target: NAME, proto: true, forced: BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME }, methods);
  }

  // define iterator
  if ((!IS_PURE || FORCED) && IterablePrototype[ITERATOR] !== defaultIterator) {
    defineBuiltIn(IterablePrototype, ITERATOR, defaultIterator, { name: DEFAULT });
  }
  Iterators[NAME] = defaultIterator;

  return methods;
};

},{"../internals/create-non-enumerable-property":28,"../internals/define-built-in":32,"../internals/export":48,"../internals/function-call":55,"../internals/function-name":56,"../internals/is-callable":80,"../internals/is-pure":86,"../internals/iterator-create-constructor":91,"../internals/iterators":96,"../internals/iterators-core":95,"../internals/object-get-prototype-of":110,"../internals/object-set-prototype-of":116,"../internals/set-to-string-tag":128,"../internals/well-known-symbol":150}],94:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');
var createIteratorProxy = require('../internals/iterator-create-proxy');
var callWithSafeIterationClosing = require('../internals/call-with-safe-iteration-closing');

var IteratorProxy = createIteratorProxy(function () {
  var iterator = this.iterator;
  var result = anObject(call(this.next, iterator));
  var done = this.done = !!result.done;
  if (!done) return callWithSafeIterationClosing(iterator, this.mapper, [result.value, this.counter++], true);
});

// `Iterator.prototype.map` method
// https://github.com/tc39/proposal-iterator-helpers
module.exports = function map(mapper) {
  anObject(this);
  aCallable(mapper);
  return new IteratorProxy(getIteratorDirect(this), {
    mapper: mapper
  });
};

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/call-with-safe-iteration-closing":18,"../internals/function-call":55,"../internals/get-iterator-direct":61,"../internals/iterator-create-proxy":92}],95:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var isCallable = require('../internals/is-callable');
var isObject = require('../internals/is-object');
var create = require('../internals/object-create');
var getPrototypeOf = require('../internals/object-get-prototype-of');
var defineBuiltIn = require('../internals/define-built-in');
var wellKnownSymbol = require('../internals/well-known-symbol');
var IS_PURE = require('../internals/is-pure');

var ITERATOR = wellKnownSymbol('iterator');
var BUGGY_SAFARI_ITERATORS = false;

// `%IteratorPrototype%` object
// https://tc39.es/ecma262/#sec-%iteratorprototype%-object
var IteratorPrototype, PrototypeOfArrayIteratorPrototype, arrayIterator;

/* eslint-disable es/no-array-prototype-keys -- safe */
if ([].keys) {
  arrayIterator = [].keys();
  // Safari 8 has buggy iterators w/o `next`
  if (!('next' in arrayIterator)) BUGGY_SAFARI_ITERATORS = true;
  else {
    PrototypeOfArrayIteratorPrototype = getPrototypeOf(getPrototypeOf(arrayIterator));
    if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype = PrototypeOfArrayIteratorPrototype;
  }
}

var NEW_ITERATOR_PROTOTYPE = !isObject(IteratorPrototype) || fails(function () {
  var test = {};
  // FF44- legacy iterators case
  return IteratorPrototype[ITERATOR].call(test) !== test;
});

if (NEW_ITERATOR_PROTOTYPE) IteratorPrototype = {};
else if (IS_PURE) IteratorPrototype = create(IteratorPrototype);

// `%IteratorPrototype%[@@iterator]()` method
// https://tc39.es/ecma262/#sec-%iteratorprototype%-@@iterator
if (!isCallable(IteratorPrototype[ITERATOR])) {
  defineBuiltIn(IteratorPrototype, ITERATOR, function () {
    return this;
  });
}

module.exports = {
  IteratorPrototype: IteratorPrototype,
  BUGGY_SAFARI_ITERATORS: BUGGY_SAFARI_ITERATORS
};

},{"../internals/define-built-in":32,"../internals/fails":49,"../internals/is-callable":80,"../internals/is-object":84,"../internals/is-pure":86,"../internals/object-create":103,"../internals/object-get-prototype-of":110,"../internals/well-known-symbol":150}],96:[function(require,module,exports){
arguments[4][69][0].apply(exports,arguments)
},{"dup":69}],97:[function(require,module,exports){
'use strict';
var toLength = require('../internals/to-length');

// `LengthOfArrayLike` abstract operation
// https://tc39.es/ecma262/#sec-lengthofarraylike
module.exports = function (obj) {
  return toLength(obj.length);
};

},{"../internals/to-length":139}],98:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var fails = require('../internals/fails');
var isCallable = require('../internals/is-callable');
var hasOwn = require('../internals/has-own-property');
var DESCRIPTORS = require('../internals/descriptors');
var CONFIGURABLE_FUNCTION_NAME = require('../internals/function-name').CONFIGURABLE;
var inspectSource = require('../internals/inspect-source');
var InternalStateModule = require('../internals/internal-state');

var enforceInternalState = InternalStateModule.enforce;
var getInternalState = InternalStateModule.get;
var $String = String;
// eslint-disable-next-line es/no-object-defineproperty -- safe
var defineProperty = Object.defineProperty;
var stringSlice = uncurryThis(''.slice);
var replace = uncurryThis(''.replace);
var join = uncurryThis([].join);

var CONFIGURABLE_LENGTH = DESCRIPTORS && !fails(function () {
  return defineProperty(function () { /* empty */ }, 'length', { value: 8 }).length !== 8;
});

var TEMPLATE = String(String).split('String');

var makeBuiltIn = module.exports = function (value, name, options) {
  if (stringSlice($String(name), 0, 7) === 'Symbol(') {
    name = '[' + replace($String(name), /^Symbol\(([^)]*)\).*$/, '$1') + ']';
  }
  if (options && options.getter) name = 'get ' + name;
  if (options && options.setter) name = 'set ' + name;
  if (!hasOwn(value, 'name') || (CONFIGURABLE_FUNCTION_NAME && value.name !== name)) {
    if (DESCRIPTORS) defineProperty(value, 'name', { value: name, configurable: true });
    else value.name = name;
  }
  if (CONFIGURABLE_LENGTH && options && hasOwn(options, 'arity') && value.length !== options.arity) {
    defineProperty(value, 'length', { value: options.arity });
  }
  try {
    if (options && hasOwn(options, 'constructor') && options.constructor) {
      if (DESCRIPTORS) defineProperty(value, 'prototype', { writable: false });
    // in V8 ~ Chrome 53, prototypes of some methods, like `Array.prototype.values`, are non-writable
    } else if (value.prototype) value.prototype = undefined;
  } catch (error) { /* empty */ }
  var state = enforceInternalState(value);
  if (!hasOwn(state, 'source')) {
    state.source = join(TEMPLATE, typeof name == 'string' ? name : '');
  } return value;
};

// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
// eslint-disable-next-line no-extend-native -- required
Function.prototype.toString = makeBuiltIn(function toString() {
  return isCallable(this) && getInternalState(this).source || inspectSource(this);
}, 'toString');

},{"../internals/descriptors":35,"../internals/fails":49,"../internals/function-name":56,"../internals/function-uncurry-this":59,"../internals/has-own-property":68,"../internals/inspect-source":74,"../internals/internal-state":77,"../internals/is-callable":80}],99:[function(require,module,exports){
'use strict';
var ceil = Math.ceil;
var floor = Math.floor;

// `Math.trunc` method
// https://tc39.es/ecma262/#sec-math.trunc
// eslint-disable-next-line es/no-math-trunc -- safe
module.exports = Math.trunc || function trunc(x) {
  var n = +x;
  return (n > 0 ? floor : ceil)(n);
};

},{}],100:[function(require,module,exports){
'use strict';
var toString = require('../internals/to-string');

module.exports = function (argument, $default) {
  return argument === undefined ? arguments.length < 2 ? '' : $default : toString(argument);
};

},{"../internals/to-string":144}],101:[function(require,module,exports){
'use strict';
var isRegExp = require('../internals/is-regexp');

var $TypeError = TypeError;

module.exports = function (it) {
  if (isRegExp(it)) {
    throw new $TypeError("The method doesn't accept regular expressions");
  } return it;
};

},{"../internals/is-regexp":87}],102:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var fails = require('../internals/fails');
var uncurryThis = require('../internals/function-uncurry-this');
var toString = require('../internals/to-string');
var trim = require('../internals/string-trim').trim;
var whitespaces = require('../internals/whitespaces');

var $parseInt = globalThis.parseInt;
var Symbol = globalThis.Symbol;
var ITERATOR = Symbol && Symbol.iterator;
var hex = /^[+-]?0x/i;
var exec = uncurryThis(hex.exec);
var FORCED = $parseInt(whitespaces + '08') !== 8 || $parseInt(whitespaces + '0x16') !== 22
  // MS Edge 18- broken with boxed symbols
  || (ITERATOR && !fails(function () { $parseInt(Object(ITERATOR)); }));

// `parseInt` method
// https://tc39.es/ecma262/#sec-parseint-string-radix
module.exports = FORCED ? function parseInt(string, radix) {
  var S = trim(toString(string));
  return $parseInt(S, (radix >>> 0) || (exec(hex, S) ? 16 : 10));
} : $parseInt;

},{"../internals/fails":49,"../internals/function-uncurry-this":59,"../internals/global-this":67,"../internals/string-trim":134,"../internals/to-string":144,"../internals/whitespaces":151}],103:[function(require,module,exports){
'use strict';
/* global ActiveXObject -- old IE, WSH */
var anObject = require('../internals/an-object');
var definePropertiesModule = require('../internals/object-define-properties');
var enumBugKeys = require('../internals/enum-bug-keys');
var hiddenKeys = require('../internals/hidden-keys');
var html = require('../internals/html');
var documentCreateElement = require('../internals/document-create-element');
var sharedKey = require('../internals/shared-key');

var GT = '>';
var LT = '<';
var PROTOTYPE = 'prototype';
var SCRIPT = 'script';
var IE_PROTO = sharedKey('IE_PROTO');

var EmptyConstructor = function () { /* empty */ };

var scriptTag = function (content) {
  return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
};

// Create object with fake `null` prototype: use ActiveX Object with cleared prototype
var NullProtoObjectViaActiveX = function (activeXDocument) {
  activeXDocument.write(scriptTag(''));
  activeXDocument.close();
  var temp = activeXDocument.parentWindow.Object;
  // eslint-disable-next-line no-useless-assignment -- avoid memory leak
  activeXDocument = null;
  return temp;
};

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var NullProtoObjectViaIFrame = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = documentCreateElement('iframe');
  var JS = 'java' + SCRIPT + ':';
  var iframeDocument;
  iframe.style.display = 'none';
  html.appendChild(iframe);
  // https://github.com/zloirock/core-js/issues/475
  iframe.src = String(JS);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(scriptTag('document.F=Object'));
  iframeDocument.close();
  return iframeDocument.F;
};

// Check for document.domain and active x support
// No need to use active x approach when document.domain is not set
// see https://github.com/es-shims/es5-shim/issues/150
// variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
// avoid IE GC bug
var activeXDocument;
var NullProtoObject = function () {
  try {
    activeXDocument = new ActiveXObject('htmlfile');
  } catch (error) { /* ignore */ }
  NullProtoObject = typeof document != 'undefined'
    ? document.domain && activeXDocument
      ? NullProtoObjectViaActiveX(activeXDocument) // old IE
      : NullProtoObjectViaIFrame()
    : NullProtoObjectViaActiveX(activeXDocument); // WSH
  var length = enumBugKeys.length;
  while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];
  return NullProtoObject();
};

hiddenKeys[IE_PROTO] = true;

// `Object.create` method
// https://tc39.es/ecma262/#sec-object.create
// eslint-disable-next-line es/no-object-create -- safe
module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    EmptyConstructor[PROTOTYPE] = anObject(O);
    result = new EmptyConstructor();
    EmptyConstructor[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = NullProtoObject();
  return Properties === undefined ? result : definePropertiesModule.f(result, Properties);
};

},{"../internals/an-object":8,"../internals/document-create-element":36,"../internals/enum-bug-keys":40,"../internals/hidden-keys":69,"../internals/html":70,"../internals/object-define-properties":104,"../internals/shared-key":129}],104:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var V8_PROTOTYPE_DEFINE_BUG = require('../internals/v8-prototype-define-bug');
var definePropertyModule = require('../internals/object-define-property');
var anObject = require('../internals/an-object');
var toIndexedObject = require('../internals/to-indexed-object');
var objectKeys = require('../internals/object-keys');

// `Object.defineProperties` method
// https://tc39.es/ecma262/#sec-object.defineproperties
// eslint-disable-next-line es/no-object-defineproperties -- safe
exports.f = DESCRIPTORS && !V8_PROTOTYPE_DEFINE_BUG ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var props = toIndexedObject(Properties);
  var keys = objectKeys(Properties);
  var length = keys.length;
  var index = 0;
  var key;
  while (length > index) definePropertyModule.f(O, key = keys[index++], props[key]);
  return O;
};

},{"../internals/an-object":8,"../internals/descriptors":35,"../internals/object-define-property":105,"../internals/object-keys":114,"../internals/to-indexed-object":137,"../internals/v8-prototype-define-bug":148}],105:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var IE8_DOM_DEFINE = require('../internals/ie8-dom-define');
var V8_PROTOTYPE_DEFINE_BUG = require('../internals/v8-prototype-define-bug');
var anObject = require('../internals/an-object');
var toPropertyKey = require('../internals/to-property-key');

var $TypeError = TypeError;
// eslint-disable-next-line es/no-object-defineproperty -- safe
var $defineProperty = Object.defineProperty;
// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var ENUMERABLE = 'enumerable';
var CONFIGURABLE = 'configurable';
var WRITABLE = 'writable';

// `Object.defineProperty` method
// https://tc39.es/ecma262/#sec-object.defineproperty
exports.f = DESCRIPTORS ? V8_PROTOTYPE_DEFINE_BUG ? function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPropertyKey(P);
  anObject(Attributes);
  if (typeof O === 'function' && P === 'prototype' && 'value' in Attributes && WRITABLE in Attributes && !Attributes[WRITABLE]) {
    var current = $getOwnPropertyDescriptor(O, P);
    if (current && current[WRITABLE]) {
      O[P] = Attributes.value;
      Attributes = {
        configurable: CONFIGURABLE in Attributes ? Attributes[CONFIGURABLE] : current[CONFIGURABLE],
        enumerable: ENUMERABLE in Attributes ? Attributes[ENUMERABLE] : current[ENUMERABLE],
        writable: false
      };
    }
  } return $defineProperty(O, P, Attributes);
} : $defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPropertyKey(P);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return $defineProperty(O, P, Attributes);
  } catch (error) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw new $TypeError('Accessors not supported');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

},{"../internals/an-object":8,"../internals/descriptors":35,"../internals/ie8-dom-define":71,"../internals/to-property-key":142,"../internals/v8-prototype-define-bug":148}],106:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var call = require('../internals/function-call');
var propertyIsEnumerableModule = require('../internals/object-property-is-enumerable');
var createPropertyDescriptor = require('../internals/create-property-descriptor');
var toIndexedObject = require('../internals/to-indexed-object');
var toPropertyKey = require('../internals/to-property-key');
var hasOwn = require('../internals/has-own-property');
var IE8_DOM_DEFINE = require('../internals/ie8-dom-define');

// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.es/ecma262/#sec-object.getownpropertydescriptor
exports.f = DESCRIPTORS ? $getOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
  O = toIndexedObject(O);
  P = toPropertyKey(P);
  if (IE8_DOM_DEFINE) try {
    return $getOwnPropertyDescriptor(O, P);
  } catch (error) { /* empty */ }
  if (hasOwn(O, P)) return createPropertyDescriptor(!call(propertyIsEnumerableModule.f, O, P), O[P]);
};

},{"../internals/create-property-descriptor":29,"../internals/descriptors":35,"../internals/function-call":55,"../internals/has-own-property":68,"../internals/ie8-dom-define":71,"../internals/object-property-is-enumerable":115,"../internals/to-indexed-object":137,"../internals/to-property-key":142}],107:[function(require,module,exports){
'use strict';
/* eslint-disable es/no-object-getownpropertynames -- safe */
var classof = require('../internals/classof-raw');
var toIndexedObject = require('../internals/to-indexed-object');
var $getOwnPropertyNames = require('../internals/object-get-own-property-names').f;
var arraySlice = require('../internals/array-slice');

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return $getOwnPropertyNames(it);
  } catch (error) {
    return arraySlice(windowNames);
  }
};

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && classof(it) === 'Window'
    ? getWindowNames(it)
    : $getOwnPropertyNames(toIndexedObject(it));
};

},{"../internals/array-slice":15,"../internals/classof-raw":20,"../internals/object-get-own-property-names":108,"../internals/to-indexed-object":137}],108:[function(require,module,exports){
'use strict';
var internalObjectKeys = require('../internals/object-keys-internal');
var enumBugKeys = require('../internals/enum-bug-keys');

var hiddenKeys = enumBugKeys.concat('length', 'prototype');

// `Object.getOwnPropertyNames` method
// https://tc39.es/ecma262/#sec-object.getownpropertynames
// eslint-disable-next-line es/no-object-getownpropertynames -- safe
exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return internalObjectKeys(O, hiddenKeys);
};

},{"../internals/enum-bug-keys":40,"../internals/object-keys-internal":113}],109:[function(require,module,exports){
'use strict';
// eslint-disable-next-line es/no-object-getownpropertysymbols -- safe
exports.f = Object.getOwnPropertySymbols;

},{}],110:[function(require,module,exports){
'use strict';
var hasOwn = require('../internals/has-own-property');
var isCallable = require('../internals/is-callable');
var toObject = require('../internals/to-object');
var sharedKey = require('../internals/shared-key');
var CORRECT_PROTOTYPE_GETTER = require('../internals/correct-prototype-getter');

var IE_PROTO = sharedKey('IE_PROTO');
var $Object = Object;
var ObjectPrototype = $Object.prototype;

// `Object.getPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.getprototypeof
// eslint-disable-next-line es/no-object-getprototypeof -- safe
module.exports = CORRECT_PROTOTYPE_GETTER ? $Object.getPrototypeOf : function (O) {
  var object = toObject(O);
  if (hasOwn(object, IE_PROTO)) return object[IE_PROTO];
  var constructor = object.constructor;
  if (isCallable(constructor) && object instanceof constructor) {
    return constructor.prototype;
  } return object instanceof $Object ? ObjectPrototype : null;
};

},{"../internals/correct-prototype-getter":26,"../internals/has-own-property":68,"../internals/is-callable":80,"../internals/shared-key":129,"../internals/to-object":140}],111:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var isObject = require('../internals/is-object');
var classof = require('../internals/classof-raw');
var ARRAY_BUFFER_NON_EXTENSIBLE = require('../internals/array-buffer-non-extensible');

// eslint-disable-next-line es/no-object-isextensible -- safe
var $isExtensible = Object.isExtensible;
var FAILS_ON_PRIMITIVES = fails(function () { $isExtensible(1); });

// `Object.isExtensible` method
// https://tc39.es/ecma262/#sec-object.isextensible
module.exports = (FAILS_ON_PRIMITIVES || ARRAY_BUFFER_NON_EXTENSIBLE) ? function isExtensible(it) {
  if (!isObject(it)) return false;
  if (ARRAY_BUFFER_NON_EXTENSIBLE && classof(it) === 'ArrayBuffer') return false;
  return $isExtensible ? $isExtensible(it) : true;
} : $isExtensible;

},{"../internals/array-buffer-non-extensible":9,"../internals/classof-raw":20,"../internals/fails":49,"../internals/is-object":84}],112:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');

module.exports = uncurryThis({}.isPrototypeOf);

},{"../internals/function-uncurry-this":59}],113:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var hasOwn = require('../internals/has-own-property');
var toIndexedObject = require('../internals/to-indexed-object');
var indexOf = require('../internals/array-includes').indexOf;
var hiddenKeys = require('../internals/hidden-keys');

var push = uncurryThis([].push);

module.exports = function (object, names) {
  var O = toIndexedObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) !hasOwn(hiddenKeys, key) && hasOwn(O, key) && push(result, key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (hasOwn(O, key = names[i++])) {
    ~indexOf(result, key) || push(result, key);
  }
  return result;
};

},{"../internals/array-includes":10,"../internals/function-uncurry-this":59,"../internals/has-own-property":68,"../internals/hidden-keys":69,"../internals/to-indexed-object":137}],114:[function(require,module,exports){
'use strict';
var internalObjectKeys = require('../internals/object-keys-internal');
var enumBugKeys = require('../internals/enum-bug-keys');

// `Object.keys` method
// https://tc39.es/ecma262/#sec-object.keys
// eslint-disable-next-line es/no-object-keys -- safe
module.exports = Object.keys || function keys(O) {
  return internalObjectKeys(O, enumBugKeys);
};

},{"../internals/enum-bug-keys":40,"../internals/object-keys-internal":113}],115:[function(require,module,exports){
'use strict';
var $propertyIsEnumerable = {}.propertyIsEnumerable;
// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// Nashorn ~ JDK8 bug
var NASHORN_BUG = getOwnPropertyDescriptor && !$propertyIsEnumerable.call({ 1: 2 }, 1);

// `Object.prototype.propertyIsEnumerable` method implementation
// https://tc39.es/ecma262/#sec-object.prototype.propertyisenumerable
exports.f = NASHORN_BUG ? function propertyIsEnumerable(V) {
  var descriptor = getOwnPropertyDescriptor(this, V);
  return !!descriptor && descriptor.enumerable;
} : $propertyIsEnumerable;

},{}],116:[function(require,module,exports){
'use strict';
/* eslint-disable no-proto -- safe */
var uncurryThisAccessor = require('../internals/function-uncurry-this-accessor');
var isObject = require('../internals/is-object');
var requireObjectCoercible = require('../internals/require-object-coercible');
var aPossiblePrototype = require('../internals/a-possible-prototype');

// `Object.setPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.setprototypeof
// Works with __proto__ only. Old v8 can't work with null proto objects.
// eslint-disable-next-line es/no-object-setprototypeof -- safe
module.exports = Object.setPrototypeOf || ('__proto__' in {} ? function () {
  var CORRECT_SETTER = false;
  var test = {};
  var setter;
  try {
    setter = uncurryThisAccessor(Object.prototype, '__proto__', 'set');
    setter(test, []);
    CORRECT_SETTER = test instanceof Array;
  } catch (error) { /* empty */ }
  return function setPrototypeOf(O, proto) {
    requireObjectCoercible(O);
    aPossiblePrototype(proto);
    if (!isObject(O)) return O;
    if (CORRECT_SETTER) setter(O, proto);
    else O.__proto__ = proto;
    return O;
  };
}() : undefined);

},{"../internals/a-possible-prototype":4,"../internals/function-uncurry-this-accessor":57,"../internals/is-object":84,"../internals/require-object-coercible":127}],117:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var isCallable = require('../internals/is-callable');
var isObject = require('../internals/is-object');

var $TypeError = TypeError;

// `OrdinaryToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-ordinarytoprimitive
module.exports = function (input, pref) {
  var fn, val;
  if (pref === 'string' && isCallable(fn = input.toString) && !isObject(val = call(fn, input))) return val;
  if (isCallable(fn = input.valueOf) && !isObject(val = call(fn, input))) return val;
  if (pref !== 'string' && isCallable(fn = input.toString) && !isObject(val = call(fn, input))) return val;
  throw new $TypeError("Can't convert object to primitive value");
};

},{"../internals/function-call":55,"../internals/is-callable":80,"../internals/is-object":84}],118:[function(require,module,exports){
'use strict';
var getBuiltIn = require('../internals/get-built-in');
var uncurryThis = require('../internals/function-uncurry-this');
var getOwnPropertyNamesModule = require('../internals/object-get-own-property-names');
var getOwnPropertySymbolsModule = require('../internals/object-get-own-property-symbols');
var anObject = require('../internals/an-object');

var concat = uncurryThis([].concat);

// all object keys, includes non-enumerable and symbols
module.exports = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
  var keys = getOwnPropertyNamesModule.f(anObject(it));
  var getOwnPropertySymbols = getOwnPropertySymbolsModule.f;
  return getOwnPropertySymbols ? concat(keys, getOwnPropertySymbols(it)) : keys;
};

},{"../internals/an-object":8,"../internals/function-uncurry-this":59,"../internals/get-built-in":60,"../internals/object-get-own-property-names":108,"../internals/object-get-own-property-symbols":109}],119:[function(require,module,exports){
'use strict';
var defineProperty = require('../internals/object-define-property').f;

module.exports = function (Target, Source, key) {
  key in Target || defineProperty(Target, key, {
    configurable: true,
    get: function () { return Source[key]; },
    set: function (it) { Source[key] = it; }
  });
};

},{"../internals/object-define-property":105}],120:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var anObject = require('../internals/an-object');
var isCallable = require('../internals/is-callable');
var classof = require('../internals/classof-raw');
var regexpExec = require('../internals/regexp-exec');

var $TypeError = TypeError;

// `RegExpExec` abstract operation
// https://tc39.es/ecma262/#sec-regexpexec
module.exports = function (R, S) {
  var exec = R.exec;
  if (isCallable(exec)) {
    var result = call(exec, R, S);
    if (result !== null) anObject(result);
    return result;
  }
  if (classof(R) === 'RegExp') return call(regexpExec, R, S);
  throw new $TypeError('RegExp#exec called on incompatible receiver');
};

},{"../internals/an-object":8,"../internals/classof-raw":20,"../internals/function-call":55,"../internals/is-callable":80,"../internals/regexp-exec":121}],121:[function(require,module,exports){
'use strict';
/* eslint-disable regexp/no-empty-capturing-group, regexp/no-empty-group, regexp/no-lazy-ends -- testing */
/* eslint-disable regexp/no-useless-quantifier -- testing */
var call = require('../internals/function-call');
var uncurryThis = require('../internals/function-uncurry-this');
var toString = require('../internals/to-string');
var regexpFlags = require('../internals/regexp-flags');
var stickyHelpers = require('../internals/regexp-sticky-helpers');
var shared = require('../internals/shared');
var create = require('../internals/object-create');
var getInternalState = require('../internals/internal-state').get;
var UNSUPPORTED_DOT_ALL = require('../internals/regexp-unsupported-dot-all');
var UNSUPPORTED_NCG = require('../internals/regexp-unsupported-ncg');

var nativeReplace = shared('native-string-replace', String.prototype.replace);
var nativeExec = RegExp.prototype.exec;
var patchedExec = nativeExec;
var charAt = uncurryThis(''.charAt);
var indexOf = uncurryThis(''.indexOf);
var replace = uncurryThis(''.replace);
var stringSlice = uncurryThis(''.slice);

var UPDATES_LAST_INDEX_WRONG = (function () {
  var re1 = /a/;
  var re2 = /b*/g;
  call(nativeExec, re1, 'a');
  call(nativeExec, re2, 'a');
  return re1.lastIndex !== 0 || re2.lastIndex !== 0;
})();

var UNSUPPORTED_Y = stickyHelpers.BROKEN_CARET;

// nonparticipating capturing group, copied from es5-shim's String#split patch.
var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y || UNSUPPORTED_DOT_ALL || UNSUPPORTED_NCG;

if (PATCH) {
  patchedExec = function exec(string) {
    var re = this;
    var state = getInternalState(re);
    var str = toString(string);
    var raw = state.raw;
    var result, reCopy, lastIndex, match, i, object, group;

    if (raw) {
      raw.lastIndex = re.lastIndex;
      result = call(patchedExec, raw, str);
      re.lastIndex = raw.lastIndex;
      return result;
    }

    var groups = state.groups;
    var sticky = UNSUPPORTED_Y && re.sticky;
    var flags = call(regexpFlags, re);
    var source = re.source;
    var charsAdded = 0;
    var strCopy = str;

    if (sticky) {
      flags = replace(flags, 'y', '');
      if (indexOf(flags, 'g') === -1) {
        flags += 'g';
      }

      strCopy = stringSlice(str, re.lastIndex);
      // Support anchored sticky behavior.
      if (re.lastIndex > 0 && (!re.multiline || re.multiline && charAt(str, re.lastIndex - 1) !== '\n')) {
        source = '(?: ' + source + ')';
        strCopy = ' ' + strCopy;
        charsAdded++;
      }
      // ^(? + rx + ) is needed, in combination with some str slicing, to
      // simulate the 'y' flag.
      reCopy = new RegExp('^(?:' + source + ')', flags);
    }

    if (NPCG_INCLUDED) {
      reCopy = new RegExp('^' + source + '$(?!\\s)', flags);
    }
    if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;

    match = call(nativeExec, sticky ? reCopy : re, strCopy);

    if (sticky) {
      if (match) {
        match.input = stringSlice(match.input, charsAdded);
        match[0] = stringSlice(match[0], charsAdded);
        match.index = re.lastIndex;
        re.lastIndex += match[0].length;
      } else re.lastIndex = 0;
    } else if (UPDATES_LAST_INDEX_WRONG && match) {
      re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
    }
    if (NPCG_INCLUDED && match && match.length > 1) {
      // Fix browsers whose `exec` methods don't consistently return `undefined`
      // for NPCG, like IE8. NOTE: This doesn't work for /(.?)?/
      call(nativeReplace, match[0], reCopy, function () {
        for (i = 1; i < arguments.length - 2; i++) {
          if (arguments[i] === undefined) match[i] = undefined;
        }
      });
    }

    if (match && groups) {
      match.groups = object = create(null);
      for (i = 0; i < groups.length; i++) {
        group = groups[i];
        object[group[0]] = match[group[1]];
      }
    }

    return match;
  };
}

module.exports = patchedExec;

},{"../internals/function-call":55,"../internals/function-uncurry-this":59,"../internals/internal-state":77,"../internals/object-create":103,"../internals/regexp-flags":122,"../internals/regexp-sticky-helpers":124,"../internals/regexp-unsupported-dot-all":125,"../internals/regexp-unsupported-ncg":126,"../internals/shared":131,"../internals/to-string":144}],122:[function(require,module,exports){
'use strict';
var anObject = require('../internals/an-object');

// `RegExp.prototype.flags` getter implementation
// https://tc39.es/ecma262/#sec-get-regexp.prototype.flags
module.exports = function () {
  var that = anObject(this);
  var result = '';
  if (that.hasIndices) result += 'd';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.dotAll) result += 's';
  if (that.unicode) result += 'u';
  if (that.unicodeSets) result += 'v';
  if (that.sticky) result += 'y';
  return result;
};

},{"../internals/an-object":8}],123:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var hasOwn = require('../internals/has-own-property');
var isPrototypeOf = require('../internals/object-is-prototype-of');
var regExpFlags = require('../internals/regexp-flags');

var RegExpPrototype = RegExp.prototype;

module.exports = function (R) {
  var flags = R.flags;
  return flags === undefined && !('flags' in RegExpPrototype) && !hasOwn(R, 'flags') && isPrototypeOf(RegExpPrototype, R)
    ? call(regExpFlags, R) : flags;
};

},{"../internals/function-call":55,"../internals/has-own-property":68,"../internals/object-is-prototype-of":112,"../internals/regexp-flags":122}],124:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var globalThis = require('../internals/global-this');

// babel-minify and Closure Compiler transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError
var $RegExp = globalThis.RegExp;

var UNSUPPORTED_Y = fails(function () {
  var re = $RegExp('a', 'y');
  re.lastIndex = 2;
  return re.exec('abcd') !== null;
});

// UC Browser bug
// https://github.com/zloirock/core-js/issues/1008
var MISSED_STICKY = UNSUPPORTED_Y || fails(function () {
  return !$RegExp('a', 'y').sticky;
});

var BROKEN_CARET = UNSUPPORTED_Y || fails(function () {
  // https://bugzilla.mozilla.org/show_bug.cgi?id=773687
  var re = $RegExp('^r', 'gy');
  re.lastIndex = 2;
  return re.exec('str') !== null;
});

module.exports = {
  BROKEN_CARET: BROKEN_CARET,
  MISSED_STICKY: MISSED_STICKY,
  UNSUPPORTED_Y: UNSUPPORTED_Y
};

},{"../internals/fails":49,"../internals/global-this":67}],125:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var globalThis = require('../internals/global-this');

// babel-minify and Closure Compiler transpiles RegExp('.', 's') -> /./s and it causes SyntaxError
var $RegExp = globalThis.RegExp;

module.exports = fails(function () {
  var re = $RegExp('.', 's');
  return !(re.dotAll && re.test('\n') && re.flags === 's');
});

},{"../internals/fails":49,"../internals/global-this":67}],126:[function(require,module,exports){
'use strict';
var fails = require('../internals/fails');
var globalThis = require('../internals/global-this');

// babel-minify and Closure Compiler transpiles RegExp('(?<a>b)', 'g') -> /(?<a>b)/g and it causes SyntaxError
var $RegExp = globalThis.RegExp;

module.exports = fails(function () {
  var re = $RegExp('(?<a>b)', 'g');
  return re.exec('b').groups.a !== 'b' ||
    'b'.replace(re, '$<a>c') !== 'bc';
});

},{"../internals/fails":49,"../internals/global-this":67}],127:[function(require,module,exports){
'use strict';
var isNullOrUndefined = require('../internals/is-null-or-undefined');

var $TypeError = TypeError;

// `RequireObjectCoercible` abstract operation
// https://tc39.es/ecma262/#sec-requireobjectcoercible
module.exports = function (it) {
  if (isNullOrUndefined(it)) throw new $TypeError("Can't call method on " + it);
  return it;
};

},{"../internals/is-null-or-undefined":83}],128:[function(require,module,exports){
'use strict';
var defineProperty = require('../internals/object-define-property').f;
var hasOwn = require('../internals/has-own-property');
var wellKnownSymbol = require('../internals/well-known-symbol');

var TO_STRING_TAG = wellKnownSymbol('toStringTag');

module.exports = function (target, TAG, STATIC) {
  if (target && !STATIC) target = target.prototype;
  if (target && !hasOwn(target, TO_STRING_TAG)) {
    defineProperty(target, TO_STRING_TAG, { configurable: true, value: TAG });
  }
};

},{"../internals/has-own-property":68,"../internals/object-define-property":105,"../internals/well-known-symbol":150}],129:[function(require,module,exports){
'use strict';
var shared = require('../internals/shared');
var uid = require('../internals/uid');

var keys = shared('keys');

module.exports = function (key) {
  return keys[key] || (keys[key] = uid(key));
};

},{"../internals/shared":131,"../internals/uid":146}],130:[function(require,module,exports){
'use strict';
var IS_PURE = require('../internals/is-pure');
var globalThis = require('../internals/global-this');
var defineGlobalProperty = require('../internals/define-global-property');

var SHARED = '__core-js_shared__';
var store = module.exports = globalThis[SHARED] || defineGlobalProperty(SHARED, {});

(store.versions || (store.versions = [])).push({
  version: '3.39.0',
  mode: IS_PURE ? 'pure' : 'global',
  copyright: '© 2014-2024 Denis Pushkarev (zloirock.ru)',
  license: 'https://github.com/zloirock/core-js/blob/v3.39.0/LICENSE',
  source: 'https://github.com/zloirock/core-js'
});

},{"../internals/define-global-property":34,"../internals/global-this":67,"../internals/is-pure":86}],131:[function(require,module,exports){
'use strict';
var store = require('../internals/shared-store');

module.exports = function (key, value) {
  return store[key] || (store[key] = value || {});
};

},{"../internals/shared-store":130}],132:[function(require,module,exports){
'use strict';
var anObject = require('../internals/an-object');
var aConstructor = require('../internals/a-constructor');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var wellKnownSymbol = require('../internals/well-known-symbol');

var SPECIES = wellKnownSymbol('species');

// `SpeciesConstructor` abstract operation
// https://tc39.es/ecma262/#sec-speciesconstructor
module.exports = function (O, defaultConstructor) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || isNullOrUndefined(S = anObject(C)[SPECIES]) ? defaultConstructor : aConstructor(S);
};

},{"../internals/a-constructor":3,"../internals/an-object":8,"../internals/is-null-or-undefined":83,"../internals/well-known-symbol":150}],133:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var toIntegerOrInfinity = require('../internals/to-integer-or-infinity');
var toString = require('../internals/to-string');
var requireObjectCoercible = require('../internals/require-object-coercible');

var charAt = uncurryThis(''.charAt);
var charCodeAt = uncurryThis(''.charCodeAt);
var stringSlice = uncurryThis(''.slice);

var createMethod = function (CONVERT_TO_STRING) {
  return function ($this, pos) {
    var S = toString(requireObjectCoercible($this));
    var position = toIntegerOrInfinity(pos);
    var size = S.length;
    var first, second;
    if (position < 0 || position >= size) return CONVERT_TO_STRING ? '' : undefined;
    first = charCodeAt(S, position);
    return first < 0xD800 || first > 0xDBFF || position + 1 === size
      || (second = charCodeAt(S, position + 1)) < 0xDC00 || second > 0xDFFF
        ? CONVERT_TO_STRING
          ? charAt(S, position)
          : first
        : CONVERT_TO_STRING
          ? stringSlice(S, position, position + 2)
          : (first - 0xD800 << 10) + (second - 0xDC00) + 0x10000;
  };
};

module.exports = {
  // `String.prototype.codePointAt` method
  // https://tc39.es/ecma262/#sec-string.prototype.codepointat
  codeAt: createMethod(false),
  // `String.prototype.at` method
  // https://github.com/mathiasbynens/String.prototype.at
  charAt: createMethod(true)
};

},{"../internals/function-uncurry-this":59,"../internals/require-object-coercible":127,"../internals/to-integer-or-infinity":138,"../internals/to-string":144}],134:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');
var requireObjectCoercible = require('../internals/require-object-coercible');
var toString = require('../internals/to-string');
var whitespaces = require('../internals/whitespaces');

var replace = uncurryThis(''.replace);
var ltrim = RegExp('^[' + whitespaces + ']+');
var rtrim = RegExp('(^|[^' + whitespaces + '])[' + whitespaces + ']+$');

// `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation
var createMethod = function (TYPE) {
  return function ($this) {
    var string = toString(requireObjectCoercible($this));
    if (TYPE & 1) string = replace(string, ltrim, '');
    if (TYPE & 2) string = replace(string, rtrim, '$1');
    return string;
  };
};

module.exports = {
  // `String.prototype.{ trimLeft, trimStart }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimstart
  start: createMethod(1),
  // `String.prototype.{ trimRight, trimEnd }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimend
  end: createMethod(2),
  // `String.prototype.trim` method
  // https://tc39.es/ecma262/#sec-string.prototype.trim
  trim: createMethod(3)
};

},{"../internals/function-uncurry-this":59,"../internals/require-object-coercible":127,"../internals/to-string":144,"../internals/whitespaces":151}],135:[function(require,module,exports){
'use strict';
/* eslint-disable es/no-symbol -- required for testing */
var V8_VERSION = require('../internals/environment-v8-version');
var fails = require('../internals/fails');
var globalThis = require('../internals/global-this');

var $String = globalThis.String;

// eslint-disable-next-line es/no-object-getownpropertysymbols -- required for testing
module.exports = !!Object.getOwnPropertySymbols && !fails(function () {
  var symbol = Symbol('symbol detection');
  // Chrome 38 Symbol has incorrect toString conversion
  // `get-own-property-symbols` polyfill symbols converted to object are not Symbol instances
  // nb: Do not call `String` directly to avoid this being optimized out to `symbol+''` which will,
  // of course, fail.
  return !$String(symbol) || !(Object(symbol) instanceof Symbol) ||
    // Chrome 38-40 symbols are not inherited from DOM collections prototypes to instances
    !Symbol.sham && V8_VERSION && V8_VERSION < 41;
});

},{"../internals/environment-v8-version":43,"../internals/fails":49,"../internals/global-this":67}],136:[function(require,module,exports){
'use strict';
var toIntegerOrInfinity = require('../internals/to-integer-or-infinity');

var max = Math.max;
var min = Math.min;

// Helper for a popular repeating case of the spec:
// Let integer be ? ToInteger(index).
// If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
module.exports = function (index, length) {
  var integer = toIntegerOrInfinity(index);
  return integer < 0 ? max(integer + length, 0) : min(integer, length);
};

},{"../internals/to-integer-or-infinity":138}],137:[function(require,module,exports){
'use strict';
// toObject with fallback for non-array-like ES3 strings
var IndexedObject = require('../internals/indexed-object');
var requireObjectCoercible = require('../internals/require-object-coercible');

module.exports = function (it) {
  return IndexedObject(requireObjectCoercible(it));
};

},{"../internals/indexed-object":72,"../internals/require-object-coercible":127}],138:[function(require,module,exports){
'use strict';
var trunc = require('../internals/math-trunc');

// `ToIntegerOrInfinity` abstract operation
// https://tc39.es/ecma262/#sec-tointegerorinfinity
module.exports = function (argument) {
  var number = +argument;
  // eslint-disable-next-line no-self-compare -- NaN check
  return number !== number || number === 0 ? 0 : trunc(number);
};

},{"../internals/math-trunc":99}],139:[function(require,module,exports){
'use strict';
var toIntegerOrInfinity = require('../internals/to-integer-or-infinity');

var min = Math.min;

// `ToLength` abstract operation
// https://tc39.es/ecma262/#sec-tolength
module.exports = function (argument) {
  var len = toIntegerOrInfinity(argument);
  return len > 0 ? min(len, 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
};

},{"../internals/to-integer-or-infinity":138}],140:[function(require,module,exports){
'use strict';
var requireObjectCoercible = require('../internals/require-object-coercible');

var $Object = Object;

// `ToObject` abstract operation
// https://tc39.es/ecma262/#sec-toobject
module.exports = function (argument) {
  return $Object(requireObjectCoercible(argument));
};

},{"../internals/require-object-coercible":127}],141:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var isObject = require('../internals/is-object');
var isSymbol = require('../internals/is-symbol');
var getMethod = require('../internals/get-method');
var ordinaryToPrimitive = require('../internals/ordinary-to-primitive');
var wellKnownSymbol = require('../internals/well-known-symbol');

var $TypeError = TypeError;
var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');

// `ToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-toprimitive
module.exports = function (input, pref) {
  if (!isObject(input) || isSymbol(input)) return input;
  var exoticToPrim = getMethod(input, TO_PRIMITIVE);
  var result;
  if (exoticToPrim) {
    if (pref === undefined) pref = 'default';
    result = call(exoticToPrim, input, pref);
    if (!isObject(result) || isSymbol(result)) return result;
    throw new $TypeError("Can't convert object to primitive value");
  }
  if (pref === undefined) pref = 'number';
  return ordinaryToPrimitive(input, pref);
};

},{"../internals/function-call":55,"../internals/get-method":65,"../internals/is-object":84,"../internals/is-symbol":88,"../internals/ordinary-to-primitive":117,"../internals/well-known-symbol":150}],142:[function(require,module,exports){
'use strict';
var toPrimitive = require('../internals/to-primitive');
var isSymbol = require('../internals/is-symbol');

// `ToPropertyKey` abstract operation
// https://tc39.es/ecma262/#sec-topropertykey
module.exports = function (argument) {
  var key = toPrimitive(argument, 'string');
  return isSymbol(key) ? key : key + '';
};

},{"../internals/is-symbol":88,"../internals/to-primitive":141}],143:[function(require,module,exports){
'use strict';
var wellKnownSymbol = require('../internals/well-known-symbol');

var TO_STRING_TAG = wellKnownSymbol('toStringTag');
var test = {};

test[TO_STRING_TAG] = 'z';

module.exports = String(test) === '[object z]';

},{"../internals/well-known-symbol":150}],144:[function(require,module,exports){
'use strict';
var classof = require('../internals/classof');

var $String = String;

module.exports = function (argument) {
  if (classof(argument) === 'Symbol') throw new TypeError('Cannot convert a Symbol value to a string');
  return $String(argument);
};

},{"../internals/classof":21}],145:[function(require,module,exports){
'use strict';
var $String = String;

module.exports = function (argument) {
  try {
    return $String(argument);
  } catch (error) {
    return 'Object';
  }
};

},{}],146:[function(require,module,exports){
'use strict';
var uncurryThis = require('../internals/function-uncurry-this');

var id = 0;
var postfix = Math.random();
var toString = uncurryThis(1.0.toString);

module.exports = function (key) {
  return 'Symbol(' + (key === undefined ? '' : key) + ')_' + toString(++id + postfix, 36);
};

},{"../internals/function-uncurry-this":59}],147:[function(require,module,exports){
'use strict';
/* eslint-disable es/no-symbol -- required for testing */
var NATIVE_SYMBOL = require('../internals/symbol-constructor-detection');

module.exports = NATIVE_SYMBOL &&
  !Symbol.sham &&
  typeof Symbol.iterator == 'symbol';

},{"../internals/symbol-constructor-detection":135}],148:[function(require,module,exports){
'use strict';
var DESCRIPTORS = require('../internals/descriptors');
var fails = require('../internals/fails');

// V8 ~ Chrome 36-
// https://bugs.chromium.org/p/v8/issues/detail?id=3334
module.exports = DESCRIPTORS && fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  return Object.defineProperty(function () { /* empty */ }, 'prototype', {
    value: 42,
    writable: false
  }).prototype !== 42;
});

},{"../internals/descriptors":35,"../internals/fails":49}],149:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var isCallable = require('../internals/is-callable');

var WeakMap = globalThis.WeakMap;

module.exports = isCallable(WeakMap) && /native code/.test(String(WeakMap));

},{"../internals/global-this":67,"../internals/is-callable":80}],150:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var shared = require('../internals/shared');
var hasOwn = require('../internals/has-own-property');
var uid = require('../internals/uid');
var NATIVE_SYMBOL = require('../internals/symbol-constructor-detection');
var USE_SYMBOL_AS_UID = require('../internals/use-symbol-as-uid');

var Symbol = globalThis.Symbol;
var WellKnownSymbolsStore = shared('wks');
var createWellKnownSymbol = USE_SYMBOL_AS_UID ? Symbol['for'] || Symbol : Symbol && Symbol.withoutSetter || uid;

module.exports = function (name) {
  if (!hasOwn(WellKnownSymbolsStore, name)) {
    WellKnownSymbolsStore[name] = NATIVE_SYMBOL && hasOwn(Symbol, name)
      ? Symbol[name]
      : createWellKnownSymbol('Symbol.' + name);
  } return WellKnownSymbolsStore[name];
};

},{"../internals/global-this":67,"../internals/has-own-property":68,"../internals/shared":131,"../internals/symbol-constructor-detection":135,"../internals/uid":146,"../internals/use-symbol-as-uid":147}],151:[function(require,module,exports){
'use strict';
// a string of all valid unicode whitespaces
module.exports = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002' +
  '\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

},{}],152:[function(require,module,exports){
'use strict';
var getBuiltIn = require('../internals/get-built-in');
var hasOwn = require('../internals/has-own-property');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var isPrototypeOf = require('../internals/object-is-prototype-of');
var setPrototypeOf = require('../internals/object-set-prototype-of');
var copyConstructorProperties = require('../internals/copy-constructor-properties');
var proxyAccessor = require('../internals/proxy-accessor');
var inheritIfRequired = require('../internals/inherit-if-required');
var normalizeStringArgument = require('../internals/normalize-string-argument');
var installErrorCause = require('../internals/install-error-cause');
var installErrorStack = require('../internals/error-stack-install');
var DESCRIPTORS = require('../internals/descriptors');
var IS_PURE = require('../internals/is-pure');

module.exports = function (FULL_NAME, wrapper, FORCED, IS_AGGREGATE_ERROR) {
  var STACK_TRACE_LIMIT = 'stackTraceLimit';
  var OPTIONS_POSITION = IS_AGGREGATE_ERROR ? 2 : 1;
  var path = FULL_NAME.split('.');
  var ERROR_NAME = path[path.length - 1];
  var OriginalError = getBuiltIn.apply(null, path);

  if (!OriginalError) return;

  var OriginalErrorPrototype = OriginalError.prototype;

  // V8 9.3- bug https://bugs.chromium.org/p/v8/issues/detail?id=12006
  if (!IS_PURE && hasOwn(OriginalErrorPrototype, 'cause')) delete OriginalErrorPrototype.cause;

  if (!FORCED) return OriginalError;

  var BaseError = getBuiltIn('Error');

  var WrappedError = wrapper(function (a, b) {
    var message = normalizeStringArgument(IS_AGGREGATE_ERROR ? b : a, undefined);
    var result = IS_AGGREGATE_ERROR ? new OriginalError(a) : new OriginalError();
    if (message !== undefined) createNonEnumerableProperty(result, 'message', message);
    installErrorStack(result, WrappedError, result.stack, 2);
    if (this && isPrototypeOf(OriginalErrorPrototype, this)) inheritIfRequired(result, this, WrappedError);
    if (arguments.length > OPTIONS_POSITION) installErrorCause(result, arguments[OPTIONS_POSITION]);
    return result;
  });

  WrappedError.prototype = OriginalErrorPrototype;

  if (ERROR_NAME !== 'Error') {
    if (setPrototypeOf) setPrototypeOf(WrappedError, BaseError);
    else copyConstructorProperties(WrappedError, BaseError, { name: true });
  } else if (DESCRIPTORS && STACK_TRACE_LIMIT in OriginalError) {
    proxyAccessor(WrappedError, OriginalError, STACK_TRACE_LIMIT);
    proxyAccessor(WrappedError, OriginalError, 'prepareStackTrace');
  }

  copyConstructorProperties(WrappedError, OriginalError);

  if (!IS_PURE) try {
    // Safari 13- bug: WebAssembly errors does not have a proper `.name`
    if (OriginalErrorPrototype.name !== ERROR_NAME) {
      createNonEnumerableProperty(OriginalErrorPrototype, 'name', ERROR_NAME);
    }
    OriginalErrorPrototype.constructor = WrappedError;
  } catch (error) { /* empty */ }

  return WrappedError;
};

},{"../internals/copy-constructor-properties":24,"../internals/create-non-enumerable-property":28,"../internals/descriptors":35,"../internals/error-stack-install":46,"../internals/get-built-in":60,"../internals/has-own-property":68,"../internals/inherit-if-required":73,"../internals/install-error-cause":75,"../internals/is-pure":86,"../internals/normalize-string-argument":100,"../internals/object-is-prototype-of":112,"../internals/object-set-prototype-of":116,"../internals/proxy-accessor":119}],153:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var $includes = require('../internals/array-includes').includes;
var fails = require('../internals/fails');
var addToUnscopables = require('../internals/add-to-unscopables');

// FF99+ bug
var BROKEN_ON_SPARSE = fails(function () {
  // eslint-disable-next-line es/no-array-prototype-includes -- detection
  return !Array(1).includes();
});

// `Array.prototype.includes` method
// https://tc39.es/ecma262/#sec-array.prototype.includes
$({ target: 'Array', proto: true, forced: BROKEN_ON_SPARSE }, {
  includes: function includes(el /* , fromIndex = 0 */) {
    return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('includes');

},{"../internals/add-to-unscopables":5,"../internals/array-includes":10,"../internals/export":48,"../internals/fails":49}],154:[function(require,module,exports){
'use strict';
var toIndexedObject = require('../internals/to-indexed-object');
var addToUnscopables = require('../internals/add-to-unscopables');
var Iterators = require('../internals/iterators');
var InternalStateModule = require('../internals/internal-state');
var defineProperty = require('../internals/object-define-property').f;
var defineIterator = require('../internals/iterator-define');
var createIterResultObject = require('../internals/create-iter-result-object');
var IS_PURE = require('../internals/is-pure');
var DESCRIPTORS = require('../internals/descriptors');

var ARRAY_ITERATOR = 'Array Iterator';
var setInternalState = InternalStateModule.set;
var getInternalState = InternalStateModule.getterFor(ARRAY_ITERATOR);

// `Array.prototype.entries` method
// https://tc39.es/ecma262/#sec-array.prototype.entries
// `Array.prototype.keys` method
// https://tc39.es/ecma262/#sec-array.prototype.keys
// `Array.prototype.values` method
// https://tc39.es/ecma262/#sec-array.prototype.values
// `Array.prototype[@@iterator]` method
// https://tc39.es/ecma262/#sec-array.prototype-@@iterator
// `CreateArrayIterator` internal method
// https://tc39.es/ecma262/#sec-createarrayiterator
module.exports = defineIterator(Array, 'Array', function (iterated, kind) {
  setInternalState(this, {
    type: ARRAY_ITERATOR,
    target: toIndexedObject(iterated), // target
    index: 0,                          // next index
    kind: kind                         // kind
  });
// `%ArrayIteratorPrototype%.next` method
// https://tc39.es/ecma262/#sec-%arrayiteratorprototype%.next
}, function () {
  var state = getInternalState(this);
  var target = state.target;
  var index = state.index++;
  if (!target || index >= target.length) {
    state.target = null;
    return createIterResultObject(undefined, true);
  }
  switch (state.kind) {
    case 'keys': return createIterResultObject(index, false);
    case 'values': return createIterResultObject(target[index], false);
  } return createIterResultObject([index, target[index]], false);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values%
// https://tc39.es/ecma262/#sec-createunmappedargumentsobject
// https://tc39.es/ecma262/#sec-createmappedargumentsobject
var values = Iterators.Arguments = Iterators.Array;

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');

// V8 ~ Chrome 45- bug
if (!IS_PURE && DESCRIPTORS && values.name !== 'values') try {
  defineProperty(values, 'name', { value: 'values' });
} catch (error) { /* empty */ }

},{"../internals/add-to-unscopables":5,"../internals/create-iter-result-object":27,"../internals/descriptors":35,"../internals/internal-state":77,"../internals/is-pure":86,"../internals/iterator-define":93,"../internals/iterators":96,"../internals/object-define-property":105,"../internals/to-indexed-object":137}],155:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var toObject = require('../internals/to-object');
var lengthOfArrayLike = require('../internals/length-of-array-like');
var setArrayLength = require('../internals/array-set-length');
var doesNotExceedSafeInteger = require('../internals/does-not-exceed-safe-integer');
var fails = require('../internals/fails');

var INCORRECT_TO_LENGTH = fails(function () {
  return [].push.call({ length: 0x100000000 }, 1) !== 4294967297;
});

// V8 <= 121 and Safari <= 15.4; FF < 23 throws InternalError
// https://bugs.chromium.org/p/v8/issues/detail?id=12681
var properErrorOnNonWritableLength = function () {
  try {
    // eslint-disable-next-line es/no-object-defineproperty -- safe
    Object.defineProperty([], 'length', { writable: false }).push();
  } catch (error) {
    return error instanceof TypeError;
  }
};

var FORCED = INCORRECT_TO_LENGTH || !properErrorOnNonWritableLength();

// `Array.prototype.push` method
// https://tc39.es/ecma262/#sec-array.prototype.push
$({ target: 'Array', proto: true, arity: 1, forced: FORCED }, {
  // eslint-disable-next-line no-unused-vars -- required for `.length`
  push: function push(item) {
    var O = toObject(this);
    var len = lengthOfArrayLike(O);
    var argCount = arguments.length;
    doesNotExceedSafeInteger(len + argCount);
    for (var i = 0; i < argCount; i++) {
      O[len] = arguments[i];
      len++;
    }
    setArrayLength(O, len);
    return len;
  }
});

},{"../internals/array-set-length":14,"../internals/does-not-exceed-safe-integer":37,"../internals/export":48,"../internals/fails":49,"../internals/length-of-array-like":97,"../internals/to-object":140}],156:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var $reduce = require('../internals/array-reduce').left;
var arrayMethodIsStrict = require('../internals/array-method-is-strict');
var CHROME_VERSION = require('../internals/environment-v8-version');
var IS_NODE = require('../internals/environment-is-node');

// Chrome 80-82 has a critical bug
// https://bugs.chromium.org/p/chromium/issues/detail?id=1049982
var CHROME_BUG = !IS_NODE && CHROME_VERSION > 79 && CHROME_VERSION < 83;
var FORCED = CHROME_BUG || !arrayMethodIsStrict('reduce');

// `Array.prototype.reduce` method
// https://tc39.es/ecma262/#sec-array.prototype.reduce
$({ target: 'Array', proto: true, forced: FORCED }, {
  reduce: function reduce(callbackfn /* , initialValue */) {
    var length = arguments.length;
    return $reduce(this, callbackfn, length, length > 1 ? arguments[1] : undefined);
  }
});

},{"../internals/array-method-is-strict":12,"../internals/array-reduce":13,"../internals/environment-is-node":41,"../internals/environment-v8-version":43,"../internals/export":48}],157:[function(require,module,exports){
'use strict';
/* eslint-disable no-unused-vars -- required for functions `.length` */
var $ = require('../internals/export');
var globalThis = require('../internals/global-this');
var apply = require('../internals/function-apply');
var wrapErrorConstructorWithCause = require('../internals/wrap-error-constructor-with-cause');

var WEB_ASSEMBLY = 'WebAssembly';
var WebAssembly = globalThis[WEB_ASSEMBLY];

// eslint-disable-next-line es/no-error-cause -- feature detection
var FORCED = new Error('e', { cause: 7 }).cause !== 7;

var exportGlobalErrorCauseWrapper = function (ERROR_NAME, wrapper) {
  var O = {};
  O[ERROR_NAME] = wrapErrorConstructorWithCause(ERROR_NAME, wrapper, FORCED);
  $({ global: true, constructor: true, arity: 1, forced: FORCED }, O);
};

var exportWebAssemblyErrorCauseWrapper = function (ERROR_NAME, wrapper) {
  if (WebAssembly && WebAssembly[ERROR_NAME]) {
    var O = {};
    O[ERROR_NAME] = wrapErrorConstructorWithCause(WEB_ASSEMBLY + '.' + ERROR_NAME, wrapper, FORCED);
    $({ target: WEB_ASSEMBLY, stat: true, constructor: true, arity: 1, forced: FORCED }, O);
  }
};

// https://tc39.es/ecma262/#sec-nativeerror
exportGlobalErrorCauseWrapper('Error', function (init) {
  return function Error(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('EvalError', function (init) {
  return function EvalError(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('RangeError', function (init) {
  return function RangeError(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('ReferenceError', function (init) {
  return function ReferenceError(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('SyntaxError', function (init) {
  return function SyntaxError(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('TypeError', function (init) {
  return function TypeError(message) { return apply(init, this, arguments); };
});
exportGlobalErrorCauseWrapper('URIError', function (init) {
  return function URIError(message) { return apply(init, this, arguments); };
});
exportWebAssemblyErrorCauseWrapper('CompileError', function (init) {
  return function CompileError(message) { return apply(init, this, arguments); };
});
exportWebAssemblyErrorCauseWrapper('LinkError', function (init) {
  return function LinkError(message) { return apply(init, this, arguments); };
});
exportWebAssemblyErrorCauseWrapper('RuntimeError', function (init) {
  return function RuntimeError(message) { return apply(init, this, arguments); };
});

},{"../internals/export":48,"../internals/function-apply":52,"../internals/global-this":67,"../internals/wrap-error-constructor-with-cause":152}],158:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var globalThis = require('../internals/global-this');
var anInstance = require('../internals/an-instance');
var anObject = require('../internals/an-object');
var isCallable = require('../internals/is-callable');
var getPrototypeOf = require('../internals/object-get-prototype-of');
var defineBuiltInAccessor = require('../internals/define-built-in-accessor');
var createProperty = require('../internals/create-property');
var fails = require('../internals/fails');
var hasOwn = require('../internals/has-own-property');
var wellKnownSymbol = require('../internals/well-known-symbol');
var IteratorPrototype = require('../internals/iterators-core').IteratorPrototype;
var DESCRIPTORS = require('../internals/descriptors');
var IS_PURE = require('../internals/is-pure');

var CONSTRUCTOR = 'constructor';
var ITERATOR = 'Iterator';
var TO_STRING_TAG = wellKnownSymbol('toStringTag');

var $TypeError = TypeError;
var NativeIterator = globalThis[ITERATOR];

// FF56- have non-standard global helper `Iterator`
var FORCED = IS_PURE
  || !isCallable(NativeIterator)
  || NativeIterator.prototype !== IteratorPrototype
  // FF44- non-standard `Iterator` passes previous tests
  || !fails(function () { NativeIterator({}); });

var IteratorConstructor = function Iterator() {
  anInstance(this, IteratorPrototype);
  if (getPrototypeOf(this) === IteratorPrototype) throw new $TypeError('Abstract class Iterator not directly constructable');
};

var defineIteratorPrototypeAccessor = function (key, value) {
  if (DESCRIPTORS) {
    defineBuiltInAccessor(IteratorPrototype, key, {
      configurable: true,
      get: function () {
        return value;
      },
      set: function (replacement) {
        anObject(this);
        if (this === IteratorPrototype) throw new $TypeError("You can't redefine this property");
        if (hasOwn(this, key)) this[key] = replacement;
        else createProperty(this, key, replacement);
      }
    });
  } else IteratorPrototype[key] = value;
};

if (!hasOwn(IteratorPrototype, TO_STRING_TAG)) defineIteratorPrototypeAccessor(TO_STRING_TAG, ITERATOR);

if (FORCED || !hasOwn(IteratorPrototype, CONSTRUCTOR) || IteratorPrototype[CONSTRUCTOR] === Object) {
  defineIteratorPrototypeAccessor(CONSTRUCTOR, IteratorConstructor);
}

IteratorConstructor.prototype = IteratorPrototype;

// `Iterator` constructor
// https://tc39.es/ecma262/#sec-iterator
$({ global: true, constructor: true, forced: FORCED }, {
  Iterator: IteratorConstructor
});

},{"../internals/an-instance":7,"../internals/an-object":8,"../internals/create-property":30,"../internals/define-built-in-accessor":31,"../internals/descriptors":35,"../internals/export":48,"../internals/fails":49,"../internals/global-this":67,"../internals/has-own-property":68,"../internals/is-callable":80,"../internals/is-pure":86,"../internals/iterators-core":95,"../internals/object-get-prototype-of":110,"../internals/well-known-symbol":150}],159:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var iterate = require('../internals/iterate');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');

// `Iterator.prototype.every` method
// https://tc39.es/ecma262/#sec-iterator.prototype.every
$({ target: 'Iterator', proto: true, real: true }, {
  every: function every(predicate) {
    anObject(this);
    aCallable(predicate);
    var record = getIteratorDirect(this);
    var counter = 0;
    return !iterate(record, function (value, stop) {
      if (!predicate(value, counter++)) return stop();
    }, { IS_RECORD: true, INTERRUPTED: true }).stopped;
  }
});

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/export":48,"../internals/get-iterator-direct":61,"../internals/iterate":89}],160:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var call = require('../internals/function-call');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');
var createIteratorProxy = require('../internals/iterator-create-proxy');
var callWithSafeIterationClosing = require('../internals/call-with-safe-iteration-closing');
var IS_PURE = require('../internals/is-pure');

var IteratorProxy = createIteratorProxy(function () {
  var iterator = this.iterator;
  var predicate = this.predicate;
  var next = this.next;
  var result, done, value;
  while (true) {
    result = anObject(call(next, iterator));
    done = this.done = !!result.done;
    if (done) return;
    value = result.value;
    if (callWithSafeIterationClosing(iterator, predicate, [value, this.counter++], true)) return value;
  }
});

// `Iterator.prototype.filter` method
// https://tc39.es/ecma262/#sec-iterator.prototype.filter
$({ target: 'Iterator', proto: true, real: true, forced: IS_PURE }, {
  filter: function filter(predicate) {
    anObject(this);
    aCallable(predicate);
    return new IteratorProxy(getIteratorDirect(this), {
      predicate: predicate
    });
  }
});

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/call-with-safe-iteration-closing":18,"../internals/export":48,"../internals/function-call":55,"../internals/get-iterator-direct":61,"../internals/is-pure":86,"../internals/iterator-create-proxy":92}],161:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var iterate = require('../internals/iterate');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');

// `Iterator.prototype.find` method
// https://tc39.es/ecma262/#sec-iterator.prototype.find
$({ target: 'Iterator', proto: true, real: true }, {
  find: function find(predicate) {
    anObject(this);
    aCallable(predicate);
    var record = getIteratorDirect(this);
    var counter = 0;
    return iterate(record, function (value, stop) {
      if (predicate(value, counter++)) return stop(value);
    }, { IS_RECORD: true, INTERRUPTED: true }).result;
  }
});

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/export":48,"../internals/get-iterator-direct":61,"../internals/iterate":89}],162:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var iterate = require('../internals/iterate');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');

// `Iterator.prototype.forEach` method
// https://tc39.es/ecma262/#sec-iterator.prototype.foreach
$({ target: 'Iterator', proto: true, real: true }, {
  forEach: function forEach(fn) {
    anObject(this);
    aCallable(fn);
    var record = getIteratorDirect(this);
    var counter = 0;
    iterate(record, function (value) {
      fn(value, counter++);
    }, { IS_RECORD: true });
  }
});

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/export":48,"../internals/get-iterator-direct":61,"../internals/iterate":89}],163:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var map = require('../internals/iterator-map');
var IS_PURE = require('../internals/is-pure');

// `Iterator.prototype.map` method
// https://tc39.es/ecma262/#sec-iterator.prototype.map
$({ target: 'Iterator', proto: true, real: true, forced: IS_PURE }, {
  map: map
});

},{"../internals/export":48,"../internals/is-pure":86,"../internals/iterator-map":94}],164:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var iterate = require('../internals/iterate');
var aCallable = require('../internals/a-callable');
var anObject = require('../internals/an-object');
var getIteratorDirect = require('../internals/get-iterator-direct');

var $TypeError = TypeError;

// `Iterator.prototype.reduce` method
// https://tc39.es/ecma262/#sec-iterator.prototype.reduce
$({ target: 'Iterator', proto: true, real: true }, {
  reduce: function reduce(reducer /* , initialValue */) {
    anObject(this);
    aCallable(reducer);
    var record = getIteratorDirect(this);
    var noInitial = arguments.length < 2;
    var accumulator = noInitial ? undefined : arguments[1];
    var counter = 0;
    iterate(record, function (value) {
      if (noInitial) {
        noInitial = false;
        accumulator = value;
      } else {
        accumulator = reducer(accumulator, value, counter);
      }
      counter++;
    }, { IS_RECORD: true });
    if (noInitial) throw new $TypeError('Reduce of empty iterator with no initial value');
    return accumulator;
  }
});

},{"../internals/a-callable":2,"../internals/an-object":8,"../internals/export":48,"../internals/get-iterator-direct":61,"../internals/iterate":89}],165:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var getBuiltIn = require('../internals/get-built-in');
var apply = require('../internals/function-apply');
var call = require('../internals/function-call');
var uncurryThis = require('../internals/function-uncurry-this');
var fails = require('../internals/fails');
var isCallable = require('../internals/is-callable');
var isSymbol = require('../internals/is-symbol');
var arraySlice = require('../internals/array-slice');
var getReplacerFunction = require('../internals/get-json-replacer-function');
var NATIVE_SYMBOL = require('../internals/symbol-constructor-detection');

var $String = String;
var $stringify = getBuiltIn('JSON', 'stringify');
var exec = uncurryThis(/./.exec);
var charAt = uncurryThis(''.charAt);
var charCodeAt = uncurryThis(''.charCodeAt);
var replace = uncurryThis(''.replace);
var numberToString = uncurryThis(1.0.toString);

var tester = /[\uD800-\uDFFF]/g;
var low = /^[\uD800-\uDBFF]$/;
var hi = /^[\uDC00-\uDFFF]$/;

var WRONG_SYMBOLS_CONVERSION = !NATIVE_SYMBOL || fails(function () {
  var symbol = getBuiltIn('Symbol')('stringify detection');
  // MS Edge converts symbol values to JSON as {}
  return $stringify([symbol]) !== '[null]'
    // WebKit converts symbol values to JSON as null
    || $stringify({ a: symbol }) !== '{}'
    // V8 throws on boxed symbols
    || $stringify(Object(symbol)) !== '{}';
});

// https://github.com/tc39/proposal-well-formed-stringify
var ILL_FORMED_UNICODE = fails(function () {
  return $stringify('\uDF06\uD834') !== '"\\udf06\\ud834"'
    || $stringify('\uDEAD') !== '"\\udead"';
});

var stringifyWithSymbolsFix = function (it, replacer) {
  var args = arraySlice(arguments);
  var $replacer = getReplacerFunction(replacer);
  if (!isCallable($replacer) && (it === undefined || isSymbol(it))) return; // IE8 returns string on undefined
  args[1] = function (key, value) {
    // some old implementations (like WebKit) could pass numbers as keys
    if (isCallable($replacer)) value = call($replacer, this, $String(key), value);
    if (!isSymbol(value)) return value;
  };
  return apply($stringify, null, args);
};

var fixIllFormed = function (match, offset, string) {
  var prev = charAt(string, offset - 1);
  var next = charAt(string, offset + 1);
  if ((exec(low, match) && !exec(hi, next)) || (exec(hi, match) && !exec(low, prev))) {
    return '\\u' + numberToString(charCodeAt(match, 0), 16);
  } return match;
};

if ($stringify) {
  // `JSON.stringify` method
  // https://tc39.es/ecma262/#sec-json.stringify
  $({ target: 'JSON', stat: true, arity: 3, forced: WRONG_SYMBOLS_CONVERSION || ILL_FORMED_UNICODE }, {
    // eslint-disable-next-line no-unused-vars -- required for `.length`
    stringify: function stringify(it, replacer, space) {
      var args = arraySlice(arguments);
      var result = apply(WRONG_SYMBOLS_CONVERSION ? stringifyWithSymbolsFix : $stringify, null, args);
      return ILL_FORMED_UNICODE && typeof result == 'string' ? replace(result, tester, fixIllFormed) : result;
    }
  });
}

},{"../internals/array-slice":15,"../internals/export":48,"../internals/fails":49,"../internals/function-apply":52,"../internals/function-call":55,"../internals/function-uncurry-this":59,"../internals/get-built-in":60,"../internals/get-json-replacer-function":64,"../internals/is-callable":80,"../internals/is-symbol":88,"../internals/symbol-constructor-detection":135}],166:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var $parseInt = require('../internals/number-parse-int');

// `parseInt` method
// https://tc39.es/ecma262/#sec-parseint-string-radix
$({ global: true, forced: parseInt !== $parseInt }, {
  parseInt: $parseInt
});

},{"../internals/export":48,"../internals/number-parse-int":102}],167:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var exec = require('../internals/regexp-exec');

// `RegExp.prototype.exec` method
// https://tc39.es/ecma262/#sec-regexp.prototype.exec
$({ target: 'RegExp', proto: true, forced: /./.exec !== exec }, {
  exec: exec
});

},{"../internals/export":48,"../internals/regexp-exec":121}],168:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4` since it's moved to entry points
require('../modules/es.regexp.exec');
var $ = require('../internals/export');
var call = require('../internals/function-call');
var isCallable = require('../internals/is-callable');
var anObject = require('../internals/an-object');
var toString = require('../internals/to-string');

var DELEGATES_TO_EXEC = function () {
  var execCalled = false;
  var re = /[ac]/;
  re.exec = function () {
    execCalled = true;
    return /./.exec.apply(this, arguments);
  };
  return re.test('abc') === true && execCalled;
}();

var nativeTest = /./.test;

// `RegExp.prototype.test` method
// https://tc39.es/ecma262/#sec-regexp.prototype.test
$({ target: 'RegExp', proto: true, forced: !DELEGATES_TO_EXEC }, {
  test: function (S) {
    var R = anObject(this);
    var string = toString(S);
    var exec = R.exec;
    if (!isCallable(exec)) return call(nativeTest, R, string);
    var result = call(exec, R, string);
    if (result === null) return false;
    anObject(result);
    return true;
  }
});

},{"../internals/an-object":8,"../internals/export":48,"../internals/function-call":55,"../internals/is-callable":80,"../internals/to-string":144,"../modules/es.regexp.exec":167}],169:[function(require,module,exports){
'use strict';
var PROPER_FUNCTION_NAME = require('../internals/function-name').PROPER;
var defineBuiltIn = require('../internals/define-built-in');
var anObject = require('../internals/an-object');
var $toString = require('../internals/to-string');
var fails = require('../internals/fails');
var getRegExpFlags = require('../internals/regexp-get-flags');

var TO_STRING = 'toString';
var RegExpPrototype = RegExp.prototype;
var nativeToString = RegExpPrototype[TO_STRING];

var NOT_GENERIC = fails(function () { return nativeToString.call({ source: 'a', flags: 'b' }) !== '/a/b'; });
// FF44- RegExp#toString has a wrong name
var INCORRECT_NAME = PROPER_FUNCTION_NAME && nativeToString.name !== TO_STRING;

// `RegExp.prototype.toString` method
// https://tc39.es/ecma262/#sec-regexp.prototype.tostring
if (NOT_GENERIC || INCORRECT_NAME) {
  defineBuiltIn(RegExpPrototype, TO_STRING, function toString() {
    var R = anObject(this);
    var pattern = $toString(R.source);
    var flags = $toString(getRegExpFlags(R));
    return '/' + pattern + '/' + flags;
  }, { unsafe: true });
}

},{"../internals/an-object":8,"../internals/define-built-in":32,"../internals/fails":49,"../internals/function-name":56,"../internals/regexp-get-flags":123,"../internals/to-string":144}],170:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var uncurryThis = require('../internals/function-uncurry-this');
var notARegExp = require('../internals/not-a-regexp');
var requireObjectCoercible = require('../internals/require-object-coercible');
var toString = require('../internals/to-string');
var correctIsRegExpLogic = require('../internals/correct-is-regexp-logic');

var stringIndexOf = uncurryThis(''.indexOf);

// `String.prototype.includes` method
// https://tc39.es/ecma262/#sec-string.prototype.includes
$({ target: 'String', proto: true, forced: !correctIsRegExpLogic('includes') }, {
  includes: function includes(searchString /* , position = 0 */) {
    return !!~stringIndexOf(
      toString(requireObjectCoercible(this)),
      toString(notARegExp(searchString)),
      arguments.length > 1 ? arguments[1] : undefined
    );
  }
});

},{"../internals/correct-is-regexp-logic":25,"../internals/export":48,"../internals/function-uncurry-this":59,"../internals/not-a-regexp":101,"../internals/require-object-coercible":127,"../internals/to-string":144}],171:[function(require,module,exports){
'use strict';
var apply = require('../internals/function-apply');
var call = require('../internals/function-call');
var uncurryThis = require('../internals/function-uncurry-this');
var fixRegExpWellKnownSymbolLogic = require('../internals/fix-regexp-well-known-symbol-logic');
var fails = require('../internals/fails');
var anObject = require('../internals/an-object');
var isCallable = require('../internals/is-callable');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var toIntegerOrInfinity = require('../internals/to-integer-or-infinity');
var toLength = require('../internals/to-length');
var toString = require('../internals/to-string');
var requireObjectCoercible = require('../internals/require-object-coercible');
var advanceStringIndex = require('../internals/advance-string-index');
var getMethod = require('../internals/get-method');
var getSubstitution = require('../internals/get-substitution');
var regExpExec = require('../internals/regexp-exec-abstract');
var wellKnownSymbol = require('../internals/well-known-symbol');

var REPLACE = wellKnownSymbol('replace');
var max = Math.max;
var min = Math.min;
var concat = uncurryThis([].concat);
var push = uncurryThis([].push);
var stringIndexOf = uncurryThis(''.indexOf);
var stringSlice = uncurryThis(''.slice);

var maybeToString = function (it) {
  return it === undefined ? it : String(it);
};

// IE <= 11 replaces $0 with the whole match, as if it was $&
// https://stackoverflow.com/questions/6024666/getting-ie-to-replace-a-regex-with-the-literal-string-0
var REPLACE_KEEPS_$0 = (function () {
  // eslint-disable-next-line regexp/prefer-escape-replacement-dollar-char -- required for testing
  return 'a'.replace(/./, '$0') === '$0';
})();

// Safari <= 13.0.3(?) substitutes nth capture where n>m with an empty string
var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = (function () {
  if (/./[REPLACE]) {
    return /./[REPLACE]('a', '$0') === '';
  }
  return false;
})();

var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
  var re = /./;
  re.exec = function () {
    var result = [];
    result.groups = { a: '7' };
    return result;
  };
  // eslint-disable-next-line regexp/no-useless-dollar-replacements -- false positive
  return ''.replace(re, '$<a>') !== '7';
});

// @@replace logic
fixRegExpWellKnownSymbolLogic('replace', function (_, nativeReplace, maybeCallNative) {
  var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? '$' : '$0';

  return [
    // `String.prototype.replace` method
    // https://tc39.es/ecma262/#sec-string.prototype.replace
    function replace(searchValue, replaceValue) {
      var O = requireObjectCoercible(this);
      var replacer = isNullOrUndefined(searchValue) ? undefined : getMethod(searchValue, REPLACE);
      return replacer
        ? call(replacer, searchValue, O, replaceValue)
        : call(nativeReplace, toString(O), searchValue, replaceValue);
    },
    // `RegExp.prototype[@@replace]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@replace
    function (string, replaceValue) {
      var rx = anObject(this);
      var S = toString(string);

      if (
        typeof replaceValue == 'string' &&
        stringIndexOf(replaceValue, UNSAFE_SUBSTITUTE) === -1 &&
        stringIndexOf(replaceValue, '$<') === -1
      ) {
        var res = maybeCallNative(nativeReplace, rx, S, replaceValue);
        if (res.done) return res.value;
      }

      var functionalReplace = isCallable(replaceValue);
      if (!functionalReplace) replaceValue = toString(replaceValue);

      var global = rx.global;
      var fullUnicode;
      if (global) {
        fullUnicode = rx.unicode;
        rx.lastIndex = 0;
      }

      var results = [];
      var result;
      while (true) {
        result = regExpExec(rx, S);
        if (result === null) break;

        push(results, result);
        if (!global) break;

        var matchStr = toString(result[0]);
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
      }

      var accumulatedResult = '';
      var nextSourcePosition = 0;
      for (var i = 0; i < results.length; i++) {
        result = results[i];

        var matched = toString(result[0]);
        var position = max(min(toIntegerOrInfinity(result.index), S.length), 0);
        var captures = [];
        var replacement;
        // NOTE: This is equivalent to
        //   captures = result.slice(1).map(maybeToString)
        // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
        // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
        // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
        for (var j = 1; j < result.length; j++) push(captures, maybeToString(result[j]));
        var namedCaptures = result.groups;
        if (functionalReplace) {
          var replacerArgs = concat([matched], captures, position, S);
          if (namedCaptures !== undefined) push(replacerArgs, namedCaptures);
          replacement = toString(apply(replaceValue, undefined, replacerArgs));
        } else {
          replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
        }
        if (position >= nextSourcePosition) {
          accumulatedResult += stringSlice(S, nextSourcePosition, position) + replacement;
          nextSourcePosition = position + matched.length;
        }
      }

      return accumulatedResult + stringSlice(S, nextSourcePosition);
    }
  ];
}, !REPLACE_SUPPORTS_NAMED_GROUPS || !REPLACE_KEEPS_$0 || REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE);

},{"../internals/advance-string-index":6,"../internals/an-object":8,"../internals/fails":49,"../internals/fix-regexp-well-known-symbol-logic":50,"../internals/function-apply":52,"../internals/function-call":55,"../internals/function-uncurry-this":59,"../internals/get-method":65,"../internals/get-substitution":66,"../internals/is-callable":80,"../internals/is-null-or-undefined":83,"../internals/regexp-exec-abstract":120,"../internals/require-object-coercible":127,"../internals/to-integer-or-infinity":138,"../internals/to-length":139,"../internals/to-string":144,"../internals/well-known-symbol":150}],172:[function(require,module,exports){
'use strict';
var call = require('../internals/function-call');
var uncurryThis = require('../internals/function-uncurry-this');
var fixRegExpWellKnownSymbolLogic = require('../internals/fix-regexp-well-known-symbol-logic');
var anObject = require('../internals/an-object');
var isNullOrUndefined = require('../internals/is-null-or-undefined');
var requireObjectCoercible = require('../internals/require-object-coercible');
var speciesConstructor = require('../internals/species-constructor');
var advanceStringIndex = require('../internals/advance-string-index');
var toLength = require('../internals/to-length');
var toString = require('../internals/to-string');
var getMethod = require('../internals/get-method');
var regExpExec = require('../internals/regexp-exec-abstract');
var stickyHelpers = require('../internals/regexp-sticky-helpers');
var fails = require('../internals/fails');

var UNSUPPORTED_Y = stickyHelpers.UNSUPPORTED_Y;
var MAX_UINT32 = 0xFFFFFFFF;
var min = Math.min;
var push = uncurryThis([].push);
var stringSlice = uncurryThis(''.slice);

// Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
// Weex JS has frozen built-in prototypes, so use try / catch wrapper
var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = !fails(function () {
  // eslint-disable-next-line regexp/no-empty-group -- required for testing
  var re = /(?:)/;
  var originalExec = re.exec;
  re.exec = function () { return originalExec.apply(this, arguments); };
  var result = 'ab'.split(re);
  return result.length !== 2 || result[0] !== 'a' || result[1] !== 'b';
});

var BUGGY = 'abbc'.split(/(b)*/)[1] === 'c' ||
  // eslint-disable-next-line regexp/no-empty-group -- required for testing
  'test'.split(/(?:)/, -1).length !== 4 ||
  'ab'.split(/(?:ab)*/).length !== 2 ||
  '.'.split(/(.?)(.?)/).length !== 4 ||
  // eslint-disable-next-line regexp/no-empty-capturing-group, regexp/no-empty-group -- required for testing
  '.'.split(/()()/).length > 1 ||
  ''.split(/.?/).length;

// @@split logic
fixRegExpWellKnownSymbolLogic('split', function (SPLIT, nativeSplit, maybeCallNative) {
  var internalSplit = '0'.split(undefined, 0).length ? function (separator, limit) {
    return separator === undefined && limit === 0 ? [] : call(nativeSplit, this, separator, limit);
  } : nativeSplit;

  return [
    // `String.prototype.split` method
    // https://tc39.es/ecma262/#sec-string.prototype.split
    function split(separator, limit) {
      var O = requireObjectCoercible(this);
      var splitter = isNullOrUndefined(separator) ? undefined : getMethod(separator, SPLIT);
      return splitter
        ? call(splitter, separator, O, limit)
        : call(internalSplit, toString(O), separator, limit);
    },
    // `RegExp.prototype[@@split]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@split
    //
    // NOTE: This cannot be properly polyfilled in engines that don't support
    // the 'y' flag.
    function (string, limit) {
      var rx = anObject(this);
      var S = toString(string);

      if (!BUGGY) {
        var res = maybeCallNative(internalSplit, rx, S, limit, internalSplit !== nativeSplit);
        if (res.done) return res.value;
      }

      var C = speciesConstructor(rx, RegExp);
      var unicodeMatching = rx.unicode;
      var flags = (rx.ignoreCase ? 'i' : '') +
                  (rx.multiline ? 'm' : '') +
                  (rx.unicode ? 'u' : '') +
                  (UNSUPPORTED_Y ? 'g' : 'y');
      // ^(? + rx + ) is needed, in combination with some S slicing, to
      // simulate the 'y' flag.
      var splitter = new C(UNSUPPORTED_Y ? '^(?:' + rx.source + ')' : rx, flags);
      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
      if (lim === 0) return [];
      if (S.length === 0) return regExpExec(splitter, S) === null ? [S] : [];
      var p = 0;
      var q = 0;
      var A = [];
      while (q < S.length) {
        splitter.lastIndex = UNSUPPORTED_Y ? 0 : q;
        var z = regExpExec(splitter, UNSUPPORTED_Y ? stringSlice(S, q) : S);
        var e;
        if (
          z === null ||
          (e = min(toLength(splitter.lastIndex + (UNSUPPORTED_Y ? q : 0)), S.length)) === p
        ) {
          q = advanceStringIndex(S, q, unicodeMatching);
        } else {
          push(A, stringSlice(S, p, q));
          if (A.length === lim) return A;
          for (var i = 1; i <= z.length - 1; i++) {
            push(A, z[i]);
            if (A.length === lim) return A;
          }
          q = p = e;
        }
      }
      push(A, stringSlice(S, p));
      return A;
    }
  ];
}, BUGGY || !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC, UNSUPPORTED_Y);

},{"../internals/advance-string-index":6,"../internals/an-object":8,"../internals/fails":49,"../internals/fix-regexp-well-known-symbol-logic":50,"../internals/function-call":55,"../internals/function-uncurry-this":59,"../internals/get-method":65,"../internals/is-null-or-undefined":83,"../internals/regexp-exec-abstract":120,"../internals/regexp-sticky-helpers":124,"../internals/require-object-coercible":127,"../internals/species-constructor":132,"../internals/to-length":139,"../internals/to-string":144}],173:[function(require,module,exports){
'use strict';
var $ = require('../internals/export');
var uncurryThis = require('../internals/function-uncurry-this-clause');
var getOwnPropertyDescriptor = require('../internals/object-get-own-property-descriptor').f;
var toLength = require('../internals/to-length');
var toString = require('../internals/to-string');
var notARegExp = require('../internals/not-a-regexp');
var requireObjectCoercible = require('../internals/require-object-coercible');
var correctIsRegExpLogic = require('../internals/correct-is-regexp-logic');
var IS_PURE = require('../internals/is-pure');

var stringSlice = uncurryThis(''.slice);
var min = Math.min;

var CORRECT_IS_REGEXP_LOGIC = correctIsRegExpLogic('startsWith');
// https://github.com/zloirock/core-js/pull/702
var MDN_POLYFILL_BUG = !IS_PURE && !CORRECT_IS_REGEXP_LOGIC && !!function () {
  var descriptor = getOwnPropertyDescriptor(String.prototype, 'startsWith');
  return descriptor && !descriptor.writable;
}();

// `String.prototype.startsWith` method
// https://tc39.es/ecma262/#sec-string.prototype.startswith
$({ target: 'String', proto: true, forced: !MDN_POLYFILL_BUG && !CORRECT_IS_REGEXP_LOGIC }, {
  startsWith: function startsWith(searchString /* , position = 0 */) {
    var that = toString(requireObjectCoercible(this));
    notARegExp(searchString);
    var index = toLength(min(arguments.length > 1 ? arguments[1] : undefined, that.length));
    var search = toString(searchString);
    return stringSlice(that, index, index + search.length) === search;
  }
});

},{"../internals/correct-is-regexp-logic":25,"../internals/export":48,"../internals/function-uncurry-this-clause":58,"../internals/is-pure":86,"../internals/not-a-regexp":101,"../internals/object-get-own-property-descriptor":106,"../internals/require-object-coercible":127,"../internals/to-length":139,"../internals/to-string":144}],174:[function(require,module,exports){
// `Symbol.prototype.description` getter
// https://tc39.es/ecma262/#sec-symbol.prototype.description
'use strict';
var $ = require('../internals/export');
var DESCRIPTORS = require('../internals/descriptors');
var globalThis = require('../internals/global-this');
var uncurryThis = require('../internals/function-uncurry-this');
var hasOwn = require('../internals/has-own-property');
var isCallable = require('../internals/is-callable');
var isPrototypeOf = require('../internals/object-is-prototype-of');
var toString = require('../internals/to-string');
var defineBuiltInAccessor = require('../internals/define-built-in-accessor');
var copyConstructorProperties = require('../internals/copy-constructor-properties');

var NativeSymbol = globalThis.Symbol;
var SymbolPrototype = NativeSymbol && NativeSymbol.prototype;

if (DESCRIPTORS && isCallable(NativeSymbol) && (!('description' in SymbolPrototype) ||
  // Safari 12 bug
  NativeSymbol().description !== undefined
)) {
  var EmptyStringDescriptionStore = {};
  // wrap Symbol constructor for correct work with undefined description
  var SymbolWrapper = function Symbol() {
    var description = arguments.length < 1 || arguments[0] === undefined ? undefined : toString(arguments[0]);
    var result = isPrototypeOf(SymbolPrototype, this)
      // eslint-disable-next-line sonarjs/inconsistent-function-call -- ok
      ? new NativeSymbol(description)
      // in Edge 13, String(Symbol(undefined)) === 'Symbol(undefined)'
      : description === undefined ? NativeSymbol() : NativeSymbol(description);
    if (description === '') EmptyStringDescriptionStore[result] = true;
    return result;
  };

  copyConstructorProperties(SymbolWrapper, NativeSymbol);
  SymbolWrapper.prototype = SymbolPrototype;
  SymbolPrototype.constructor = SymbolWrapper;

  var NATIVE_SYMBOL = String(NativeSymbol('description detection')) === 'Symbol(description detection)';
  var thisSymbolValue = uncurryThis(SymbolPrototype.valueOf);
  var symbolDescriptiveString = uncurryThis(SymbolPrototype.toString);
  var regexp = /^Symbol\((.*)\)[^)]+$/;
  var replace = uncurryThis(''.replace);
  var stringSlice = uncurryThis(''.slice);

  defineBuiltInAccessor(SymbolPrototype, 'description', {
    configurable: true,
    get: function description() {
      var symbol = thisSymbolValue(this);
      if (hasOwn(EmptyStringDescriptionStore, symbol)) return '';
      var string = symbolDescriptiveString(symbol);
      var desc = NATIVE_SYMBOL ? stringSlice(string, 7, -1) : replace(string, regexp, '$1');
      return desc === '' ? undefined : desc;
    }
  });

  $({ global: true, constructor: true, forced: true }, {
    Symbol: SymbolWrapper
  });
}

},{"../internals/copy-constructor-properties":24,"../internals/define-built-in-accessor":31,"../internals/descriptors":35,"../internals/export":48,"../internals/function-uncurry-this":59,"../internals/global-this":67,"../internals/has-own-property":68,"../internals/is-callable":80,"../internals/object-is-prototype-of":112,"../internals/to-string":144}],175:[function(require,module,exports){
'use strict';
var FREEZING = require('../internals/freezing');
var globalThis = require('../internals/global-this');
var uncurryThis = require('../internals/function-uncurry-this');
var defineBuiltIns = require('../internals/define-built-ins');
var InternalMetadataModule = require('../internals/internal-metadata');
var collection = require('../internals/collection');
var collectionWeak = require('../internals/collection-weak');
var isObject = require('../internals/is-object');
var enforceInternalState = require('../internals/internal-state').enforce;
var fails = require('../internals/fails');
var NATIVE_WEAK_MAP = require('../internals/weak-map-basic-detection');

var $Object = Object;
// eslint-disable-next-line es/no-array-isarray -- safe
var isArray = Array.isArray;
// eslint-disable-next-line es/no-object-isextensible -- safe
var isExtensible = $Object.isExtensible;
// eslint-disable-next-line es/no-object-isfrozen -- safe
var isFrozen = $Object.isFrozen;
// eslint-disable-next-line es/no-object-issealed -- safe
var isSealed = $Object.isSealed;
// eslint-disable-next-line es/no-object-freeze -- safe
var freeze = $Object.freeze;
// eslint-disable-next-line es/no-object-seal -- safe
var seal = $Object.seal;

var IS_IE11 = !globalThis.ActiveXObject && 'ActiveXObject' in globalThis;
var InternalWeakMap;

var wrapper = function (init) {
  return function WeakMap() {
    return init(this, arguments.length ? arguments[0] : undefined);
  };
};

// `WeakMap` constructor
// https://tc39.es/ecma262/#sec-weakmap-constructor
var $WeakMap = collection('WeakMap', wrapper, collectionWeak);
var WeakMapPrototype = $WeakMap.prototype;
var nativeSet = uncurryThis(WeakMapPrototype.set);

// Chakra Edge bug: adding frozen arrays to WeakMap unfreeze them
var hasMSEdgeFreezingBug = function () {
  return FREEZING && fails(function () {
    var frozenArray = freeze([]);
    nativeSet(new $WeakMap(), frozenArray, 1);
    return !isFrozen(frozenArray);
  });
};

// IE11 WeakMap frozen keys fix
// We can't use feature detection because it crash some old IE builds
// https://github.com/zloirock/core-js/issues/485
if (NATIVE_WEAK_MAP) if (IS_IE11) {
  InternalWeakMap = collectionWeak.getConstructor(wrapper, 'WeakMap', true);
  InternalMetadataModule.enable();
  var nativeDelete = uncurryThis(WeakMapPrototype['delete']);
  var nativeHas = uncurryThis(WeakMapPrototype.has);
  var nativeGet = uncurryThis(WeakMapPrototype.get);
  defineBuiltIns(WeakMapPrototype, {
    'delete': function (key) {
      if (isObject(key) && !isExtensible(key)) {
        var state = enforceInternalState(this);
        if (!state.frozen) state.frozen = new InternalWeakMap();
        return nativeDelete(this, key) || state.frozen['delete'](key);
      } return nativeDelete(this, key);
    },
    has: function has(key) {
      if (isObject(key) && !isExtensible(key)) {
        var state = enforceInternalState(this);
        if (!state.frozen) state.frozen = new InternalWeakMap();
        return nativeHas(this, key) || state.frozen.has(key);
      } return nativeHas(this, key);
    },
    get: function get(key) {
      if (isObject(key) && !isExtensible(key)) {
        var state = enforceInternalState(this);
        if (!state.frozen) state.frozen = new InternalWeakMap();
        return nativeHas(this, key) ? nativeGet(this, key) : state.frozen.get(key);
      } return nativeGet(this, key);
    },
    set: function set(key, value) {
      if (isObject(key) && !isExtensible(key)) {
        var state = enforceInternalState(this);
        if (!state.frozen) state.frozen = new InternalWeakMap();
        nativeHas(this, key) ? nativeSet(this, key, value) : state.frozen.set(key, value);
      } else nativeSet(this, key, value);
      return this;
    }
  });
// Chakra Edge frozen keys fix
} else if (hasMSEdgeFreezingBug()) {
  defineBuiltIns(WeakMapPrototype, {
    set: function set(key, value) {
      var arrayIntegrityLevel;
      if (isArray(key)) {
        if (isFrozen(key)) arrayIntegrityLevel = freeze;
        else if (isSealed(key)) arrayIntegrityLevel = seal;
      }
      nativeSet(this, key, value);
      if (arrayIntegrityLevel) arrayIntegrityLevel(key);
      return this;
    }
  });
}

},{"../internals/collection":23,"../internals/collection-weak":22,"../internals/define-built-ins":33,"../internals/fails":49,"../internals/freezing":51,"../internals/function-uncurry-this":59,"../internals/global-this":67,"../internals/internal-metadata":76,"../internals/internal-state":77,"../internals/is-object":84,"../internals/weak-map-basic-detection":149}],176:[function(require,module,exports){
'use strict';
// TODO: Remove this module from `core-js@4` since it's replaced to module below
require('../modules/es.weak-map.constructor');

},{"../modules/es.weak-map.constructor":175}],177:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.constructor');

},{"../modules/es.iterator.constructor":158}],178:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.every');

},{"../modules/es.iterator.every":159}],179:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.filter');

},{"../modules/es.iterator.filter":160}],180:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.find');

},{"../modules/es.iterator.find":161}],181:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.for-each');

},{"../modules/es.iterator.for-each":162}],182:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.map');

},{"../modules/es.iterator.map":163}],183:[function(require,module,exports){
'use strict';
// TODO: Remove from `core-js@4`
require('../modules/es.iterator.reduce');

},{"../modules/es.iterator.reduce":164}],184:[function(require,module,exports){
'use strict';
var globalThis = require('../internals/global-this');
var DOMIterables = require('../internals/dom-iterables');
var DOMTokenListPrototype = require('../internals/dom-token-list-prototype');
var ArrayIteratorMethods = require('../modules/es.array.iterator');
var createNonEnumerableProperty = require('../internals/create-non-enumerable-property');
var setToStringTag = require('../internals/set-to-string-tag');
var wellKnownSymbol = require('../internals/well-known-symbol');

var ITERATOR = wellKnownSymbol('iterator');
var ArrayValues = ArrayIteratorMethods.values;

var handlePrototype = function (CollectionPrototype, COLLECTION_NAME) {
  if (CollectionPrototype) {
    // some Chrome versions have non-configurable methods on DOMTokenList
    if (CollectionPrototype[ITERATOR] !== ArrayValues) try {
      createNonEnumerableProperty(CollectionPrototype, ITERATOR, ArrayValues);
    } catch (error) {
      CollectionPrototype[ITERATOR] = ArrayValues;
    }
    setToStringTag(CollectionPrototype, COLLECTION_NAME, true);
    if (DOMIterables[COLLECTION_NAME]) for (var METHOD_NAME in ArrayIteratorMethods) {
      // some Chrome versions have non-configurable methods on DOMTokenList
      if (CollectionPrototype[METHOD_NAME] !== ArrayIteratorMethods[METHOD_NAME]) try {
        createNonEnumerableProperty(CollectionPrototype, METHOD_NAME, ArrayIteratorMethods[METHOD_NAME]);
      } catch (error) {
        CollectionPrototype[METHOD_NAME] = ArrayIteratorMethods[METHOD_NAME];
      }
    }
  }
};

for (var COLLECTION_NAME in DOMIterables) {
  handlePrototype(globalThis[COLLECTION_NAME] && globalThis[COLLECTION_NAME].prototype, COLLECTION_NAME);
}

handlePrototype(DOMTokenListPrototype, 'DOMTokenList');

},{"../internals/create-non-enumerable-property":28,"../internals/dom-iterables":38,"../internals/dom-token-list-prototype":39,"../internals/global-this":67,"../internals/set-to-string-tag":128,"../internals/well-known-symbol":150,"../modules/es.array.iterator":154}],185:[function(require,module,exports){
function e(e){return e&&"object"==typeof e&&"default"in e?e:{default:e}}var n=/*#__PURE__*/e(require("jsep"));function t(){return t=Object.assign?Object.assign.bind():function(e){for(var n=1;n<arguments.length;n++){var t=arguments[n];for(var r in t)Object.prototype.hasOwnProperty.call(t,r)&&(e[r]=t[r])}return e},t.apply(this,arguments)}var r=/*#__PURE__*/function(){function e(e,n){this.context=void 0,this.isAsync=void 0,this.context=e,this.isAsync=n}e.addUnaryOp=function(t,r){n.default.addUnaryOp(t),e.unops[t]=r},e.addBinaryOp=function(t,r,o,i){var a,s,u;"function"==typeof r?u=r:(a=r,"function"==typeof o?u=o:(s=o,u=i)),n.default.addBinaryOp(t,a||1,s),e.binops[t]=u},e.addEvaluator=function(n,t){e.evaluators[n]=t},e.registerPlugin=function(){[].slice.call(arguments).forEach(function(n){n.init&&e.parse.plugins.register(n),n.initEval&&n.initEval.call(e,e)})},e.eval=function(n,t){return new e(t).eval(n)},e.evalAsync=function(n,t){try{return Promise.resolve(new e(t,!0).eval(n))}catch(e){return Promise.reject(e)}},e.compile=function(n){return e.eval.bind(null,e.jsep(n))},e.compileAsync=function(n){return e.evalAsync.bind(null,e.jsep(n))},e.evalExpr=function(n,t){return e.compile(n)(t)},e.evalExprAsync=function(n,t){return e.compileAsync(n)(t)};var r=e.prototype;return r.eval=function(n,t){void 0===t&&(t=function(e){return e});var r=e.evaluators[n.type]||e.evaluators.default;if(!r)throw new Error("unknown node type: "+JSON.stringify(n,null,2));return this.evalSyncAsync(r.bind(this)(n,this.context),function(e){return n._value=e,t(e)})},r.evalSyncAsync=function(e,n){return this.isAsync?Promise.resolve(e).then(n):n(e)},r.evalArrayExpression=function(e){return this.evalArray(e.elements)},r.evalArray=function(e){var n=this,t=e.map(function(e){return n.eval(e)}),r=function(n){return n.reduce(function(n,t,r){return"SpreadElement"===e[r].type?[].concat(n,t):(n.push(t),n)},[])};return this.isAsync?Promise.all(t).then(r):r(t)},r.evalBinaryExpression=function(n){var t=this;if("||"===n.operator)return this.eval(n.left,function(e){return e||t.eval(n.right)});if("&&"===n.operator)return this.eval(n.left,function(e){return e&&t.eval(n.right)});var r=[this.eval(n.left),this.eval(n.right)],o=function(t){return e.binops[n.operator](t[0],t[1])};return this.isAsync?Promise.all(r).then(o):o(r)},r.evalCompoundExpression=function(e){var n=this;return this.isAsync?e.body.reduce(function(e,t){return e.then(function(){return n.eval(t)})},Promise.resolve()):e.body.map(function(e){return n.eval(e)})[e.body.length-1]},r.evalCallExpression=function(e){var n=this;return this.evalSyncAsync(this.evalCall(e.callee),function(t){var r=t[0],o=t[1];return n.evalSyncAsync(n.evalArray(e.arguments),function(t){return r.apply(o===e.callee?n.context:o,t)})})},r.evalCall=function(n){return"MemberExpression"===n.type?this.evalSyncAsync(this.evaluateMember(n),function(t){return e.validateFnAndCall(t[1],t[0],n)}):this.eval(n,function(t){return e.validateFnAndCall(t,n)})},r.evalConditionalExpression=function(e){var n=this;return this.eval(e.test,function(t){return n.eval(t?e.consequent:e.alternate)})},r.evalIdentifier=function(e){return this.context[e.name]},e.evalLiteral=function(e){return e.value},r.evalMemberExpression=function(e){return this.evalSyncAsync(this.evaluateMember(e),function(e){return e[1]})},r.evaluateMember=function(e){var n=this;return this.eval(e.object,function(t){return n.evalSyncAsync(e.computed?n.eval(e.property):e.property.name,function(n){if(/^__proto__|prototype|constructor$/.test(n))throw Error('Access to member "'+n+'" disallowed.');return[t,(e.optional?t||{}:t)[n],n]})})},r.evalThisExpression=function(){return this.context},r.evalUnaryExpression=function(n){return this.eval(n.argument,function(t){return e.unops[n.operator](t)})},r.evalArrowFunctionExpression=function(n){var t=this;return this.isAsync!==n.async?e[n.async?"evalAsync":"eval"](n,this.context):function(){var r=t.evalArrowContext(n,[].slice.call(arguments));return e[n.async?"evalAsync":"eval"](n.body,r)}},r.evalArrowContext=function(e,n){var r=this,o=t({},this.context);return(e.params||[]).forEach(function(e,i){if("AssignmentExpression"===e.type&&(void 0===n[i]&&(n[i]=r.eval(e.right)),e=e.left),"Identifier"===e.type)o[e.name]=n[i];else if("ArrayExpression"===e.type)e.elements.forEach(function(e,t){var a=n[i][t];if("AssignmentExpression"===e.type&&(void 0===a&&(a=r.eval(e.right)),e=e.left),"Identifier"!==e.type)throw new Error("Unexpected arrow function argument");o[e.name]=a});else if("ObjectExpression"===e.type){var a=[];e.properties.forEach(function(e){var s,u=e;if("AssignmentExpression"===u.type&&(u=u.left),"Property"===u.type)s="Identifier"===u.key.type?u.key.name:r.eval(u.key).toString();else if("Identifier"===u.type)s=u.name;else{if("SpreadElement"!==u.type||"Identifier"!==u.argument.type)throw new Error("Unexpected arrow function argument");s=u.argument.name}var l=n[i][s];"SpreadElement"===u.type?(l=t({},n[i]),a.forEach(function(e){delete l[e]})):void 0===l&&"AssignmentExpression"===e.type&&(l=r.eval(e.right)),o[s]=l,a.push(s)})}else{if("SpreadElement"!==e.type||"Identifier"!==e.argument.type)throw new Error("Unexpected arrow function argument");o[e.argument.name]=n.slice(i)}}),o},r.evalAssignmentExpression=function(n){var t=this;return this.evalSyncAsync(this.getContextAndKey(n.left),function(r){var o=r[0],i=r[1];return t.eval(n.right,function(t){return e.assignOps[n.operator](o,i,t)})})},r.evalUpdateExpression=function(n){return this.evalSyncAsync(this.getContextAndKey(n.argument),function(t){return e.evalUpdateOperation(n,t[0],t[1])})},r.evalAwaitExpression=function(n){return e.evalAsync(n.argument,this.context)},e.evalUpdateOperation=function(e,n,t){return e.prefix?"++"===e.operator?++n[t]:--n[t]:"++"===e.operator?n[t]++:n[t]--},r.getContextAndKey=function(e){var n=this;if("MemberExpression"===e.type)return this.evalSyncAsync(this.evaluateMember(e),function(e){return[e[0],e[2]]});if("Identifier"===e.type)return[this.context,e.name];if("ConditionalExpression"===e.type)return this.eval(e.test,function(t){return n.getContextAndKey(t?e.consequent:e.alternate)});throw new Error("Invalid Member Key")},r.evalNewExpression=function(n){var t=this;return this.evalSyncAsync(this.evalCall(n.callee),function(r){var o=r[0];return t.evalSyncAsync(t.evalArray(n.arguments),function(t){return e.construct(o,t,n)})})},r.evalObjectExpression=function(n){var t=this,r={},o=n.properties.map(function(n){if("SpreadElement"===n.type)Object.assign(r,e.eval(n.argument,t.context));else if("Property"===n.type)return t.evalSyncAsync("Identifier"===n.key.type?n.key.name:t.eval(n.key),function(e){return t.eval(n.shorthand?n.key:n.value,function(n){r[e]=n})})});return this.isAsync?Promise.all(o).then(function(){return r}):r},r.evalSpreadElement=function(e){return this.eval(e.argument)},r.evalTaggedTemplateExpression=function(e){var n=[this.evalCall(e.tag),this.evalSyncAsync(this.evalArray(e.quasi.expressions),function(n){return[e.quasi.quasis.map(function(e){return e.value.cooked})].concat(n)})],t=function(e){var n=e[0];return n[0].apply(n[1],e[1])};return this.isAsync?Promise.all(n).then(t):t(n)},r.evalTemplateLiteral=function(e){return this.evalSyncAsync(this.evalArray(e.expressions),function(n){return e.quasis.reduce(function(e,t,r){return e+=t.value.cooked,t.tail||(e+=n[r]),e},"")})},e.construct=function(n,t,r){try{return new(Function.prototype.bind.apply(n,[null].concat(t)))}catch(n){throw new Error(e.nodeFunctionName(r.callee)+" is not a constructor")}},e.validateFnAndCall=function(n,t,r){if("function"!=typeof n){if(!n&&r&&r.optional)return[function(){},t];var o=e.nodeFunctionName(r||t);throw new Error("'"+o+"' is not a function")}return[n,t]},e.nodeFunctionName=function(e){return e&&(e.name||e.property&&e.property.name)},e}();r.jsep=n.default,r.parse=n.default,r.evaluate=r.eval,r.evaluators={ArrayExpression:r.prototype.evalArrayExpression,LogicalExpression:r.prototype.evalBinaryExpression,BinaryExpression:r.prototype.evalBinaryExpression,CallExpression:r.prototype.evalCallExpression,Compound:r.prototype.evalCompoundExpression,ConditionalExpression:r.prototype.evalConditionalExpression,Identifier:r.prototype.evalIdentifier,Literal:r.evalLiteral,OptionalMemberExpression:r.prototype.evalMemberExpression,MemberExpression:r.prototype.evalMemberExpression,ThisExpression:r.prototype.evalThisExpression,UnaryExpression:r.prototype.evalUnaryExpression,ArrowFunctionExpression:r.prototype.evalArrowFunctionExpression,AssignmentExpression:r.prototype.evalAssignmentExpression,UpdateExpression:r.prototype.evalUpdateExpression,AwaitExpression:r.prototype.evalAwaitExpression,NewExpression:r.prototype.evalNewExpression,ObjectExpression:r.prototype.evalObjectExpression,SpreadElement:r.prototype.evalSpreadElement,TaggedTemplateExpression:r.prototype.evalTaggedTemplateExpression,TemplateLiteral:r.prototype.evalTemplateLiteral},r.DEFAULT_PRECEDENCE={"||":1,"&&":2,"|":3,"^":4,"&":5,"==":6,"!=":6,"===":6,"!==":6,"<":7,">":7,"<=":7,">=":7,"<<":8,">>":8,">>>":8,"+":9,"-":9,"*":10,"/":10,"%":10},r.binops={"||":function(e,n){return e||n},"&&":function(e,n){return e&&n},"|":function(e,n){return e|n},"^":function(e,n){return e^n},"&":function(e,n){return e&n},"==":function(e,n){return e==n},"!=":function(e,n){return e!=n},"===":function(e,n){return e===n},"!==":function(e,n){return e!==n},"<":function(e,n){return e<n},">":function(e,n){return e>n},"<=":function(e,n){return e<=n},">=":function(e,n){return e>=n},"<<":function(e,n){return e<<n},">>":function(e,n){return e>>n},">>>":function(e,n){return e>>>n},"+":function(e,n){return e+n},"-":function(e,n){return e-n},"*":function(e,n){return e*n},"/":function(e,n){return e/n},"%":function(e,n){return e%n}},r.unops={"-":function(e){return-e},"+":function(e){return+e},"~":function(e){return~e},"!":function(e){return!e}},r.assignOps={"=":function(e,n,t){return e[n]=t},"*=":function(e,n,t){return e[n]*=t},"**=":function(e,n,t){var r;return e[r=n]=Math.pow(e[r],t)},"/=":function(e,n,t){return e[n]/=t},"%=":function(e,n,t){return e[n]%=t},"+=":function(e,n,t){return e[n]+=t},"-=":function(e,n,t){return e[n]-=t},"<<=":function(e,n,t){return e[n]<<=t},">>=":function(e,n,t){return e[n]>>=t},">>>=":function(e,n,t){return e[n]>>>=t},"&=":function(e,n,t){return e[n]&=t},"^=":function(e,n,t){return e[n]^=t},"|=":function(e,n,t){return e[n]|=t}};var o=r.DEFAULT_PRECEDENCE,i=r.evaluators,a=r.binops,s=r.unops,u=r.assignOps,l=r.addUnaryOp,c=r.addBinaryOp,p=r.addEvaluator,f=r.registerPlugin,v=r.eval,y=r.evalAsync,d=r.compile,x=r.compileAsync,E=r.evalExpr,h=r.evalExprAsync;Object.defineProperty(exports,"jsep",{enumerable:!0,get:function(){return n.default}}),Object.defineProperty(exports,"parse",{enumerable:!0,get:function(){return n.default}}),exports.DEFAULT_PRECEDENCE=o,exports.addBinaryOp=c,exports.addEvaluator=p,exports.addUnaryOp=l,exports.assignOps=u,exports.binops=a,exports.compile=d,exports.compileAsync=x,exports.default=r,exports.evalAsync=y,exports.evalExpr=E,exports.evalExprAsync=h,exports.evaluate=v,exports.evaluators=i,exports.registerPlugin=f,exports.unops=s;


},{"jsep":186}],186:[function(require,module,exports){
'use strict';

/**
 * @implements {IHooks}
 */
class Hooks {
	/**
	 * @callback HookCallback
	 * @this {*|Jsep} this
	 * @param {Jsep} env
	 * @returns: void
	 */
	/**
	 * Adds the given callback to the list of callbacks for the given hook.
	 *
	 * The callback will be invoked when the hook it is registered for is run.
	 *
	 * One callback function can be registered to multiple hooks and the same hook multiple times.
	 *
	 * @param {string|object} name The name of the hook, or an object of callbacks keyed by name
	 * @param {HookCallback|boolean} callback The callback function which is given environment variables.
	 * @param {?boolean} [first=false] Will add the hook to the top of the list (defaults to the bottom)
	 * @public
	 */
	add(name, callback, first) {
		if (typeof arguments[0] != 'string') {
			// Multiple hook callbacks, keyed by name
			for (let name in arguments[0]) {
				this.add(name, arguments[0][name], arguments[1]);
			}
		}
		else {
			(Array.isArray(name) ? name : [name]).forEach(function (name) {
				this[name] = this[name] || [];

				if (callback) {
					this[name][first ? 'unshift' : 'push'](callback);
				}
			}, this);
		}
	}

	/**
	 * Runs a hook invoking all registered callbacks with the given environment variables.
	 *
	 * Callbacks will be invoked synchronously and in the order in which they were registered.
	 *
	 * @param {string} name The name of the hook.
	 * @param {Object<string, any>} env The environment variables of the hook passed to all callbacks registered.
	 * @public
	 */
	run(name, env) {
		this[name] = this[name] || [];
		this[name].forEach(function (callback) {
			callback.call(env && env.context ? env.context : env, env);
		});
	}
}

/**
 * @implements {IPlugins}
 */
class Plugins {
	constructor(jsep) {
		this.jsep = jsep;
		this.registered = {};
	}

	/**
	 * @callback PluginSetup
	 * @this {Jsep} jsep
	 * @returns: void
	 */
	/**
	 * Adds the given plugin(s) to the registry
	 *
	 * @param {object} plugins
	 * @param {string} plugins.name The name of the plugin
	 * @param {PluginSetup} plugins.init The init function
	 * @public
	 */
	register(...plugins) {
		plugins.forEach((plugin) => {
			if (typeof plugin !== 'object' || !plugin.name || !plugin.init) {
				throw new Error('Invalid JSEP plugin format');
			}
			if (this.registered[plugin.name]) {
				// already registered. Ignore.
				return;
			}
			plugin.init(this.jsep);
			this.registered[plugin.name] = plugin;
		});
	}
}

//     JavaScript Expression Parser (JSEP) 1.4.0

class Jsep {
	/**
	 * @returns {string}
	 */
	static get version() {
		// To be filled in by the template
		return '1.4.0';
	}

	/**
	 * @returns {string}
	 */
	static toString() {
		return 'JavaScript Expression Parser (JSEP) v' + Jsep.version;
	};

	// ==================== CONFIG ================================
	/**
	 * @method addUnaryOp
	 * @param {string} op_name The name of the unary op to add
	 * @returns {Jsep}
	 */
	static addUnaryOp(op_name) {
		Jsep.max_unop_len = Math.max(op_name.length, Jsep.max_unop_len);
		Jsep.unary_ops[op_name] = 1;
		return Jsep;
	}

	/**
	 * @method jsep.addBinaryOp
	 * @param {string} op_name The name of the binary op to add
	 * @param {number} precedence The precedence of the binary op (can be a float). Higher number = higher precedence
	 * @param {boolean} [isRightAssociative=false] whether operator is right-associative
	 * @returns {Jsep}
	 */
	static addBinaryOp(op_name, precedence, isRightAssociative) {
		Jsep.max_binop_len = Math.max(op_name.length, Jsep.max_binop_len);
		Jsep.binary_ops[op_name] = precedence;
		if (isRightAssociative) {
			Jsep.right_associative.add(op_name);
		}
		else {
			Jsep.right_associative.delete(op_name);
		}
		return Jsep;
	}

	/**
	 * @method addIdentifierChar
	 * @param {string} char The additional character to treat as a valid part of an identifier
	 * @returns {Jsep}
	 */
	static addIdentifierChar(char) {
		Jsep.additional_identifier_chars.add(char);
		return Jsep;
	}

	/**
	 * @method addLiteral
	 * @param {string} literal_name The name of the literal to add
	 * @param {*} literal_value The value of the literal
	 * @returns {Jsep}
	 */
	static addLiteral(literal_name, literal_value) {
		Jsep.literals[literal_name] = literal_value;
		return Jsep;
	}

	/**
	 * @method removeUnaryOp
	 * @param {string} op_name The name of the unary op to remove
	 * @returns {Jsep}
	 */
	static removeUnaryOp(op_name) {
		delete Jsep.unary_ops[op_name];
		if (op_name.length === Jsep.max_unop_len) {
			Jsep.max_unop_len = Jsep.getMaxKeyLen(Jsep.unary_ops);
		}
		return Jsep;
	}

	/**
	 * @method removeAllUnaryOps
	 * @returns {Jsep}
	 */
	static removeAllUnaryOps() {
		Jsep.unary_ops = {};
		Jsep.max_unop_len = 0;

		return Jsep;
	}

	/**
	 * @method removeIdentifierChar
	 * @param {string} char The additional character to stop treating as a valid part of an identifier
	 * @returns {Jsep}
	 */
	static removeIdentifierChar(char) {
		Jsep.additional_identifier_chars.delete(char);
		return Jsep;
	}

	/**
	 * @method removeBinaryOp
	 * @param {string} op_name The name of the binary op to remove
	 * @returns {Jsep}
	 */
	static removeBinaryOp(op_name) {
		delete Jsep.binary_ops[op_name];

		if (op_name.length === Jsep.max_binop_len) {
			Jsep.max_binop_len = Jsep.getMaxKeyLen(Jsep.binary_ops);
		}
		Jsep.right_associative.delete(op_name);

		return Jsep;
	}

	/**
	 * @method removeAllBinaryOps
	 * @returns {Jsep}
	 */
	static removeAllBinaryOps() {
		Jsep.binary_ops = {};
		Jsep.max_binop_len = 0;

		return Jsep;
	}

	/**
	 * @method removeLiteral
	 * @param {string} literal_name The name of the literal to remove
	 * @returns {Jsep}
	 */
	static removeLiteral(literal_name) {
		delete Jsep.literals[literal_name];
		return Jsep;
	}

	/**
	 * @method removeAllLiterals
	 * @returns {Jsep}
	 */
	static removeAllLiterals() {
		Jsep.literals = {};

		return Jsep;
	}
	// ==================== END CONFIG ============================


	/**
	 * @returns {string}
	 */
	get char() {
		return this.expr.charAt(this.index);
	}

	/**
	 * @returns {number}
	 */
	get code() {
		return this.expr.charCodeAt(this.index);
	};


	/**
	 * @param {string} expr a string with the passed in express
	 * @returns Jsep
	 */
	constructor(expr) {
		// `index` stores the character number we are currently at
		// All of the gobbles below will modify `index` as we move along
		this.expr = expr;
		this.index = 0;
	}

	/**
	 * static top-level parser
	 * @returns {jsep.Expression}
	 */
	static parse(expr) {
		return (new Jsep(expr)).parse();
	}

	/**
	 * Get the longest key length of any object
	 * @param {object} obj
	 * @returns {number}
	 */
	static getMaxKeyLen(obj) {
		return Math.max(0, ...Object.keys(obj).map(k => k.length));
	}

	/**
	 * `ch` is a character code in the next three functions
	 * @param {number} ch
	 * @returns {boolean}
	 */
	static isDecimalDigit(ch) {
		return (ch >= 48 && ch <= 57); // 0...9
	}

	/**
	 * Returns the precedence of a binary operator or `0` if it isn't a binary operator. Can be float.
	 * @param {string} op_val
	 * @returns {number}
	 */
	static binaryPrecedence(op_val) {
		return Jsep.binary_ops[op_val] || 0;
	}

	/**
	 * Looks for start of identifier
	 * @param {number} ch
	 * @returns {boolean}
	 */
	static isIdentifierStart(ch) {
		return  (ch >= 65 && ch <= 90) || // A...Z
			(ch >= 97 && ch <= 122) || // a...z
			(ch >= 128 && !Jsep.binary_ops[String.fromCharCode(ch)]) || // any non-ASCII that is not an operator
			(Jsep.additional_identifier_chars.has(String.fromCharCode(ch))); // additional characters
	}

	/**
	 * @param {number} ch
	 * @returns {boolean}
	 */
	static isIdentifierPart(ch) {
		return Jsep.isIdentifierStart(ch) || Jsep.isDecimalDigit(ch);
	}

	/**
	 * throw error at index of the expression
	 * @param {string} message
	 * @throws
	 */
	throwError(message) {
		const error = new Error(message + ' at character ' + this.index);
		error.index = this.index;
		error.description = message;
		throw error;
	}

	/**
	 * Run a given hook
	 * @param {string} name
	 * @param {jsep.Expression|false} [node]
	 * @returns {?jsep.Expression}
	 */
	runHook(name, node) {
		if (Jsep.hooks[name]) {
			const env = { context: this, node };
			Jsep.hooks.run(name, env);
			return env.node;
		}
		return node;
	}

	/**
	 * Runs a given hook until one returns a node
	 * @param {string} name
	 * @returns {?jsep.Expression}
	 */
	searchHook(name) {
		if (Jsep.hooks[name]) {
			const env = { context: this };
			Jsep.hooks[name].find(function (callback) {
				callback.call(env.context, env);
				return env.node;
			});
			return env.node;
		}
	}

	/**
	 * Push `index` up to the next non-space character
	 */
	gobbleSpaces() {
		let ch = this.code;
		// Whitespace
		while (ch === Jsep.SPACE_CODE
		|| ch === Jsep.TAB_CODE
		|| ch === Jsep.LF_CODE
		|| ch === Jsep.CR_CODE) {
			ch = this.expr.charCodeAt(++this.index);
		}
		this.runHook('gobble-spaces');
	}

	/**
	 * Top-level method to parse all expressions and returns compound or single node
	 * @returns {jsep.Expression}
	 */
	parse() {
		this.runHook('before-all');
		const nodes = this.gobbleExpressions();

		// If there's only one expression just try returning the expression
		const node = nodes.length === 1
		  ? nodes[0]
			: {
				type: Jsep.COMPOUND,
				body: nodes
			};
		return this.runHook('after-all', node);
	}

	/**
	 * top-level parser (but can be reused within as well)
	 * @param {number} [untilICode]
	 * @returns {jsep.Expression[]}
	 */
	gobbleExpressions(untilICode) {
		let nodes = [], ch_i, node;

		while (this.index < this.expr.length) {
			ch_i = this.code;

			// Expressions can be separated by semicolons, commas, or just inferred without any
			// separators
			if (ch_i === Jsep.SEMCOL_CODE || ch_i === Jsep.COMMA_CODE) {
				this.index++; // ignore separators
			}
			else {
				// Try to gobble each expression individually
				if (node = this.gobbleExpression()) {
					nodes.push(node);
					// If we weren't able to find a binary expression and are out of room, then
					// the expression passed in probably has too much
				}
				else if (this.index < this.expr.length) {
					if (ch_i === untilICode) {
						break;
					}
					this.throwError('Unexpected "' + this.char + '"');
				}
			}
		}

		return nodes;
	}

	/**
	 * The main parsing function.
	 * @returns {?jsep.Expression}
	 */
	gobbleExpression() {
		const node = this.searchHook('gobble-expression') || this.gobbleBinaryExpression();
		this.gobbleSpaces();

		return this.runHook('after-expression', node);
	}

	/**
	 * Search for the operation portion of the string (e.g. `+`, `===`)
	 * Start by taking the longest possible binary operations (3 characters: `===`, `!==`, `>>>`)
	 * and move down from 3 to 2 to 1 character until a matching binary operation is found
	 * then, return that binary operation
	 * @returns {string|boolean}
	 */
	gobbleBinaryOp() {
		this.gobbleSpaces();
		let to_check = this.expr.substr(this.index, Jsep.max_binop_len);
		let tc_len = to_check.length;

		while (tc_len > 0) {
			// Don't accept a binary op when it is an identifier.
			// Binary ops that start with a identifier-valid character must be followed
			// by a non identifier-part valid character
			if (Jsep.binary_ops.hasOwnProperty(to_check) && (
				!Jsep.isIdentifierStart(this.code) ||
				(this.index + to_check.length < this.expr.length && !Jsep.isIdentifierPart(this.expr.charCodeAt(this.index + to_check.length)))
			)) {
				this.index += tc_len;
				return to_check;
			}
			to_check = to_check.substr(0, --tc_len);
		}
		return false;
	}

	/**
	 * This function is responsible for gobbling an individual expression,
	 * e.g. `1`, `1+2`, `a+(b*2)-Math.sqrt(2)`
	 * @returns {?jsep.BinaryExpression}
	 */
	gobbleBinaryExpression() {
		let node, biop, prec, stack, biop_info, left, right, i, cur_biop;

		// First, try to get the leftmost thing
		// Then, check to see if there's a binary operator operating on that leftmost thing
		// Don't gobbleBinaryOp without a left-hand-side
		left = this.gobbleToken();
		if (!left) {
			return left;
		}
		biop = this.gobbleBinaryOp();

		// If there wasn't a binary operator, just return the leftmost node
		if (!biop) {
			return left;
		}

		// Otherwise, we need to start a stack to properly place the binary operations in their
		// precedence structure
		biop_info = { value: biop, prec: Jsep.binaryPrecedence(biop), right_a: Jsep.right_associative.has(biop) };

		right = this.gobbleToken();

		if (!right) {
			this.throwError("Expected expression after " + biop);
		}

		stack = [left, biop_info, right];

		// Properly deal with precedence using [recursive descent](http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm)
		while ((biop = this.gobbleBinaryOp())) {
			prec = Jsep.binaryPrecedence(biop);

			if (prec === 0) {
				this.index -= biop.length;
				break;
			}

			biop_info = { value: biop, prec, right_a: Jsep.right_associative.has(biop) };

			cur_biop = biop;

			// Reduce: make a binary expression from the three topmost entries.
			const comparePrev = prev => biop_info.right_a && prev.right_a
				? prec > prev.prec
				: prec <= prev.prec;
			while ((stack.length > 2) && comparePrev(stack[stack.length - 2])) {
				right = stack.pop();
				biop = stack.pop().value;
				left = stack.pop();
				node = {
					type: Jsep.BINARY_EXP,
					operator: biop,
					left,
					right
				};
				stack.push(node);
			}

			node = this.gobbleToken();

			if (!node) {
				this.throwError("Expected expression after " + cur_biop);
			}

			stack.push(biop_info, node);
		}

		i = stack.length - 1;
		node = stack[i];

		while (i > 1) {
			node = {
				type: Jsep.BINARY_EXP,
				operator: stack[i - 1].value,
				left: stack[i - 2],
				right: node
			};
			i -= 2;
		}

		return node;
	}

	/**
	 * An individual part of a binary expression:
	 * e.g. `foo.bar(baz)`, `1`, `"abc"`, `(a % 2)` (because it's in parenthesis)
	 * @returns {boolean|jsep.Expression}
	 */
	gobbleToken() {
		let ch, to_check, tc_len, node;

		this.gobbleSpaces();
		node = this.searchHook('gobble-token');
		if (node) {
			return this.runHook('after-token', node);
		}

		ch = this.code;

		if (Jsep.isDecimalDigit(ch) || ch === Jsep.PERIOD_CODE) {
			// Char code 46 is a dot `.` which can start off a numeric literal
			return this.gobbleNumericLiteral();
		}

		if (ch === Jsep.SQUOTE_CODE || ch === Jsep.DQUOTE_CODE) {
			// Single or double quotes
			node = this.gobbleStringLiteral();
		}
		else if (ch === Jsep.OBRACK_CODE) {
			node = this.gobbleArray();
		}
		else {
			to_check = this.expr.substr(this.index, Jsep.max_unop_len);
			tc_len = to_check.length;

			while (tc_len > 0) {
				// Don't accept an unary op when it is an identifier.
				// Unary ops that start with a identifier-valid character must be followed
				// by a non identifier-part valid character
				if (Jsep.unary_ops.hasOwnProperty(to_check) && (
					!Jsep.isIdentifierStart(this.code) ||
					(this.index + to_check.length < this.expr.length && !Jsep.isIdentifierPart(this.expr.charCodeAt(this.index + to_check.length)))
				)) {
					this.index += tc_len;
					const argument = this.gobbleToken();
					if (!argument) {
						this.throwError('missing unaryOp argument');
					}
					return this.runHook('after-token', {
						type: Jsep.UNARY_EXP,
						operator: to_check,
						argument,
						prefix: true
					});
				}

				to_check = to_check.substr(0, --tc_len);
			}

			if (Jsep.isIdentifierStart(ch)) {
				node = this.gobbleIdentifier();
				if (Jsep.literals.hasOwnProperty(node.name)) {
					node = {
						type: Jsep.LITERAL,
						value: Jsep.literals[node.name],
						raw: node.name,
					};
				}
				else if (node.name === Jsep.this_str) {
					node = { type: Jsep.THIS_EXP };
				}
			}
			else if (ch === Jsep.OPAREN_CODE) { // open parenthesis
				node = this.gobbleGroup();
			}
		}

		if (!node) {
			return this.runHook('after-token', false);
		}

		node = this.gobbleTokenProperty(node);
		return this.runHook('after-token', node);
	}

	/**
	 * Gobble properties of of identifiers/strings/arrays/groups.
	 * e.g. `foo`, `bar.baz`, `foo['bar'].baz`
	 * It also gobbles function calls:
	 * e.g. `Math.acos(obj.angle)`
	 * @param {jsep.Expression} node
	 * @returns {jsep.Expression}
	 */
	gobbleTokenProperty(node) {
		this.gobbleSpaces();

		let ch = this.code;
		while (ch === Jsep.PERIOD_CODE || ch === Jsep.OBRACK_CODE || ch === Jsep.OPAREN_CODE || ch === Jsep.QUMARK_CODE) {
			let optional;
			if (ch === Jsep.QUMARK_CODE) {
				if (this.expr.charCodeAt(this.index + 1) !== Jsep.PERIOD_CODE) {
					break;
				}
				optional = true;
				this.index += 2;
				this.gobbleSpaces();
				ch = this.code;
			}
			this.index++;

			if (ch === Jsep.OBRACK_CODE) {
				node = {
					type: Jsep.MEMBER_EXP,
					computed: true,
					object: node,
					property: this.gobbleExpression()
				};
				if (!node.property) {
					this.throwError('Unexpected "' + this.char + '"');
				}
				this.gobbleSpaces();
				ch = this.code;
				if (ch !== Jsep.CBRACK_CODE) {
					this.throwError('Unclosed [');
				}
				this.index++;
			}
			else if (ch === Jsep.OPAREN_CODE) {
				// A function call is being made; gobble all the arguments
				node = {
					type: Jsep.CALL_EXP,
					'arguments': this.gobbleArguments(Jsep.CPAREN_CODE),
					callee: node
				};
			}
			else if (ch === Jsep.PERIOD_CODE || optional) {
				if (optional) {
					this.index--;
				}
				this.gobbleSpaces();
				node = {
					type: Jsep.MEMBER_EXP,
					computed: false,
					object: node,
					property: this.gobbleIdentifier(),
				};
			}

			if (optional) {
				node.optional = true;
			} // else leave undefined for compatibility with esprima

			this.gobbleSpaces();
			ch = this.code;
		}

		return node;
	}

	/**
	 * Parse simple numeric literals: `12`, `3.4`, `.5`. Do this by using a string to
	 * keep track of everything in the numeric literal and then calling `parseFloat` on that string
	 * @returns {jsep.Literal}
	 */
	gobbleNumericLiteral() {
		let number = '', ch, chCode;

		while (Jsep.isDecimalDigit(this.code)) {
			number += this.expr.charAt(this.index++);
		}

		if (this.code === Jsep.PERIOD_CODE) { // can start with a decimal marker
			number += this.expr.charAt(this.index++);

			while (Jsep.isDecimalDigit(this.code)) {
				number += this.expr.charAt(this.index++);
			}
		}

		ch = this.char;

		if (ch === 'e' || ch === 'E') { // exponent marker
			number += this.expr.charAt(this.index++);
			ch = this.char;

			if (ch === '+' || ch === '-') { // exponent sign
				number += this.expr.charAt(this.index++);
			}

			while (Jsep.isDecimalDigit(this.code)) { // exponent itself
				number += this.expr.charAt(this.index++);
			}

			if (!Jsep.isDecimalDigit(this.expr.charCodeAt(this.index - 1)) ) {
				this.throwError('Expected exponent (' + number + this.char + ')');
			}
		}

		chCode = this.code;

		// Check to make sure this isn't a variable name that start with a number (123abc)
		if (Jsep.isIdentifierStart(chCode)) {
			this.throwError('Variable names cannot start with a number (' +
				number + this.char + ')');
		}
		else if (chCode === Jsep.PERIOD_CODE || (number.length === 1 && number.charCodeAt(0) === Jsep.PERIOD_CODE)) {
			this.throwError('Unexpected period');
		}

		return {
			type: Jsep.LITERAL,
			value: parseFloat(number),
			raw: number
		};
	}

	/**
	 * Parses a string literal, staring with single or double quotes with basic support for escape codes
	 * e.g. `"hello world"`, `'this is\nJSEP'`
	 * @returns {jsep.Literal}
	 */
	gobbleStringLiteral() {
		let str = '';
		const startIndex = this.index;
		const quote = this.expr.charAt(this.index++);
		let closed = false;

		while (this.index < this.expr.length) {
			let ch = this.expr.charAt(this.index++);

			if (ch === quote) {
				closed = true;
				break;
			}
			else if (ch === '\\') {
				// Check for all of the common escape codes
				ch = this.expr.charAt(this.index++);

				switch (ch) {
					case 'n': str += '\n'; break;
					case 'r': str += '\r'; break;
					case 't': str += '\t'; break;
					case 'b': str += '\b'; break;
					case 'f': str += '\f'; break;
					case 'v': str += '\x0B'; break;
					default : str += ch;
				}
			}
			else {
				str += ch;
			}
		}

		if (!closed) {
			this.throwError('Unclosed quote after "' + str + '"');
		}

		return {
			type: Jsep.LITERAL,
			value: str,
			raw: this.expr.substring(startIndex, this.index),
		};
	}

	/**
	 * Gobbles only identifiers
	 * e.g.: `foo`, `_value`, `$x1`
	 * Also, this function checks if that identifier is a literal:
	 * (e.g. `true`, `false`, `null`) or `this`
	 * @returns {jsep.Identifier}
	 */
	gobbleIdentifier() {
		let ch = this.code, start = this.index;

		if (Jsep.isIdentifierStart(ch)) {
			this.index++;
		}
		else {
			this.throwError('Unexpected ' + this.char);
		}

		while (this.index < this.expr.length) {
			ch = this.code;

			if (Jsep.isIdentifierPart(ch)) {
				this.index++;
			}
			else {
				break;
			}
		}
		return {
			type: Jsep.IDENTIFIER,
			name: this.expr.slice(start, this.index),
		};
	}

	/**
	 * Gobbles a list of arguments within the context of a function call
	 * or array literal. This function also assumes that the opening character
	 * `(` or `[` has already been gobbled, and gobbles expressions and commas
	 * until the terminator character `)` or `]` is encountered.
	 * e.g. `foo(bar, baz)`, `my_func()`, or `[bar, baz]`
	 * @param {number} termination
	 * @returns {jsep.Expression[]}
	 */
	gobbleArguments(termination) {
		const args = [];
		let closed = false;
		let separator_count = 0;

		while (this.index < this.expr.length) {
			this.gobbleSpaces();
			let ch_i = this.code;

			if (ch_i === termination) { // done parsing
				closed = true;
				this.index++;

				if (termination === Jsep.CPAREN_CODE && separator_count && separator_count >= args.length){
					this.throwError('Unexpected token ' + String.fromCharCode(termination));
				}

				break;
			}
			else if (ch_i === Jsep.COMMA_CODE) { // between expressions
				this.index++;
				separator_count++;

				if (separator_count !== args.length) { // missing argument
					if (termination === Jsep.CPAREN_CODE) {
						this.throwError('Unexpected token ,');
					}
					else if (termination === Jsep.CBRACK_CODE) {
						for (let arg = args.length; arg < separator_count; arg++) {
							args.push(null);
						}
					}
				}
			}
			else if (args.length !== separator_count && separator_count !== 0) {
				// NOTE: `&& separator_count !== 0` allows for either all commas, or all spaces as arguments
				this.throwError('Expected comma');
			}
			else {
				const node = this.gobbleExpression();

				if (!node || node.type === Jsep.COMPOUND) {
					this.throwError('Expected comma');
				}

				args.push(node);
			}
		}

		if (!closed) {
			this.throwError('Expected ' + String.fromCharCode(termination));
		}

		return args;
	}

	/**
	 * Responsible for parsing a group of things within parentheses `()`
	 * that have no identifier in front (so not a function call)
	 * This function assumes that it needs to gobble the opening parenthesis
	 * and then tries to gobble everything within that parenthesis, assuming
	 * that the next thing it should see is the close parenthesis. If not,
	 * then the expression probably doesn't have a `)`
	 * @returns {boolean|jsep.Expression}
	 */
	gobbleGroup() {
		this.index++;
		let nodes = this.gobbleExpressions(Jsep.CPAREN_CODE);
		if (this.code === Jsep.CPAREN_CODE) {
			this.index++;
			if (nodes.length === 1) {
				return nodes[0];
			}
			else if (!nodes.length) {
				return false;
			}
			else {
				return {
					type: Jsep.SEQUENCE_EXP,
					expressions: nodes,
				};
			}
		}
		else {
			this.throwError('Unclosed (');
		}
	}

	/**
	 * Responsible for parsing Array literals `[1, 2, 3]`
	 * This function assumes that it needs to gobble the opening bracket
	 * and then tries to gobble the expressions as arguments.
	 * @returns {jsep.ArrayExpression}
	 */
	gobbleArray() {
		this.index++;

		return {
			type: Jsep.ARRAY_EXP,
			elements: this.gobbleArguments(Jsep.CBRACK_CODE)
		};
	}
}

// Static fields:
const hooks = new Hooks();
Object.assign(Jsep, {
	hooks,
	plugins: new Plugins(Jsep),

	// Node Types
	// ----------
	// This is the full set of types that any JSEP node can be.
	// Store them here to save space when minified
	COMPOUND:        'Compound',
	SEQUENCE_EXP:    'SequenceExpression',
	IDENTIFIER:      'Identifier',
	MEMBER_EXP:      'MemberExpression',
	LITERAL:         'Literal',
	THIS_EXP:        'ThisExpression',
	CALL_EXP:        'CallExpression',
	UNARY_EXP:       'UnaryExpression',
	BINARY_EXP:      'BinaryExpression',
	ARRAY_EXP:       'ArrayExpression',

	TAB_CODE:    9,
	LF_CODE:     10,
	CR_CODE:     13,
	SPACE_CODE:  32,
	PERIOD_CODE: 46, // '.'
	COMMA_CODE:  44, // ','
	SQUOTE_CODE: 39, // single quote
	DQUOTE_CODE: 34, // double quotes
	OPAREN_CODE: 40, // (
	CPAREN_CODE: 41, // )
	OBRACK_CODE: 91, // [
	CBRACK_CODE: 93, // ]
	QUMARK_CODE: 63, // ?
	SEMCOL_CODE: 59, // ;
	COLON_CODE:  58, // :


	// Operations
	// ----------
	// Use a quickly-accessible map to store all of the unary operators
	// Values are set to `1` (it really doesn't matter)
	unary_ops: {
		'-': 1,
		'!': 1,
		'~': 1,
		'+': 1
	},

	// Also use a map for the binary operations but set their values to their
	// binary precedence for quick reference (higher number = higher precedence)
	// see [Order of operations](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)
	binary_ops: {
		'||': 1, '??': 1,
		'&&': 2, '|': 3, '^': 4, '&': 5,
		'==': 6, '!=': 6, '===': 6, '!==': 6,
		'<': 7, '>': 7, '<=': 7, '>=': 7,
		'<<': 8, '>>': 8, '>>>': 8,
		'+': 9, '-': 9,
		'*': 10, '/': 10, '%': 10,
		'**': 11,
	},

	// sets specific binary_ops as right-associative
	right_associative: new Set(['**']),

	// Additional valid identifier chars, apart from a-z, A-Z and 0-9 (except on the starting char)
	additional_identifier_chars: new Set(['$', '_']),

	// Literals
	// ----------
	// Store the values to return for the various literals we may encounter
	literals: {
		'true': true,
		'false': false,
		'null': null
	},

	// Except for `this`, which is special. This could be changed to something like `'self'` as well
	this_str: 'this',
});
Jsep.max_unop_len = Jsep.getMaxKeyLen(Jsep.unary_ops);
Jsep.max_binop_len = Jsep.getMaxKeyLen(Jsep.binary_ops);

// Backward Compatibility:
const jsep = expr => (new Jsep(expr)).parse();
const stdClassProps = Object.getOwnPropertyNames(class Test{});
Object.getOwnPropertyNames(Jsep)
	.filter(prop => !stdClassProps.includes(prop) && jsep[prop] === undefined)
	.forEach((m) => {
		jsep[m] = Jsep[m];
	});
jsep.Jsep = Jsep; // allows for const { Jsep } = require('jsep');

const CONDITIONAL_EXP = 'ConditionalExpression';

var ternary = {
	name: 'ternary',

	init(jsep) {
		// Ternary expression: test ? consequent : alternate
		jsep.hooks.add('after-expression', function gobbleTernary(env) {
			if (env.node && this.code === jsep.QUMARK_CODE) {
				this.index++;
				const test = env.node;
				const consequent = this.gobbleExpression();

				if (!consequent) {
					this.throwError('Expected expression');
				}

				this.gobbleSpaces();

				if (this.code === jsep.COLON_CODE) {
					this.index++;
					const alternate = this.gobbleExpression();

					if (!alternate) {
						this.throwError('Expected expression');
					}
					env.node = {
						type: CONDITIONAL_EXP,
						test,
						consequent,
						alternate,
					};

					// check for operators of higher priority than ternary (i.e. assignment)
					// jsep sets || at 1, and assignment at 0.9, and conditional should be between them
					if (test.operator && jsep.binary_ops[test.operator] <= 0.9) {
						let newTest = test;
						while (newTest.right.operator && jsep.binary_ops[newTest.right.operator] <= 0.9) {
							newTest = newTest.right;
						}
						env.node.test = newTest.right;
						newTest.right = env.node;
						env.node = test;
					}
				}
				else {
					this.throwError('Expected :');
				}
			}
		});
	},
};

// Add default plugins:

jsep.plugins.register(ternary);

module.exports = jsep;


},{}],187:[function(require,module,exports){
//! moment.js
//! version : 2.30.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, (function () { 'use strict';

    var hookCallback;

    function hooks() {
        return hookCallback.apply(null, arguments);
    }

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback(callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return (
            input instanceof Array ||
            Object.prototype.toString.call(input) === '[object Array]'
        );
    }

    function isObject(input) {
        // IE8 will treat undefined and null as object if it wasn't for
        // input != null
        return (
            input != null &&
            Object.prototype.toString.call(input) === '[object Object]'
        );
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function isObjectEmpty(obj) {
        if (Object.getOwnPropertyNames) {
            return Object.getOwnPropertyNames(obj).length === 0;
        } else {
            var k;
            for (k in obj) {
                if (hasOwnProp(obj, k)) {
                    return false;
                }
            }
            return true;
        }
    }

    function isUndefined(input) {
        return input === void 0;
    }

    function isNumber(input) {
        return (
            typeof input === 'number' ||
            Object.prototype.toString.call(input) === '[object Number]'
        );
    }

    function isDate(input) {
        return (
            input instanceof Date ||
            Object.prototype.toString.call(input) === '[object Date]'
        );
    }

    function map(arr, fn) {
        var res = [],
            i,
            arrLen = arr.length;
        for (i = 0; i < arrLen; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function createUTC(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty: false,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: false,
            invalidEra: null,
            invalidMonth: null,
            invalidFormat: false,
            userInvalidated: false,
            iso: false,
            parsedDateParts: [],
            era: null,
            meridiem: null,
            rfc2822: false,
            weekdayMismatch: false,
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function (fun) {
            var t = Object(this),
                len = t.length >>> 0,
                i;

            for (i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    function isValid(m) {
        var flags = null,
            parsedParts = false,
            isNowValid = m._d && !isNaN(m._d.getTime());
        if (isNowValid) {
            flags = getParsingFlags(m);
            parsedParts = some.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            isNowValid =
                flags.overflow < 0 &&
                !flags.empty &&
                !flags.invalidEra &&
                !flags.invalidMonth &&
                !flags.invalidWeekday &&
                !flags.weekdayMismatch &&
                !flags.nullInput &&
                !flags.invalidFormat &&
                !flags.userInvalidated &&
                (!flags.meridiem || (flags.meridiem && parsedParts));
            if (m._strict) {
                isNowValid =
                    isNowValid &&
                    flags.charsLeftOver === 0 &&
                    flags.unusedTokens.length === 0 &&
                    flags.bigHour === undefined;
            }
        }
        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        } else {
            return isNowValid;
        }
        return m._isValid;
    }

    function createInvalid(flags) {
        var m = createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        } else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var momentProperties = (hooks.momentProperties = []),
        updateInProgress = false;

    function copyConfig(to, from) {
        var i,
            prop,
            val,
            momentPropertiesLen = momentProperties.length;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentPropertiesLen > 0) {
            for (i = 0; i < momentPropertiesLen; i++) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    // Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        if (!this.isValid()) {
            this._d = new Date(NaN);
        }
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment(obj) {
        return (
            obj instanceof Moment || (obj != null && obj._isAMomentObject != null)
        );
    }

    function warn(msg) {
        if (
            hooks.suppressDeprecationWarnings === false &&
            typeof console !== 'undefined' &&
            console.warn
        ) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                var args = [],
                    arg,
                    i,
                    key,
                    argLen = arguments.length;
                for (i = 0; i < argLen; i++) {
                    arg = '';
                    if (typeof arguments[i] === 'object') {
                        arg += '\n[' + i + '] ';
                        for (key in arguments[0]) {
                            if (hasOwnProp(arguments[0], key)) {
                                arg += key + ': ' + arguments[0][key] + ', ';
                            }
                        }
                        arg = arg.slice(0, -2); // Remove trailing comma and space
                    } else {
                        arg = arguments[i];
                    }
                    args.push(arg);
                }
                warn(
                    msg +
                        '\nArguments: ' +
                        Array.prototype.slice.call(args).join('') +
                        '\n' +
                        new Error().stack
                );
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    hooks.suppressDeprecationWarnings = false;
    hooks.deprecationHandler = null;

    function isFunction(input) {
        return (
            (typeof Function !== 'undefined' && input instanceof Function) ||
            Object.prototype.toString.call(input) === '[object Function]'
        );
    }

    function set(config) {
        var prop, i;
        for (i in config) {
            if (hasOwnProp(config, i)) {
                prop = config[i];
                if (isFunction(prop)) {
                    this[i] = prop;
                } else {
                    this['_' + i] = prop;
                }
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
        // TODO: Remove "ordinalParse" fallback in next major release.
        this._dayOfMonthOrdinalParseLenient = new RegExp(
            (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
                '|' +
                /\d{1,2}/.source
        );
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig),
            prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (
                hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])
            ) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function (obj) {
            var i,
                res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var defaultCalendar = {
        sameDay: '[Today at] LT',
        nextDay: '[Tomorrow at] LT',
        nextWeek: 'dddd [at] LT',
        lastDay: '[Yesterday at] LT',
        lastWeek: '[Last] dddd [at] LT',
        sameElse: 'L',
    };

    function calendar(key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (
            (sign ? (forceSign ? '+' : '') : '-') +
            Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) +
            absNumber
        );
    }

    var formattingTokens =
            /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|N{1,5}|YYYYYY|YYYYY|YYYY|YY|y{2,4}|yo?|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,
        localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
        formatFunctions = {},
        formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken(token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function () {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(
                    func.apply(this, arguments),
                    token
                );
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens),
            i,
            length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '',
                i;
            for (i = 0; i < length; i++) {
                output += isFunction(array[i])
                    ? array[i].call(mom, format)
                    : array[i];
            }
            return output;
        };
    }

    // format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] =
            formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(
                localFormattingTokens,
                replaceLongDateFormatTokens
            );
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var defaultLongDateFormat = {
        LTS: 'h:mm:ss A',
        LT: 'h:mm A',
        L: 'MM/DD/YYYY',
        LL: 'MMMM D, YYYY',
        LLL: 'MMMM D, YYYY h:mm A',
        LLLL: 'dddd, MMMM D, YYYY h:mm A',
    };

    function longDateFormat(key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper
            .match(formattingTokens)
            .map(function (tok) {
                if (
                    tok === 'MMMM' ||
                    tok === 'MM' ||
                    tok === 'DD' ||
                    tok === 'dddd'
                ) {
                    return tok.slice(1);
                }
                return tok;
            })
            .join('');

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate() {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d',
        defaultDayOfMonthOrdinalParse = /\d{1,2}/;

    function ordinal(number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future: 'in %s',
        past: '%s ago',
        s: 'a few seconds',
        ss: '%d seconds',
        m: 'a minute',
        mm: '%d minutes',
        h: 'an hour',
        hh: '%d hours',
        d: 'a day',
        dd: '%d days',
        w: 'a week',
        ww: '%d weeks',
        M: 'a month',
        MM: '%d months',
        y: 'a year',
        yy: '%d years',
    };

    function relativeTime(number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return isFunction(output)
            ? output(number, withoutSuffix, string, isFuture)
            : output.replace(/%d/i, number);
    }

    function pastFuture(diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {
        D: 'date',
        dates: 'date',
        date: 'date',
        d: 'day',
        days: 'day',
        day: 'day',
        e: 'weekday',
        weekdays: 'weekday',
        weekday: 'weekday',
        E: 'isoWeekday',
        isoweekdays: 'isoWeekday',
        isoweekday: 'isoWeekday',
        DDD: 'dayOfYear',
        dayofyears: 'dayOfYear',
        dayofyear: 'dayOfYear',
        h: 'hour',
        hours: 'hour',
        hour: 'hour',
        ms: 'millisecond',
        milliseconds: 'millisecond',
        millisecond: 'millisecond',
        m: 'minute',
        minutes: 'minute',
        minute: 'minute',
        M: 'month',
        months: 'month',
        month: 'month',
        Q: 'quarter',
        quarters: 'quarter',
        quarter: 'quarter',
        s: 'second',
        seconds: 'second',
        second: 'second',
        gg: 'weekYear',
        weekyears: 'weekYear',
        weekyear: 'weekYear',
        GG: 'isoWeekYear',
        isoweekyears: 'isoWeekYear',
        isoweekyear: 'isoWeekYear',
        w: 'week',
        weeks: 'week',
        week: 'week',
        W: 'isoWeek',
        isoweeks: 'isoWeek',
        isoweek: 'isoWeek',
        y: 'year',
        years: 'year',
        year: 'year',
    };

    function normalizeUnits(units) {
        return typeof units === 'string'
            ? aliases[units] || aliases[units.toLowerCase()]
            : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {
        date: 9,
        day: 11,
        weekday: 11,
        isoWeekday: 11,
        dayOfYear: 4,
        hour: 13,
        millisecond: 16,
        minute: 14,
        month: 8,
        quarter: 7,
        second: 15,
        weekYear: 1,
        isoWeekYear: 1,
        week: 5,
        isoWeek: 5,
        year: 1,
    };

    function getPrioritizedUnits(unitsObj) {
        var units = [],
            u;
        for (u in unitsObj) {
            if (hasOwnProp(unitsObj, u)) {
                units.push({ unit: u, priority: priorities[u] });
            }
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    var match1 = /\d/, //       0 - 9
        match2 = /\d\d/, //      00 - 99
        match3 = /\d{3}/, //     000 - 999
        match4 = /\d{4}/, //    0000 - 9999
        match6 = /[+-]?\d{6}/, // -999999 - 999999
        match1to2 = /\d\d?/, //       0 - 99
        match3to4 = /\d\d\d\d?/, //     999 - 9999
        match5to6 = /\d\d\d\d\d\d?/, //   99999 - 999999
        match1to3 = /\d{1,3}/, //       0 - 999
        match1to4 = /\d{1,4}/, //       0 - 9999
        match1to6 = /[+-]?\d{1,6}/, // -999999 - 999999
        matchUnsigned = /\d+/, //       0 - inf
        matchSigned = /[+-]?\d+/, //    -inf - inf
        matchOffset = /Z|[+-]\d\d:?\d\d/gi, // +00:00 -00:00 +0000 -0000 or Z
        matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi, // +00 -00 +00:00 -00:00 +0000 -0000 or Z
        matchTimestamp = /[+-]?\d+(\.\d{1,3})?/, // 123456789 123456789.123
        // any word (or two) characters or numbers including two/three word month in arabic.
        // includes scottish gaelic two word and hyphenated months
        matchWord =
            /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i,
        match1to2NoLeadingZero = /^[1-9]\d?/, //         1-99
        match1to2HasZero = /^([1-9]\d|\d)/, //           0-99
        regexes;

    regexes = {};

    function addRegexToken(token, regex, strictRegex) {
        regexes[token] = isFunction(regex)
            ? regex
            : function (isStrict, localeData) {
                  return isStrict && strictRegex ? strictRegex : regex;
              };
    }

    function getParseRegexForToken(token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(
            s
                .replace('\\', '')
                .replace(
                    /\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,
                    function (matched, p1, p2, p3, p4) {
                        return p1 || p2 || p3 || p4;
                    }
                )
        );
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    function absFloor(number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

    var tokens = {};

    function addParseToken(token, callback) {
        var i,
            func = callback,
            tokenLen;
        if (typeof token === 'string') {
            token = [token];
        }
        if (isNumber(callback)) {
            func = function (input, array) {
                array[callback] = toInt(input);
            };
        }
        tokenLen = token.length;
        for (i = 0; i < tokenLen; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken(token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    var YEAR = 0,
        MONTH = 1,
        DATE = 2,
        HOUR = 3,
        MINUTE = 4,
        SECOND = 5,
        MILLISECOND = 6,
        WEEK = 7,
        WEEKDAY = 8;

    // FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? zeroFill(y, 4) : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY', 4], 0, 'year');
    addFormatToken(0, ['YYYYY', 5], 0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // PARSING

    addRegexToken('Y', matchSigned);
    addRegexToken('YY', match1to2, match2);
    addRegexToken('YYYY', match1to4, match4);
    addRegexToken('YYYYY', match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] =
            input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

    // HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    // HOOKS

    hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear() {
        return isLeapYear(this.year());
    }

    function makeGetSet(unit, keepTime) {
        return function (value) {
            if (value != null) {
                set$1(this, unit, value);
                hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get(this, unit);
            }
        };
    }

    function get(mom, unit) {
        if (!mom.isValid()) {
            return NaN;
        }

        var d = mom._d,
            isUTC = mom._isUTC;

        switch (unit) {
            case 'Milliseconds':
                return isUTC ? d.getUTCMilliseconds() : d.getMilliseconds();
            case 'Seconds':
                return isUTC ? d.getUTCSeconds() : d.getSeconds();
            case 'Minutes':
                return isUTC ? d.getUTCMinutes() : d.getMinutes();
            case 'Hours':
                return isUTC ? d.getUTCHours() : d.getHours();
            case 'Date':
                return isUTC ? d.getUTCDate() : d.getDate();
            case 'Day':
                return isUTC ? d.getUTCDay() : d.getDay();
            case 'Month':
                return isUTC ? d.getUTCMonth() : d.getMonth();
            case 'FullYear':
                return isUTC ? d.getUTCFullYear() : d.getFullYear();
            default:
                return NaN; // Just in case
        }
    }

    function set$1(mom, unit, value) {
        var d, isUTC, year, month, date;

        if (!mom.isValid() || isNaN(value)) {
            return;
        }

        d = mom._d;
        isUTC = mom._isUTC;

        switch (unit) {
            case 'Milliseconds':
                return void (isUTC
                    ? d.setUTCMilliseconds(value)
                    : d.setMilliseconds(value));
            case 'Seconds':
                return void (isUTC ? d.setUTCSeconds(value) : d.setSeconds(value));
            case 'Minutes':
                return void (isUTC ? d.setUTCMinutes(value) : d.setMinutes(value));
            case 'Hours':
                return void (isUTC ? d.setUTCHours(value) : d.setHours(value));
            case 'Date':
                return void (isUTC ? d.setUTCDate(value) : d.setDate(value));
            // case 'Day': // Not real
            //    return void (isUTC ? d.setUTCDay(value) : d.setDay(value));
            // case 'Month': // Not used because we need to pass two variables
            //     return void (isUTC ? d.setUTCMonth(value) : d.setMonth(value));
            case 'FullYear':
                break; // See below ...
            default:
                return; // Just in case
        }

        year = value;
        month = mom.month();
        date = mom.date();
        date = date === 29 && month === 1 && !isLeapYear(year) ? 28 : date;
        void (isUTC
            ? d.setUTCFullYear(year, month, date)
            : d.setFullYear(year, month, date));
    }

    // MOMENTS

    function stringGet(units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }

    function stringSet(units, value) {
        if (typeof units === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units),
                i,
                prioritizedLen = prioritized.length;
            for (i = 0; i < prioritizedLen; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function mod(n, x) {
        return ((n % x) + x) % x;
    }

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    function daysInMonth(year, month) {
        if (isNaN(year) || isNaN(month)) {
            return NaN;
        }
        var modMonth = mod(month, 12);
        year += (month - modMonth) / 12;
        return modMonth === 1
            ? isLeapYear(year)
                ? 29
                : 28
            : 31 - ((modMonth % 7) % 2);
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

    // PARSING

    addRegexToken('M', match1to2, match1to2NoLeadingZero);
    addRegexToken('MM', match1to2, match2);
    addRegexToken('MMM', function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var defaultLocaleMonths =
            'January_February_March_April_May_June_July_August_September_October_November_December'.split(
                '_'
            ),
        defaultLocaleMonthsShort =
            'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
        MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,
        defaultMonthsShortRegex = matchWord,
        defaultMonthsRegex = matchWord;

    function localeMonths(m, format) {
        if (!m) {
            return isArray(this._months)
                ? this._months
                : this._months['standalone'];
        }
        return isArray(this._months)
            ? this._months[m.month()]
            : this._months[
                  (this._months.isFormat || MONTHS_IN_FORMAT).test(format)
                      ? 'format'
                      : 'standalone'
              ][m.month()];
    }

    function localeMonthsShort(m, format) {
        if (!m) {
            return isArray(this._monthsShort)
                ? this._monthsShort
                : this._monthsShort['standalone'];
        }
        return isArray(this._monthsShort)
            ? this._monthsShort[m.month()]
            : this._monthsShort[
                  MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'
              ][m.month()];
    }

    function handleStrictParse(monthName, format, strict) {
        var i,
            ii,
            mom,
            llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(
                    mom,
                    ''
                ).toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse(monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp(
                    '^' + this.months(mom, '').replace('.', '') + '$',
                    'i'
                );
                this._shortMonthsParse[i] = new RegExp(
                    '^' + this.monthsShort(mom, '').replace('.', '') + '$',
                    'i'
                );
            }
            if (!strict && !this._monthsParse[i]) {
                regex =
                    '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (
                strict &&
                format === 'MMMM' &&
                this._longMonthsParse[i].test(monthName)
            ) {
                return i;
            } else if (
                strict &&
                format === 'MMM' &&
                this._shortMonthsParse[i].test(monthName)
            ) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function setMonth(mom, value) {
        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (!isNumber(value)) {
                    return mom;
                }
            }
        }

        var month = value,
            date = mom.date();

        date = date < 29 ? date : Math.min(date, daysInMonth(mom.year(), month));
        void (mom._isUTC
            ? mom._d.setUTCMonth(month, date)
            : mom._d.setMonth(month, date));
        return mom;
    }

    function getSetMonth(value) {
        if (value != null) {
            setMonth(this, value);
            hooks.updateOffset(this, true);
            return this;
        } else {
            return get(this, 'Month');
        }
    }

    function getDaysInMonth() {
        return daysInMonth(this.year(), this.month());
    }

    function monthsShortRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict
                ? this._monthsShortStrictRegex
                : this._monthsShortRegex;
        }
    }

    function monthsRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict
                ? this._monthsStrictRegex
                : this._monthsRegex;
        }
    }

    function computeMonthsParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [],
            longPieces = [],
            mixedPieces = [],
            i,
            mom,
            shortP,
            longP;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            shortP = regexEscape(this.monthsShort(mom, ''));
            longP = regexEscape(this.months(mom, ''));
            shortPieces.push(shortP);
            longPieces.push(longP);
            mixedPieces.push(longP);
            mixedPieces.push(shortP);
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp(
            '^(' + longPieces.join('|') + ')',
            'i'
        );
        this._monthsShortStrictRegex = new RegExp(
            '^(' + shortPieces.join('|') + ')',
            'i'
        );
    }

    function createDate(y, m, d, h, M, s, ms) {
        // can't just apply() to create a date:
        // https://stackoverflow.com/q/181348
        var date;
        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            date = new Date(y + 400, m, d, h, M, s, ms);
            if (isFinite(date.getFullYear())) {
                date.setFullYear(y);
            }
        } else {
            date = new Date(y, m, d, h, M, s, ms);
        }

        return date;
    }

    function createUTCDate(y) {
        var date, args;
        // the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            args = Array.prototype.slice.call(arguments);
            // preserve leap years using a full 400 year cycle, then reset
            args[0] = y + 400;
            date = new Date(Date.UTC.apply(null, args));
            if (isFinite(date.getUTCFullYear())) {
                date.setUTCFullYear(y);
            }
        } else {
            date = new Date(Date.UTC.apply(null, arguments));
        }

        return date;
    }

    // start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
            fwd = 7 + dow - doy,
            // first-week day local weekday -- which local weekday is fwd
            fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

    // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear,
            resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear,
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek,
            resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear,
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

    // FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // PARSING

    addRegexToken('w', match1to2, match1to2NoLeadingZero);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W', match1to2, match1to2NoLeadingZero);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(
        ['w', 'ww', 'W', 'WW'],
        function (input, week, config, token) {
            week[token.substr(0, 1)] = toInt(input);
        }
    );

    // HELPERS

    // LOCALES

    function localeWeek(mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow: 0, // Sunday is the first day of the week.
        doy: 6, // The week that contains Jan 6th is the first week of the year.
    };

    function localeFirstDayOfWeek() {
        return this._week.dow;
    }

    function localeFirstDayOfYear() {
        return this._week.doy;
    }

    // MOMENTS

    function getSetWeek(input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek(input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    // FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // PARSING

    addRegexToken('d', match1to2);
    addRegexToken('e', match1to2);
    addRegexToken('E', match1to2);
    addRegexToken('dd', function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd', function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd', function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

    // LOCALES
    function shiftWeekdays(ws, n) {
        return ws.slice(n, 7).concat(ws.slice(0, n));
    }

    var defaultLocaleWeekdays =
            'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
        defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
        defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
        defaultWeekdaysRegex = matchWord,
        defaultWeekdaysShortRegex = matchWord,
        defaultWeekdaysMinRegex = matchWord;

    function localeWeekdays(m, format) {
        var weekdays = isArray(this._weekdays)
            ? this._weekdays
            : this._weekdays[
                  m && m !== true && this._weekdays.isFormat.test(format)
                      ? 'format'
                      : 'standalone'
              ];
        return m === true
            ? shiftWeekdays(weekdays, this._week.dow)
            : m
              ? weekdays[m.day()]
              : weekdays;
    }

    function localeWeekdaysShort(m) {
        return m === true
            ? shiftWeekdays(this._weekdaysShort, this._week.dow)
            : m
              ? this._weekdaysShort[m.day()]
              : this._weekdaysShort;
    }

    function localeWeekdaysMin(m) {
        return m === true
            ? shiftWeekdays(this._weekdaysMin, this._week.dow)
            : m
              ? this._weekdaysMin[m.day()]
              : this._weekdaysMin;
    }

    function handleStrictParse$1(weekdayName, format, strict) {
        var i,
            ii,
            mom,
            llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(
                    mom,
                    ''
                ).toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(
                    mom,
                    ''
                ).toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse(weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return handleStrictParse$1.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp(
                    '^' + this.weekdays(mom, '').replace('.', '\\.?') + '$',
                    'i'
                );
                this._shortWeekdaysParse[i] = new RegExp(
                    '^' + this.weekdaysShort(mom, '').replace('.', '\\.?') + '$',
                    'i'
                );
                this._minWeekdaysParse[i] = new RegExp(
                    '^' + this.weekdaysMin(mom, '').replace('.', '\\.?') + '$',
                    'i'
                );
            }
            if (!this._weekdaysParse[i]) {
                regex =
                    '^' +
                    this.weekdays(mom, '') +
                    '|^' +
                    this.weekdaysShort(mom, '') +
                    '|^' +
                    this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (
                strict &&
                format === 'dddd' &&
                this._fullWeekdaysParse[i].test(weekdayName)
            ) {
                return i;
            } else if (
                strict &&
                format === 'ddd' &&
                this._shortWeekdaysParse[i].test(weekdayName)
            ) {
                return i;
            } else if (
                strict &&
                format === 'dd' &&
                this._minWeekdaysParse[i].test(weekdayName)
            ) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        var day = get(this, 'Day');
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    function weekdaysRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict
                ? this._weekdaysStrictRegex
                : this._weekdaysRegex;
        }
    }

    function weekdaysShortRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict
                ? this._weekdaysShortStrictRegex
                : this._weekdaysShortRegex;
        }
    }

    function weekdaysMinRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict
                ? this._weekdaysMinStrictRegex
                : this._weekdaysMinRegex;
        }
    }

    function computeWeekdaysParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [],
            shortPieces = [],
            longPieces = [],
            mixedPieces = [],
            i,
            mom,
            minp,
            shortp,
            longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, 1]).day(i);
            minp = regexEscape(this.weekdaysMin(mom, ''));
            shortp = regexEscape(this.weekdaysShort(mom, ''));
            longp = regexEscape(this.weekdays(mom, ''));
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp(
            '^(' + longPieces.join('|') + ')',
            'i'
        );
        this._weekdaysShortStrictRegex = new RegExp(
            '^(' + shortPieces.join('|') + ')',
            'i'
        );
        this._weekdaysMinStrictRegex = new RegExp(
            '^(' + minPieces.join('|') + ')',
            'i'
        );
    }

    // FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return (
            '' +
            hFormat.apply(this) +
            zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2)
        );
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return (
            '' +
            this.hours() +
            zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2)
        );
    });

    function meridiem(token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(
                this.hours(),
                this.minutes(),
                lowercase
            );
        });
    }

    meridiem('a', true);
    meridiem('A', false);

    // PARSING

    function matchMeridiem(isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a', matchMeridiem);
    addRegexToken('A', matchMeridiem);
    addRegexToken('H', match1to2, match1to2HasZero);
    addRegexToken('h', match1to2, match1to2NoLeadingZero);
    addRegexToken('k', match1to2, match1to2NoLeadingZero);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);
    addRegexToken('kk', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['k', 'kk'], function (input, array, config) {
        var kInput = toInt(input);
        array[HOUR] = kInput === 24 ? 0 : kInput;
    });
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4,
            pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4,
            pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

    // LOCALES

    function localeIsPM(input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return (input + '').toLowerCase().charAt(0) === 'p';
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i,
        // Setting the hour should keep the time, because the user explicitly
        // specified which hour they want. So trying to maintain the same hour (in
        // a new timezone) makes sense. Adding/subtracting hours does not follow
        // this rule.
        getSetHour = makeGetSet('Hours', true);

    function localeMeridiem(hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }

    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse,
    };

    // internal storage for locale config files
    var locales = {},
        localeFamilies = {},
        globalLocale;

    function commonPrefix(arr1, arr2) {
        var i,
            minl = Math.min(arr1.length, arr2.length);
        for (i = 0; i < minl; i += 1) {
            if (arr1[i] !== arr2[i]) {
                return i;
            }
        }
        return minl;
    }

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0,
            j,
            next,
            locale,
            split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (
                    next &&
                    next.length >= j &&
                    commonPrefix(split, next) >= j - 1
                ) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return globalLocale;
    }

    function isLocaleNameSane(name) {
        // Prevent names that look like filesystem paths, i.e contain '/' or '\'
        // Ensure name is available and function returns boolean
        return !!(name && name.match('^[^/\\\\]*$'));
    }

    function loadLocale(name) {
        var oldLocale = null,
            aliasedRequire;
        // TODO: Find a better way to register and load all the locales in Node
        if (
            locales[name] === undefined &&
            typeof module !== 'undefined' &&
            module &&
            module.exports &&
            isLocaleNameSane(name)
        ) {
            try {
                oldLocale = globalLocale._abbr;
                aliasedRequire = require;
                aliasedRequire('./locale/' + name);
                getSetGlobalLocale(oldLocale);
            } catch (e) {
                // mark as not found to avoid repeating expensive file require call causing high CPU
                // when trying to find en-US, en_US, en-us for every format call
                locales[name] = null; // null means not found
            }
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function getSetGlobalLocale(key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = getLocale(key);
            } else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            } else {
                if (typeof console !== 'undefined' && console.warn) {
                    //warn user if arguments are passed but the locale could not be set
                    console.warn(
                        'Locale ' + key + ' not found. Did you forget to load it?'
                    );
                }
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale(name, config) {
        if (config !== null) {
            var locale,
                parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple(
                    'defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                        'an existing locale. moment.defineLocale(localeName, ' +
                        'config) should only be used for creating a new locale ' +
                        'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.'
                );
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    locale = loadLocale(config.parentLocale);
                    if (locale != null) {
                        parentConfig = locale._config;
                    } else {
                        if (!localeFamilies[config.parentLocale]) {
                            localeFamilies[config.parentLocale] = [];
                        }
                        localeFamilies[config.parentLocale].push({
                            name: name,
                            config: config,
                        });
                        return null;
                    }
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            if (localeFamilies[name]) {
                localeFamilies[name].forEach(function (x) {
                    defineLocale(x.name, x.config);
                });
            }

            // backwards compat for now: also set the locale
            // make sure we set the locale AFTER all child locales have been
            // created, so we won't end up with the child locale set.
            getSetGlobalLocale(name);

            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale,
                tmpLocale,
                parentConfig = baseConfig;

            if (locales[name] != null && locales[name].parentLocale != null) {
                // Update existing child locale in-place to avoid memory-leaks
                locales[name].set(mergeConfigs(locales[name]._config, config));
            } else {
                // MERGE
                tmpLocale = loadLocale(name);
                if (tmpLocale != null) {
                    parentConfig = tmpLocale._config;
                }
                config = mergeConfigs(parentConfig, config);
                if (tmpLocale == null) {
                    // updateLocale is called for creating a new locale
                    // Set abbr so it will have a name (getters return
                    // undefined otherwise).
                    config.abbr = name;
                }
                locale = new Locale(config);
                locale.parentLocale = locales[name];
                locales[name] = locale;
            }

            // backwards compat for now: also set the locale
            getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                    if (name === getSetGlobalLocale()) {
                        getSetGlobalLocale(name);
                    }
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

    // returns locale data
    function getLocale(key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function listLocales() {
        return keys(locales);
    }

    function checkOverflow(m) {
        var overflow,
            a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow =
                a[MONTH] < 0 || a[MONTH] > 11
                    ? MONTH
                    : a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH])
                      ? DATE
                      : a[HOUR] < 0 ||
                          a[HOUR] > 24 ||
                          (a[HOUR] === 24 &&
                              (a[MINUTE] !== 0 ||
                                  a[SECOND] !== 0 ||
                                  a[MILLISECOND] !== 0))
                        ? HOUR
                        : a[MINUTE] < 0 || a[MINUTE] > 59
                          ? MINUTE
                          : a[SECOND] < 0 || a[SECOND] > 59
                            ? SECOND
                            : a[MILLISECOND] < 0 || a[MILLISECOND] > 999
                              ? MILLISECOND
                              : -1;

            if (
                getParsingFlags(m)._overflowDayOfYear &&
                (overflow < YEAR || overflow > DATE)
            ) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

    // iso 8601 regex
    // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex =
            /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        basicIsoRegex =
            /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d|))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        tzRegex = /Z|[+-]\d\d(?::?\d\d)?/,
        isoDates = [
            ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
            ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
            ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
            ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
            ['YYYY-DDD', /\d{4}-\d{3}/],
            ['YYYY-MM', /\d{4}-\d\d/, false],
            ['YYYYYYMMDD', /[+-]\d{10}/],
            ['YYYYMMDD', /\d{8}/],
            ['GGGG[W]WWE', /\d{4}W\d{3}/],
            ['GGGG[W]WW', /\d{4}W\d{2}/, false],
            ['YYYYDDD', /\d{7}/],
            ['YYYYMM', /\d{6}/, false],
            ['YYYY', /\d{4}/, false],
        ],
        // iso time formats and regexes
        isoTimes = [
            ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
            ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
            ['HH:mm:ss', /\d\d:\d\d:\d\d/],
            ['HH:mm', /\d\d:\d\d/],
            ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
            ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
            ['HHmmss', /\d\d\d\d\d\d/],
            ['HHmm', /\d\d\d\d/],
            ['HH', /\d\d/],
        ],
        aspNetJsonRegex = /^\/?Date\((-?\d+)/i,
        // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
        rfc2822 =
            /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/,
        obsOffsets = {
            UT: 0,
            GMT: 0,
            EDT: -4 * 60,
            EST: -5 * 60,
            CDT: -5 * 60,
            CST: -6 * 60,
            MDT: -6 * 60,
            MST: -7 * 60,
            PDT: -7 * 60,
            PST: -8 * 60,
        };

    // date from iso format
    function configFromISO(config) {
        var i,
            l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime,
            dateFormat,
            timeFormat,
            tzFormat,
            isoDatesLen = isoDates.length,
            isoTimesLen = isoTimes.length;

        if (match) {
            getParsingFlags(config).iso = true;
            for (i = 0, l = isoDatesLen; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimesLen; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    function extractFromRFC2822Strings(
        yearStr,
        monthStr,
        dayStr,
        hourStr,
        minuteStr,
        secondStr
    ) {
        var result = [
            untruncateYear(yearStr),
            defaultLocaleMonthsShort.indexOf(monthStr),
            parseInt(dayStr, 10),
            parseInt(hourStr, 10),
            parseInt(minuteStr, 10),
        ];

        if (secondStr) {
            result.push(parseInt(secondStr, 10));
        }

        return result;
    }

    function untruncateYear(yearStr) {
        var year = parseInt(yearStr, 10);
        if (year <= 49) {
            return 2000 + year;
        } else if (year <= 999) {
            return 1900 + year;
        }
        return year;
    }

    function preprocessRFC2822(s) {
        // Remove comments and folding whitespace and replace multiple-spaces with a single space
        return s
            .replace(/\([^()]*\)|[\n\t]/g, ' ')
            .replace(/(\s\s+)/g, ' ')
            .replace(/^\s\s*/, '')
            .replace(/\s\s*$/, '');
    }

    function checkWeekday(weekdayStr, parsedInput, config) {
        if (weekdayStr) {
            // TODO: Replace the vanilla JS Date object with an independent day-of-week check.
            var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
                weekdayActual = new Date(
                    parsedInput[0],
                    parsedInput[1],
                    parsedInput[2]
                ).getDay();
            if (weekdayProvided !== weekdayActual) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return false;
            }
        }
        return true;
    }

    function calculateOffset(obsOffset, militaryOffset, numOffset) {
        if (obsOffset) {
            return obsOffsets[obsOffset];
        } else if (militaryOffset) {
            // the only allowed military tz is Z
            return 0;
        } else {
            var hm = parseInt(numOffset, 10),
                m = hm % 100,
                h = (hm - m) / 100;
            return h * 60 + m;
        }
    }

    // date and time from ref 2822 format
    function configFromRFC2822(config) {
        var match = rfc2822.exec(preprocessRFC2822(config._i)),
            parsedArray;
        if (match) {
            parsedArray = extractFromRFC2822Strings(
                match[4],
                match[3],
                match[2],
                match[5],
                match[6],
                match[7]
            );
            if (!checkWeekday(match[1], parsedArray, config)) {
                return;
            }

            config._a = parsedArray;
            config._tzm = calculateOffset(match[8], match[9], match[10]);

            config._d = createUTCDate.apply(null, config._a);
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

            getParsingFlags(config).rfc2822 = true;
        } else {
            config._isValid = false;
        }
    }

    // date from 1) ASP.NET, 2) ISO, 3) RFC 2822 formats, or 4) optional fallback if parsing isn't strict
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);
        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        configFromRFC2822(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        if (config._strict) {
            config._isValid = false;
        } else {
            // Final attempt, use Input Fallback
            hooks.createFromInputFallback(config);
        }
    }

    hooks.createFromInputFallback = deprecate(
        'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
            'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
            'discouraged. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.',
        function (config) {
            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
        }
    );

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(hooks.now());
        if (config._useUTC) {
            return [
                nowValue.getUTCFullYear(),
                nowValue.getUTCMonth(),
                nowValue.getUTCDate(),
            ];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray(config) {
        var i,
            date,
            input = [],
            currentDate,
            expectedWeekday,
            yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear != null) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (
                config._dayOfYear > daysInYear(yearToUse) ||
                config._dayOfYear === 0
            ) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] =
                config._a[i] == null ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (
            config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0
        ) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(
            null,
            input
        );
        expectedWeekday = config._useUTC
            ? config._d.getUTCDay()
            : config._d.getDay();

        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }

        // check for mismatching day of week
        if (
            config._w &&
            typeof config._w.d !== 'undefined' &&
            config._w.d !== expectedWeekday
        ) {
            getParsingFlags(config).weekdayMismatch = true;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow, curWeek;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(
                w.GG,
                config._a[YEAR],
                weekOfYear(createLocal(), 1, 4).year
            );
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            curWeek = weekOfYear(createLocal(), dow, doy);

            weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

            // Default to current week.
            week = defaults(w.w, curWeek.week);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from beginning of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to beginning of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

    // constant that refers to the ISO standard
    hooks.ISO_8601 = function () {};

    // constant that refers to the RFC 2822 form
    hooks.RFC_2822 = function () {};

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === hooks.ISO_8601) {
            configFromISO(config);
            return;
        }
        if (config._f === hooks.RFC_2822) {
            configFromRFC2822(config);
            return;
        }
        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i,
            parsedInput,
            tokens,
            token,
            skipped,
            stringLength = string.length,
            totalParsedInputLength = 0,
            era,
            tokenLen;

        tokens =
            expandFormat(config._f, config._locale).match(formattingTokens) || [];
        tokenLen = tokens.length;
        for (i = 0; i < tokenLen; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) ||
                [])[0];
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(
                    string.indexOf(parsedInput) + parsedInput.length
                );
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                } else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            } else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver =
            stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (
            config._a[HOUR] <= 12 &&
            getParsingFlags(config).bigHour === true &&
            config._a[HOUR] > 0
        ) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(
            config._locale,
            config._a[HOUR],
            config._meridiem
        );

        // handle era
        era = getParsingFlags(config).era;
        if (era !== null) {
            config._a[YEAR] = config._locale.erasConvertYear(era, config._a[YEAR]);
        }

        configFromArray(config);
        checkOverflow(config);
    }

    function meridiemFixWrap(locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    // date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig,
            bestMoment,
            scoreToBeat,
            i,
            currentScore,
            validFormatFound,
            bestFormatIsValid = false,
            configfLen = config._f.length;

        if (configfLen === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < configfLen; i++) {
            currentScore = 0;
            validFormatFound = false;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (isValid(tempConfig)) {
                validFormatFound = true;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (!bestFormatIsValid) {
                if (
                    scoreToBeat == null ||
                    currentScore < scoreToBeat ||
                    validFormatFound
                ) {
                    scoreToBeat = currentScore;
                    bestMoment = tempConfig;
                    if (validFormatFound) {
                        bestFormatIsValid = true;
                    }
                }
            } else {
                if (currentScore < scoreToBeat) {
                    scoreToBeat = currentScore;
                    bestMoment = tempConfig;
                }
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i),
            dayOrDate = i.day === undefined ? i.date : i.day;
        config._a = map(
            [i.year, i.month, dayOrDate, i.hour, i.minute, i.second, i.millisecond],
            function (obj) {
                return obj && parseInt(obj, 10);
            }
        );

        configFromArray(config);
    }

    function createFromConfig(config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig(config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || getLocale(config._l);

        if (input === null || (format === undefined && input === '')) {
            return createInvalid({ nullInput: true });
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isDate(input)) {
            config._d = input;
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (format) {
            configFromStringAndFormat(config);
        } else {
            configFromInput(config);
        }

        if (!isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (isUndefined(input)) {
            config._d = new Date(hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (isObject(input)) {
            configFromObject(config);
        } else if (isNumber(input)) {
            // from milliseconds
            config._d = new Date(input);
        } else {
            hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC(input, format, locale, strict, isUTC) {
        var c = {};

        if (format === true || format === false) {
            strict = format;
            format = undefined;
        }

        if (locale === true || locale === false) {
            strict = locale;
            locale = undefined;
        }

        if (
            (isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)
        ) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function createLocal(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
            'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
            function () {
                var other = createLocal.apply(null, arguments);
                if (this.isValid() && other.isValid()) {
                    return other < this ? this : other;
                } else {
                    return createInvalid();
                }
            }
        ),
        prototypeMax = deprecate(
            'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
            function () {
                var other = createLocal.apply(null, arguments);
                if (this.isValid() && other.isValid()) {
                    return other > this ? this : other;
                } else {
                    return createInvalid();
                }
            }
        );

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

    // TODO: Use [].sort instead?
    function min() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function () {
        return Date.now ? Date.now() : +new Date();
    };

    var ordering = [
        'year',
        'quarter',
        'month',
        'week',
        'day',
        'hour',
        'minute',
        'second',
        'millisecond',
    ];

    function isDurationValid(m) {
        var key,
            unitHasDecimal = false,
            i,
            orderLen = ordering.length;
        for (key in m) {
            if (
                hasOwnProp(m, key) &&
                !(
                    indexOf.call(ordering, key) !== -1 &&
                    (m[key] == null || !isNaN(m[key]))
                )
            ) {
                return false;
            }
        }

        for (i = 0; i < orderLen; ++i) {
            if (m[ordering[i]]) {
                if (unitHasDecimal) {
                    return false; // only allow non-integers for smallest unit
                }
                if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                    unitHasDecimal = true;
                }
            }
        }

        return true;
    }

    function isValid$1() {
        return this._isValid;
    }

    function createInvalid$1() {
        return createDuration(NaN);
    }

    function Duration(duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || normalizedInput.isoWeek || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        this._isValid = isDurationValid(normalizedInput);

        // representation for dateAddRemove
        this._milliseconds =
            +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days + weeks * 7;
        // It is impossible to translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months + quarters * 3 + years * 12;

        this._data = {};

        this._locale = getLocale();

        this._bubble();
    }

    function isDuration(obj) {
        return obj instanceof Duration;
    }

    function absRound(number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

    // compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if (
                (dontConvert && array1[i] !== array2[i]) ||
                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))
            ) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    // FORMATTING

    function offset(token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset(),
                sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return (
                sign +
                zeroFill(~~(offset / 60), 2) +
                separator +
                zeroFill(~~offset % 60, 2)
            );
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z', matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = (string || '').match(matcher),
            chunk,
            parts,
            minutes;

        if (matches === null) {
            return null;
        }

        chunk = matches[matches.length - 1] || [];
        parts = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        minutes = +(parts[1] * 60) + toInt(parts[2]);

        return minutes === 0 ? 0 : parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff =
                (isMoment(input) || isDate(input)
                    ? input.valueOf()
                    : createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            hooks.updateOffset(res, false);
            return res;
        } else {
            return createLocal(input).local();
        }
    }

    function getDateOffset(m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset());
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    hooks.updateOffset = function () {};

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset(input, keepLocalTime, keepMinutes) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
                if (input === null) {
                    return this;
                }
            } else if (Math.abs(input) < 16 && !keepMinutes) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    addSubtract(
                        this,
                        createDuration(input - offset, 'm'),
                        1,
                        false
                    );
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone(input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC(keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal(keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset() {
        if (this._tzm != null) {
            this.utcOffset(this._tzm, false, true);
        } else if (typeof this._i === 'string') {
            var tZone = offsetFromString(matchOffset, this._i);
            if (tZone != null) {
                this.utcOffset(tZone);
            } else {
                this.utcOffset(0, true);
            }
        }
        return this;
    }

    function hasAlignedHourOffset(input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime() {
        return (
            this.utcOffset() > this.clone().month(0).utcOffset() ||
            this.utcOffset() > this.clone().month(5).utcOffset()
        );
    }

    function isDaylightSavingTimeShifted() {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {},
            other;

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
            this._isDSTShifted =
                this.isValid() && compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal() {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset() {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc() {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

    // ASP.NET json date format regex
    var aspNetRegex = /^(-|\+)?(?:(\d*)[. ])?(\d+):(\d+)(?::(\d+)(\.\d*)?)?$/,
        // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
        // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
        // and further modified to allow for strings containing both week and day
        isoRegex =
            /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

    function createDuration(input, key) {
        var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms: input._milliseconds,
                d: input._days,
                M: input._months,
            };
        } else if (isNumber(input) || !isNaN(+input)) {
            duration = {};
            if (key) {
                duration[key] = +input;
            } else {
                duration.milliseconds = +input;
            }
        } else if ((match = aspNetRegex.exec(input))) {
            sign = match[1] === '-' ? -1 : 1;
            duration = {
                y: 0,
                d: toInt(match[DATE]) * sign,
                h: toInt(match[HOUR]) * sign,
                m: toInt(match[MINUTE]) * sign,
                s: toInt(match[SECOND]) * sign,
                ms: toInt(absRound(match[MILLISECOND] * 1000)) * sign, // the millisecond decimal point is included in the match
            };
        } else if ((match = isoRegex.exec(input))) {
            sign = match[1] === '-' ? -1 : 1;
            duration = {
                y: parseIso(match[2], sign),
                M: parseIso(match[3], sign),
                w: parseIso(match[4], sign),
                d: parseIso(match[5], sign),
                h: parseIso(match[6], sign),
                m: parseIso(match[7], sign),
                s: parseIso(match[8], sign),
            };
        } else if (duration == null) {
            // checks for null or undefined
            duration = {};
        } else if (
            typeof duration === 'object' &&
            ('from' in duration || 'to' in duration)
        ) {
            diffRes = momentsDifference(
                createLocal(duration.from),
                createLocal(duration.to)
            );

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        if (isDuration(input) && hasOwnProp(input, '_isValid')) {
            ret._isValid = input._isValid;
        }

        return ret;
    }

    createDuration.fn = Duration.prototype;
    createDuration.invalid = createInvalid$1;

    function parseIso(inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = {};

        res.months =
            other.month() - base.month() + (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +base.clone().add(res.months, 'M');

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return { milliseconds: 0, months: 0 };
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

    // TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(
                    name,
                    'moment().' +
                        name +
                        '(period, number) is deprecated. Please use moment().' +
                        name +
                        '(number, period). ' +
                        'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.'
                );
                tmp = val;
                val = period;
                period = tmp;
            }

            dur = createDuration(val, period);
            addSubtract(this, dur, direction);
            return this;
        };
    }

    function addSubtract(mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (months) {
            setMonth(mom, get(mom, 'Month') + months * isAdding);
        }
        if (days) {
            set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
        }
        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (updateOffset) {
            hooks.updateOffset(mom, days || months);
        }
    }

    var add = createAdder(1, 'add'),
        subtract = createAdder(-1, 'subtract');

    function isString(input) {
        return typeof input === 'string' || input instanceof String;
    }

    // type MomentInput = Moment | Date | string | number | (number | string)[] | MomentInputObject | void; // null | undefined
    function isMomentInput(input) {
        return (
            isMoment(input) ||
            isDate(input) ||
            isString(input) ||
            isNumber(input) ||
            isNumberOrStringArray(input) ||
            isMomentInputObject(input) ||
            input === null ||
            input === undefined
        );
    }

    function isMomentInputObject(input) {
        var objectTest = isObject(input) && !isObjectEmpty(input),
            propertyTest = false,
            properties = [
                'years',
                'year',
                'y',
                'months',
                'month',
                'M',
                'days',
                'day',
                'd',
                'dates',
                'date',
                'D',
                'hours',
                'hour',
                'h',
                'minutes',
                'minute',
                'm',
                'seconds',
                'second',
                's',
                'milliseconds',
                'millisecond',
                'ms',
            ],
            i,
            property,
            propertyLen = properties.length;

        for (i = 0; i < propertyLen; i += 1) {
            property = properties[i];
            propertyTest = propertyTest || hasOwnProp(input, property);
        }

        return objectTest && propertyTest;
    }

    function isNumberOrStringArray(input) {
        var arrayTest = isArray(input),
            dataTypeTest = false;
        if (arrayTest) {
            dataTypeTest =
                input.filter(function (item) {
                    return !isNumber(item) && isString(input);
                }).length === 0;
        }
        return arrayTest && dataTypeTest;
    }

    function isCalendarSpec(input) {
        var objectTest = isObject(input) && !isObjectEmpty(input),
            propertyTest = false,
            properties = [
                'sameDay',
                'nextDay',
                'lastDay',
                'nextWeek',
                'lastWeek',
                'sameElse',
            ],
            i,
            property;

        for (i = 0; i < properties.length; i += 1) {
            property = properties[i];
            propertyTest = propertyTest || hasOwnProp(input, property);
        }

        return objectTest && propertyTest;
    }

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6
            ? 'sameElse'
            : diff < -1
              ? 'lastWeek'
              : diff < 0
                ? 'lastDay'
                : diff < 1
                  ? 'sameDay'
                  : diff < 2
                    ? 'nextDay'
                    : diff < 7
                      ? 'nextWeek'
                      : 'sameElse';
    }

    function calendar$1(time, formats) {
        // Support for single parameter, formats only overload to the calendar function
        if (arguments.length === 1) {
            if (!arguments[0]) {
                time = undefined;
                formats = undefined;
            } else if (isMomentInput(arguments[0])) {
                time = arguments[0];
                formats = undefined;
            } else if (isCalendarSpec(arguments[0])) {
                formats = arguments[0];
                time = undefined;
            }
        }
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = hooks.calendarFormat(this, sod) || 'sameElse',
            output =
                formats &&
                (isFunction(formats[format])
                    ? formats[format].call(this, now)
                    : formats[format]);

        return this.format(
            output || this.localeData().calendar(format, this, createLocal(now))
        );
    }

    function clone() {
        return new Moment(this);
    }

    function isAfter(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween(from, to, units, inclusivity) {
        var localFrom = isMoment(from) ? from : createLocal(from),
            localTo = isMoment(to) ? to : createLocal(to);
        if (!(this.isValid() && localFrom.isValid() && localTo.isValid())) {
            return false;
        }
        inclusivity = inclusivity || '()';
        return (
            (inclusivity[0] === '('
                ? this.isAfter(localFrom, units)
                : !this.isBefore(localFrom, units)) &&
            (inclusivity[1] === ')'
                ? this.isBefore(localTo, units)
                : !this.isAfter(localTo, units))
        );
    }

    function isSame(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return (
                this.clone().startOf(units).valueOf() <= inputMs &&
                inputMs <= this.clone().endOf(units).valueOf()
            );
        }
    }

    function isSameOrAfter(input, units) {
        return this.isSame(input, units) || this.isAfter(input, units);
    }

    function isSameOrBefore(input, units) {
        return this.isSame(input, units) || this.isBefore(input, units);
    }

    function diff(input, units, asFloat) {
        var that, zoneDelta, output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        switch (units) {
            case 'year':
                output = monthDiff(this, that) / 12;
                break;
            case 'month':
                output = monthDiff(this, that);
                break;
            case 'quarter':
                output = monthDiff(this, that) / 3;
                break;
            case 'second':
                output = (this - that) / 1e3;
                break; // 1000
            case 'minute':
                output = (this - that) / 6e4;
                break; // 1000 * 60
            case 'hour':
                output = (this - that) / 36e5;
                break; // 1000 * 60 * 60
            case 'day':
                output = (this - that - zoneDelta) / 864e5;
                break; // 1000 * 60 * 60 * 24, negate dst
            case 'week':
                output = (this - that - zoneDelta) / 6048e5;
                break; // 1000 * 60 * 60 * 24 * 7, negate dst
            default:
                output = this - that;
        }

        return asFloat ? output : absFloor(output);
    }

    function monthDiff(a, b) {
        if (a.date() < b.date()) {
            // end-of-month calculations work correct when the start month has more
            // days than the end month.
            return -monthDiff(b, a);
        }
        // difference in months
        var wholeMonthDiff = (b.year() - a.year()) * 12 + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2,
            adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString() {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function toISOString(keepOffset) {
        if (!this.isValid()) {
            return null;
        }
        var utc = keepOffset !== true,
            m = utc ? this.clone().utc() : this;
        if (m.year() < 0 || m.year() > 9999) {
            return formatMoment(
                m,
                utc
                    ? 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]'
                    : 'YYYYYY-MM-DD[T]HH:mm:ss.SSSZ'
            );
        }
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            if (utc) {
                return this.toDate().toISOString();
            } else {
                return new Date(this.valueOf() + this.utcOffset() * 60 * 1000)
                    .toISOString()
                    .replace('Z', formatMoment(m, 'Z'));
            }
        }
        return formatMoment(
            m,
            utc ? 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYY-MM-DD[T]HH:mm:ss.SSSZ'
        );
    }

    /**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */
    function inspect() {
        if (!this.isValid()) {
            return 'moment.invalid(/* ' + this._i + ' */)';
        }
        var func = 'moment',
            zone = '',
            prefix,
            year,
            datetime,
            suffix;
        if (!this.isLocal()) {
            func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
            zone = 'Z';
        }
        prefix = '[' + func + '("]';
        year = 0 <= this.year() && this.year() <= 9999 ? 'YYYY' : 'YYYYYY';
        datetime = '-MM-DD[T]HH:mm:ss.SSS';
        suffix = zone + '[")]';

        return this.format(prefix + year + datetime + suffix);
    }

    function format(inputString) {
        if (!inputString) {
            inputString = this.isUtc()
                ? hooks.defaultFormatUtc
                : hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from(time, withoutSuffix) {
        if (
            this.isValid() &&
            ((isMoment(time) && time.isValid()) || createLocal(time).isValid())
        ) {
            return createDuration({ to: this, from: time })
                .locale(this.locale())
                .humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow(withoutSuffix) {
        return this.from(createLocal(), withoutSuffix);
    }

    function to(time, withoutSuffix) {
        if (
            this.isValid() &&
            ((isMoment(time) && time.isValid()) || createLocal(time).isValid())
        ) {
            return createDuration({ from: this, to: time })
                .locale(this.locale())
                .humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow(withoutSuffix) {
        return this.to(createLocal(), withoutSuffix);
    }

    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function locale(key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate(
        'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
        function (key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }
    );

    function localeData() {
        return this._locale;
    }

    var MS_PER_SECOND = 1000,
        MS_PER_MINUTE = 60 * MS_PER_SECOND,
        MS_PER_HOUR = 60 * MS_PER_MINUTE,
        MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR;

    // actual modulo - handles negative numbers (for dates before 1970):
    function mod$1(dividend, divisor) {
        return ((dividend % divisor) + divisor) % divisor;
    }

    function localStartOfDate(y, m, d) {
        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            return new Date(y + 400, m, d) - MS_PER_400_YEARS;
        } else {
            return new Date(y, m, d).valueOf();
        }
    }

    function utcStartOfDate(y, m, d) {
        // Date.UTC remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
        } else {
            return Date.UTC(y, m, d);
        }
    }

    function startOf(units) {
        var time, startOfDate;
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond' || !this.isValid()) {
            return this;
        }

        startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

        switch (units) {
            case 'year':
                time = startOfDate(this.year(), 0, 1);
                break;
            case 'quarter':
                time = startOfDate(
                    this.year(),
                    this.month() - (this.month() % 3),
                    1
                );
                break;
            case 'month':
                time = startOfDate(this.year(), this.month(), 1);
                break;
            case 'week':
                time = startOfDate(
                    this.year(),
                    this.month(),
                    this.date() - this.weekday()
                );
                break;
            case 'isoWeek':
                time = startOfDate(
                    this.year(),
                    this.month(),
                    this.date() - (this.isoWeekday() - 1)
                );
                break;
            case 'day':
            case 'date':
                time = startOfDate(this.year(), this.month(), this.date());
                break;
            case 'hour':
                time = this._d.valueOf();
                time -= mod$1(
                    time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE),
                    MS_PER_HOUR
                );
                break;
            case 'minute':
                time = this._d.valueOf();
                time -= mod$1(time, MS_PER_MINUTE);
                break;
            case 'second':
                time = this._d.valueOf();
                time -= mod$1(time, MS_PER_SECOND);
                break;
        }

        this._d.setTime(time);
        hooks.updateOffset(this, true);
        return this;
    }

    function endOf(units) {
        var time, startOfDate;
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond' || !this.isValid()) {
            return this;
        }

        startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

        switch (units) {
            case 'year':
                time = startOfDate(this.year() + 1, 0, 1) - 1;
                break;
            case 'quarter':
                time =
                    startOfDate(
                        this.year(),
                        this.month() - (this.month() % 3) + 3,
                        1
                    ) - 1;
                break;
            case 'month':
                time = startOfDate(this.year(), this.month() + 1, 1) - 1;
                break;
            case 'week':
                time =
                    startOfDate(
                        this.year(),
                        this.month(),
                        this.date() - this.weekday() + 7
                    ) - 1;
                break;
            case 'isoWeek':
                time =
                    startOfDate(
                        this.year(),
                        this.month(),
                        this.date() - (this.isoWeekday() - 1) + 7
                    ) - 1;
                break;
            case 'day':
            case 'date':
                time = startOfDate(this.year(), this.month(), this.date() + 1) - 1;
                break;
            case 'hour':
                time = this._d.valueOf();
                time +=
                    MS_PER_HOUR -
                    mod$1(
                        time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE),
                        MS_PER_HOUR
                    ) -
                    1;
                break;
            case 'minute':
                time = this._d.valueOf();
                time += MS_PER_MINUTE - mod$1(time, MS_PER_MINUTE) - 1;
                break;
            case 'second':
                time = this._d.valueOf();
                time += MS_PER_SECOND - mod$1(time, MS_PER_SECOND) - 1;
                break;
        }

        this._d.setTime(time);
        hooks.updateOffset(this, true);
        return this;
    }

    function valueOf() {
        return this._d.valueOf() - (this._offset || 0) * 60000;
    }

    function unix() {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate() {
        return new Date(this.valueOf());
    }

    function toArray() {
        var m = this;
        return [
            m.year(),
            m.month(),
            m.date(),
            m.hour(),
            m.minute(),
            m.second(),
            m.millisecond(),
        ];
    }

    function toObject() {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds(),
        };
    }

    function toJSON() {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function isValid$2() {
        return isValid(this);
    }

    function parsingFlags() {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt() {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict,
        };
    }

    addFormatToken('N', 0, 0, 'eraAbbr');
    addFormatToken('NN', 0, 0, 'eraAbbr');
    addFormatToken('NNN', 0, 0, 'eraAbbr');
    addFormatToken('NNNN', 0, 0, 'eraName');
    addFormatToken('NNNNN', 0, 0, 'eraNarrow');

    addFormatToken('y', ['y', 1], 'yo', 'eraYear');
    addFormatToken('y', ['yy', 2], 0, 'eraYear');
    addFormatToken('y', ['yyy', 3], 0, 'eraYear');
    addFormatToken('y', ['yyyy', 4], 0, 'eraYear');

    addRegexToken('N', matchEraAbbr);
    addRegexToken('NN', matchEraAbbr);
    addRegexToken('NNN', matchEraAbbr);
    addRegexToken('NNNN', matchEraName);
    addRegexToken('NNNNN', matchEraNarrow);

    addParseToken(
        ['N', 'NN', 'NNN', 'NNNN', 'NNNNN'],
        function (input, array, config, token) {
            var era = config._locale.erasParse(input, token, config._strict);
            if (era) {
                getParsingFlags(config).era = era;
            } else {
                getParsingFlags(config).invalidEra = input;
            }
        }
    );

    addRegexToken('y', matchUnsigned);
    addRegexToken('yy', matchUnsigned);
    addRegexToken('yyy', matchUnsigned);
    addRegexToken('yyyy', matchUnsigned);
    addRegexToken('yo', matchEraYearOrdinal);

    addParseToken(['y', 'yy', 'yyy', 'yyyy'], YEAR);
    addParseToken(['yo'], function (input, array, config, token) {
        var match;
        if (config._locale._eraYearOrdinalRegex) {
            match = input.match(config._locale._eraYearOrdinalRegex);
        }

        if (config._locale.eraYearOrdinalParse) {
            array[YEAR] = config._locale.eraYearOrdinalParse(input, match);
        } else {
            array[YEAR] = parseInt(input, 10);
        }
    });

    function localeEras(m, format) {
        var i,
            l,
            date,
            eras = this._eras || getLocale('en')._eras;
        for (i = 0, l = eras.length; i < l; ++i) {
            switch (typeof eras[i].since) {
                case 'string':
                    // truncate time
                    date = hooks(eras[i].since).startOf('day');
                    eras[i].since = date.valueOf();
                    break;
            }

            switch (typeof eras[i].until) {
                case 'undefined':
                    eras[i].until = +Infinity;
                    break;
                case 'string':
                    // truncate time
                    date = hooks(eras[i].until).startOf('day').valueOf();
                    eras[i].until = date.valueOf();
                    break;
            }
        }
        return eras;
    }

    function localeErasParse(eraName, format, strict) {
        var i,
            l,
            eras = this.eras(),
            name,
            abbr,
            narrow;
        eraName = eraName.toUpperCase();

        for (i = 0, l = eras.length; i < l; ++i) {
            name = eras[i].name.toUpperCase();
            abbr = eras[i].abbr.toUpperCase();
            narrow = eras[i].narrow.toUpperCase();

            if (strict) {
                switch (format) {
                    case 'N':
                    case 'NN':
                    case 'NNN':
                        if (abbr === eraName) {
                            return eras[i];
                        }
                        break;

                    case 'NNNN':
                        if (name === eraName) {
                            return eras[i];
                        }
                        break;

                    case 'NNNNN':
                        if (narrow === eraName) {
                            return eras[i];
                        }
                        break;
                }
            } else if ([name, abbr, narrow].indexOf(eraName) >= 0) {
                return eras[i];
            }
        }
    }

    function localeErasConvertYear(era, year) {
        var dir = era.since <= era.until ? +1 : -1;
        if (year === undefined) {
            return hooks(era.since).year();
        } else {
            return hooks(era.since).year() + (year - era.offset) * dir;
        }
    }

    function getEraName() {
        var i,
            l,
            val,
            eras = this.localeData().eras();
        for (i = 0, l = eras.length; i < l; ++i) {
            // truncate time
            val = this.clone().startOf('day').valueOf();

            if (eras[i].since <= val && val <= eras[i].until) {
                return eras[i].name;
            }
            if (eras[i].until <= val && val <= eras[i].since) {
                return eras[i].name;
            }
        }

        return '';
    }

    function getEraNarrow() {
        var i,
            l,
            val,
            eras = this.localeData().eras();
        for (i = 0, l = eras.length; i < l; ++i) {
            // truncate time
            val = this.clone().startOf('day').valueOf();

            if (eras[i].since <= val && val <= eras[i].until) {
                return eras[i].narrow;
            }
            if (eras[i].until <= val && val <= eras[i].since) {
                return eras[i].narrow;
            }
        }

        return '';
    }

    function getEraAbbr() {
        var i,
            l,
            val,
            eras = this.localeData().eras();
        for (i = 0, l = eras.length; i < l; ++i) {
            // truncate time
            val = this.clone().startOf('day').valueOf();

            if (eras[i].since <= val && val <= eras[i].until) {
                return eras[i].abbr;
            }
            if (eras[i].until <= val && val <= eras[i].since) {
                return eras[i].abbr;
            }
        }

        return '';
    }

    function getEraYear() {
        var i,
            l,
            dir,
            val,
            eras = this.localeData().eras();
        for (i = 0, l = eras.length; i < l; ++i) {
            dir = eras[i].since <= eras[i].until ? +1 : -1;

            // truncate time
            val = this.clone().startOf('day').valueOf();

            if (
                (eras[i].since <= val && val <= eras[i].until) ||
                (eras[i].until <= val && val <= eras[i].since)
            ) {
                return (
                    (this.year() - hooks(eras[i].since).year()) * dir +
                    eras[i].offset
                );
            }
        }

        return this.year();
    }

    function erasNameRegex(isStrict) {
        if (!hasOwnProp(this, '_erasNameRegex')) {
            computeErasParse.call(this);
        }
        return isStrict ? this._erasNameRegex : this._erasRegex;
    }

    function erasAbbrRegex(isStrict) {
        if (!hasOwnProp(this, '_erasAbbrRegex')) {
            computeErasParse.call(this);
        }
        return isStrict ? this._erasAbbrRegex : this._erasRegex;
    }

    function erasNarrowRegex(isStrict) {
        if (!hasOwnProp(this, '_erasNarrowRegex')) {
            computeErasParse.call(this);
        }
        return isStrict ? this._erasNarrowRegex : this._erasRegex;
    }

    function matchEraAbbr(isStrict, locale) {
        return locale.erasAbbrRegex(isStrict);
    }

    function matchEraName(isStrict, locale) {
        return locale.erasNameRegex(isStrict);
    }

    function matchEraNarrow(isStrict, locale) {
        return locale.erasNarrowRegex(isStrict);
    }

    function matchEraYearOrdinal(isStrict, locale) {
        return locale._eraYearOrdinalRegex || matchUnsigned;
    }

    function computeErasParse() {
        var abbrPieces = [],
            namePieces = [],
            narrowPieces = [],
            mixedPieces = [],
            i,
            l,
            erasName,
            erasAbbr,
            erasNarrow,
            eras = this.eras();

        for (i = 0, l = eras.length; i < l; ++i) {
            erasName = regexEscape(eras[i].name);
            erasAbbr = regexEscape(eras[i].abbr);
            erasNarrow = regexEscape(eras[i].narrow);

            namePieces.push(erasName);
            abbrPieces.push(erasAbbr);
            narrowPieces.push(erasNarrow);
            mixedPieces.push(erasName);
            mixedPieces.push(erasAbbr);
            mixedPieces.push(erasNarrow);
        }

        this._erasRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._erasNameRegex = new RegExp('^(' + namePieces.join('|') + ')', 'i');
        this._erasAbbrRegex = new RegExp('^(' + abbrPieces.join('|') + ')', 'i');
        this._erasNarrowRegex = new RegExp(
            '^(' + narrowPieces.join('|') + ')',
            'i'
        );
    }

    // FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken(token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg', 'weekYear');
    addWeekYearFormatToken('ggggg', 'weekYear');
    addWeekYearFormatToken('GGGG', 'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    // PARSING

    addRegexToken('G', matchSigned);
    addRegexToken('g', matchSigned);
    addRegexToken('GG', match1to2, match2);
    addRegexToken('gg', match1to2, match2);
    addRegexToken('GGGG', match1to4, match4);
    addRegexToken('gggg', match1to4, match4);
    addRegexToken('GGGGG', match1to6, match6);
    addRegexToken('ggggg', match1to6, match6);

    addWeekParseToken(
        ['gggg', 'ggggg', 'GGGG', 'GGGGG'],
        function (input, week, config, token) {
            week[token.substr(0, 2)] = toInt(input);
        }
    );

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = hooks.parseTwoDigitYear(input);
    });

    // MOMENTS

    function getSetWeekYear(input) {
        return getSetWeekYearHelper.call(
            this,
            input,
            this.week(),
            this.weekday() + this.localeData()._week.dow,
            this.localeData()._week.dow,
            this.localeData()._week.doy
        );
    }

    function getSetISOWeekYear(input) {
        return getSetWeekYearHelper.call(
            this,
            input,
            this.isoWeek(),
            this.isoWeekday(),
            1,
            4
        );
    }

    function getISOWeeksInYear() {
        return weeksInYear(this.year(), 1, 4);
    }

    function getISOWeeksInISOWeekYear() {
        return weeksInYear(this.isoWeekYear(), 1, 4);
    }

    function getWeeksInYear() {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getWeeksInWeekYear() {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.weekYear(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

    // FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter(input) {
        return input == null
            ? Math.ceil((this.month() + 1) / 3)
            : this.month((input - 1) * 3 + (this.month() % 3));
    }

    // FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // PARSING

    addRegexToken('D', match1to2, match1to2NoLeadingZero);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        // TODO: Remove "ordinalParse" fallback in next major release.
        return isStrict
            ? locale._dayOfMonthOrdinalParse || locale._ordinalParse
            : locale._dayOfMonthOrdinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0]);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    // FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // PARSING

    addRegexToken('DDD', match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

    // HELPERS

    // MOMENTS

    function getSetDayOfYear(input) {
        var dayOfYear =
            Math.round(
                (this.clone().startOf('day') - this.clone().startOf('year')) / 864e5
            ) + 1;
        return input == null ? dayOfYear : this.add(input - dayOfYear, 'd');
    }

    // FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // PARSING

    addRegexToken('m', match1to2, match1to2HasZero);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    // FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

    // PARSING

    addRegexToken('s', match1to2, match1to2HasZero);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    // FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });

    // PARSING

    addRegexToken('S', match1to3, match1);
    addRegexToken('SS', match1to3, match2);
    addRegexToken('SSS', match1to3, match3);

    var token, getSetMillisecond;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }

    getSetMillisecond = makeGetSet('Milliseconds', false);

    // FORMATTING

    addFormatToken('z', 0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr() {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName() {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var proto = Moment.prototype;

    proto.add = add;
    proto.calendar = calendar$1;
    proto.clone = clone;
    proto.diff = diff;
    proto.endOf = endOf;
    proto.format = format;
    proto.from = from;
    proto.fromNow = fromNow;
    proto.to = to;
    proto.toNow = toNow;
    proto.get = stringGet;
    proto.invalidAt = invalidAt;
    proto.isAfter = isAfter;
    proto.isBefore = isBefore;
    proto.isBetween = isBetween;
    proto.isSame = isSame;
    proto.isSameOrAfter = isSameOrAfter;
    proto.isSameOrBefore = isSameOrBefore;
    proto.isValid = isValid$2;
    proto.lang = lang;
    proto.locale = locale;
    proto.localeData = localeData;
    proto.max = prototypeMax;
    proto.min = prototypeMin;
    proto.parsingFlags = parsingFlags;
    proto.set = stringSet;
    proto.startOf = startOf;
    proto.subtract = subtract;
    proto.toArray = toArray;
    proto.toObject = toObject;
    proto.toDate = toDate;
    proto.toISOString = toISOString;
    proto.inspect = inspect;
    if (typeof Symbol !== 'undefined' && Symbol.for != null) {
        proto[Symbol.for('nodejs.util.inspect.custom')] = function () {
            return 'Moment<' + this.format() + '>';
        };
    }
    proto.toJSON = toJSON;
    proto.toString = toString;
    proto.unix = unix;
    proto.valueOf = valueOf;
    proto.creationData = creationData;
    proto.eraName = getEraName;
    proto.eraNarrow = getEraNarrow;
    proto.eraAbbr = getEraAbbr;
    proto.eraYear = getEraYear;
    proto.year = getSetYear;
    proto.isLeapYear = getIsLeapYear;
    proto.weekYear = getSetWeekYear;
    proto.isoWeekYear = getSetISOWeekYear;
    proto.quarter = proto.quarters = getSetQuarter;
    proto.month = getSetMonth;
    proto.daysInMonth = getDaysInMonth;
    proto.week = proto.weeks = getSetWeek;
    proto.isoWeek = proto.isoWeeks = getSetISOWeek;
    proto.weeksInYear = getWeeksInYear;
    proto.weeksInWeekYear = getWeeksInWeekYear;
    proto.isoWeeksInYear = getISOWeeksInYear;
    proto.isoWeeksInISOWeekYear = getISOWeeksInISOWeekYear;
    proto.date = getSetDayOfMonth;
    proto.day = proto.days = getSetDayOfWeek;
    proto.weekday = getSetLocaleDayOfWeek;
    proto.isoWeekday = getSetISODayOfWeek;
    proto.dayOfYear = getSetDayOfYear;
    proto.hour = proto.hours = getSetHour;
    proto.minute = proto.minutes = getSetMinute;
    proto.second = proto.seconds = getSetSecond;
    proto.millisecond = proto.milliseconds = getSetMillisecond;
    proto.utcOffset = getSetOffset;
    proto.utc = setOffsetToUTC;
    proto.local = setOffsetToLocal;
    proto.parseZone = setOffsetToParsedOffset;
    proto.hasAlignedHourOffset = hasAlignedHourOffset;
    proto.isDST = isDaylightSavingTime;
    proto.isLocal = isLocal;
    proto.isUtcOffset = isUtcOffset;
    proto.isUtc = isUtc;
    proto.isUTC = isUtc;
    proto.zoneAbbr = getZoneAbbr;
    proto.zoneName = getZoneName;
    proto.dates = deprecate(
        'dates accessor is deprecated. Use date instead.',
        getSetDayOfMonth
    );
    proto.months = deprecate(
        'months accessor is deprecated. Use month instead',
        getSetMonth
    );
    proto.years = deprecate(
        'years accessor is deprecated. Use year instead',
        getSetYear
    );
    proto.zone = deprecate(
        'moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/',
        getSetZone
    );
    proto.isDSTShifted = deprecate(
        'isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information',
        isDaylightSavingTimeShifted
    );

    function createUnix(input) {
        return createLocal(input * 1000);
    }

    function createInZone() {
        return createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat(string) {
        return string;
    }

    var proto$1 = Locale.prototype;

    proto$1.calendar = calendar;
    proto$1.longDateFormat = longDateFormat;
    proto$1.invalidDate = invalidDate;
    proto$1.ordinal = ordinal;
    proto$1.preparse = preParsePostFormat;
    proto$1.postformat = preParsePostFormat;
    proto$1.relativeTime = relativeTime;
    proto$1.pastFuture = pastFuture;
    proto$1.set = set;
    proto$1.eras = localeEras;
    proto$1.erasParse = localeErasParse;
    proto$1.erasConvertYear = localeErasConvertYear;
    proto$1.erasAbbrRegex = erasAbbrRegex;
    proto$1.erasNameRegex = erasNameRegex;
    proto$1.erasNarrowRegex = erasNarrowRegex;

    proto$1.months = localeMonths;
    proto$1.monthsShort = localeMonthsShort;
    proto$1.monthsParse = localeMonthsParse;
    proto$1.monthsRegex = monthsRegex;
    proto$1.monthsShortRegex = monthsShortRegex;
    proto$1.week = localeWeek;
    proto$1.firstDayOfYear = localeFirstDayOfYear;
    proto$1.firstDayOfWeek = localeFirstDayOfWeek;

    proto$1.weekdays = localeWeekdays;
    proto$1.weekdaysMin = localeWeekdaysMin;
    proto$1.weekdaysShort = localeWeekdaysShort;
    proto$1.weekdaysParse = localeWeekdaysParse;

    proto$1.weekdaysRegex = weekdaysRegex;
    proto$1.weekdaysShortRegex = weekdaysShortRegex;
    proto$1.weekdaysMinRegex = weekdaysMinRegex;

    proto$1.isPM = localeIsPM;
    proto$1.meridiem = localeMeridiem;

    function get$1(format, index, field, setter) {
        var locale = getLocale(),
            utc = createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl(format, index, field) {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return get$1(format, index, field, 'month');
        }

        var i,
            out = [];
        for (i = 0; i < 12; i++) {
            out[i] = get$1(format, i, field, 'month');
        }
        return out;
    }

    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function listWeekdaysImpl(localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = getLocale(),
            shift = localeSorted ? locale._week.dow : 0,
            i,
            out = [];

        if (index != null) {
            return get$1(format, (index + shift) % 7, field, 'day');
        }

        for (i = 0; i < 7; i++) {
            out[i] = get$1(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function listMonths(format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function listMonthsShort(format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function listWeekdays(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function listWeekdaysShort(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function listWeekdaysMin(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    getSetGlobalLocale('en', {
        eras: [
            {
                since: '0001-01-01',
                until: +Infinity,
                offset: 1,
                name: 'Anno Domini',
                narrow: 'AD',
                abbr: 'AD',
            },
            {
                since: '0000-12-31',
                until: -Infinity,
                offset: 1,
                name: 'Before Christ',
                narrow: 'BC',
                abbr: 'BC',
            },
        ],
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function (number) {
            var b = number % 10,
                output =
                    toInt((number % 100) / 10) === 1
                        ? 'th'
                        : b === 1
                          ? 'st'
                          : b === 2
                            ? 'nd'
                            : b === 3
                              ? 'rd'
                              : 'th';
            return number + output;
        },
    });

    // Side effect imports

    hooks.lang = deprecate(
        'moment.lang is deprecated. Use moment.locale instead.',
        getSetGlobalLocale
    );
    hooks.langData = deprecate(
        'moment.langData is deprecated. Use moment.localeData instead.',
        getLocale
    );

    var mathAbs = Math.abs;

    function abs() {
        var data = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days = mathAbs(this._days);
        this._months = mathAbs(this._months);

        data.milliseconds = mathAbs(data.milliseconds);
        data.seconds = mathAbs(data.seconds);
        data.minutes = mathAbs(data.minutes);
        data.hours = mathAbs(data.hours);
        data.months = mathAbs(data.months);
        data.years = mathAbs(data.years);

        return this;
    }

    function addSubtract$1(duration, input, value, direction) {
        var other = createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days += direction * other._days;
        duration._months += direction * other._months;

        return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function add$1(input, value) {
        return addSubtract$1(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function subtract$1(input, value) {
        return addSubtract$1(this, input, value, -1);
    }

    function absCeil(number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble() {
        var milliseconds = this._milliseconds,
            days = this._days,
            months = this._months,
            data = this._data,
            seconds,
            minutes,
            hours,
            years,
            monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (
            !(
                (milliseconds >= 0 && days >= 0 && months >= 0) ||
                (milliseconds <= 0 && days <= 0 && months <= 0)
            )
        ) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds = absFloor(milliseconds / 1000);
        data.seconds = seconds % 60;

        minutes = absFloor(seconds / 60);
        data.minutes = minutes % 60;

        hours = absFloor(minutes / 60);
        data.hours = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days = days;
        data.months = months;
        data.years = years;

        return this;
    }

    function daysToMonths(days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return (days * 4800) / 146097;
    }

    function monthsToDays(months) {
        // the reverse of daysToMonths
        return (months * 146097) / 4800;
    }

    function as(units) {
        if (!this.isValid()) {
            return NaN;
        }
        var days,
            months,
            milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'quarter' || units === 'year') {
            days = this._days + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            switch (units) {
                case 'month':
                    return months;
                case 'quarter':
                    return months / 3;
                case 'year':
                    return months / 12;
            }
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week':
                    return days / 7 + milliseconds / 6048e5;
                case 'day':
                    return days + milliseconds / 864e5;
                case 'hour':
                    return days * 24 + milliseconds / 36e5;
                case 'minute':
                    return days * 1440 + milliseconds / 6e4;
                case 'second':
                    return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond':
                    return Math.floor(days * 864e5) + milliseconds;
                default:
                    throw new Error('Unknown unit ' + units);
            }
        }
    }

    function makeAs(alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms'),
        asSeconds = makeAs('s'),
        asMinutes = makeAs('m'),
        asHours = makeAs('h'),
        asDays = makeAs('d'),
        asWeeks = makeAs('w'),
        asMonths = makeAs('M'),
        asQuarters = makeAs('Q'),
        asYears = makeAs('y'),
        valueOf$1 = asMilliseconds;

    function clone$1() {
        return createDuration(this);
    }

    function get$2(units) {
        units = normalizeUnits(units);
        return this.isValid() ? this[units + 's']() : NaN;
    }

    function makeGetter(name) {
        return function () {
            return this.isValid() ? this._data[name] : NaN;
        };
    }

    var milliseconds = makeGetter('milliseconds'),
        seconds = makeGetter('seconds'),
        minutes = makeGetter('minutes'),
        hours = makeGetter('hours'),
        days = makeGetter('days'),
        months = makeGetter('months'),
        years = makeGetter('years');

    function weeks() {
        return absFloor(this.days() / 7);
    }

    var round = Math.round,
        thresholds = {
            ss: 44, // a few seconds to seconds
            s: 45, // seconds to minute
            m: 45, // minutes to hour
            h: 22, // hours to day
            d: 26, // days to month/week
            w: null, // weeks to month
            M: 11, // months to year
        };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime$1(posNegDuration, withoutSuffix, thresholds, locale) {
        var duration = createDuration(posNegDuration).abs(),
            seconds = round(duration.as('s')),
            minutes = round(duration.as('m')),
            hours = round(duration.as('h')),
            days = round(duration.as('d')),
            months = round(duration.as('M')),
            weeks = round(duration.as('w')),
            years = round(duration.as('y')),
            a =
                (seconds <= thresholds.ss && ['s', seconds]) ||
                (seconds < thresholds.s && ['ss', seconds]) ||
                (minutes <= 1 && ['m']) ||
                (minutes < thresholds.m && ['mm', minutes]) ||
                (hours <= 1 && ['h']) ||
                (hours < thresholds.h && ['hh', hours]) ||
                (days <= 1 && ['d']) ||
                (days < thresholds.d && ['dd', days]);

        if (thresholds.w != null) {
            a =
                a ||
                (weeks <= 1 && ['w']) ||
                (weeks < thresholds.w && ['ww', weeks]);
        }
        a = a ||
            (months <= 1 && ['M']) ||
            (months < thresholds.M && ['MM', months]) ||
            (years <= 1 && ['y']) || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set the rounding function for relative time strings
    function getSetRelativeTimeRounding(roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof roundingFunction === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

    // This function allows you to set a threshold for relative time strings
    function getSetRelativeTimeThreshold(threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        if (threshold === 's') {
            thresholds.ss = limit - 1;
        }
        return true;
    }

    function humanize(argWithSuffix, argThresholds) {
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var withSuffix = false,
            th = thresholds,
            locale,
            output;

        if (typeof argWithSuffix === 'object') {
            argThresholds = argWithSuffix;
            argWithSuffix = false;
        }
        if (typeof argWithSuffix === 'boolean') {
            withSuffix = argWithSuffix;
        }
        if (typeof argThresholds === 'object') {
            th = Object.assign({}, thresholds, argThresholds);
            if (argThresholds.s != null && argThresholds.ss == null) {
                th.ss = argThresholds.s - 1;
            }
        }

        locale = this.localeData();
        output = relativeTime$1(this, !withSuffix, th, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var abs$1 = Math.abs;

    function sign(x) {
        return (x > 0) - (x < 0) || +x;
    }

    function toISOString$1() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var seconds = abs$1(this._milliseconds) / 1000,
            days = abs$1(this._days),
            months = abs$1(this._months),
            minutes,
            hours,
            years,
            s,
            total = this.asSeconds(),
            totalSign,
            ymSign,
            daysSign,
            hmsSign;

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes = absFloor(seconds / 60);
        hours = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';

        totalSign = total < 0 ? '-' : '';
        ymSign = sign(this._months) !== sign(total) ? '-' : '';
        daysSign = sign(this._days) !== sign(total) ? '-' : '';
        hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

        return (
            totalSign +
            'P' +
            (years ? ymSign + years + 'Y' : '') +
            (months ? ymSign + months + 'M' : '') +
            (days ? daysSign + days + 'D' : '') +
            (hours || minutes || seconds ? 'T' : '') +
            (hours ? hmsSign + hours + 'H' : '') +
            (minutes ? hmsSign + minutes + 'M' : '') +
            (seconds ? hmsSign + s + 'S' : '')
        );
    }

    var proto$2 = Duration.prototype;

    proto$2.isValid = isValid$1;
    proto$2.abs = abs;
    proto$2.add = add$1;
    proto$2.subtract = subtract$1;
    proto$2.as = as;
    proto$2.asMilliseconds = asMilliseconds;
    proto$2.asSeconds = asSeconds;
    proto$2.asMinutes = asMinutes;
    proto$2.asHours = asHours;
    proto$2.asDays = asDays;
    proto$2.asWeeks = asWeeks;
    proto$2.asMonths = asMonths;
    proto$2.asQuarters = asQuarters;
    proto$2.asYears = asYears;
    proto$2.valueOf = valueOf$1;
    proto$2._bubble = bubble;
    proto$2.clone = clone$1;
    proto$2.get = get$2;
    proto$2.milliseconds = milliseconds;
    proto$2.seconds = seconds;
    proto$2.minutes = minutes;
    proto$2.hours = hours;
    proto$2.days = days;
    proto$2.weeks = weeks;
    proto$2.months = months;
    proto$2.years = years;
    proto$2.humanize = humanize;
    proto$2.toISOString = toISOString$1;
    proto$2.toString = toISOString$1;
    proto$2.toJSON = toISOString$1;
    proto$2.locale = locale;
    proto$2.localeData = localeData;

    proto$2.toIsoString = deprecate(
        'toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)',
        toISOString$1
    );
    proto$2.lang = lang;

    // FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

    //! moment.js

    hooks.version = '2.30.1';

    setHookCallback(createLocal);

    hooks.fn = proto;
    hooks.min = min;
    hooks.max = max;
    hooks.now = now;
    hooks.utc = createUTC;
    hooks.unix = createUnix;
    hooks.months = listMonths;
    hooks.isDate = isDate;
    hooks.locale = getSetGlobalLocale;
    hooks.invalid = createInvalid;
    hooks.duration = createDuration;
    hooks.isMoment = isMoment;
    hooks.weekdays = listWeekdays;
    hooks.parseZone = createInZone;
    hooks.localeData = getLocale;
    hooks.isDuration = isDuration;
    hooks.monthsShort = listMonthsShort;
    hooks.weekdaysMin = listWeekdaysMin;
    hooks.defineLocale = defineLocale;
    hooks.updateLocale = updateLocale;
    hooks.locales = listLocales;
    hooks.weekdaysShort = listWeekdaysShort;
    hooks.normalizeUnits = normalizeUnits;
    hooks.relativeTimeRounding = getSetRelativeTimeRounding;
    hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
    hooks.calendarFormat = getCalendarFormat;
    hooks.prototype = proto;

    // currently HTML5 input type only supports 24-hour formats
    hooks.HTML5_FMT = {
        DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm', // <input type="datetime-local" />
        DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss', // <input type="datetime-local" step="1" />
        DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS', // <input type="datetime-local" step="0.001" />
        DATE: 'YYYY-MM-DD', // <input type="date" />
        TIME: 'HH:mm', // <input type="time" />
        TIME_SECONDS: 'HH:mm:ss', // <input type="time" step="1" />
        TIME_MS: 'HH:mm:ss.SSS', // <input type="time" step="0.001" />
        WEEK: 'GGGG-[W]WW', // <input type="week" />
        MONTH: 'YYYY-MM', // <input type="month" />
    };

    return hooks;

})));

},{}],188:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NamedState = exports.Named = exports.ComponentState = exports.Component = exports.AnnotatedState = exports.Annotated = void 0;
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.error.cause.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.push.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.test.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/esnext.iterator.constructor.js");
require("core-js/modules/esnext.iterator.filter.js");
require("core-js/modules/esnext.iterator.map.js");
require("core-js/modules/web.dom-collections.iterator.js");
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This module is required by the tasks and evaluator modules
// A design-time protocol consists of a tree of Component sub-class objects

// This abstract class is at the top of the component inheritance hierarchy
class Component {
  constructor(options) {
    ({
      _parent: this._parent,
      meta: this.meta
    } = options || {});
  }

  // List expected attributes for this class.
  // Used by validate function to raise warnings for unexpected attributes..
  static _attributes() {
    return ['meta', '_parent'];
  }

  // Keys that should not be flattened, see @flatten()
  static _ignoreKeys() {
    return ['_parent'];
  }

  // returns array of warning/error validations for this component and it's children
  // see also helper functions, _createWarningValidation and _createErrorValidation
  validate(identifiers) {
    if (identifiers == null) {
      identifiers = [];
    }
    const result = [];
    for (var key of Object.keys(this)) {
      if (!this.constructor._attributes().includes(key)) {
        result.push(this._createWarningValidation(key, "unexpected attribute - ".concat(key)));
      }
    }
    return result;
  }

  // returns false if there are any error validations
  isValid() {
    return this.validate().filter(issue => issue.type === 'Error').length === 0;
  }

  // overridden to add a class attribute, ignore empty values and drop the circular _parent attribute when serialised
  toJSON() {
    const result = {};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_parent'].includes(key) && !(value == null)) {
        result[key] = value;
      }
    }
    return result;
  }

  // Returns a pojo (plain old javascript object) version of component without
  // child components (hence it's flatness).  Used in runtime getComponent calls.
  flatten() {
    const result = {
      class: this.constructor.name
    };
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!this.constructor._ignoreKeys().includes(key) && value != null) {
        result[key] = value;
      }
    }
    return result;
  }

  // for walking down the tree.  to be overridden in sub-classes.
  _child() {
    return null;
  }

  // returns root component of tree
  _getRoot() {
    if (this._parent != null) {
      return this._parent._getRoot();
    } else {
      return this;
    }
  }

  // 'private' methods for creating validation messages
  _createValidation(type, attribute, message) {
    return {
      type,
      path: this.path != null ? this.path() : undefined,
      attribute,
      msg: message
    };
  }
  _createErrorValidation(attribute, message) {
    return this._createValidation('Error', attribute, message);
  }
  _createWarningValidation(attribute, message) {
    return this._createValidation('Warning', attribute, message);
  }
}

// A state class holds runtime state and state management functions
// The component state class hierarchy matches the component hierarchy
exports.Component = Component;
class ComponentState {
  constructor(options) {
    ({
      path: this.path,
      _enactment: this._enactment
    } = options || {});
    // runtime component paths may include a task cycle index
    // which is removed and stored as the design path
    const comps = this.path.split(Named.PATH_SEPARATOR);
    const mapped = comps.map(function (comp) {
      const idx = comp.indexOf("[");
      if (idx > -1) {
        return comp.slice(0, idx);
      } else {
        return comp;
      }
    });
    this.design = mapped.join(Named.PATH_SEPARATOR);
  }

  // overridden to add a class attribute and drop the circular _enactment attribute when serialised
  toJSON() {
    const result = {};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_enactment'].includes(key) && !(value == null)) {
        result[key] = value;
      }
    }
    return result;
  }

  // provide data object that combines design and state
  getComponent() {
    const result = this._enactment.protocol.getComponent(this.design).flatten();
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['_enactment'].includes(key) && !(value == null)) {
        result[key] = value;
      }
    }
    result.toJSON = this.toJSON;
    return result;
  }
  _parentStatePath() {
    const comps = this.path.split(Named.PATH_SEPARATOR);
    if (comps.length > 1) {
      return comps.slice(0, comps.length - 1).join(Named.PATH_SEPARATOR);
    } else {
      return null;
    }
  }
}

// Abstract class. An annotated component holds caption and description
exports.ComponentState = ComponentState;
class Annotated extends Component {
  constructor(options) {
    super(options);
    ({
      caption: this.caption,
      description: this.description
    } = options || {});
  }
  static _attributes() {
    return super._attributes(...arguments).concat(['caption', 'description']);
  }

  // Does the named attribute (caption/description) include references to data values?
  _isDynamic(attribute) {
    if (this[attribute] != null) {
      return this[attribute].indexOf("${") > -1;
    } else {
      return false;
    }
  }
}

// maintain state of dynamic caption and description attributes
exports.Annotated = Annotated;
class AnnotatedState extends ComponentState {
  constructor(options) {
    super(options);
    ({
      caption: this.caption,
      description: this.description
    } = options || {});
  }

  // replaces instances of ${expression} with the corresponding evaluated expression
  _getDynamic(attribute) {
    let result = this._enactment.protocol.getComponent(this.design)[attribute];
    if (result != null) {
      let app;
      const matches = [];
      // regex breakdown:
      // \$\{ matches the opening bracket
      // [^$]* matches any character that isnt a $, multiple times
      // the brackets around [^$]* capture whatever's in it
      // \} closes the expression
      const regex = /\$\{([^$]*)\}/g;
      while ((app = regex.exec(result)) != null) {
        matches.push(app);
      }
      if (matches.length > 0) {
        for (let match of matches) {
          let exp = match[1]; // contents of capture group
          let resolved = this._enactment.evaluate(exp, this.design);
          result = result.replace(match[0], resolved != null ? resolved : "");
        }
      }
    }
    return result;
  }

  // By default dynamic attributes cannot be updated
  _isUpdateable() {
    return false;
  }

  // The state engine.  Returns an array of state updates
  _updateState() {
    const result = [];
    const updateDynamic = attribute => {
      if (this._enactment.protocol.getComponent(this.design)._isDynamic(attribute) && this._isUpdateable()) {
        const newvalue = this._getDynamic(attribute);
        if (newvalue !== this[attribute]) {
          return result.push({
            path: this.path,
            attribute,
            value: newvalue
          });
        }
      }
    };
    // # Note that the next block simplifies the API (just look at state for
    // # the value of possibly dynamic attributes) but creates unnecessary
    // # duplication in the enactment so isnt used.  Its kept here for context.
    // else if @_enactment.protocol.getComponent(@path)[attribute]? and (not @[attribute]?)
    //   result.push
    //     path: @path
    //     attribute: attribute
    //     value: @_enactment.protocol.getComponent(@path)[attribute]
    updateDynamic('caption');
    updateDynamic('description');
    return result;
  }
}

// A named component's name should be unique amongst its siblings
// and can be used to construct a path to the component within the protocol
// This abstract class is the superclass for a lot of classes in the design
exports.AnnotatedState = AnnotatedState;
class Named extends Annotated {
  constructor(options) {
    super(options);
    ({
      name: this.name
    } = options || {});
    if (this.name == null) {
      throw new Error("new Named(obj): Unable to create protocol component without a name attribute");
    }
  }
  static _attributes() {
    return super._attributes(...arguments).concat(['name']);
  }

  // a component's path uniquely locates it in the protocol
  path() {
    if (this._parent != null) {
      return "".concat(this._parent.path()).concat(Named.PATH_SEPARATOR).concat(this.name);
    } else {
      return this.name;
    }
  }

  // return the subPaths of all descendent components.
  // A subPath is a path that starts at this component, irrespective of whether
  // this component is the root of the component tree
  subPaths() {
    return [this.name];
  }

  // Get descendent component as identified by it's subPath
  getComponent(subPath) {
    let names = subPath.split(Named.PATH_SEPARATOR);
    const root = names[0];
    if (root === this.name) {
      names = names.slice(1);
      let component = this;
      while (names.length > 0) {
        var child = component._child(names[0]);
        if (child != null) {
          component = component._child(names[0]);
          names = names.slice(1);
        } else {
          throw new Error("Unable to getComponent '".concat(subPath, "' from '").concat(this.path(), "'"));
        }
      }
      return component;
    } else {
      throw new Error("Unable to getComponent '".concat(subPath, "' from '").concat(this.path(), "'"));
    }
  }
  validate() {
    const result = super.validate();
    if (!/^[\w\d]+$/.test(this.name)) {
      result.push(this._createErrorValidation('name', "names can only include alpha-numeric characters and underscores"));
    }
    if (this.name.indexOf('_') === 0) {
      result.push(this._createErrorValidation('name', 'name cannot start with an underscore'));
    }
    return result;
  }
}

// placeholder that maintains the principle of least surprise
exports.Named = Named;
_defineProperty(Named, "PATH_SEPARATOR", ":");
class NamedState extends AnnotatedState {}
exports.NamedState = NamedState;
},{"core-js/modules/es.array.includes.js":153,"core-js/modules/es.array.push.js":155,"core-js/modules/es.error.cause.js":157,"core-js/modules/es.regexp.exec.js":167,"core-js/modules/es.regexp.test.js":168,"core-js/modules/es.string.includes.js":170,"core-js/modules/es.string.replace.js":171,"core-js/modules/es.string.split.js":172,"core-js/modules/es.symbol.description.js":174,"core-js/modules/esnext.iterator.constructor.js":177,"core-js/modules/esnext.iterator.filter.js":179,"core-js/modules/esnext.iterator.map.js":182,"core-js/modules/web.dom-collections.iterator.js":184}],189:[function(require,module,exports){
"use strict";

require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Enactment = void 0;
require("core-js/modules/es.error.cause.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.push.js");
require("core-js/modules/es.parse-int.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/es.string.starts-with.js");
require("core-js/modules/esnext.iterator.constructor.js");
require("core-js/modules/esnext.iterator.filter.js");
require("core-js/modules/esnext.iterator.for-each.js");
require("core-js/modules/web.dom-collections.iterator.js");
var Evaluator = _interopRequireWildcard(require("./evaluator.js"));
var Protocol = _interopRequireWildcard(require("./tasks.js"));
var _moment = _interopRequireDefault(require("moment"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// An enactment is a serialisable runtime instance of a protocol.
class Enactment {
  static inflate(json) {
    const pojo = JSON.parse(json);
    for (const act of pojo._audit) {
      act.timestamp = (0, _moment.default)(act.timestamp, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
    }
    for (const path of Object.keys(pojo._data)) {
      for (const key of Object.keys(pojo._data[path])) {
        pojo._data[path][key].forEach((val, idx) => {
          pojo._data[path][key][idx].when = (0, _moment.default)(val.when, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
        });
      }
    }
    return new Enactment({
      enactment: pojo
    });
  }

  // Options:
  // * protocol | enactment - an enactment can be created from a protocol or a serialised enactment
  // * start - a protocol can be automatically started by setting start to true
  // * validate - a protocol will be automatically validated and throw an error for invalid protocols unless this is set to false
  // * tickInterval - a tickInterval sets an automatic timeout (seconds) for cycling the state engine - defaults to undefined
  // * stackLimit - max number of cycle chains in any engine event - defaults to 1000
  // * options - an extensible set of default runtime values, keyed by task type which for proformajs tasks are:
  //   * Enquiry.useDefaults - use default values in enquiries when value unknown - defaults to false
  //   * Candidate.autoConfirmRecommended - automatically commit recommended candidates in non-autonomous decisions - defaults to false
  //   * Decision.autoConfirmMostSupported - automatically commit the candidate with the highest support in a decision - defaults to false
  constructor(options) {
    ({
      protocol: this.protocol,
      options: this.options
    } = options || {});
    this.listeners = {};
    if (options.enactment != null) {
      let data, dataMap, inflated, key, type, value;
      for (key of Object.keys(options.enactment)) {
        value = options.enactment[key];
        this[key] = value;
      }
      // 'inflate' protocol, state, data and audit
      this.protocol = Protocol.buildTask(this.protocol);
      this.tickInterval = options.enactment.tickInterval;
      this.stackLimit = options.enactment.stackLimit;
      for (key of Object.keys(this._state)) {
        value = this._state[key];
        inflated = new Protocol[value.class](value);
        inflated._enactment = this;
        this._state[key] = inflated;
      }
      for (var path in this._data) {
        data = this._data[path];
        dataMap = {}; // map of data definition types by data definition name
        for (var dataDef of this.protocol.getComponent(path).allDataDefinitions()) {
          dataMap[dataDef.name] = dataDef.toJSON().class;
        }
        for (key in data) {
          var versions = data[key];
          inflated = [];
          for (var version of versions) {
            type = dataMap[key];
            if (type != null) {
              version.value = Evaluator.dataDefinitionClassMap.get(type).inflate(version.value);
              inflated.push(version);
            } else {
              console.log('inflate data: missing key from datamap', key, dataMap);
            }
          }
          this._data[path][key] = inflated;
        }
      }
      for (var event of this._audit) {
        if (event.action.data != null) {
          for (key in event.action.data) {
            value = event.action.data[key];
            type = dataMap[key];
            if (type != null) {
              event.action.data[key] = Evaluator.dataDefinitionClassMap.get(type).inflate(value);
            } else {
              console.log('inflate audit data: missing key from datamap', key, dataMap);
            }
          }
        }
      }
      if (this.tickInterval != null) {
        // add function to automatically cycle enactment that can be passed to setTimeout
        this._tick = () => this._cycle({
          action: 'tick'
        });
        if (this._state[this.protocol.name].state === 'in_progress') {
          this.tick = setTimeout(this._tick, this.tickInterval * 1000);
        }
      }
    } else {
      // TODO allow uri to be passed
      if (this.protocol == null) {
        throw new Error('Missing Protocol - cannot create enactment');
      }
      const validate = options.validate || true;
      if (validate && !this.protocol.isValid()) {
        throw new Error('Invalid Protocol - cannot create enactment');
      }
      this._state = {};
      this._data = {};
      this._audit = [];
      this._triggers = {};
      if (this.protocol.cyclic) {
        this.protocol._buildState(this, 0);
      } else {
        this.protocol._buildState(this);
      }
      this.tickInterval = options.tickInterval || undefined; // seconds, see also @lastTicked()
      if (this.tickInterval != null) {
        // add function to automatically cycle enactment that can be passed to setTimeout
        this._tick = () => this._cycle({
          action: 'tick'
        });
      }
    }
    if (options.start || false) {
      this.start();
    }
    this.stackLimit = options.stackLimit || 1000;
  }

  // start enactment
  // errors if enactment is already started
  start() {
    if (!this.startedAt()) {
      const timestamp = (0, _moment.default)();
      this._cycle({
        timestamp,
        action: 'start'
      });
      if (this.listeners["started"] != null) {
        for (var listener of this.listeners["started"]) {
          var evt = {
            event: 'started',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
      return this;
    } else {
      throw new Error('Cannot start an enactment that has already started');
    }
  }

  // get data object that merges state and design of component by path
  // setting the second 'shallow' parameter to true will ignore sub-components
  getComponent(path, shallow) {
    if (shallow == null) {
      shallow = false;
    }
    if (this._state[path] != null) {
      return this._state[path].getComponent(shallow);
    } else {
      // inert data Definitions dont have a state, but still ought to be retrievable
      const result = this.protocol.getComponent(path).flatten();
      result.path = path;
      return result;
    }
  }

  // Cyclic tasks mean runtime paths may be different from designtime paths.
  // Returns the path of the "latest" task associated with the designPath or echos back the designPath
  runtimeFromDesignPath(designPath) {
    // extracts indexes from a path
    const indexes = function indexes(path) {
      const vals = [];
      for (var comp of path.split(Evaluator.PATH_SEPARATOR)) {
        if (comp.indexOf("[") > 0) {
          vals.push(parseInt(comp.substr(comp.lastIndexOf('[') + 1, comp.lastIndexOf(']') - comp.lastIndexOf('[') - 1)));
        }
      }
      return vals;
    };
    // compares two arrays of indexes, returns true if test is newer than existing
    const better = function better(test, existing) {
      const test_indexes = indexes(test);
      const existing_indexes = indexes(existing);
      for (let idx = 0; idx < test_indexes.length; idx++) {
        var val = test_indexes[idx];
        if (val > existing_indexes[idx]) {
          return true;
        }
      }
      return false;
    };
    let result = null;
    for (var key in this._state) {
      var val = this._state[key];
      if (val.design === designPath) {
        if (result == null) {
          result = key;
        } else {
          if (better(key, result)) {
            result = key;
          }
        }
      }
    }
    // NB if the designPath was a sibling component name the above algorithm wont have worked
    if (result === null) {
      return designPath;
    } else {
      return result;
    }
  }

  // return all task components
  getTasks() {
    const result = [];
    for (var path of Object.keys(this._state)) {
      var task = this._state[path];
      if (task.state != null) {
        result.push(this._state[path].getComponent());
      }
    }
    return result;
  }

  // return all data definitions referred to in this enactment and their current value
  getDataDefinitions() {
    const result = [];
    const indexes = [];
    for (var path of Object.keys(this._state)) {
      var task = this._state[path];
      if (task.state != null) {
        for (var dataDef of this.protocol.getComponent(task.design).dataDefinitions) {
          var comp = dataDef.flatten();
          comp.path = task.design + Evaluator.PATH_SEPARATOR + comp.name;
          if (!indexes.includes(comp.path)) {
            var values = this._data[task.design] != null ? this._data[task.design][comp.name] : undefined;
            if (values != null) {
              comp.value = values[values.length - 1].value;
            }
            result.push(comp);
            indexes.push(comp.path);
          }
        }
      }
    }
    return result;
  }

  // returns object with at least one of the following attributes
  //   started: boolean
  //   finished: boolean
  //   warnings: array ot compoent paths for active warnings
  //   completeable: array of completeable task paths
  //   cancellabel: array of cancellable task paths
  //   requested: object whose keys are task paths with requested data
  //   confirmable: object whose keys are task paths with confirmable candidates
  // TODO: make this extensible so that state is looped through only once
  getStatus() {
    const result = {
      started: this.startedAt() != null
    };
    if (result.started) {
      let keys;
      if (this.protocol.cyclic) {
        keys = Object.keys(this._state).filter(key => key.indexOf(Evaluator.PATH_SEPARATOR) === -1);
        result.finished = ['completed', 'discarded'].includes(this._state[keys.pop()].state);
      } else {
        result.finished = ['completed', 'discarded'].includes(this._state[this.protocol.name].state);
      }
      if (!result.finished) {
        result.completeable = [];
        result.cancellable = [];
        result.warnings = [];
        result.requested = {};
        result.confirmable = {};
        result.triggers = [];
        for (var key of Object.keys(this._state)) {
          var value = this._state[key];
          if (value.state != null) {
            if (value.cancellable != null && value.cancellable) {
              result.cancellable.push(key);
            }
            if (value.state === 'in_progress') {
              if (value.completeable != null && value.completeable) {
                result.completeable.push(key);
              }
              if (value.requested != null) {
                result.requested[key] = value.requested();
              } else if (value.recommended != null) {
                var recommended = this._state[key].recommended();
                var confirmed = this._state[key].confirmed();
                for (var candidate of this.protocol.getComponent(this._state[key].design).candidates) {
                  result.confirmable[candidate.path()] = {
                    isRecommended: recommended.includes(candidate.name),
                    isConfirmed: confirmed.includes(candidate.name)
                  };
                }
              }
            }
            // check for available trigger
            var eventTrigger = value.trigger();
            if (eventTrigger != null && !result.triggers.includes(eventTrigger)) {
              result.triggers.push(eventTrigger);
            }
          }
          // TODO: smells bad? - replace use of instanceof here
          if (value instanceof Protocol.WarningState && value.active != null && value.active) {
            result.warnings.push(key);
          }
        }
      }
    }
    return result;
  }

  // complete task by path, errors if task not completeable
  complete(path) {
    if (this._state[path] != null) {
      this._state[path].complete();
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // cancel task by path. erros if task not cancellable.
  cancel(path) {
    if (this._state[path] != null) {
      if (this._state[path] != null) {
        this._state[path].cancel();
      }
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // retrieve current data view at a given task path
  get(path) {
    let key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    const dataDefs = this.protocol.getComponent(this._state[path].design).allDataDefinitions();
    if (key != null) {
      return this._state[path].get(key);
    } else {
      const result = {};
      for (var dataDef of dataDefs) {
        result[dataDef.name] = this._state[path].get(dataDef.name);
      }
      return result;
    }
  }

  // provide data (as object whose keys must be valid data definitions) to the engine
  set(path, data) {
    if (this._state[path] != null) {
      this._state[path].set(data);
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // clear data value from engine state
  unset(path, type) {
    if (this._state[path] != null) {
      this._state[path].unset(type);
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }
  edit(path, index, value) {
    let arr = path.split(Evaluator.PATH_SEPARATOR);
    let type = arr.pop();
    let taskpath = arr.join(Evaluator.PATH_SEPARATOR);
    if (this._state[taskpath] != null) {
      this._state[taskpath].edit(type, index, value);
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // get current view of all *known* data (or external state)
  // Note that *known* data is different to *all* possible data as it will not
  // contain data for dataDefinitions that have yet to be encountered during enactment.
  getData() {
    const result = {};
    for (var path of Object.keys(this._data)) {
      var data = this._data[path];
      result[path] = {};
      for (var key of Object.keys(data)) {
        result[path][key] = this._state[this.runtimeFromDesignPath(path)].get(key);
      }
    }
    return result;
  }

  // set current data in bulk, e.g. as returned from getData()
  setData(indexed) {
    const result = [];
    for (var path of Object.keys(indexed)) {
      var data = indexed[path];
      result.push(this.set(path, data));
    }
    return result;
  }

  // confirm decision candidate
  confirm(path) {
    if (this._state[path] != null) {
      this._state[path].confirm();
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // unconfirm decision candidate
  unconfirm(path) {
    if (this._state[path] != null) {
      this._state[path].unconfirm();
      return this;
    } else {
      throw new Error("unknown component '".concat(path, "'"));
    }
  }

  // evaluate an expression - a way of probing state
  // if path is omitted then the the root path will be used
  evaluate(exp, path) {
    const evaluator = Evaluator.buildEvaluator(this);
    return evaluator.evaluate(exp, path);
  }

  // return timestamp (javascript Date) of last cycle or null if not started
  lastCycled() {
    if (this._audit.length > 0) {
      return this._audit[this._audit.length - 1].timestamp;
    } else {
      return null;
    }
  }

  // return timestamp (javascript Date) when started or null if not started
  startedAt() {
    if (this._audit.length > 0) {
      return this._audit[0].timestamp;
    } else {
      return null;
    }
  }
  sendTrigger(trigger) {
    const triggers = Object.keys(this._triggers);
    if (triggers.includes(trigger)) {
      const update = [];
      for (var path of this._triggers[trigger]) {
        if (this._state[path].state === 'dormant') {
          var comp = this.getComponent(path, true);
          var preCondition = comp.preCondition == null || comp.preCondition === '' || this.evaluate(comp.preCondition, path);
          update.push({
            path,
            attribute: 'state',
            value: preCondition ? 'in_progress' : 'discarded'
          });
        }
      }
      if (update.length > 0) {
        this._cycle({
          action: {
            trigger
          },
          seed: update
        });
      }
      return this;
    } else {
      throw new Error("unknown trigger ('".concat(trigger, "'). Expect one of ").concat(triggers));
    }
  }

  // this override needed for a ticking enactment
  toJSON() {
    const result = {};
    for (var key of Object.keys(this)) {
      var value = this[key];
      if (!['tick'].includes(key) && !(value == null)) {
        result[key] = value;
      }
    }
    return result;
  }

  // add event listener
  // possible keys: "started", "finished", "cycled"
  // a listener is a function with one event parameter
  on(key, listener) {
    if (this.listeners[key] == null) {
      this.listeners[key] = [];
    }
    return this.listeners[key].push(listener);
  }

  // used to decide whether or not continue ticking and to send finished event
  _isFinished() {
    let protocolRootPath = this.protocol.name;
    if (this.protocol.cyclic) {
      // find the highest cycle
      const keys = Object.keys(this._state).filter(key => key.startsWith(protocolRootPath) && key.slice(protocolRootPath.length).indexOf(Evaluator.PATH_SEPARATOR) === -1);
      protocolRootPath = keys.pop();
    }
    return ['completed', 'discarded'].includes(this._state[protocolRootPath].state);
  }

  // To cycle the engine, every component is checked for updates that
  // can be applied to the state.  The updates are collected together and then
  // applied en masse. (they are order independent)
  // This is repeated until there are no more updates to be made.
  // Options:
  // * action - should be provided at the start of a chain of cycles for audit purposes
  // * seed - updates to kick off a chain of cycles, e.g. to complete a task
  // * timestamp - a timestamp indicates that this cycle is part of a chain of cycles and should be audited under the same timestamp
  _cycle(options) {
    // clear tick timeout if set (we'll apply it again once this set of cycles is complete)
    let name, value;
    if (options == null) {
      options = {};
    }
    if (this.tick != null) {
      clearTimeout(this.tick);
    }
    const timestamp = options.timestamp || (0, _moment.default)();
    // an evaluator is built for every engine cycle to ensure that the starting state is properly captured/cached
    const evaluator = Evaluator.buildEvaluator(this, timestamp);
    let result = options.seed || [];
    for (var key of Object.keys(this._state)) {
      value = this._state[key];
      if (value._updateState != null) {
        result = result.concat(value._updateState(evaluator));
      } else {
        throw new Error("mis-configured runtime component: ".concat(value));
      }
    }
    if (result.length > 0) {
      if (this._audit.length > 0 && options.timestamp != null) {
        this._audit[this._audit.length - 1].deltas.push(result);
        if (this._audit[this._audit.length - 1].deltas.length > this.stackLimit) {
          throw new Error("stack size exception - delta's array is larger than stackLimit (".concat(this.stackLimit, ")"));
        }
      } else {
        this._audit.push({
          action: options.action,
          timestamp,
          deltas: [result]
        });
      }
      for (var delta of result) {
        this._state[delta.path][delta.attribute] = delta.value;
        if (delta.data != null) {
          var parts = delta.path.split(Evaluator.PATH_SEPARATOR);
          name = parts.pop();
          var parent = parts.join(Evaluator.PATH_SEPARATOR);
          if (this._data[parent] == null) {
            this._data[parent] = {};
          }
          if (this._data[parent][name] == null) {
            this._data[parent][name] = [];
          }
          this._data[parent][name].push({
            value: delta.value,
            when: timestamp
          });
        }
      }
      return this._cycle({
        timestamp
      }); // iterate until stabilised
    } else {
      // this set of cycles is finished.  But there are some checks to do before we finish up.
      let evt, listener;
      if (options.timestamp == null && options.action !== 'tick') {
        // data changes may have no effect on state, but should still be auditted
        this._audit.push({
          action: options.action,
          timestamp,
          deltas: []
        });
      }
      if (this.tickInterval != null && !this._isFinished()) {
        // set tick if there are still things to do
        if (this._state[this.runtimeFromDesignPath(this.protocol.name)].state === 'in_progress') {
          this.tick = setTimeout(this._tick, this.tickInterval * 1000);
        }
      }
      // raise events if there are listeners
      if (this.listeners["cycled"] != null) {
        for (listener of this.listeners["cycled"]) {
          evt = {
            event: 'cycled',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
      if (this.listeners["finished"] != null && this._isFinished()) {
        for (listener of this.listeners["finished"]) {
          evt = {
            event: 'finished',
            timestamp,
            enactment: this
          };
          listener(evt);
        }
      }
    }
  }
}
exports.Enactment = Enactment;
},{"./evaluator.js":190,"./tasks.js":192,"core-js/modules/es.array.includes.js":153,"core-js/modules/es.array.push.js":155,"core-js/modules/es.error.cause.js":157,"core-js/modules/es.parse-int.js":166,"core-js/modules/es.regexp.exec.js":167,"core-js/modules/es.string.includes.js":170,"core-js/modules/es.string.split.js":172,"core-js/modules/es.string.starts-with.js":173,"core-js/modules/es.weak-map.js":176,"core-js/modules/esnext.iterator.constructor.js":177,"core-js/modules/esnext.iterator.filter.js":179,"core-js/modules/esnext.iterator.for-each.js":181,"core-js/modules/web.dom-collections.iterator.js":184,"moment":187}],190:[function(require,module,exports){
"use strict";

require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateExpression = exports.reservedWords = exports.dataDefinitionClassMap = exports.buildEvaluator = exports.WarningState = exports.Warning = exports.Text = exports.PATH_SEPARATOR = exports.NumberDataDefinitionState = exports.Integer = exports.Float = exports.Date = exports.DataDefinitionState = exports.DataDefinition = exports.Boolean = void 0;
require("core-js/modules/es.error.cause.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.push.js");
require("core-js/modules/es.array.reduce.js");
require("core-js/modules/es.parse-int.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/es.string.starts-with.js");
require("core-js/modules/esnext.iterator.constructor.js");
require("core-js/modules/esnext.iterator.every.js");
require("core-js/modules/esnext.iterator.filter.js");
require("core-js/modules/esnext.iterator.map.js");
require("core-js/modules/esnext.iterator.reduce.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _jseEval = require("jse-eval");
var _moment = _interopRequireDefault(require("moment"));
var Core = _interopRequireWildcard(require("./core.js"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This evaluator allows javascript expressions to interrogate the state of
// an enactment at runtime, via a set of predicates defined in the
// EvaluatorContext class

// The interface of this evaluator consists of two methods,
// * validateExpression(expression, path, expression_name, local_identifiers)
// * buildEvaluator(enactment)
// a static array
// * reservedWords
// and a set of data definition classes that inherit from Core.Named and
// implement an accept(value) method

const PATH_SEPARATOR = exports.PATH_SEPARATOR = Core.Named.PATH_SEPARATOR;

// At design time expressions can be validated to a certain extent by checking
// that the expression is valid javascript and that every non-reserved token is
// recognised within the protocol.
const validateExpression = exports.validateExpression = function validateExpression(expression, path, expression_name, local_identifiers) {
  var _findIdentifiers = function findIdentifiers(obj, result) {
    if (result == null) {
      result = [];
    }
    if (obj.type != null && obj.type === 'Identifier') {
      result.push(obj.name);
    } else if (obj.type != null && obj.type === 'Literal' && typeof obj.value === 'string') {
      result.push(obj.value);
    } else {
      for (var key of Object.keys(obj)) {
        var value = obj[key];
        if (typeof obj === 'object') {
          result = _findIdentifiers(value, result);
        }
      }
    }
    return result;
  };
  const context = local_identifiers.concat(reservedWords);
  try {
    const ast = (0, _jseEval.parse)(expression);
    const expression_identifiers = _findIdentifiers(ast);
    const result = [];
    for (var identifier of expression_identifiers) {
      if (!context.includes(identifier)) {
        result.push({
          type: 'Error',
          attribute: expression_name,
          path,
          msg: "Unknown component/predicate: ".concat(identifier)
        });
      }
    }
    return result;
  } catch (error) {
    return [{
      type: 'Error',
      path,
      msg: "parse Error: ".concat(error),
      attribute: expression_name
    }];
  }
};
class EvaluatorContext {
  constructor(enactment, timestamp) {
    this._enactment = enactment;
    this._timestamp = timestamp;
    // add current data
    for (const path of Object.keys(enactment._data)) {
      for (const key of Object.keys(enactment._data[path])) {
        var arr = enactment._data[path][key];
        if (Array.isArray(arr) && arr[arr.length - 1].value != null) {
          this[key] = arr[arr.length - 1].value;
        }
      }
    }
    // add tasks
    for (const key of Object.keys(enactment._state)) {
      this[key] = enactment._state[key];
    }
  }
  _evaluate(exp, calling_path) {
    this._path = calling_path;
    const defs = calling_path != null ? this._enactment.protocol.getComponent(calling_path).allDataDefinitions().map(def => def.name) : this._enactment.protocol.allDataDefinitions().map(def => def.name);
    if ((exp != null ? exp.length : undefined) > 0) {
      try {
        const ast = (0, _jseEval.parse)(exp);
        if (Object.keys(ast).length == 2 && ast.type == "Identifier" && ast.name && !defs.includes(ast.name)) {
          throw new Error("illegal Identifier");
        }
        if (ast.type == "CallExpression" && ast.callee && ast.callee.type == "Identifier" && ast.callee.name.startsWith("_")) {
          throw new Error("illegal Identifier");
        }
        return (0, _jseEval.evaluate)(ast, this);
      } catch (e) {
        e.message = "_evaluate failed for \"".concat(exp, "\" from ").concat(calling_path ? calling_path : this._enactment.protocol.name, " (").concat(e.message, ")");
        throw e;
      }
    }
  }

  // returns time (as moment) that task identified by path reached a particular state using the audit
  _getTime(runtimepath, state) {
    for (var event of this._enactment._audit) {
      for (var cycle of event.deltas) {
        for (var update of cycle) {
          if (update.path === runtimepath && update.value === state) {
            return (0, _moment.default)(event.timestamp);
          }
        }
      }
    }
  }

  // look for sibling as well as full path
  _getComponent(runtimepath) {
    if (this[runtimepath] != null) {
      return this[runtimepath];
    } else {
      // assume that runtimepath is in fact the name of a sibling component
      const paths = this._path.split(PATH_SEPARATOR);
      paths.splice(paths.length - 1, 1, runtimepath);
      const siblingpath = this._enactment.runtimeFromDesignPath(paths.join(PATH_SEPARATOR));
      if (this[siblingpath] != null) {
        return this[siblingpath];
      } else {
        // finally check for child path
        if (paths.length > 0) {
          const childpath = this._enactment.runtimeFromDesignPath(this._path + PATH_SEPARATOR + runtimepath);
          if (this[childpath] != null) {
            return this[childpath];
          }
        }
      }
    }
  }
  _testTask(taskpath, test) {
    const task = this._getComponent(this._enactment.runtimeFromDesignPath(taskpath));
    if (task != null) {
      return test(task);
    } else {
      throw new Error("unknown task: ".concat(taskpath));
    }
  }
  now() {
    return this._timestamp || (0, _moment.default)();
  }
  is_dormant(taskpath) {
    return this._testTask(taskpath, task => task.state === 'dormant');
  }
  is_in_progress(taskpath) {
    return this._testTask(taskpath, task => task.state === 'in_progress');
  }
  is_completed(taskpath) {
    return this._testTask(taskpath, task => task.state === 'completed');
  }
  is_discarded(taskpath) {
    return this._testTask(taskpath, task => task.state === 'discarded');
  }
  is_finished(taskpath) {
    return this._testTask(taskpath, task => ['discarded', 'completed'].includes(task.state));
  }
  in_progress_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(runtimepath, 'in_progress');
    } else {
      throw new Error("unknown task: ".concat(taskpath));
    }
  }
  completed_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(this._enactment.runtimeFromDesignPath(taskpath), 'completed');
    } else {
      throw new Error("unknown task: ".concat(taskpath));
    }
  }
  discarded_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      return this._getTime(this._enactment.runtimeFromDesignPath(taskpath), 'discarded');
    } else {
      throw new Error("unknown task: ".concat(taskpath));
    }
  }
  finished_time(taskpath) {
    const runtimepath = this._enactment.runtimeFromDesignPath(taskpath);
    const task = this._getComponent(runtimepath);
    if (task != null) {
      if (task.state === 'completed') {
        return this._getTime(runtimepath, 'completed');
      } else {
        return this._getTime(runtimepath, 'discarded');
      }
    } else {
      throw new Error("unknown task: ".concat(taskpath));
    }
  }
  is_valid(dataDef) {
    const comp = this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter(def => def.name === dataDef)[0];
    if (comp != null) {
      for (var warning of comp.warnings) {
        if (this[warning.path()].active) {
          return false;
        }
      }
      return true;
    } else {
      throw new Error("unknown data definition: ".concat(dataDef));
    }
  }
  is_known(dataDef) {
    const comp = this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter(def => def.name === dataDef)[0];
    if (comp != null) {
      return this[dataDef] != null;
    } else {
      throw new Error("unknown data definition: ".concat(dataDef));
    }
  }
  caption(dataDef) {
    const getRangeItem = (value, range) => range.filter(item => item.value === value)[0];
    const comp = this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter(def => def.name === dataDef)[0];
    if (comp != null) {
      if (comp.range != null && this[dataDef] != null) {
        if (comp.multiValued) {
          const rangeItems = this[dataDef].map(value => getRangeItem(value, comp.range));
          if (rangeItems.length > 0 && rangeItems[0] != null) {
            return rangeItems.map(item => item.caption);
          } else {
            return this[dataDef];
          }
        } else {
          const rangeItem = getRangeItem(this[dataDef], comp.range);
          return (rangeItem != null ? rangeItem.caption : undefined) || this[dataDef];
        }
      } else {
        return this[dataDef];
      }
    } else {
      throw new Error("unknown data definition: ".concat(dataDef));
    }
  }
  captions(dataDef) {
    const arr = this.caption(dataDef);
    return arr.slice(0, +-2 + 1 || undefined).join(", ") + " and " + arr.slice(-1);
  }
  net_support(candidatepath) {
    let candidate = this._getComponent(this._enactment.runtimeFromDesignPath(candidatepath));
    if (candidate == null) {
      // following checks only work if path is a single name, i.e. the candidate
      if (candidatepath.split(PATH_SEPARATOR).length === 1) {
        const caller = this[this._enactment.runtimeFromDesignPath(this._path)];
        if (caller._getCandidateFromName != null) {
          candidate = caller._getCandidateFromName(candidatepath);
        }
      }
    }
    if (candidate != null) {
      return candidate.support || 0;
    } else {
      throw new Error("unknown candidate: ".concat(candidatepath));
    }
  }
  result_set(decisionpath) {
    const comp = this._getComponent(this._enactment.runtimeFromDesignPath(decisionpath));
    return comp ? comp.confirmed ? comp.confirmed() : [] : null;
  }
  result_of(decisionpath) {
    const comp = this._getComponent(this._enactment.runtimeFromDesignPath(decisionpath));
    const confirmed = comp ? comp.confirmed ? comp.confirmed() : null : null;
    if ((confirmed != null ? confirmed.length : undefined) === 0) {
      return undefined;
    } else if ((confirmed != null ? confirmed.length : undefined) === 1) {
      return confirmed[0];
    } else {
      return confirmed;
    }
  }
  includes(arr, value) {
    return arr != null && arr.includes(value);
  }
  last_updated(dataDef) {
    const comp = this._enactment.protocol.getComponent(this._path).allDataDefinitions().filter(def => def.name === dataDef)[0];
    if (comp != null) {
      for (var path of Object.keys(this._enactment._data)) {
        var vals = this._enactment._data[path];
        for (var key of Object.keys(vals)) {
          var arr = vals[key];
          if (key === dataDef) {
            return (0, _moment.default)(arr[arr.length - 1].when);
          }
        }
      }
      return undefined;
    } else {
      throw new Error("unknown data definition: ".concat(dataDef));
    }
  }
  num_finished(paths) {
    return paths.filter(path => this.is_finished(path)).length;
  }
  num_completed(paths) {
    return paths.filter(path => this.is_completed(path)).length;
  }
  date_part_hour(datetime) {
    return this[datetime] != null ? this[datetime].hour() : undefined;
  }
  round(float) {
    return Math.round(float);
  }
  abs(float) {
    return Math.abs(float);
  }
  random() {
    return Math.random();
  }
  sin(float) {
    return Math.sin(float);
  }
  cos(float) {
    return Math.cos(float);
  }
  tan(float) {
    return Math.tan(float);
  }
  asin(float) {
    return Math.asin(float);
  }
  acos(float) {
    return Math.acos(float);
  }
  atan(float) {
    return Math.atan(float);
  }
  count(arr) {
    return arr != null ? arr.length : undefined;
  }
  sum(arr) {
    return arr.reduce((cum, val) => cum + val);
  }
  min(arr) {
    return arr.reduce(function (cum, val) {
      if (cum != null && val < cum) {
        return val;
      } else {
        return cum;
      }
    });
  }
  max(arr) {
    return arr.reduce(function (cum, val) {
      if (cum != null && val > cum) {
        return val;
      } else {
        return cum;
      }
    });
  }
  nth(arr, idx) {
    return arr[idx];
  }
  exp(float) {
    return Math.exp(float);
  }
  log(float) {
    return Math.log(float);
  }
  // TODO: union / intersect / diff
  // returns index of current cycle of indicated task designPath or -1 if not cyclic
  index(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath || this._path);
    if (runtimePath != null) {
      if (this._enactment.getComponent(runtimePath).state != null) {
        return this._enactment.getComponent(runtimePath).index;
      } else {
        return this._enactment.getComponent(runtimePath).index - 1;
      }
    } else {
      return -1;
    }
  }
  // returns path of previous runtime cycle of indicated designPath
  last_finished(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath);
    if (runtimePath.lastIndexOf(']') === runtimePath.length - 1) {
      // get current index number from path
      const currentIndex = parseInt(runtimePath.substr(runtimePath.lastIndexOf('[') + 1, runtimePath.lastIndexOf(']') - runtimePath.lastIndexOf('[') - 1));
      return runtimePath.substr(0, runtimePath.lastIndexOf('[') + 1) + (currentIndex - 1) + ']';
    } else {
      return designPath;
    }
  }
  // returns the runtime path of the calling component (if the designPath is ommitted) or the passed designpath
  path(designPath) {
    const runtimePath = this._enactment.runtimeFromDesignPath(designPath || this._path);
    const comp = this._getComponent(runtimePath);
    if (comp != null) {
      return runtimePath;
    } else {
      throw new Error("unknown component: ".concat(designPath));
    }
  }
  join(arr1, arr2, arr3) {
    // concaternate arr1 and arr2 and remove contents of arr3 from final array
    const result = (arr1 || []).concat(arr2 || []);
    if (arr3 != null) {
      return result.filter(item => !arr3.includes(item));
    } else {
      return result;
    }
  }
}

// A fairly raw evaluation strategy that seems good enough to at least get up and running
class JavascriptEvaluator {
  constructor(enactment, timestamp) {
    this.context = new EvaluatorContext(enactment, timestamp);
  }

  // evaluate an expression from the context of an (optional) designPath
  evaluate(exp, calling_path) {
    return this.context._evaluate(exp, calling_path);
  }
}
const buildEvaluator = (enactment, timestamp) => new JavascriptEvaluator(enactment, timestamp);
exports.buildEvaluator = buildEvaluator;
const reservedWords = exports.reservedWords = Object.getOwnPropertyNames(EvaluatorContext.prototype).filter(key => key.indexOf("_") !== 0).concat(['diff', 'add', 'subtract', 'undefined', 'years', 'months', 'weeks', 'days', 'hours', 'seconds', 'milliseconds']);

// Warnings provide a mechanism to range check data
class Warning extends Core.Annotated {
  constructor(options) {
    super(options);
    ({
      idx: this.idx,
      warnCondition: this.warnCondition
    } = options || {});
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['idx', 'warnCondition']);
  }
  path() {
    return "".concat(this._parent.path()).concat(PATH_SEPARATOR).concat(this.idx);
  }
  subPaths() {
    return [this.idx.toString()];
  }
  validate(identifiers) {
    if (identifiers == null) {
      identifiers = [];
    }
    let result = super.validate();
    if (this.warnCondition == null || this.warnCondition === '') {
      result.push({
        type: 'Error',
        attribute: 'warnCondition',
        msg: "missing or empty"
      });
    } else {
      result = result.concat(validateExpression(this.warnCondition, this.path(), 'warnCondition', identifiers));
    }
    return result;
  }
  allDataDefinitions() {
    return this._parent.allDataDefinitions();
  }
  toJSON() {
    const result = super.toJSON();
    delete result.idx;
    return result;
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.idx;
    return enactment._state[statePath] = new WarningState({
      path: statePath,
      _enactment: enactment
    });
  }
}
exports.Warning = Warning;
class WarningState extends Core.AnnotatedState {
  constructor(options) {
    super(options);
    ({
      active: this.active
    } = options || {});
  }
  _updateState(evaluator) {
    const result = [];
    const comp = this._enactment.protocol.getComponent(this.design);
    const warnCondition = comp ? comp.warnCondition : null;
    if (warnCondition != null) {
      const active = evaluator.evaluate(warnCondition, this.design);
      if (active !== this.active) {
        result.push({
          path: this.path,
          attribute: 'active',
          value: active
        });
      }
    }
    return result;
  }
}

// A DataDefinition is a placeholder for external state as understood by the protocol.
// Use the accept method to constrain the possible values to be held within sub-classes.
// @defaultCondition - a combination of defaultValue and valueCondition - a default value expression
exports.WarningState = WarningState;
class DataDefinition extends Core.Named {
  constructor(options) {
    super(options);
    ({
      range: this.range,
      valueCondition: this.valueCondition,
      defaultCondition: this.defaultCondition
    } = options || {});
    if (options.defaultValue !== undefined) {
      try {
        this.defaultValue = this.constructor.inflate(options.defaultValue);
      } catch (e) {
        // get the raw value and use protocol validation to signal the problem
        this.defaultValue = options.defaultValue;
      }
    } else {
      this.defaultValue = undefined;
    }
    this.multiValued = options.multiValued || false;
    this.warnings = [];
    if (options.warnings != null && Array.isArray(options.warnings)) {
      for (var meta of options.warnings) {
        var warning = new Warning(meta);
        this.addWarning(warning);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['range', 'multiValued', 'valueCondition', 'defaultValue', 'warnings', 'defaultCondition']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['warnings']);
  }

  // override this when you need to do class specific deserialisation
  static inflate(val) {
    return val;
  }

  // override this when you want to control how a value is displayed
  static render(val) {
    return val.toString();
  }
  _child(idx) {
    return this.warnings[idx];
  }
  subPaths() {
    return super.subPaths(...arguments).concat(this.warnings.map(warning => this.name + PATH_SEPARATOR + warning.idx));
  }
  addWarning(warning) {
    warning._parent = this;
    warning.idx = this.warnings.length;
    return this.warnings.push(warning);
  }

  // test to see if this data definition will accept this value
  accept(value) {
    if (this.multiValued) {
      if (Array.isArray(value)) {
        return value.every(item => this._accept(item));
      } else {
        return false;
      }
    } else {
      return this._accept(value);
    }
  }

  // check atomic value
  _accept(value) {
    if (this.range != null) {
      // check that value is in range
      const values = (this.range[0] != null ? this.range[0].value : undefined) != null ? this.range.map(element => element.value) : this.range;
      return values.includes(value);
    } else {
      return true; // default response
    }
  }
  validate(identifiers) {
    let element;
    if (identifiers == null) {
      identifiers = [];
    }
    let result = super.validate();
    if (this.range != null) {
      for (element of this.range) {
        if (element.value != null) {
          if (!this._accept(element.value)) {
            result.push(this._createErrorValidation('range', "Incompatible range value: ".concat(element.value)));
          }
        } else {
          if (!this._accept(element)) {
            result.push(this._createErrorValidation('range', "Incompatible range value: ".concat(element)));
          }
        }
      }
    }
    // though this next check looks better placed in Named, exports.reservedWords is not available to core
    if (reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    if (this.defaultValue != null) {
      let value, values;
      if (this.multiValued) {
        // defaultValue should be array
        if (!Array.isArray(this.defaultValue)) {
          result.push(this._createErrorValidation('defaultValue', "Expected array for multiValued dataDefinition"));
        } else {
          for (value of this.defaultValue) {
            if (!this._accept(value)) {
              result.push(this._createErrorValidation('defaultValue', "Incompatible defaultValue: ".concat(value)));
            }
            if (this.range != null) {
              values = this.range.length > 0 ? Object.keys(this.range[0]).includes('value') ? this.range.map(item => item.value) : this.range : null;
              if (!values.includes(value)) {
                result.push(this._createErrorValidation('defaultValue', "defaultValue not in range: ".concat(value)));
              }
            }
          }
        }
      } else {
        if (!this.accept(this.defaultValue)) {
          result.push(this._createErrorValidation('defaultValue', "Incompatible defaultValue: ".concat(this.defaultValue)));
        }
        if (this.range != null) {
          values = this.range.length > 0 ? Object.keys(this.range[0]).includes('value') ? this.range.map(item => item.value) : this.range : null;
          if (!values.includes(this.defaultValue)) {
            result.push(this._createErrorValidation('defaultValue', "defaultValue not in range: ".concat(this.defaultValue)));
          }
        }
      }
    }
    if (this.valueCondition === '') {
      result.push(this._createWarningValidation('valueCondition', "empty expression"));
    }
    if (this.valueCondition != null) {
      result = result.concat(validateExpression(this.valueCondition, this.path(), 'valueCondition', identifiers));
    }
    if (this.defaultCondition === '') {
      result.push(this._createWarningValidation('defaultCondition', "empty expression"));
    }
    if (this.defaultCondition != null) {
      result = result.concat(validateExpression(this.defaultCondition, this.path(), 'defaultCondition', identifiers));
    }
    if (this.warnings != null) {
      for (let idx = 0; idx < this.warnings.length; idx++) {
        var warning = this.warnings[idx];
        result = result.concat(warning.validate(identifiers));
      }
    }
    return result;
  }
  isDerived() {
    return this.valueCondition != null;
  }
  allDataDefinitions() {
    return this._parent.allDataDefinitions();
  }
  toJSON() {
    const result = super.toJSON();
    // remove default values set in the constructor
    if (result.multiValued === false) {
      delete result.multiValued;
    }
    if (result.warnings.length === 0) {
      delete result.warnings;
    }
    return result;
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new DataDefinitionState({
        path: statePath,
        _enactment: enactment
      });
    }
    if (this.warnings != null) {
      return this.warnings.map(warning => warning._buildState(enactment, statePath));
    }
  }
}

// dataDefinitions with a valueCondition attribute need a state object
exports.DataDefinition = DataDefinition;
class DataDefinitionState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({
      value: this.value
    } = options || {});
  }
  getComponent(shallow) {
    if (shallow == null) {
      shallow = false;
    }
    const design = this._enactment.protocol.getComponent(this.path);
    const result = super.getComponent();
    if (!shallow) {
      result.warnings = [];
      for (let idx = 0; idx < design.warnings.length; idx++) {
        var stateObj = this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + idx];
        if (stateObj != null) {
          result.warnings.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  // returns true if values are different
  _differentValues(left, right) {
    if (Array.isArray(left) && Array.isArray(right)) {
      return left.every((val, idx) => this._differentValues(val, right[idx]));
    } else {
      // note that undefined !== 4 evaluates to true (!)
      // but typeof undefined == 'undefined'
      // so we can test for differences of typeof
      return typeof left !== typeof right || left !== right;
    }
  }
  _updateState(evaluator) {
    const result = super._updateState();
    const designPath = this._enactment._state[this.path].design;
    const {
      valueCondition
    } = this._enactment.protocol.getComponent(designPath);
    if (valueCondition != null) {
      const value = evaluator.evaluate(valueCondition, designPath);
      if (this._differentValues(value, this.value)) {
        result.push({
          path: this.path,
          data: true,
          attribute: 'value',
          value
        });
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "DataDefinitionState";
    return result;
  }
}

// A Date accepts moment objects that wrap and extend the javascript date
// literal, and provide useful methods such as format and diff
exports.DataDefinitionState = DataDefinitionState;
class Date extends DataDefinition {
  constructor(options) {
    super(options);
  }

  // TODO: handle array
  static inflate(val) {
    let result = val;
    if (val != null) {
      if (val._isAMomentObject == null || !val._isAMomentObject) {
        if (val instanceof Date) {
          result = (0, _moment.default)(val);
        } else {
          result = (0, _moment.default)(val, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
        }
      }
      if (!result.isValid()) {
        throw new Error("inflating date string ".concat(val, " yields invalid momentjs object"));
      }
    }
    return result;
  }

  // TODO use locale or mask attribute to define format
  static render(val) {
    return val.format("DD/MM/YYYY");
  }
  _accept(value) {
    return value._isAMomentObject != null && value._isAMomentObject;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Date";
    return result;
  }
}
exports.Date = Date;
class Integer extends DataDefinition {
  constructor(options) {
    super(options);
  }
  static inflate(val) {
    if (typeof val === 'string') {
      return JSON.parse(val);
    } else {
      return val;
    }
  }
  _accept(value) {
    return typeof value === 'number' && super._accept(value);
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new NumberDataDefinitionState({
        path: statePath,
        _enactment: enactment
      });
    }
    if (this.warnings != null) {
      return this.warnings.map(warning => warning._buildState(enactment, statePath));
    }
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Integer";
    return result;
  }
}
exports.Integer = Integer;
class NumberDataDefinitionState extends DataDefinitionState {
  // in javascript NaN!=NaN so a NaN value would recurse infinitely without this extra guard
  _differentValues(left, right) {
    if (Array.isArray(left) && Array.isArray(right)) {
      return left.every((val, idx) => this._differentValues(val, right[idx]));
    } else {
      return typeof left !== typeof right || !isNaN(left) && left !== right;
    }
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "NumberDataDefinitionState";
    return result;
  }
}
exports.NumberDataDefinitionState = NumberDataDefinitionState;
class Float extends DataDefinition {
  constructor(options) {
    super(options);
  }
  static inflate(val) {
    if (typeof val === 'string') {
      return JSON.parse(val);
    } else {
      return val;
    }
  }
  _accept(value) {
    return typeof value === 'number' && super._accept(value);
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    if (this.valueCondition != null) {
      enactment._state[statePath] = new NumberDataDefinitionState({
        path: statePath,
        _enactment: enactment
      });
    }
    if (this.warnings != null) {
      return this.warnings.map(warning => warning._buildState(enactment, statePath));
    }
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Float";
    return result;
  }
}
exports.Float = Float;
class Text extends DataDefinition {
  constructor(options) {
    super(options);
  }
  _accept(value) {
    return typeof value === 'string' && super._accept(value);
  }
  static inflate(value) {
    // strings are not JSON.parsed unless there's a strong indication that the need it
    // see numeric_text example
    if ((value != null ? value.startsWith : undefined) != null && value.startsWith("[")) {
      return JSON.parse(value);
    } else {
      return value;
    }
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Text";
    return result;
  }
}
exports.Text = Text;
class Boolean extends DataDefinition {
  constructor(options) {
    super(options);
  }
  _accept(value) {
    return value === true || value === false;
  }
  static inflate(value) {
    if (typeof value === 'string') {
      return JSON.parse(value);
    } else {
      return value;
    }
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Boolean";
    return result;
  }
}
exports.Boolean = Boolean;
const dataDefinitionClassMap = exports.dataDefinitionClassMap = new Map([['Integer', Integer], ['Float', Float], ['Text', Text], ['Date', Date], ['Boolean', Boolean]]);
},{"./core.js":188,"core-js/modules/es.array.includes.js":153,"core-js/modules/es.array.push.js":155,"core-js/modules/es.array.reduce.js":156,"core-js/modules/es.error.cause.js":157,"core-js/modules/es.parse-int.js":166,"core-js/modules/es.regexp.exec.js":167,"core-js/modules/es.regexp.to-string.js":169,"core-js/modules/es.string.includes.js":170,"core-js/modules/es.string.split.js":172,"core-js/modules/es.string.starts-with.js":173,"core-js/modules/es.weak-map.js":176,"core-js/modules/esnext.iterator.constructor.js":177,"core-js/modules/esnext.iterator.every.js":178,"core-js/modules/esnext.iterator.filter.js":179,"core-js/modules/esnext.iterator.map.js":182,"core-js/modules/esnext.iterator.reduce.js":183,"core-js/modules/web.dom-collections.iterator.js":184,"jse-eval":185,"moment":187}],191:[function(require,module,exports){
"use strict";

require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Rewinder = void 0;
require("core-js/modules/es.error.cause.js");
require("core-js/modules/es.array.push.js");
require("core-js/modules/es.json.stringify.js");
require("core-js/modules/esnext.iterator.constructor.js");
require("core-js/modules/esnext.iterator.filter.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _enactment = require("./enactment.js");
var Evaluator = _interopRequireWildcard(require("./evaluator.js"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Allows you to construct earlier versions of an `Enactment` based on the enactment's `_audit`.

class Rewinder {
  constructor(enactment) {
    this.enactment = enactment;
    this.origin = new _enactment.Enactment({
      start: false,
      protocol: enactment.protocol,
      options: enactment.options
    });
    this.versions = []; // cache of version state
    this.maxIdx = -1; // max index of the current cache
  }

  // the passed enactment has an `_audit` array of timestamped interactions that have changed its state
  // version(idx) returns a clone of that enactment at each indexed audit event
  version(idx) {
    if (idx < 0 || idx > this.enactment._audit.length - 1) {
      throw new Error('Index out of range, ' + idx);
    } else {
      if (idx > this.maxIdx) {
        let rewind = this.maxIdx == -1 ? this.origin : this.version(this.maxIdx);
        for (let act of this.enactment._audit.slice(this.maxIdx + 1, idx + 1)) {
          if (act.action == 'start') {
            rewind.start();
          } else if (act.action.complete) {
            rewind.complete(act.action.complete);
          } else if (act.action.set) {
            rewind.set(act.action.set, act.action.data);
          } else if (act.action.unset) {
            rewind.unset(act.action.unset, act.action.type);
          } else if (act.action.confirm) {
            rewind.confirm(act.action.confirm);
          } else if (act.action.unconfirm) {
            rewind.unconfirm(act.action.unconfirm);
          } else if (act.action.cancel) {
            rewind.cancel(act.action.cancel);
          } else if (act.action.trigger) {
            rewind.sendTrigger(act.action.trigger);
          } else if (act.action.edit) {
            rewind.edit(act.action.edit + Evaluator.PATH_SEPARATOR + act.action.type, act.action.index, act.action.value);
          } else {
            throw new Error('unknown action: ', act.action);
          }
          let clone = _enactment.Enactment.inflate(JSON.stringify(rewind));
          this.versions.push({
            state: clone._state,
            triggers: clone._triggers
          });
        }
        this.maxIdx = idx;
      }
      let final = this.origin;
      final._state = this.versions[idx].state;
      let data = {};
      for (let path of Object.keys(this.enactment._data)) {
        data[path] = {};
        for (let dd of Object.keys(this.enactment._data[path])) {
          let arr = this.enactment._data[path][dd].filter(val => val.when.valueOf() <= this.enactment._audit[idx].timestamp.valueOf());
          if (arr.length > 0) {
            data[path][dd] = arr;
          }
        }
      }
      final._data = data;
      final._triggers = this.versions[idx].triggers;
      final._audit = this.enactment._audit.slice(0, idx + 1);
      return _enactment.Enactment.inflate(JSON.stringify(final));
    }
  }
}
exports.Rewinder = Rewinder;
},{"./enactment.js":189,"./evaluator.js":190,"core-js/modules/es.array.push.js":155,"core-js/modules/es.error.cause.js":157,"core-js/modules/es.json.stringify.js":165,"core-js/modules/es.weak-map.js":176,"core-js/modules/esnext.iterator.constructor.js":177,"core-js/modules/esnext.iterator.filter.js":179,"core-js/modules/web.dom-collections.iterator.js":184}],192:[function(require,module,exports){
"use strict";

require("core-js/modules/es.weak-map.js");
require("core-js/modules/esnext.iterator.for-each.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  inflate: true,
  buildTask: true,
  Task: true,
  TaskState: true,
  Action: true,
  ActionState: true,
  Enquiry: true,
  EnquiryState: true,
  Source: true,
  SourceState: true,
  Plan: true,
  PlanState: true,
  Decision: true,
  DecisionState: true,
  Candidate: true,
  CandidateState: true,
  Argument: true,
  ArgumentState: true
};
exports.inflate = exports.buildTask = exports.TaskState = exports.Task = exports.SourceState = exports.Source = exports.PlanState = exports.Plan = exports.EnquiryState = exports.Enquiry = exports.DecisionState = exports.Decision = exports.CandidateState = exports.Candidate = exports.ArgumentState = exports.Argument = exports.ActionState = exports.Action = void 0;
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.error.cause.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.push.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/es.string.starts-with.js");
require("core-js/modules/esnext.iterator.constructor.js");
require("core-js/modules/esnext.iterator.filter.js");
require("core-js/modules/esnext.iterator.find.js");
require("core-js/modules/esnext.iterator.map.js");
require("core-js/modules/web.dom-collections.iterator.js");
var Core = _interopRequireWildcard(require("./core.js"));
var Evaluator = _interopRequireWildcard(require("./evaluator.js"));
Object.keys(Evaluator).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === Evaluator[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return Evaluator[key];
    }
  });
});
var _moment = _interopRequireDefault(require("moment"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
// proformajs is a lightweight PROforma clinical decision support system engine
// Copyright (C) 2017 - Openclinical CIC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
A Protocol has two life-stages: json <-> component-tree

Creating a protocol object, i.e. a component-tree with a task at it's root
takes the declarative attributes (which are serialised in the toJson call) and
adds some design-time validation and runtime state management behaviours:
* the ability for a component to "look up the tree", using the assigned @_parent attribute and @children() function
* the ability to locate a component in the guideline via the path() function
* validation which ensures that paths arent ambiguous and expressions refer to known things
* the ability to create a stateful instance of the component that can be manipulated in an enactment
*/

// Deserialise a PROforma JSON string
const inflate = exports.inflate = function inflate(json) {
  const obj = JSON.parse(json);
  return this.buildTask(obj);
};

// build a proformajs task object from a serialised version
// converts a tree of pojos to a tree of components, with _parent references and validate methods
// TODO (later!): handle different versions of proformajs documents
const buildTask = exports.buildTask = function buildTask(obj) {
  if (obj.class != null) {
    try {
      // ES6 trick for dynamically instantiating task
      return new (taskClassMap.get(obj.class))(obj);
    } catch (e) {
      throw new Error('cannot create Protocol ' + obj.class, e);
    }
  } else {
    throw new Error('cannot parse PROforma task: missing class attribute');
  }
};

// A vanilla Task that may be used as a placeholder or extended to be used concretely
class Task extends Core.Named {
  constructor(options) {
    super(options);
    ({
      preCondition: this.preCondition,
      waitCondition: this.waitCondition,
      abortCondition: this.abortCondition,
      terminateCondition: this.terminateCondition,
      eventTrigger: this.eventTrigger,
      cycleUntil: this.cycleUntil
    } = options || {});
    this.optional = options.optional != null ? options.optional : false;
    this.autonomous = options.autonomous != null ? options.autonomous : false;
    this.cyclic = options.cyclic != null ? options.cyclic : false;
    this.dataDefinitions = [];
    if (options.dataDefinitions != null && Array.isArray(options.dataDefinitions)) {
      for (var meta of options.dataDefinitions) {
        if (meta.class != null) {
          var dataDefinition = new Evaluator[meta.class](meta);
          this.addDataDefinition(dataDefinition);
        } else {
          throw new Error('cannot construct dataDefinition: missing class attribute');
        }
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['preCondition', 'waitCondition', 'optional', 'dataDefinitions', 'autonomous', 'abortCondition', 'terminateCondition', 'eventTrigger', 'cyclic', 'cycleUntil']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['dataDefinitions']);
  }
  addDataDefinition(dataDefinition) {
    //TODO: throw error if dataDefinition is of wrong type?
    dataDefinition._parent = this;
    this.dataDefinitions.push(dataDefinition);
    return this;
  }

  // returns data definitions defined for this task and all it's ancestor tasks
  allDataDefinitions() {
    if (this._parent != null) {
      return this._parent.allDataDefinitions().concat(this.dataDefinitions);
    } else {
      return this.dataDefinitions;
    }
  }
  subPaths() {
    return super.subPaths(...arguments).concat(this.dataDefinitions.map(dataDef => this.name + Evaluator.PATH_SEPARATOR + dataDef.name));
  }

  // returns all sub-tasks
  allTasks() {
    return [this];
  }
  validate() {
    let result = super.validate();
    const names = [];
    const identifiers = this._identifiers();
    for (var definition of this.dataDefinitions) {
      if (names.includes(definition.name)) {
        result.push(this._createErrorValidation('name', "duplicate dataDefinition names"));
      }
      names.push(definition.name);
      if (definition._parent !== this) {
        result.push(this._createErrorValidation('_parent', "mis-attached dataDefinition: ".concat(definition.name)));
      }
      result = result.concat(definition.validate(identifiers));
    }
    if (this.preCondition === '') {
      result.push(this._createWarningValidation('preCondition', "empty expression"));
    }
    if (this.waitCondition === '') {
      result.push(this._createWarningValidation('waitCondition', "empty expression"));
    }
    if (this.preCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.preCondition, this.path(), 'preCondition', identifiers));
    }
    if (this.waitCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.waitCondition, this.path(), 'waitCondition', identifiers));
    }
    if (this.abortCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.abortCondition, this.path(), 'abortCondition', identifiers));
    }
    if (this.terminateCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.terminateCondition, this.path(), 'terminateCondition', identifiers));
    }
    if (this.cycleUntil != null) {
      result = result.concat(Evaluator.validateExpression(this.cycleUntil, this.path(), 'cycleUntil', identifiers));
    }
    if (Evaluator.reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    // if @eventTrigger? and @preCondition?
    //   result.push @_createWarningValidation 'preCondition', "preConditions are ignored if a task has an eventTrigger"
    if (this.eventTrigger != null && this.waitCondition != null) {
      result.push(this._createWarningValidation('waitCondition', "waitConditions are ignored if a task has an eventTrigger"));
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Task";
    // remove default values set in the constructor
    if (result.optional === false) {
      delete result.optional;
    }
    if (result.cyclic === false) {
      delete result.cyclic;
    }
    if (result.autonomous === false) {
      delete result.autonomous;
    }
    if (result.dataDefinitions.length === 0) {
      delete result.dataDefinitions;
    }
    return result;
  }
  _child(name) {
    return this.dataDefinitions.filter(child => child.name === name)[0];
  }
  _identifiers() {
    const result = [];
    for (var dataDef of this.allDataDefinitions()) {
      result.push(dataDef.name);
      if (dataDef.range != null && dataDef.constructor.name === Evaluator.dataDefinitionClassMap.get("Text").name) {
        for (var rangeItem of dataDef.range) {
          if (rangeItem.value != null) {
            result.push(rangeItem.value);
          } else {
            result.push(rangeItem);
          }
        }
      }
    }
    // At first only sibling task names and their candidate names were captured here,
    // but some guidelines reference candidates in sibling children tasks so
    // it now captures all task and candidate names.   This may be overkill.
    for (var task of this._getRoot().allTasks()) {
      result.push(task.name);
      if (task.candidates != null) {
        for (var candidate of task.candidates) {
          result.push(candidate.name);
        }
      }
    }
    return result.concat(this._getRoot().subPaths());
  }
  _buildState(enactment, index) {
    let parentStatePath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    if (index == null) {
      index = -1;
    }
    const statePath = (parentStatePath != null ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index > -1 ? "[" + index + "]" : "");
    if (index > -1) {
      enactment._state[statePath] = new TaskState({
        path: statePath,
        _enactment: enactment,
        index
      });
    } else {
      enactment._state[statePath] = new TaskState({
        path: statePath,
        _enactment: enactment
      });
    }
    for (var dataDef of this.dataDefinitions) {
      dataDef._buildState(enactment, statePath);
    }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}
exports.Task = Task;
class TaskState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({
      state: this.state,
      completeable: this.completeable,
      cancellable: this.cancellable,
      index: this.index,
      cycleable: this.cycleable
    } = options || {});
  }

  // returns true if task is ready to complete
  // expected to be overidden by sub-classes
  _isCompleteable() {
    return this.state === 'in_progress';
  }

  // returns true if task can be cancellad
  _isCancellable() {
    return this.state === 'in_progress';
  }

  // true if dynamic attributes can be updated
  _isUpdateable() {
    return this.state === 'in_progress';
  }

  // complete task
  complete() {
    if (this._isCompleteable()) {
      const comp = this._enactment.protocol.getComponent(this.design);
      if (comp.cyclic && this.cycleable) {
        comp._buildState(this._enactment, this.index + 1, this._parentStatePath());
      }
      return this._enactment._cycle({
        action: {
          complete: this.path
        },
        seed: [{
          path: this.path,
          attribute: 'state',
          value: 'completed'
        }]
      });
    } else {
      throw new Error('Cannot complete a task which is not completeable');
    }
  }

  // cancel task
  cancel() {
    if (this._isCancellable()) {
      const comp = this._enactment.protocol.getComponent(this.design);
      if (comp.cyclic && this.cycleable) {
        comp._buildState(this._enactment, this.index + 1, this._parentStatePath());
      }
      return this._enactment._cycle({
        action: {
          cancel: this.path
        },
        seed: [{
          path: this.path,
          attribute: 'state',
          value: 'discarded'
        }]
      });
    } else {
      throw new Error('Cannot cancel a task which is not cancellable');
    }
  }

  // retrive current value of requested dataDefinition
  get(key) {
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === key);
    if (target != null) {
      const targettask = target._parent.path();
      const values = this._enactment._data[targettask] != null ? this._enactment._data[targettask][key] : undefined;
      if (values != null) {
        return values[values.length - 1].value;
      } else {
        return undefined;
      }
    } else {
      return undefined; // or raise unknown datadefinition error?
    }
  }

  // provide runtime data, in the form of an object so that
  // multiple data items can be supplied at once.
  // Invalid data is filtered out and signalled by an Error
  // *after* the valid data has been set
  set(data) {
    let dataDefinition;
    let added = false;
    const timestamp = (0, _moment.default)();
    const errors = [];
    for (dataDefinition of Object.keys(data || {})) {
      var value = data[dataDefinition];
      var target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === dataDefinition);
      if (target != null) {
        if (target.isDerived()) {
          errors.push({
            dataDefinition,
            message: 'cannot set a derived data definition value'
          });
        } else {
          var valid = true;
          if (!target.accept(value)) {
            valid = false;
            errors.push({
              dataDefinition,
              message: "incompatible data provided: ".concat(value)
            });
          }
          // todo: who, how
          var taskpath = target._parent.path();
          if (valid) {
            if (this._enactment._data[taskpath] == null) {
              this._enactment._data[taskpath] = {};
            }
            if (this._enactment._data[taskpath][dataDefinition] == null) {
              this._enactment._data[taskpath][dataDefinition] = [];
            }
            this._enactment._data[taskpath][dataDefinition].push({
              value,
              when: timestamp
            });
            added = true;
          }
        }
      } else {
        errors.push({
          dataDefinition,
          message: "unknown dataDefinition"
        });
      }
    }
    if (added) {
      this._enactment._cycle({
        action: {
          set: this.path,
          data
        }
      });
    }
    if (errors.length > 0) {
      let msg = "";
      for (let idx = 0; idx < errors.length; idx++) {
        var error = errors[idx];
        msg = msg + "".concat(error.dataDefinition, " - ").concat(error.message);
        if (idx < errors.length - 1) {
          msg = msg + "\n";
        }
      }
      throw new Error(msg);
    }
  }
  unset(type) {
    const timestamp = (0, _moment.default)();
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === type);
    if (target != null) {
      const taskpath = target._parent.path();
      if (this._enactment._data[taskpath] == null) {
        this._enactment._data[taskpath] = {};
      }
      if (this._enactment._data[taskpath][type] == null) {
        this._enactment._data[taskpath][type] = [];
      }
      this._enactment._data[taskpath][type].push({
        value: undefined,
        when: timestamp
      });
      return this._enactment._cycle({
        action: {
          unset: this.path,
          type
        }
      });
    } else {
      throw new Error("unable to unset ".concat(type, " - not a valid datadefinition in ").concat(this.path));
    }
  }

  // provide a mechanism for correcting mistakes / typos from set
  // instead of adding a new timestamp of data, you can go back and tweak
  // the tweak is of course reflected in the audit
  edit(type, index, value) {
    const target = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(member => member.name === type);
    if (target != null) {
      const taskpath = target._parent.path();
      if (Array.isArray(this._enactment._data[taskpath][type]) && this._enactment._data[taskpath][type].length > index) {
        this._enactment._data[taskpath][type][index].value = value;
      }
      return this._enactment._cycle({
        action: {
          edit: this.path,
          type,
          index,
          value
        }
      });
    } else {
      throw new Error("unable to edit ".concat(type, " - not a valid datadefinition in ").concat(this.path));
    }
  }
  trigger() {
    // offers a trigger if
    // * its design has a trigger,
    // * its state is currently dormant
    // * and it's parent (if it has one) is in_progress
    let result = null;
    const {
      eventTrigger
    } = this._enactment.protocol.getComponent(this.design);
    if (eventTrigger != null) {
      const parent = this._enactment._state[this._parentStatePath()];
      if (this.state === 'dormant' && (parent != null ? parent.state === 'in_progress' : true)) {
        result = eventTrigger;
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "TaskState";
    return result;
  }

  // return the value and all meta of a visible data definition
  _getData(type) {
    const def = this._enactment.protocol.getComponent(this.design).allDataDefinitions().find(dataDef => dataDef.name === type);
    let result = null;
    if (def != null) {
      result = def.flatten();
      const dataDef = this._enactment._data[def._parent.path()];
      if (dataDef) {
        const data = dataDef[type];
        if (data != null && data.length > 0) {
          result.value = data[data.length - 1].value;
        } else {
          result.value = undefined;
        }
      }
    }
    return result;
  }

  // The state engine.  Returns an array of state updates
  _updateState(evaluator) {
    let comp, cycleable;
    const result = super._updateState();
    if (this.state == null) {
      result.push({
        path: this.path,
        attribute: 'state',
        value: 'dormant'
      });
    } else {
      let _parentstate, cancellable, completeable;
      comp = this._enactment.protocol.getComponent(this.design);
      const _parentpath = this._parentStatePath();
      if (_parentpath != null) {
        _parentstate = this._enactment._state[_parentpath].state;
      }
      if (this.state === 'dormant' && comp.eventTrigger == null && (_parentpath == null || _parentstate === 'in_progress') && (comp.waitCondition == null || comp.waitCondition === '' || evaluator.evaluate(comp.waitCondition, this.design))) {
        const preCondition = comp.preCondition == null || comp.preCondition === '' || evaluator.evaluate(comp.preCondition, this.design);
        result.push({
          path: this.path,
          attribute: 'state',
          value: preCondition ? 'in_progress' : 'discarded'
        });
      } else if (this.state === 'dormant' && comp.optional && ['completed', 'discarded'].includes(_parentstate)) {
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'discarded'
        });
      } else if (this.state === 'in_progress') {
        completeable = this._isCompleteable();
        if (completeable !== this.completeable) {
          result.push({
            path: this.path,
            attribute: 'completeable',
            value: completeable
          });
        }
        cancellable = this._isCancellable();
        if (cancellable !== this.cancellable) {
          result.push({
            path: this.path,
            attribute: 'cancellable',
            value: cancellable
          });
        }
        cycleable = comp.cycleUntil == null || !evaluator.evaluate(comp.cycleUntil, this.design);
        if (cycleable !== this.cycleable) {
          result.push({
            path: this.path,
            attribute: 'cycleable',
            value: cycleable
          });
        }
        if (comp.abortCondition != null && evaluator.evaluate(comp.abortCondition, this.design)) {
          if (this.index != null && this.cycleable) {
            comp._buildState(this._enactment, this.index + 1, _parentpath);
          }
          result.push({
            path: this.path,
            attribute: 'state',
            value: 'discarded',
            comment: 'abortCondition true'
          });
        } else if (comp.terminateCondition != null && evaluator.evaluate(comp.terminateCondition, this.design)) {
          if (this.index != null && this.cycleable) {
            comp._buildState(this._enactment, this.index + 1, _parentpath);
          }
          result.push({
            path: this.path,
            attribute: 'state',
            value: 'completed',
            comment: 'terminateCondition true'
          });
        }
        if (comp.optional && ['completed', 'discarded'].includes(_parentstate)) {
          result.push({
            path: this.path,
            attribute: 'state',
            value: _parentstate
          });
        }
      } else {
        // in case there are any loose ends for completed or discarded tasks
        if (this.completeable) {
          result.push({
            path: this.path,
            attribute: 'completeable',
            value: false
          });
        }
        if (this.cancellable) {
          result.push({
            path: this.path,
            attribute: 'cancellable',
            value: false
          });
        }
        if (this.cycleable) {
          result.push({
            path: this.path,
            attribute: 'cycleable',
            value: false
          });
        }
      }
    }
    if ((result != null ? result.length : undefined) === 0) {
      // this check ensures that enquiry state has settled, without it requested state can be set at the same time as completed state
      if (comp.autonomous && this._isCompleteable()) {
        if (this.index != null && this.cycleable) {
          comp._buildState(this._enactment, this.index + 1, this._parentStatePath());
        }
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'completed'
        });
      }
    }
    return result;
  }
}

// An Action is a placeholder for a different kind of task
// TODO: add an action method
exports.TaskState = TaskState;
class Action extends Task {
  toJSON() {
    const result = super.toJSON();
    result.class = "Action";
    return result;
  }
}
exports.Action = Action;
class ActionState extends TaskState {
  toJSON() {
    const result = super.toJSON();
    result.class = "ActionState";
    return result;
  }
}

// An Enquiry is a task that actively attempts to gather data
// sources = Source[]
// useDefaults = runtime flag
exports.ActionState = ActionState;
class Enquiry extends Task {
  constructor(options) {
    super(options);
    ({
      useDefaults: this.useDefaults
    } = options || {});
    this.sources = [];
    if (options.sources != null && Array.isArray(options.sources)) {
      for (var meta of options.sources) {
        var source = new Source(meta);
        this.addSource(source);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['sources', 'useDefaults']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['sources']);
  }
  addSource(source) {
    // if (!(source instanceof exports.Source)) {
    //   throw new Error(`${source} must be of type Source`);
    // }
    source._parent = this;
    this.sources.push(source);
    return this;
  }
  subPaths() {
    const result = super.subPaths();
    for (var source of this.sources) {
      var path = source.path();
      if (!result.includes(path)) {
        result.push(path);
      }
    }
    return result;
  }

  // note that checking a source relates to an existing data definition is left to the source component
  validate() {
    let result = super.validate();
    const identifiers = this._identifiers();
    const names = [];
    if (this.sources.length === 0) {
      result.push(this._createWarningValidation('sources', "no sources defined"));
    } else {
      for (var source of this.sources) {
        if (names.includes(source.type)) {
          result.push(this._createErrorValidation('sources', "duplicate source type: ".concat(source.type)));
        }
        var def = this.allDataDefinitions().find(dataDef => dataDef.name === source.type);
        if ((def != null ? def.valueCondition : undefined) != null && def.valueCondition.length > 0) {
          result.push(this._createErrorValidation('sources', "cannot source a dynamic data definition: ".concat(source.type)));
        }
        names.push(source.type);
        if (source._parent !== this) {
          result.push(this._createErrorValidation('sources', "mis-attached source: ".concat(source.type)));
        }
        result = result.concat(source.validate(identifiers));
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Enquiry";
    // remove default values set in the constructor
    if (result.sources.length === 0) {
      delete result.sources;
    }
    return result;
  }

  // TODO: handle case when datadefinition and source are defined in the same task
  _child(name) {
    return this.sources.filter(child => child.type === name)[0] || super._child(name);
  }
  _buildState(enactment, index) {
    let parentStatePath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    if (index == null) {
      index = -1;
    }
    const statePath = (parentStatePath != null ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index > -1 ? "[" + index + "]" : "");
    if (index > -1) {
      enactment._state[statePath] = new EnquiryState({
        path: statePath,
        _enactment: enactment,
        index
      });
    } else {
      enactment._state[statePath] = new EnquiryState({
        path: statePath,
        _enactment: enactment
      });
    }
    for (var source of this.sources) {
      source._buildState(enactment, statePath);
    }
    for (var dataDef of this.dataDefinitions) {
      dataDef._buildState(enactment, statePath);
    }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}

// TODO: Find a way to include a dynamic data item as a read-only source
exports.Enquiry = Enquiry;
class EnquiryState extends TaskState {
  constructor(options) {
    if (options == null) {
      options = {};
    }
    super(options);
  }

  // returns array of requested source types
  requested() {
    const result = [];
    for (var source of this._enactment.protocol.getComponent(this.design).sources) {
      if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + source.type].requested) {
        result.push(this._enactment.protocol.getComponent(source.path()).type);
      }
    }
    return result;
  }

  // carefully constructed to combine source and dataDefinition attributes in sources
  getComponent(shallow) {
    if (shallow == null) {
      shallow = false;
    }
    const design = this._enactment.protocol.getComponent(this.design);
    const result = super.getComponent();
    if (!shallow) {
      result.sources = [];
      for (var source of design.sources) {
        var component = this._getData(source.type);
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(this.design) + Core.Named.PATH_SEPARATOR + source.type];
        if (stateObj != null) {
          var object = stateObj.getComponent();
          for (const key in object) {
            const value = object[key];
            if (!['class'].includes(key)) {
              component[key] = value;
            }
          }
          result.sources.push(component);
        }
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "EnquiryState";
    return result;
  }
  _isCompleteable() {
    const sources = this._enactment.protocol.getComponent(this.design).sources.filter(source => this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + source.type].requested);
    return this.state === 'in_progress' && sources.length === 0;
  }
  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    const comp = this._enactment.protocol.getComponent(this.design);
    // TODO: Why are default values not added in the burst / update cycle?
    if (comp.useDefaults || this._enactment.options != null && this._enactment.options.Enquiry != null && Object.keys(this._enactment.options.Enquiry).includes("useDefaults") && this._enactment.options.Enquiry.useDefaults) {
      // check for a change to in_progress and set default values
      if ((result != null ? result.length : undefined) > 0) {
        let set_defaults = false;
        for (var update of result) {
          if (update.attribute === 'state' && update.value === 'in_progress') {
            set_defaults = true;
          }
        }
        if (set_defaults) {
          // TODO: get timestamp from this set of cycles...
          const timestamp = (0, _moment.default)();
          for (var source of comp.sources) {
            for (var dataDef of comp.allDataDefinitions()) {
              if (dataDef.name === source.type) {
                if (dataDef.defaultValue != null || dataDef.defaultCondition != null) {
                  var taskpath = dataDef._parent.path();
                  // only supply default if there is no existing value
                  const taskpathData = this._enactment.getData()[taskpath];
                  if (!taskpathData || taskpathData[dataDef.name] == null) {
                    if (this._enactment._data[taskpath] == null) {
                      this._enactment._data[taskpath] = {};
                    }
                    if (this._enactment._data[taskpath][dataDef.name] == null) {
                      this._enactment._data[taskpath][dataDef.name] = [];
                    }
                    if (dataDef.defaultValue != null) {
                      this._enactment._data[taskpath][dataDef.name].push({
                        value: dataDef.defaultValue,
                        when: timestamp
                      });
                    } else if (dataDef.defaultCondition != null) {
                      const val = evaluator.evaluate(dataDef.defaultCondition, this.design);
                      if (dataDef.accept(val)) {
                        this._enactment._data[taskpath][dataDef.name].push({
                          value: val,
                          when: timestamp
                        });
                      } else {
                        console.log(timestamp, dataDef.defaultCondition, 'rejected');
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return result;
  }
}

// A Source is referenced by an Enquiry and itself references a DataDefinition.
// It does not extend Named as it has no need for name, caption and description
// attributes which are derived instead from the named dataDefinition but this
// means that the path function must be redefined.
exports.EnquiryState = EnquiryState;
class Source extends Core.Component {
  constructor(options) {
    super(options);
    ({
      type: this.type,
      requestCondition: this.requestCondition
    } = options || {});
    if (this.type == null) {
      throw new Error('new Protocol.Source(obj): Unable to create enquiry source without a type attribute');
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['type', 'requestCondition']);
  }
  path() {
    if (this._parent != null) {
      return "".concat(this._parent.path(), ":").concat(this.type);
    } else {
      return this.type;
    }
  }
  allDataDefinitions() {
    return this._parent.allDataDefinitions();
  }
  validate(identifiers) {
    let result = super.validate();
    // identifiers can be (optionally) passed as an optimisation
    if (identifiers == null) {
      if (this._parent != null && this._parent.allDataDefinitions().length > 0) {
        identifiers = this._parent.allDataDefinitions().map(dataDefinition => dataDefinition.name);
      } else {
        identifiers = [];
      }
    }
    if (!identifiers.includes(this.type)) {
      result.push(this._createErrorValidation('type', "unknown data definition - ".concat(this.type)));
    }
    if (this.requestCondition != null && this.requestCondition.length === 0) {
      result.push(this._createWarningValidation('requestCondition', 'empty expression'));
    }
    if (this.requestCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.requestCondition, this.path(), 'requestCondition', identifiers));
    }
    return result;
  }

  // a source's subPaths are routed through to it's data definitions...
  subPaths() {
    const dd = this.allDataDefinitions().find(dd => dd.name === this.type);
    return (dd != null ? dd.subPaths() : undefined) || [this.type];
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Evaluator.PATH_SEPARATOR + this.type;
    return enactment._state[statePath] = new SourceState({
      path: statePath,
      _enactment: enactment
    });
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Source";
    return result;
  }
}
exports.Source = Source;
class SourceState extends Core.ComponentState {
  constructor(options) {
    super(options);
    ({
      requested: this.requested
    } = options || {});
  }
  _updateState(evaluator) {
    const result = [];
    const _parentstate = this._enactment._state[this._parentStatePath()].state;
    if (_parentstate != null) {
      let requested;
      if (_parentstate === 'in_progress') {
        if (this._enactment.protocol.getComponent(this.design).requestCondition != null) {
          requested = evaluator.evaluate(this._enactment.protocol.getComponent(this.design).requestCondition, this.design);
          if (requested !== this.requested) {
            result.push({
              path: this.path,
              attribute: 'requested',
              value: requested
            });
          }
        }
      } else if (this.requested) {
        result.push({
          path: this.path,
          attribute: 'requested',
          value: false
        });
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "SourceState";
    return result;
  }
}

// A Plan is a task that contains sub-tasks
exports.SourceState = SourceState;
class Plan extends Task {
  constructor(options) {
    super(options);
    this.tasks = [];
    if (options.tasks != null && Array.isArray(options.tasks)) {
      for (var meta of options.tasks) {
        if (meta.class != null) {
          // ES6 trick for dynamically instantiating task
          const task = new (taskClassMap.get(meta.class))(meta);
          this.addTask(task);
        } else {
          throw new Error('cannot parse PROforma task: missing class attribute');
        }
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['tasks']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['tasks']);
  }
  addTask(task) {
    // if (!(task instanceof exports.Task)) {
    //   throw new Error("Plan.addTask(task): task must be of type Task (or one of it's sub-types)");
    // }
    task._parent = this;
    this.tasks.push(task);
    return this;
  }
  subPaths() {
    let result = [];
    for (var task of this.tasks) {
      result = result.concat(task.subPaths().map(subPath => this.name + Evaluator.PATH_SEPARATOR + subPath));
    }
    return super.subPaths(...arguments).concat(result);
  }

  // returns all sub-tasks
  allTasks() {
    let result = [this];
    for (var task of this.tasks) {
      result = result.concat(task.allTasks());
    }
    return result;
  }
  validate() {
    let result = super.validate();
    const names = [];
    const datadefs = this.dataDefinitions.map(dataDef => dataDef.name);
    if (this.tasks.length === 0) {
      result.push(this._createWarningValidation('tasks', "no sub-tasks defined"));
    }
    for (var task of this.tasks) {
      if (names.includes(task.name)) {
        result.push(this._createErrorValidation('tasks', "duplicate sub-task names: ".concat(task.name)));
      }
      if (datadefs.includes(task.name)) {
        result.push(this._createErrorValidation('tasks', "task name clashes with dataDefinition: ".concat(task.name)));
      }
      names.push(task.name);
      if (task._parent !== this) {
        result.push(this._createErrorValidation('tasks', "mis-attached sub-task: ".concat(task.name)));
      }
      result = result.concat(task.validate());
    }
    if (this.tasks.filter(task => task.optional === false).length === 0 && this.autonomous && this.cyclic) {
      result.push(this._createErrorValidation('cyclic', "an autonomous cyclic plan with no non-optional tasks is pathological"));
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Plan";
    // remove default values set in the constructor
    if (result.tasks.length === 0) {
      delete result.tasks;
    }
    return result;
  }
  _child(name) {
    return this.tasks.filter(task => task.name === name)[0] || super._child(name);
  }
  _buildState(enactment, index) {
    let parentStatePath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    if (index == null) {
      index = -1;
    }
    const statePath = (parentStatePath != null ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index > -1 ? "[" + index + "]" : "");
    if (index > -1) {
      enactment._state[statePath] = new PlanState({
        path: statePath,
        _enactment: enactment,
        index
      });
    } else {
      enactment._state[statePath] = new PlanState({
        path: statePath,
        _enactment: enactment
      });
    }
    for (var task of this.tasks) {
      if (task.cyclic) {
        task._buildState(enactment, 0, statePath);
      } else {
        task._buildState(enactment, -1, statePath);
      }
    }
    for (var dataDef of this.dataDefinitions) {
      dataDef._buildState(enactment, statePath);
    }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}
exports.Plan = Plan;
class PlanState extends TaskState {
  constructor(options) {
    super(options);
  }
  getComponent(shallow) {
    if (shallow == null) {
      shallow = false;
    }
    const design = this._enactment.protocol.getComponent(this.design);
    const result = super.getComponent();
    if (!shallow) {
      result.tasks = [];
      for (var task of design.tasks) {
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(task.path())];
        if (stateObj != null) {
          result.tasks.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "PlanState";
    return result;
  }

  // a plan is completeable if all it's non-optional sub-tasks are finished
  _isCompleteable() {
    if (this.state === 'in_progress') {
      for (var task of this._enactment.protocol.getComponent(this.design).tasks) {
        var taskStatePath = this._enactment.runtimeFromDesignPath(task.path());
        var task_state = this._enactment._state[taskStatePath].state;
        if (task_state != null) {
          if (!task.optional && ['dormant', 'in_progress'].includes(task_state)) {
            return false;
          }
          if (task.optional && task_state === 'in_progress') {
            return false; // dont auto-complete optional tasks that are in_progress
          }
        } else {
          return false; // task_state can be undefined when a task is first instantiated
        }
      }
      return true;
    } else {
      return false;
    }
  }
  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    if (this.state === 'discarded') {
      const comp = this._enactment.protocol.getComponent(this.design);
      // if a task has been cancelled / aborted then it needs to handle unfinished sub-tasks
      for (let task of comp.tasks) {
        let taskStatePath = this.path + Evaluator.PATH_SEPARATOR + task.name;
        if (task.cyclic) {
          // find the highest cycle
          // note that we dont use runtimeFromDesignPath because a new plan and sub tasks will already have been created
          var keys = Object.keys(this._enactment._state).filter(key => key.startsWith(taskStatePath) && key.slice(taskStatePath.length).indexOf(Evaluator.PATH_SEPARATOR) === -1);
          taskStatePath = keys.pop();
        }
        let task_state = this._enactment._state[taskStatePath].state;
        if (['dormant', 'in_progress'].includes(task_state)) {
          result.push({
            path: taskStatePath,
            attribute: 'state',
            value: 'discarded'
          });
        }
      }
    }
    return result;
  }
}

// A Decision is a task that represents decisions and keeps a track of their state
// TODO should candidate recommended state change when not in_progress
exports.PlanState = PlanState;
class Decision extends Task {
  constructor(options) {
    super(options);
    ({
      autoConfirmMostSupported: this.autoConfirmMostSupported
    } = options || {});
    this.candidates = [];
    if (options.candidates != null && Array.isArray(options.candidates)) {
      for (var meta of options.candidates) {
        var candidate = new Candidate(meta);
        this.addCandidate(candidate);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['candidates', 'autoConfirmMostSupported']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['candidates']);
  }
  addCandidate(candidate) {
    // if (!(candidate instanceof exports.Candidate)) {
    //   throw new Error("Decision.addCandidate(candidate): candidate must be of type Candidate");
    // }
    candidate._parent = this;
    this.candidates.push(candidate);
    return this;
  }
  subPaths() {
    let result = [];
    for (var candidate of this.candidates) {
      result = result.concat(candidate.subPaths().map(subPath => this.name + Evaluator.PATH_SEPARATOR + subPath));
    }
    return super.subPaths(...arguments).concat(result);
  }
  validate() {
    let result = super.validate();
    const names = [];
    const identifiers = this._identifiers();
    const datadefs = this.dataDefinitions.map(dataDef => dataDef.name);
    if (this.candidates.length === 0) {
      result.push(this._createWarningValidation('candidates', "no candidates defined"));
    }
    for (var candidate of this.candidates) {
      if (names.includes(candidate.name)) {
        result.push(this._createErrorValidation('candidates', "non-uniquely named sub-candidates: ".concat(candidate.name)));
      }
      if (datadefs.includes(candidate.name)) {
        result.push(this._createErrorValidation('candidates', "dataDefinition/candidate name clash: ".concat(candidate.name)));
      }
      names.push(candidate.name);
      if (candidate._parent !== this) {
        result.push(this._createErrorValidation('candidates', "mis-attached sub-candidate: ".concat(candidate.name)));
      }
      result = result.concat(candidate.validate(identifiers));
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Decision";
    // remove default values set in the constructor
    if (result.candidates.length === 0) {
      delete result.candidates;
    }
    return result;
  }
  _identifiers() {
    return super._identifiers(...arguments).concat(this.candidates.map(candidate => candidate.name));
  }
  _child(name) {
    return this.candidates.filter(candidate => candidate.name === name)[0] || super._child(name);
  }
  _buildState(enactment, index) {
    let parentStatePath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    if (index == null) {
      index = -1;
    }
    const statePath = (parentStatePath != null ? parentStatePath + Evaluator.PATH_SEPARATOR : "") + this.name + (index > -1 ? "[" + index + "]" : "");
    if (index > -1) {
      enactment._state[statePath] = new DecisionState({
        path: statePath,
        _enactment: enactment,
        index
      });
    } else {
      enactment._state[statePath] = new DecisionState({
        path: statePath,
        _enactment: enactment
      });
    }
    for (var candidate of this.candidates) {
      candidate._buildState(enactment, statePath);
    }
    for (var dataDef of this.dataDefinitions) {
      dataDef._buildState(enactment, statePath);
    }
    if (this.eventTrigger != null) {
      if (!Object.keys(enactment._triggers).includes(this.eventTrigger)) {
        enactment._triggers[this.eventTrigger] = [];
      }
      return enactment._triggers[this.eventTrigger].push(statePath);
    }
  }
}
exports.Decision = Decision;
class DecisionState extends TaskState {
  constructor(options) {
    super(options);
  }

  // returns array of confirmed candidate names
  confirmed() {
    const result = [];
    for (var candidate of this._enactment.protocol.getComponent(this.design).candidates) {
      if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].confirmed) {
        result.push(candidate.name);
      }
    }
    return result;
  }

  // returns array of recommended candidate names
  recommended() {
    const result = [];
    for (var candidate of this._enactment.protocol.getComponent(this.design).candidates) {
      if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].recommended != null && this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name].recommended) {
        result.push(candidate.name);
      }
    }
    return result;
  }
  getComponent(shallow) {
    if (shallow == null) {
      shallow = false;
    }
    const result = super.getComponent();
    if (!shallow) {
      result.candidates = [];
      const design = this._enactment.protocol.getComponent(this.design);
      for (var candidate of design.candidates) {
        var stateObj = this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + candidate.name];
        if (stateObj != null) {
          result.candidates.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "DecisionState";
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const candidate = this._enactment._state[this.path + Evaluator.PATH_SEPARATOR + name];
    if (candidate != null) {
      return candidate;
    } else {
      // look at candidates of sibling decisions
      const paths = this.design.split(Evaluator.PATH_SEPARATOR);
      const taskname = paths.pop();
      if (paths.length > 0) {
        const parentdesign = this._enactment.protocol.getComponent(paths.join(Evaluator.PATH_SEPARATOR));
        if (parentdesign.tasks != null) {
          for (var task of parentdesign.tasks) {
            if (task.candidates != null && task.name !== taskname) {
              for (var cand of task.candidates) {
                if (cand.name === name) {
                  return this._enactment._state[this._enactment.runtimeFromDesignPath(cand.path())];
                }
              }
            }
          }
        }
      }
    }
  }
  _isCompleteable() {
    return this.state === 'in_progress' && this.confirmed().length > 0 || this._enactment.protocol.getComponent(this.design).candidates.length === 0;
  }
  _updateState(evaluator) {
    const result = super._updateState(evaluator);
    const comp = this._enactment.protocol.getComponent(this.design);
    if (this.state === 'in_progress') {
      if (comp.autonomous && this._isCompleteable()) {
        if (this.index != null && this.cycleable) {
          comp._buildState(this._enactment, this.index + 1, this._parentStatePath());
        }
        result.push({
          path: this.path,
          attribute: 'state',
          value: 'completed'
        });
      }
      if (comp.autoConfirmMostSupported || this._enactment.options != null && this._enactment.options.Decision != null && Object.keys(this._enactment.options.Decision).includes('autoConfirmMostSupported') && this._enactment.options.Decision.autoConfirmMostSupported) {
        // commit candidate with highest support
        let most_supported = {};
        for (var candidate of comp.candidates) {
          var candidateruntimepath = this._enactment.runtimeFromDesignPath(this.path) + Core.Named.PATH_SEPARATOR + candidate.name;
          var candidate_state = this._enactment._state[candidateruntimepath];
          if (most_supported.path == null || candidate_state.support != null && candidate_state.support > most_supported.support) {
            if (candidate_state.support != null) {
              most_supported = {
                path: candidateruntimepath,
                support: candidate_state.support,
                confirmed: candidate_state.confirmed
              };
            }
          }
        }
        if (most_supported.path != null && !most_supported.confirmed && most_supported.support > 0) {
          result.push({
            path: most_supported.path,
            attribute: 'confirmed',
            value: true
          });
        }
      }
    }
    return result;
  }
}

// A Candidate is used by a Decision to represent decision choices
exports.DecisionState = DecisionState;
class Candidate extends Core.Named {
  constructor(options) {
    super(options);
    ({
      recommendCondition: this.recommendCondition,
      autoConfirmRecommended: this.autoConfirmRecommended
    } = options || {});
    this.arguments = [];
    if (options.arguments != null && Array.isArray(options.arguments)) {
      for (var meta of options.arguments) {
        var argument = new Argument(meta);
        this.addArgument(argument);
      }
    }
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['recommendCondition', 'arguments', 'autoConfirmRecommended']);
  }

  // see Core.Component.flatten()
  static _ignoreKeys() {
    return super._ignoreKeys(...arguments).concat(['arguments']);
  }
  addArgument(argument) {
    // if (!(argument instanceof exports.Argument)) {
    //   throw new Error("Candidate.addArgument(argument): argument must be of type Argument");
    // }
    argument._parent = this;
    argument.idx = this.arguments.length;
    this.arguments.push(argument);
    return this;
  }
  toJSON() {
    const result = super.toJSON();
    result.class = "Candidate";
    // remove default values set in the constructor
    if (result.arguments.length === 0) {
      delete result.arguments;
    }
    return result;
  }
  subPaths() {
    return super.subPaths(...arguments).concat(this.arguments.map(argument => this.name + Evaluator.PATH_SEPARATOR + argument.idx));
  }
  allDataDefinitions() {
    return this._parent.allDataDefinitions();
  }

  // Note no warning for empty arguments because logic may be all in recommendCondition by design
  validate(identifiers) {
    if (identifiers == null) {
      identifiers = [];
    }
    let result = super.validate();
    if (this._parent == null) {
      result.push(this._createErrorValidation('_parent', "a Candidate must be attached to a Decision"));
    }
    for (var argument of this.arguments) {
      if (argument._parent !== this) {
        result.push(this._createErrorValidation('arguments', "mis-attached sub-argument: ".concat(argument.name)));
      }
      result = result.concat(argument.validate(identifiers));
    }
    if (Evaluator.reservedWords.includes(this.name)) {
      result.push(this._createErrorValidation('name', "cannot be a reserved word"));
    }
    if (this.recommendCondition == null || this.recommendCondition === '') {
      result.push(this._createWarningValidation('recommendCondition', "missing or empty expression"));
    }
    if (this.recommendCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.recommendCondition, this.path(), 'recommendCondition', identifiers));
    }
    return result;
  }
  _child(idx) {
    return this.arguments[idx];
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.name;
    enactment._state[statePath] = new CandidateState({
      path: statePath,
      _enactment: enactment
    });
    return this.arguments.map(argument => argument._buildState(enactment, statePath));
  }
}
exports.Candidate = Candidate;
class CandidateState extends Core.NamedState {
  constructor(options) {
    super(options);
    ({
      recommended: this.recommended,
      support: this.support
    } = options || {});
    this.confirmed = options.confirmed || false;
    if (this.support === "++") {
      this.support = +Infinity;
    }
    if (this.support === "--") {
      this.support = -Infinity;
    }
  }
  confirm() {
    return this._setConfirm(true);
  }
  unconfirm() {
    return this._setConfirm(false);
  }
  _isUpdateable() {
    return this._enactment._state[this._parentStatePath()].state === 'in_progress';
  }
  getComponent(shallow) {
    if (shallow == null) {
      shallow = false;
    }
    const result = super.getComponent();
    if (!shallow) {
      result.arguments = [];
      const design = this._enactment.protocol.getComponent(this.design);
      for (let idx = 0; idx < design.arguments.length; idx++) {
        var stateObj = this._enactment._state[this._enactment.runtimeFromDesignPath(this.design) + Core.Named.PATH_SEPARATOR + idx];
        if (stateObj != null) {
          result.arguments.push(stateObj.getComponent());
        }
      }
    }
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const paths = this.path.split(Evaluator.PATH_SEPARATOR);
    const test = paths.pop();
    if (test === name) {
      return this;
    } else {
      const state = this._enactment._state[paths.join(Evaluator.PATH_SEPARATOR)];
      return state ? state._getCandidateFromName(name) : null;
    }
  }
  _setConfirm(value) {
    if (value !== this.confirmed) {
      return this._enactment._cycle({
        action: value ? {
          confirm: this.design
        } : {
          unconfirm: this.design
        },
        seed: [{
          path: this.path,
          attribute: 'confirmed',
          value
        }]
      });
    }
  }
  _updateState(evaluator) {
    const result = super._updateState();
    if (this._enactment._state[this._parentStatePath()].state === 'in_progress') {
      let recommended;
      let support = 0;
      const candidate = this._enactment.protocol.getComponent(this.design);
      for (var argument of candidate.arguments) {
        if (this._enactment._state[this.path + Core.Named.PATH_SEPARATOR + argument.idx].active) {
          support = support + argument.support;
        }
      }
      if (candidate.recommendCondition != null) {
        recommended = evaluator.evaluate(candidate.recommendCondition, this.design);
      }
      if (!isNaN(support) && support !== this.support) {
        result.push({
          path: this.path,
          attribute: 'support',
          value: support
        });
      }
      if (recommended !== this.recommended) {
        result.push({
          path: this.path,
          attribute: 'recommended',
          value: recommended
        });
      } else if (this.recommended && !this.confirmed) {
        if (this._enactment.protocol.getComponent(this.design).autoConfirmRecommended || this._enactment.options && this._enactment.options.Candidate && this._enactment.options.Candidate.autoConfirmRecommended) {
          result.push({
            path: this.path,
            attribute: 'confirmed',
            value: true
          });
        }
      }
    }
    return result;
  }

  // overridden to handle ++ and -- values which get nullified when the result of @getComponent is stringified
  toJSON() {
    const result = super.toJSON();
    result.class = "CandidateState";
    if (this.support > 0 && !isFinite(this.support)) {
      result.support = "++";
    } else if (this.support < 0 && !isFinite(this.support)) {
      result.support = "--";
    }
    return result;
  }
}

// Arguments are aggregated by candidates
exports.CandidateState = CandidateState;
class Argument extends Core.Annotated {
  constructor(options) {
    super(options);
    ({
      idx: this.idx,
      caption: this.caption,
      description: this.description,
      support: this.support,
      activeCondition: this.activeCondition
    } = options || {});
    if (this.support === "+") {
      this.support = 1;
    }
    if (this.support === "++") {
      this.support = +Infinity;
    }
    if (this.support === "-") {
      this.support = -1;
    }
    if (this.support === "--") {
      this.support = -Infinity;
    }
  }
  allDataDefinitions() {
    return this._parent.allDataDefinitions();
  }

  // see Core.Component.validate()
  static _attributes() {
    return super._attributes(...arguments).concat(['idx', 'support', 'activeCondition']);
  }
  path() {
    return "".concat(this._parent != null ? this._parent.path() : undefined).concat(Core.Named.PATH_SEPARATOR).concat(this.idx);
  }
  subPaths() {
    return [this.idx.toString()];
  }
  validate(identifiers) {
    if (identifiers == null) {
      identifiers = [];
    }
    let result = [];
    if (this._parent == null) {
      result.push(this._createErrorValidation('_parent', "an Argument must be attached to a Candidate"));
    }
    if (this.activeCondition == null || this.activeCondition === '') {
      result.push(this._createErrorValidation('activeCondition', "required attribute"));
    }
    if (this.caption == null) {
      result.push(this._createErrorValidation('caption', "required attribute"));
    }
    if (this.support == null) {
      result.push(this._createErrorValidation('support', "required attribute"));
    } else if (!(['+', '-', '++', '--'].includes(this.support) || typeof this.support === 'number')) {
      result.push(this._createErrorValidation('support', "must be numeric or in '+', '-', '--', '++'"));
    }
    if (this.idx == null) {
      result.push(this._createErrorValidation('idx', "required attribute"));
    }
    if (this.activeCondition != null) {
      result = result.concat(Evaluator.validateExpression(this.activeCondition, this.path(), 'activeCondition', identifiers));
    }
    return result;
  }
  _buildState(enactment, parentStatePath) {
    const statePath = parentStatePath + Core.Named.PATH_SEPARATOR + this.idx;
    return enactment._state[statePath] = new ArgumentState({
      path: statePath,
      _enactment: enactment
    });
  }

  // overridden to handle ++ and -- values which get nullified by JSON.stringify
  toJSON() {
    const result = super.toJSON();
    result.class = "Argument";
    if (this.support > 0 && !isFinite(this.support)) {
      result.support = "++";
    } else if (this.support < 0 && !isFinite(this.support)) {
      result.support = "--";
    }
    delete result.idx;
    return result;
  }
}
exports.Argument = Argument;
class ArgumentState extends Core.AnnotatedState {
  constructor(options) {
    super(options);
    ({
      active: this.active
    } = options || {});
  }
  _isUpdateable() {
    return true;
  }

  // TODO handle weight as expression instead of constant
  _updateState(evaluator) {
    const result = super._updateState();
    const {
      activeCondition
    } = this._enactment.protocol.getComponent(this.design);
    const active = evaluator.evaluate(activeCondition, this.design);
    if (active !== this.active) {
      result.push({
        path: this.path,
        attribute: 'active',
        value: active
      });
    }
    return result;
  }

  // used for net_support
  _getCandidateFromName(name) {
    const paths = this.path.split(Evaluator.PATH_SEPARATOR);
    paths.pop();
    const state = this._enactment._state[paths.join(Evaluator.PATH_SEPARATOR)];
    return state ? state._getCandidateFromName(name) : null;
  }

  // overridden to handle ++ and -- values which get nullified when the result of @getComponent is stringified
  toJSON() {
    const result = super.toJSON();
    result.class = "ArgumentState";
    if (this.support > 0 && !isFinite(this.support)) {
      result.support = "++";
    } else if (this.support < 0 && !isFinite(this.support)) {
      result.support = "--";
    }
    return result;
  }
}

// mapping that mitigates minification issues
exports.ArgumentState = ArgumentState;
const taskClassMap = new Map([['Task', Task], ['Action', Action], ['Enquiry', Enquiry], ['Decision', Decision], ['Plan', Plan]]);
},{"./core.js":188,"./evaluator.js":190,"core-js/modules/es.array.includes.js":153,"core-js/modules/es.array.push.js":155,"core-js/modules/es.error.cause.js":157,"core-js/modules/es.regexp.exec.js":167,"core-js/modules/es.regexp.to-string.js":169,"core-js/modules/es.string.includes.js":170,"core-js/modules/es.string.split.js":172,"core-js/modules/es.string.starts-with.js":173,"core-js/modules/es.symbol.description.js":174,"core-js/modules/es.weak-map.js":176,"core-js/modules/esnext.iterator.constructor.js":177,"core-js/modules/esnext.iterator.filter.js":179,"core-js/modules/esnext.iterator.find.js":180,"core-js/modules/esnext.iterator.for-each.js":181,"core-js/modules/esnext.iterator.map.js":182,"core-js/modules/web.dom-collections.iterator.js":184,"moment":187}]},{},[1]);
