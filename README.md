# PROformajs

PRO*forma* is a clinical decision support system (CDSS) language
(see [Sutton and Fox 2003](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC212780/)).
This is a javascript PRO*forma* engine.

## Demo

A demo that allows you to try out proformajs protocols can be found at https://openclinical.gitlab.io/proformajs-demo/?_demo.

## Documentation

Documentation can be found at https://openclinical.gitlab.io/proformajs.

## Testing

This project comes with a set of unit tests in the ``test/`` directory.  To run them you'll need to have [Nodejs](https://nodejs.org) (v16+) and [Git](https://git-scm.com/) installed:

```
you@yourmachine:~$ git clone https://gitlab.com/openclinical/proformajs.git
you@yourmachine:~$ cd proformajs
you@yourmachine:~$ npm install
you@yourmachine:~$ npm test

> @openclinical/proformajs@0.7.1 test
> node node_modules/mocha/bin/mocha --exit

  When building protocols
    it should be possible to programmatically build
      ✔ a single Task
      ✔ an enquiry

...

176 passing (2s)
2 pending
```

## Packaging

proformajs is distributed as an NPM module, [@openclinical/proformajs](https://www.npmjs.com/package/@openclinical/proformajs).

Running ``npm run dist`` will generate a single file that can be included in a browser, ``proforma.browser.js`` in the dist/ folder.

## Licensing

proformajs is owned by Openclinical CIC and dual-licensed with GPLv3 and a commercial license (please email licensing@openclinical.net for more details).  Contributions to the project are very welcome, and will be recognised via the contrib file, but will need to have copyright assigned to the CIC.
